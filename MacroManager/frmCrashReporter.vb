﻿Imports System.Windows.Forms

Public Class frmCrashReporter

    Private _ex As Exception

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        'Dim ex As New NotImplementedException

        If _ex Is Nothing Then
            Try
                Throw New NotImplementedException
            Catch ex As Exception
                _ex = ex
            End Try
        End If

       
        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Public Sub New(ex As Exception)
        ' This call is required by the designer.
        InitializeComponent()

        _ex = ex
        
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click
        'Label1.
    End Sub

    Private Sub CrashReporter_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtErrorMessage.Text = _ex.Message

        App.Documents.OfType(Of Word.Document).ToList.ForEach(Sub(n) chklstFiles.Items.Add(n.FullName))
        Enumerable.Range(0, chklstFiles.Items.Count).ToList.ForEach(Sub(n) chklstFiles.SetItemCheckState(n, Windows.Forms.CheckState.Checked))

        ChangeSubmitReport()
        ChangeIncludeFiles()

    End Sub

    Private Sub chkSumbitReport_CheckedChanged(sender As Object, e As EventArgs) Handles chkSumbitReport.CheckedChanged
        ChangeSubmitReport()
    End Sub

    Sub ChangeSubmitReport()
        pnlSubmitReport.Enabled = chkSumbitReport.Checked
    End Sub
    Sub ChangeIncludeFiles()
        chkIncludeFiles.Enabled = chkIncludeFiles.Checked
    End Sub

    Private Sub chkIncludeFiles_CheckedChanged(sender As Object, e As EventArgs) Handles chkIncludeFiles.CheckedChanged
        ChangeIncludeFiles()
    End Sub

    Private Sub butShowDetails_Click(sender As Object, e As EventArgs) Handles butShowDetails.Click
        Dim MyFrmCrashReporterDetails As New frmCrashReporterDetails(_ex.ToString)
        MyFrmCrashReporterDetails.ShowDialog()
    End Sub

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        If Me.chkSumbitReport.Checked Then SubmitReport()
        Me.Close()
    End Sub

    Private Sub SubmitReport()
        MessageBox.Show("Report sent.  Thank you.")
    End Sub
End Class