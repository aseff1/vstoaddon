﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCrashReporter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.butShowDetails = New System.Windows.Forms.Button()
        Me.butOK = New System.Windows.Forms.Button()
        Me.txtErrorMessage = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chkIncludeFiles = New System.Windows.Forms.CheckBox()
        Me.chkSumbitReport = New System.Windows.Forms.CheckBox()
        Me.txtComments = New System.Windows.Forms.TextBox()
        Me.chklstFiles = New System.Windows.Forms.CheckedListBox()
        Me.pnlSubmitReport = New System.Windows.Forms.Panel()
        Me.pnlSubmitReport.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(210, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Macro Manager crashed."
        '
        'butShowDetails
        '
        Me.butShowDetails.Location = New System.Drawing.Point(14, 259)
        Me.butShowDetails.Name = "butShowDetails"
        Me.butShowDetails.Size = New System.Drawing.Size(111, 23)
        Me.butShowDetails.TabIndex = 1
        Me.butShowDetails.Text = "Show Details"
        Me.butShowDetails.UseVisualStyleBackColor = True
        '
        'butOK
        '
        Me.butOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.butOK.Location = New System.Drawing.Point(162, 478)
        Me.butOK.Name = "butOK"
        Me.butOK.Size = New System.Drawing.Size(75, 23)
        Me.butOK.TabIndex = 2
        Me.butOK.Text = "OK"
        Me.butOK.UseVisualStyleBackColor = True
        '
        'txtErrorMessage
        '
        Me.txtErrorMessage.Location = New System.Drawing.Point(12, 65)
        Me.txtErrorMessage.Multiline = True
        Me.txtErrorMessage.Name = "txtErrorMessage"
        Me.txtErrorMessage.ReadOnly = True
        Me.txtErrorMessage.Size = New System.Drawing.Size(374, 56)
        Me.txtErrorMessage.TabIndex = 3
        Me.txtErrorMessage.Text = "Error Message"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(115, 20)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Our bad!  Whoa!"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(11, 164)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 23)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Comments:"
        '
        'chkIncludeFiles
        '
        Me.chkIncludeFiles.AutoSize = True
        Me.chkIncludeFiles.Checked = True
        Me.chkIncludeFiles.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIncludeFiles.Location = New System.Drawing.Point(14, 12)
        Me.chkIncludeFiles.Name = "chkIncludeFiles"
        Me.chkIncludeFiles.Size = New System.Drawing.Size(174, 17)
        Me.chkIncludeFiles.TabIndex = 7
        Me.chkIncludeFiles.Text = "Include file(s) I was working on."
        Me.chkIncludeFiles.UseVisualStyleBackColor = True
        '
        'chkSumbitReport
        '
        Me.chkSumbitReport.AutoSize = True
        Me.chkSumbitReport.Checked = True
        Me.chkSumbitReport.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSumbitReport.Location = New System.Drawing.Point(15, 147)
        Me.chkSumbitReport.Name = "chkSumbitReport"
        Me.chkSumbitReport.Size = New System.Drawing.Size(198, 17)
        Me.chkSumbitReport.TabIndex = 8
        Me.chkSumbitReport.Text = "Submit crash report to help assist me"
        Me.chkSumbitReport.UseVisualStyleBackColor = True
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(14, 181)
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Size = New System.Drawing.Size(333, 61)
        Me.txtComments.TabIndex = 10
        '
        'chklstFiles
        '
        Me.chklstFiles.FormattingEnabled = True
        Me.chklstFiles.Location = New System.Drawing.Point(33, 35)
        Me.chklstFiles.Name = "chklstFiles"
        Me.chklstFiles.Size = New System.Drawing.Size(246, 94)
        Me.chklstFiles.TabIndex = 11
        '
        'pnlSubmitReport
        '
        Me.pnlSubmitReport.Controls.Add(Me.chkIncludeFiles)
        Me.pnlSubmitReport.Controls.Add(Me.chklstFiles)
        Me.pnlSubmitReport.Controls.Add(Me.butShowDetails)
        Me.pnlSubmitReport.Controls.Add(Me.txtComments)
        Me.pnlSubmitReport.Controls.Add(Me.Label4)
        Me.pnlSubmitReport.Location = New System.Drawing.Point(33, 170)
        Me.pnlSubmitReport.Name = "pnlSubmitReport"
        Me.pnlSubmitReport.Size = New System.Drawing.Size(355, 290)
        Me.pnlSubmitReport.TabIndex = 12
        '
        'frmCrashReporter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(398, 509)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlSubmitReport)
        Me.Controls.Add(Me.chkSumbitReport)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtErrorMessage)
        Me.Controls.Add(Me.butOK)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCrashReporter"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CrashReporter"
        Me.pnlSubmitReport.ResumeLayout(False)
        Me.pnlSubmitReport.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butShowDetails As System.Windows.Forms.Button
    Friend WithEvents butOK As System.Windows.Forms.Button
    Friend WithEvents txtErrorMessage As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkIncludeFiles As System.Windows.Forms.CheckBox
    Friend WithEvents chkSumbitReport As System.Windows.Forms.CheckBox
    Friend WithEvents txtComments As System.Windows.Forms.TextBox
    Friend WithEvents chklstFiles As System.Windows.Forms.CheckedListBox
    Friend WithEvents pnlSubmitReport As System.Windows.Forms.Panel
End Class
