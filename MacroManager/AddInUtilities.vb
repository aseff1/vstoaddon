﻿Imports System.Runtime.InteropServices
Imports ClassLib


<ComVisible(True)> _
<ClassInterface(ClassInterfaceType.None)> _
Public Class AddInUtilities
    Implements IAddInUtilities

    Public Sub RefreshMacroLibraryAssembly() Implements IAddInUtilities.RefreshMacroLibraryAssembly
        InstanciateMacroLibrary.RefreshMacroLibraryAssembly()
    End Sub

    Public Sub CallMacro(ModuleAndMacroName As String) Implements IAddInUtilities.CallMacro
        MacroCalling.CallMacro(ModuleAndMacroName)
    End Sub

End Class