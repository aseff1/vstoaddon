﻿Imports System.Diagnostics
Imports System.Windows.Forms

Public Class ThisAddIn

#Region "AddIn Events"


    ''' <summary>
    ''' http://msdn.microsoft.com/en-us/library/bb608614
    ''' </summary>
    ''' <remarks></remarks>
    Private utilities As AddInUtilities

    ''' <summary>
    ''' http://msdn.microsoft.com/en-us/library/bb608614
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides Function RequestComAddInAutomationService() As Object
        If utilities Is Nothing Then
            utilities = New AddInUtilities()
        End If
        Return utilities
    End Function


    Private Sub ThisAddIn_Startup() Handles Me.Startup
        Try
            App = Globals.ThisAddIn.Application
            '"a" will fall out of scope as soon as thie method is done, but we don't care.  
            'The whole point of running
            'InstanciateMacroLibrary is just to set values to 
            'shared MyMacroLibraryMain and MyMacroLibraryMain.Application 
            Dim a As New InstanciateMacroLibrary(App)
            PopulateMacroDictionary()
            'GetMacroListFromOffice()
            'UpdateVBA()



            'Dim x = MacroDictionary
            'MyMacroLibrary = MyInstanciateMacroLibrary.MyMacroLibrary
            'MacroList = GetMacroList()
            'Stop
            'For Each t As Type In x.GetTypes
            '    Debug.Print(t.FullName)
            'Next
        Catch ex As Exception
            Select Case ex.Message
                Case "Programmatic access to Visual Basic Project is not trusted."
                    LogError(ex)
                    'Stop
                Case Else
                    LogError(ex)
                    MessageBox.Show(ex.Message)
                    'Throw
            End Select
        End Try

    End Sub

    Private Sub ThisAddIn_Shutdown() Handles Me.Shutdown

    End Sub

#End Region

#Region "Application Events"
    Private Sub Application_DocumentBeforeClose(Doc As Microsoft.Office.Interop.Word.Document, ByRef Cancel As Boolean) Handles Application.DocumentBeforeClose

    End Sub

    Private Sub Application_DocumentBeforePrint(Doc As Microsoft.Office.Interop.Word.Document, ByRef Cancel As Boolean) Handles Application.DocumentBeforePrint

    End Sub

    Private Sub Application_DocumentBeforeSave(Doc As Microsoft.Office.Interop.Word.Document, ByRef SaveAsUI As Boolean, ByRef Cancel As Boolean) Handles Application.DocumentBeforeSave

    End Sub

    Private Sub Application_DocumentChange() Handles Application.DocumentChange

    End Sub

    Private Sub Application_DocumentOpen(Doc As Microsoft.Office.Interop.Word.Document) Handles Application.DocumentOpen

    End Sub

    Private Sub Application_DocumentSync(Doc As Microsoft.Office.Interop.Word.Document, SyncEventType As Microsoft.Office.Core.MsoSyncEventType) Handles Application.DocumentSync

    End Sub

    Private Sub Application_NewDocument(Doc As Microsoft.Office.Interop.Word.Document) Handles Application.NewDocument

    End Sub

    Private Sub Application_ProtectedViewWindowActivate(PvWindow As Word.ProtectedViewWindow) Handles Application.ProtectedViewWindowActivate

    End Sub

    Private Sub Application_ProtectedViewWindowBeforeClose(PvWindow As Word.ProtectedViewWindow, CloseReason As Integer, ByRef Cancel As Boolean) Handles Application.ProtectedViewWindowBeforeClose

    End Sub

    Private Sub Application_ProtectedViewWindowBeforeEdit(PvWindow As Word.ProtectedViewWindow, ByRef Cancel As Boolean) Handles Application.ProtectedViewWindowBeforeEdit

    End Sub

    Private Sub Application_ProtectedViewWindowDeactivate(PvWindow As Word.ProtectedViewWindow) Handles Application.ProtectedViewWindowDeactivate

    End Sub

    Private Sub Application_ProtectedViewWindowOpen(PvWindow As Word.ProtectedViewWindow) Handles Application.ProtectedViewWindowOpen

    End Sub

    Private Sub Application_ProtectedViewWindowSize(PvWindow As Word.ProtectedViewWindow) Handles Application.ProtectedViewWindowSize

    End Sub

    Private Sub Application_Quit() Handles Application.Quit

    End Sub

    Private Sub Application_Startup() Handles Application.Startup

    End Sub

    Private Sub Application_WindowActivate(Doc As Microsoft.Office.Interop.Word.Document, Wn As Microsoft.Office.Interop.Word.Window) Handles Application.WindowActivate

    End Sub

    Private Sub Application_WindowDeactivate(Doc As Microsoft.Office.Interop.Word.Document, Wn As Microsoft.Office.Interop.Word.Window) Handles Application.WindowDeactivate

    End Sub

    Private Sub Application_WindowSelectionChange(Sel As Microsoft.Office.Interop.Word.Selection) Handles Application.WindowSelectionChange

    End Sub

    Private Sub Application_WindowSize(Doc As Microsoft.Office.Interop.Word.Document, Wn As Microsoft.Office.Interop.Word.Window) Handles Application.WindowSize

    End Sub

    Private Sub Application_XMLSelectionChange(Sel As Microsoft.Office.Interop.Word.Selection, OldXMLNode As Microsoft.Office.Interop.Word.XMLNode, NewXMLNode As Microsoft.Office.Interop.Word.XMLNode, ByRef Reason As Integer) Handles Application.XMLSelectionChange

    End Sub


#End Region


End Class
