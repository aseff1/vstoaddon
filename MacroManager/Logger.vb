﻿Imports System.Diagnostics
Imports System.IO
Imports System.Configuration
Module Logger

    Enum LogType
        None = 0
        Debugger = 1
        File = 2
    End Enum

    Public LogStatus As LogType = My.Settings.LogStatus
    Public nl As String = Environment.NewLine

    Public Sub LogError(ex As Exception)
        Dim AllMessages As IEnumerable(Of String)
        AllMessages = ex.GetAllExceptionMessagesNumbered.Select(Function(n) String.Concat(n.Key.ToString, ":  ", n.Value))
        Dim msg As String
        msg = String.Concat("ERROR:", nl, String.Join(nl, AllMessages), nl, nl, ex.StackTrace)
        Log(msg)
    End Sub

    Public Sub Log(msg As String)
        Log(msg, LogStatus)
    End Sub

    Public Sub Log(msg As String, MyLogType As LogType)
        Select Case MyLogType
            Case LogType.None
            Case LogType.Debugger
                Debug.Print(msg)
            Case LogType.File
                Using sw As StreamWriter = File.AppendText(My.Settings.LogFilePath)
                    sw.WriteLine("")
                    sw.WriteLine("--")
                    sw.WriteLine(String.Concat(Now.ToShortDateString, " ", Now.ToLongTimeString))
                    sw.WriteLine(msg)
                    sw.Flush()
                    sw.Close()
                End Using
        End Select
    End Sub

End Module
