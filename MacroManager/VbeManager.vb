﻿Imports Microsoft.Vbe.Interop
Imports pk = Microsoft.Vbe.Interop.vbext_ProcKind
Imports ct = Microsoft.Vbe.Interop.vbext_ComponentType
Imports System.IO
Imports sf = Microsoft.Office.Interop.Word.WdSaveFormat

Module VbeManager

    ''' we have to have a prebuild thing to prevent users from having a macro called CallMacro.
    ''' we can also use this technique to prevent the aabbcc issue with the dictionary
    ''' we also need a post build to put the dll in the proper path which the MacroManage is looking for.
    ''' we then need to refresh/reload the MacroManager's version of MacroLibrary.
    ''' Or, we can just hijack Macro Run and CustomizeKeyboard and do it then, every time...
    ''' I just realized..Extensibility has to be turned on.  Is there a way for VSTO to turn it on automatically??...Now I'm thinking--we don't need it.
    ''' Because Word isn't using Extensibility--VSTO is. And we already added Microsoft.Vbe.Interop to this project in VS.  Truthfully, even Word may
    ''' never need it if you use late binding.
    ''' But, we may not have access to the Model.  According to http://stackoverflow.com/a/5301556/1378356  !!!!, you need to look here:
    ''' HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\Word\Security\AccessVBOM
    ''' and  HKEY_LOCAL_MACHINE\Software\Microsoft\Office\15.0\Word\Security\AccessVBOM but it may be readonly.  See there.  
    ''' Perhaps we can force ownership of that key and then change it??
    ''' also consider HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\Word\StatusBar\ MacroRecord=1 to prevent recording?? or can you change it from the object model?? Or just do a refresh b4--see OfficeHijacker.vb
    ''' Dim query = 'From m In MacroDictionary.Keys Select  GetGenerateMacroText(m)
    Public Function GetGenerateMacrosText() As StringBuilder
        Dim query As IEnumerable(Of String) = MacroDictionary.Keys.Select(Function(k) k.Split(dl)) _
                                              .Select(Function(a) GetGenerateMacroText(a(0), a(1)))

        Return New StringBuilder(String.Join("", query))

    End Function


    Const MACRO_CALLER_MODULE_PREFIX As String = "MacroCaller_"


    ''' <summary>
    ''' Just for one macro
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetGenerateMacroText(ModuleName As String, MacroName As String) As String
        Return "Sub " & MacroName & "()" _
                & Environment.NewLine & vbTab & "CallMacro """ & ModuleName & dl & MacroName & """" _
                & Environment.NewLine & "End Sub" & Environment.NewLine

    End Function

    Private OurVBA_TemplateName As String = "MacroManagerVBA"
    Private Function GetOur_VBA_Project() As VBProject 'Word.Template
        Dim templates = App.Templates.OfType(Of Word.Template).Where(Function(n) n.Name = OurVBA_TemplateName)
        If templates.Count = 1 Then Return templates.Single.VBProject

        Try
            'Dim temp As Word.Template
            Dim doc As Word.Document = App.Documents.Add()
            Dim ThePath As String = Path.Combine(App.StartupPath, OurVBA_TemplateName & ".dotm")
            doc.SaveAs2(ThePath, sf.wdFormatTemplate)

            'close word and reopen Word, so that the template will be loaded in the background.
            doc.Close(Word.WdSaveOptions.wdSaveChanges)
            'App.Templates
            'App.COMAddIns
            App.AddIns.Add(ThePath, True)
        Catch ex As Exception
            'make sure to catch or there'll be an infinite loop
        End Try
        Return GetOur_VBA_Project
    End Function


    Private ReadOnly Property MacroCallerModule(ModuleName As String) As CodeModule
        Get
            Dim Modules As IEnumerable(Of VBComponent) = MacroCallerModules.Where(Function(n) n.Name = ModuleName)
            If Modules.Count = 1 Then Return Modules.Single
            Try
                Return GetAddNewModule(ModuleName)
            Catch ex As Exception
                'I think sometimes the COM errors fail silently, so I just want to make sure that we really get it.
                Throw New Exception("Error creating the " & MACRO_CALLER_MODULE_PREFIX & ModuleName & " module in VBA.", ex)
            End Try
        End Get
    End Property

    Private Function GetAddNewModule(ModuleName As String) As CodeModule
        Dim VBComp As VBComponent = GetOur_VBA_Project.VBComponents.Add(ct.vbext_ct_StdModule)
        With VBComp
            .Name = MACRO_CALLER_MODULE_PREFIX & ModuleName
            With .CodeModule
                .InsertLines(.CountOfLines + 1, "Option Explicit")
                .InsertLines(.CountOfLines + 1, "")
                .InsertLines(.CountOfLines + 1, "'Test")
            End With
            Return .CodeModule
        End With
    End Function

    Private ReadOnly Property MacroCallerModules() As List(Of CodeModule)
        Get
            Return GetOur_VBA_Project.VBComponents.OfType(Of VBComponent).Where(Function(n) _
                n.Type = vbext_ComponentType.vbext_ct_StdModule _
                AndAlso n.Name.StartsWith(MACRO_CALLER_MODULE_PREFIX)) _
                .Select(Function(b) b.CodeModule).ToList

        End Get
    End Property

#Region "Old"
    'Private ReadOnly Property MacroCallerModule(ModuleName As String) As CodeModule
    '    Get
    '        Const MACRO_CALLER_MODULE_PREFIX As String = "MacroCaller_"
    '        Dim md As VBComponent
    '        Try
    '            md = App.NormalTemplate.VBProject.VBComponents.OfType(Of VBComponent). _
    '            Where(Function(n) n.Type = vbext_ComponentType.vbext_ct_StdModule AndAlso n.Name = MACRO_CALLER_MODULE_PREFIX & ModuleName)

    '        Catch ex As Exception
    '            Select Case ex.Message
    '                Case "Programmatic access to Visual Basic Project is not trusted."
    '                    LogError(ex)
    '                    'Stop
    '                Case Else
    '                    Throw
    '            End Select
    '            Return Nothing
    '        End Try

    '        If md.Count = 1 Then
    '            Return md.Single
    '        Else
    '            Try
    '                Dim VBComp As VBComponent = App.NormalTemplate.VBProject.VBComponents.Add(ct.vbext_ct_StdModule)
    '                With VBComp
    '                    .Name = MACRO_CALLER_MODULE_PREFIX
    '                    With .CodeModule
    '                        .InsertLines(.CountOfLines + 1, "Option Explicit")
    '                        .InsertLines(.CountOfLines + 1, "")
    '                        .InsertLines(.CountOfLines + 1, "'Test")
    '                    End With
    '                End With

    '                'The above better work, or the below will be an infinite loop:
    '                Return MacroCallerModule
    '            Catch ex As Exception
    '                'I think sometimes the COM errors fail silently, so I just want to make sure that we really get it so that there's no infinite loop.
    '                Throw New Exception("Error creating the " & MACRO_CALLER_MODULE_PREFIX & " module in VBA.", ex)
    '            End Try
    '        End If
    '    End Get
    'End Property

#End Region


    ''' <summary>
    ''' Will this remove all keyboard shortcuts??
    ''' </summary>
    ''' <remarks></remarks>
    Sub UpdateVBA_All()
        Throw New NotImplementedException
        AddCodeToVBA_Module(GetGenerateMacrosText.ToString)
    End Sub

    Sub AddCodeToVBA_Module(ModuleNameAndMultiLineCode As String)
        AddCodeToVBA_Module(MacroSplit(ModuleNameAndMultiLineCode))
    End Sub

    Sub AddCodeToVBA_Module(ModuleNameAndMultiLineCode As String())
        AddCodeToVBA_Module(ModuleNameAndMultiLineCode(0), ModuleNameAndMultiLineCode(1))
    End Sub

    Sub AddCodeToVBA_Module(ModuleName As String, MultiLineCode As String)
        With MacroCallerModule(ModuleName)
            .DeleteLines(1, .CountOfLines)
            .InsertLines(.CountOfLines + 1, MultiLineCode)
        End With
    End Sub

    ''' <summary>
    ''' This way is more logical, and prevents the removal of existing keyboard shortcuts
    ''' It's like an upsert and delsert.
    ''' </summary>
    ''' <remarks></remarks>
    Sub UpdateVBA()
        Dim MacroListFromOffice As List(Of String) = GetMacroListFromOffice()

        'Delete what's in VBA and not in MacroLibrary:
        MacroListFromOffice.Except(MacroDictionary.Keys.OfType(Of String)).ForEach(Sub(m) DeleteMacroFromModule(m))

        'Insert into VBA what's not there yet but that's MacroLibrary:
        MacroDictionary.Keys.OfType(Of String).Except(MacroListFromOffice).ForEach(Sub(m) AddCodeToVBA_Module(m))


    End Sub

    ''' <summary>
    ''' http://www.cpearson.com/excel/vbe.aspx  Listing All Procedures In A Module
    ''' </summary>
    ''' <remarks></remarks>
    Private Function GetProceduresInModule(MyModule As CodeModule) As List(Of String)
        Dim MyList As New List(Of String)
        Dim LineNum As Long = 0
        Dim ProcKind As vbext_ProcKind

        With MyModule
            Dim NumLines As Long = .CountOfDeclarationLines + 1
            Do Until LineNum >= .CountOfLines
                Dim ProcName As String = .ProcOfLine(LineNum, ProcKind)
                'Rng.Value = ProcName
                MyList.Add(String.Concat(MyModule.Name, dl, ProcName))
                'Rng(1, 2).Value = ProcKindString(ProcKind)
                LineNum = .ProcStartLine(ProcName, ProcKind) + .ProcCountLines(ProcName, ProcKind) + 1
                'Rng = Rng(2, 1)
            Loop
        End With

        Return MyList.Distinct.ToList
    End Function
    Public Function GetMacroListFromOffice() As List(Of String)
        Dim MyList As New List(Of String)

        For Each MyModule As CodeModule In MacroCallerModules
            With MyModule
                MyList.AddRange(GetProceduresInModule(MyModule))
            End With
        Next MyModule

        Return MyList.Distinct.ToList

    End Function

    Sub DeleteMacroFromModule(ModuleAndMacroName As String)
        DeleteMacroFromModule(MacroSplit(ModuleAndMacroName))
    End Sub

    Sub DeleteMacroFromModule(ModuleAndMacroName As String())
        DeleteMacroFromModule(ModuleAndMacroName(0), ModuleAndMacroName(1))
    End Sub

    Public Function MacroSplit(ModuleAndMacroName As String) As String()
        Return ModuleAndMacroName.Split(dl)
    End Function

    Public Function MacroJoin(ParamArray args As String()) As String
        Return String.Join(dl, args)
    End Function
    Sub DeleteMacroFromModule(ModuleName As String, MacroName As String)

        With MacroCallerModule(ModuleName)
            .DeleteLines(.ProcStartLine(MacroName, pk.vbext_pk_Proc), .ProcCountLines(MacroName, pk.vbext_pk_Proc))
        End With
    End Sub

    Function ProcKindString(ProcKind As vbext_ProcKind) As String
        Select Case ProcKind
            Case pk.vbext_pk_Get : Return "Property Get"
            Case pk.vbext_pk_Let : Return "Property Let"
            Case pk.vbext_pk_Set : Return "Property Set"
            Case pk.vbext_pk_Proc : Return "Sub Or Function"
            Case Else : Return "Unknown Type: " & CStr(ProcKind)
        End Select
    End Function

#Region "Lock VBProject"

    'first open VBA and then hide the window using http://www.codeproject.com/Articles/2286/Window-Hiding-with-C
    'and first getting the handle using EnumChild stuff...but then SendKeys won't work...!
    ''' <summary>
    ''' http://xlvba.fr.yuku.com/reply/663/Re-LockUnlock-VBA-Project-programmatically#.U_BUG_mbZu8
    ''' Note that this second procedure could be very easily put into a loop where different passwords are tried until the project is eventually unlocked (i.e. by using a Do Until Application.VBE.ActiveVBProject.Protection = 0 loop) so don't regard locking the VBA project as being 100% secure  
    ''' Simply put, there is a difference between "Locking" a project so that there's no inadvertant tampering, and "Securing" your project against the possible 'theft' of your code. The password merely locks your project, it doesn't secure it.
    ''' However we need to note here that there's no need to become paranoid about this apparent lack of "security". Most people that have enough knowledge to crack the password for your locked project and to possibly 'steal' your code have no real NEED for it and simply wouldn't be bothered - it'd be simpler to just write their own code.
    ''' </summary>
    ''' <remarks></remarks>
    Sub LockVBAProject(Password As String, Lock As Boolean)
        With App

            '//execute the controls to lock the project\\
            .VBE.CommandBars("Menu Bar").Controls("Tools").Controls("VBAProject Properties...").Execute()

            '//activate 'protection'\\
            .SendKeys("^{TAB}")

            '//CAUTION: this either checks OR UNchecks the\\
            '//"Lock Project for Viewing" checkbox, if it's already\\
            '//been locked for viewing, then this will UNlock it\\
            .SendKeys("{ }")

            '//enter password (password is Password in this example)\\
            .SendKeys("{TAB}" & Password)

            '//confirm password\\
            .SendKeys("{TAB}" & Password)

            '//scroll down to OK key\\
            .SendKeys("{TAB}")

            '//click OK key\\
            .SendKeys("{ENTER}")

            'the project is now locked - this takes effect
            'the very next time the book's opened...
        End With

    End Sub



    Sub UnLockAndViewVBAProject(Password As String)
        With App
            '//we execute the "VBAProject Properties..." control\\
            '//just to invoke the password dialog, the password\\
            '//must be given before the dialog can be shown :) \\
            .SendKeys(Password)
            .SendKeys("{ENTER}")

            '//now reset the project password and relock it so\\
            '//that it's locked again when the workbook's closed\\
            .VBE.CommandBars("Menu Bar").Controls("Tools").Controls("VBAProject Properties...").Execute()
            .SendKeys("^{TAB}")
            .SendKeys("{TAB}" & Password)
            .SendKeys("{TAB}" & Password)
            .SendKeys("{TAB}")
            .SendKeys("{ENTER}")
        End With
    End Sub


    ''http://www.ozgrid.com/forum/showthread.php?t=13006&p=65776#post65776
    ''need reference To VBA Extensibility 
    ''need To make sure that the target project Is the active project 
    'Sub test()
    '    UnprotectVBProject(Workbooks("ABook.xls"), "password")
    'End Sub

    'Sub UnprotectVBProject(WB As Workbook, ByVal Password As String)
    '    ' 
    '    ' Bill Manville, 29-Jan-2000 
    '    ' 
    '    Dim VBP As VBProject, oWin As VBIDE.Window
    '    Dim wbActive As Workbook
    '    Dim i As Integer

    '    VBP = WB.VBProject
    '    wbActive = ActiveWorkbook

    '    If VBP.Protection <> vbext_pp_locked Then Exit Sub

    '    Application.ScreenUpdating = False

    '    ' Close any code windows To ensure we hit the right project 
    '    For Each oWin In VBP.VBE.Windows
    '        If InStr(oWin.Caption, "(") > 0 Then oWin.Close()
    '    Next oWin

    '    WB.Activate()
    '    ' now use lovely SendKeys To unprotect 
    '    Application.OnKey "%{F11}"
    '    SendKeys("%{F11}%TE" & Password & "~~%{F11}", True)

    '    If VBP.Protection = vbext_pp_locked Then
    '        ' failed - maybe wrong password 
    '        SendKeys("%{F11}%TE", True)
    '    End If

    '    ' leave no evidence of the password 
    '    Password = ""
    '    ' go back To the previously active workbook 
    '    wbActive.Activate()

    'End Sub

    ' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
    'Sub ProtectVBProject(WB As Workbook, ByVal Password As String)

    '    Dim VBP As VBProject, oWin As VBIDE.Window
    '    Dim wbActive As Workbook
    '    Dim i As Integer

    '    VBP = WB.VBProject
    '    wbActive = ActiveWorkbook

    '    ' Close any code windows To ensure we hit the right project 
    '    For Each oWin In VBP.VBE.Windows
    '        If InStr(oWin.Caption, "(") > 0 Then oWin.Close()
    '    Next oWin

    '    WB.Activate()
    '    ' now use lovely SendKeys To unprotect 
    '    Application.OnKey "%{F11}"
    '    SendKeys "+{TAB}{RIGHT}%V{+}{TAB}" & Password & "{TAB}" & Password & "~"
    '    Application.VBE.CommandBars(1).FindControl(Id:=2578, recursive:=True).Execute()
    '    WB.Save()
    'End Sub
    ' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' 
#End Region

End Module
