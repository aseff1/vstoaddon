﻿Imports Microsoft.Office.Tools.Ribbon
Imports dg = System.Diagnostics

Public Class MainRibbon

    Private Sub MainRibbon_Load(ByVal sender As System.Object, ByVal e As RibbonUIEventArgs) Handles MyBase.Load
        ChangeLogValue()
        ChangeDebugValue()
    End Sub

    Private Sub chkLog_Click(sender As Object, e As RibbonControlEventArgs) Handles chkLog.Click
        ChangeLogValue()
    End Sub

    Private Sub ChangeLogValue()
        My.Settings.LogStatus = If(chkLog.Checked, 2, 0)
    End Sub

    Private Sub chkDebug_Click(sender As Object, e As RibbonControlEventArgs) Handles chkDebug.Click
        ChangeDebugValue()
    End Sub

    Private Sub ChangeDebugValue()
        My.Settings.IsInDebugMode = chkDebug.Checked
    End Sub

    <dg.DebuggerStepThrough()> _
    Private Sub butRun_Click(sender As Object, e As RibbonControlEventArgs) Handles butRun.Click
        Dim x As String = InputBox("Run:")
        If x <> "" Then CallMacro(x)
    End Sub

    Private Sub butRefresh_Click(sender As Object, e As RibbonControlEventArgs) Handles butRefresh.Click
        InstanciateMacroLibrary.RefreshMacroLibraryAssembly()
    End Sub

  

    Private Sub RunError_Click(sender As Object, e As RibbonControlEventArgs) Handles RunError.Click
        Dim MyFrmCrashReporter As New frmCrashReporter()
        MyFrmCrashReporter.ShowDialog()


    End Sub
End Class
