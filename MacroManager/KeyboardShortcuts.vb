﻿Imports System.Xml.Linq
Imports System.IO

Public Class KeyboardShortcuts

    Public Shared Function GetCustomizationsXmlStreamFromTemplateFile(TemplateFullPath As String)
        'example: "C:\Users\Avraham\AppData\Roaming\Microsoft\Templates\Normal.dotm"
        'Use SharpZipLib to create an in-memory stream--zip Normal.dotm.  Turn into zip. then go to /word/customizations.xml
        'and pass that as an overload to LoadFromCustomizations

    End Function
    Public Shared Function LoadFromCustomizations(FullPath As String) As List(Of KeyboardShortcut)
        'Customizations.xml
        Dim doc As XDocument = XDocument.Load(FullPath)
        'this is not good bec. it's on 2 levels--(I'm not sure why yet).  It'll have to be a little more complex.
        Dim query = From a In doc.Descendants("wne:keymaps") Select New KeyboardShortcut(a.Element("wne:kcmPrimary"), a.Element("wne:fciName"), a.Element("wne:swArg"))

        Return query.ToList

    End Function

End Class

Public Class KeyboardShortcut
    Public kcmPrimary As String
    Public fciName As String
    Public swArg As String

    Public Sub New(kcmPrimary As String, fciName As String, swArg As String)
        Me.kcmPrimary = kcmPrimary
        Me.fciName = fciName
        Me.swArg = swArg
    End Sub
End Class
