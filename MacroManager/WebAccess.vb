﻿Imports System.IO
Imports System.Net

Module WebAccess


    Public Sub CheckForLatestVersion()

    End Sub

    Public Sub SendErrorLog()
        'First confirm with user...
        UploadFileToFTP("", "", "", My.Settings.LogFilePath)
    End Sub

    ''' <summary>
    ''' http://www.codeproject.com/Tips/300226/Upload-file-to-server-using-ftp
    ''' </summary>
    ''' <param name="FTP_URL">e.g. ftp://serverip/foldername/foldername</param>
    ''' <param name="User"></param>
    ''' <param name="Password"></param>
    ''' <param name="FullFileName">including path; e.g. “d:/test.docx”</param>
    ''' <remarks></remarks>
    Public Sub UploadFileToFTP(FTP_URL As String, User As String, Password As String, FullFileName As String)
        Try
            Dim FileName As String = Path.GetFileName(FullFileName)
            Dim ftpfullpath As String = FTP_URL
            Dim ftp As FtpWebRequest = DirectCast(FtpWebRequest.Create(ftpfullpath), FtpWebRequest)
            ftp.Credentials = New NetworkCredential(User, Password)

            ftp.KeepAlive = True
            ftp.UseBinary = True
            ftp.Method = WebRequestMethods.Ftp.UploadFile

            Dim fs As FileStream = File.OpenRead(FullFileName)
            Dim buffer As Byte() = New Byte(fs.Length - 1) {}
            fs.Read(buffer, 0, buffer.Length)
            fs.Close()

            Dim FTP_Stream As Stream = ftp.GetRequestStream()
            FTP_Stream.Write(buffer, 0, buffer.Length)
            FTP_Stream.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Module
