﻿Imports Microsoft.Office.Interop

Public Module GlobalStuff

    ''' <summary>
    ''' This is the instance of Word in which MacroManager lives.  Should happen to be the same value as Application, which is the instance of MacroLibrary.
    ''' </summary>
    ''' <remarks></remarks>
    Public App As Word.Application '= Globals.ThisAddIn.Application
    Public MyMacroLibraryMain 'As MacroLibrary.Main '= New MacroLibrary.Main(App)   'will be loaded from Assembly

    ''' <summary>
    ''' delimiter
    ''' </summary>
    ''' <remarks></remarks>
    Public Const dl As String = "."


End Module
