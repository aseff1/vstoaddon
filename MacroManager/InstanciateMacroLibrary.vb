﻿Imports System.Diagnostics
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices
Imports System.Reflection

'<ComVisible(True)> _
'<ClassInterface(ClassInterfaceType.None)> _
Class InstanciateMacroLibrary

    'Implements ClassLib.IAddInUtilities
    Public Shared Sub RefreshMacroLibraryAssembly() 'Implements ClassLib.IAddInUtilities.RefreshMacroLibraryAssembly
        'Throw New NotImplementedException
        MyMacroLibraryMain = ClassLib.ReloadMacroLibrary.ReloadMacroLibrary(My.Settings.MacroLibraryPath)
    End Sub

    'Public MyMacroLibrary As MacroLibrary.Main = New MacroLibrary.Main  'load from file?

    ''' <summary>
    ''' we don't really need this, bec. this class is instanciated with "App" which is the current instance of Word.  That gets pushed to the MacroLibrary.  This returns that.  It's kind of like coming full-circle.  
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Application As Word.Application
        Get
            Return DirectCast(MyMacroLibraryMain.Application, Application)
            'Notice that this is shared, so we're not getting it from the instance.  It NEEDS to be shared so that the modules wihtin MacroLibary can call "Application" implicitly, without having to prepend the class name--just like in VBA.

            'I'm changing that, as follows:  Main.Application can't be shared or it won't be able to imolement the interface (which we want to make it eaiser to load the assmbly dynamically.  But it encapsulates a private shared application object.  To resolve issue above regarding VBA syntax, we're anyway using GlobalStuff.Application which is shared (it's in a module) which returns Applicatoin property of a private instance of Main.

        End Get
    End Property



#Region "Constructors"
    ''' <summary>
    ''' This overload accepts an existing Word.Application instance
    ''' </summary>
    ''' <param name="wrd"></param>
    ''' <remarks></remarks>
    Public Sub New(wrd As Word.Application)
        RefreshMacroLibraryAssembly()
        MyMacroLibraryMain.Application = wrd     'see comment in Property Application above
    End Sub

    ''' <summary>
    ''' This overload accepts the ID of an existing Word application
    ''' </summary>
    ''' <param name="ProcessID"></param>
    ''' <remarks></remarks>
    Public Sub New(ProcessID As Integer)
        RefreshMacroLibraryAssembly()
        Dim WordProcess As Process = Process.GetProcessesByName("word").Where(Function(n) n.Id = ProcessID).Single
        Dim hwnd As Integer = CType(WordProcess.MainWindowHandle, System.Int32)
        'MacroLibrary.Main.Application = GetWordApp(hwnd)     '593458    'see comment in Property Application above
        MyMacroLibraryMain.Application = GetWordApp(hwnd)     '593458    'see comment in Property Application above

    End Sub

    ''' <summary>
    ''' This overload is used when there's exactly 1 or 0 existing instances of Word already in memory, so the system will figure out which on it is if it exists.  If it doesn't exist, you have the option of creating a new instance.
    ''' </summary>
    ''' <param name="ShouldCreateIfDoesntExist"></param>
    ''' <remarks></remarks>
    Public Sub New(ShouldCreateIfDoesntExist As CreateIfDoesntExist)
        RefreshMacroLibraryAssembly()

        Dim WordProcesses As Process() = Process.GetProcessesByName("word")

        Select Case WordProcesses.Count
            Case 0
                If ShouldCreateIfDoesntExist = CreateIfDoesntExist.Yes Then
                    MyMacroLibraryMain.Application = New Word.Application    'see comment in Property Application above
                    MyMacroLibraryMain.Application.Visible = True
                Else
                    MyMacroLibraryMain.Application = Nothing
                End If
            Case 1
                MyMacroLibraryMain.Application = DirectCast(Microsoft.VisualBasic.Interaction.GetObject(Nothing, "Word.Application"), Application)   'see comment in Property Application above
            Case Else
                Throw New Exception("There is more than one instance of Word, so I don't know which one you want.  To solve this problem, make sure that there's only one instance running, or call one of the other overloads to this procedure.")

        End Select

    End Sub


    Public Enum CreateIfDoesntExist
        Yes
        No
    End Enum


#End Region

#Region "Comments"
    'Getting the Application Object in a Shimmed Automation Add-in
    'Building a managed automation add-in is one way to implement Excel user-defined functions (UDFs). The traditional way is to build an XLL and there was a healthy discussion about the pros and cons of this in one of my posts. In an even earlier one, I talked about shimming your managed automation add-ins. The reasons for shimming a managed COM add-in, along with the basic techniques, are now reasonably well-understood. However, there is an extra wrinkle for a certain class of managed automation add-ins.

    'Recall that a COM add-in implements IDTExtensibility2, and in the OnConnection call the Office host will pass in a pointer to its Application object. This allows the add-in to interact with the host object model (OM).

    'Automation add-ins may implement IDTExtensibility2, but the only point of doing this would be if you want your automation add-in to serve a dual purpose as a regular COM add-in. If Excel loads it as a COM add-in, it will use the IDTExtensibility2 interface. If Excel loads it as an automation add-in, it won't. It is very rare (and questionable design) for anyone to build a dual-purpose COM/automation add-in, so it usually doesn't make any sense for an automation add-in to implement IDTExtensibility2.

    'Most automation add-ins are merely function libraries, providing custom functions that are additional to the inbuilt Excel cell functions. These functions can do anything you like, and typically perform custom calculations, make use of existing class libraries of domain (business) functionality, or call back-end services. These functions are mostly independent of Excel, that is, they are unaware that they are being used in the context of Excel. Therefore, shimming automation add-ins that consist only of such functions is straightforward.

    'Now for the wrinkle: suppose you want to build an automation add-in that wants to use the Excel OM. How do you get hold of the OM? For a COM add-in, it's easy - you get the OM Application object for free. For an automation add-in, Excel simply calls into your functions - it doesn't pass you an Application object anywhere.

    'One solution is to use the native Win32 APIs that support Active Accessibility. This technology (shipped as standard with Windows) allows developers to make applications more accessible to people with vision, hearing or motion disabilities. You can take advantage of this to connect to an accessible application via the Accessibility APIs. Once you've connected to the accessible object, you can then get to its native OM (if the application supports this). To do this, you can use P/Invoke with some native APIs.

    'Here’s how. Let’s say we have a managed automation add-in that exposes some functions for temperature conversion. This is based on my post about volatile UDFs. The functions use a factor in their calculations which defaults to 32.0 but which can also be retrieved dynamically from the active worksheet:

#End Region
#Region "Unamanaged Window Management"

    ''' <summary>
    ''' In order to walk Excel’s list of child windows to find one that supports accessibility, we need to call EnumChildWindows. This API expects a callback function as its 2nd parameter. So, we’ll declare a delegate type that matches the signature of the callback (takes a pair of incoming and outgoing integer parameters). The incoming int will be the HWND of a window to be examined, the outgoing int will be the HWND of a window that we determine does support accessibility
    ''' </summary>
    ''' <param name="hwnd"></param>
    ''' <param name="lParam"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Delegate Function EnumChildCallback(hwnd As Integer, ByRef lParam As Integer) As Boolean

    ''' <summary>
    ''' We’ll declare an instance of the delegate as a class member, to ensure that it stays alive long enough to complete its work - which might be longer than the life of the function that sets it up
    ''' </summary>
    ''' <remarks></remarks>
    Private cb As EnumChildCallback


    ''' <summary>
    ''' https://web.archive.org/web/20130331152057/http://blogs.officezealot.com/whitechapel/archive/2005/04/10/4514.aspx
    ''' </summary>
    ''' <param name="hwnd">Handle of Main Window</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetWordApp(hwnd As Integer) As Word.Application
        ' First, get word's main window handle.
        'Dim hwnd As Integer = CInt(Process.GetCurrentProcess().MainWindowHandle)

        ' We need to enumerate the child windows to find one that
        ' supports accessibility. To do this, instantiate the
        ' delegate and wrap the callback method in it, then call
        ' EnumChildWindows, passing the delegate as the 2nd arg.
        If hwnd <> 0 Then
            Dim hwndChild As Integer = 0
            cb = New EnumChildCallback(AddressOf EnumChildProc)     'added AddressOf 
            Dim AccessibilityClassName As String = GetAccessibilityClassName()
            EnumChildWindows(hwnd, cb, hwndChild)

            ' If we found an accessible child window, call
            ' AccessibleObjectFromWindow, passing the constant
            ' OBJID_NATIVEOM (defined in winuser.h) and
            ' IID_IDispatch - we want an IDispatch pointer
            ' into the native object model.
            If hwndChild <> 0 Then
                Dim OBJID_NATIVEOM As UInteger = GetOBJID_NATIVEOM() '&HFFFFFFF0UI
                Dim IID_IDispatch As Guid = GetGUID()  '00020400-0000-0000-C000-000000000046
                Dim ptr As Word.Window = Nothing

                Dim hr As Integer = AccessibleObjectFromWindow(hwndChild, OBJID_NATIVEOM, IID_IDispatch.ToByteArray(), ptr)
                If hr >= 0 Then
                    'If we successfully got a native OM IDispatch pointer, we can QI this for an word Application 
                    '(using the implicit cast operator supplied in the PIA).
                    Return ptr.Application
                End If
            End If
        End If
        Return Nothing
    End Function

    Private Function EnumChildProc(hwndChild As Integer, ByRef lParam As Integer) As Boolean
        Dim buf As New StringBuilder(128)
        GetClassName(hwndChild, buf, 128)
        If buf.ToString() = GetAccessibilityClassName() Then '"word7" Then
            lParam = hwndChild
            Return False
        End If
        Debug.Print(hwndChild.ToString)
        Return True
    End Function

#End Region

#Region "Office App Agnostic"
    Function GetGUID() As Guid
        Select Case Application.GetType
            Case GetType(Word.Application)
                Throw New NotImplementedException()
                Return New Guid("")
            Case GetType(Excel.Application)
                Return New Guid("00020400-0000-0000-C000-000000000046")
        End Select
        Return Nothing
    End Function

    Function GetAccessibilityClassName() As String
        Select Case Application.GetType
            Case GetType(Word.Application)
                Throw New NotImplementedException()
                'Return New Guid("")
            Case GetType(Excel.Application)
                Return "EXCEL7"
        End Select
        Return Nothing
    End Function

    Function GetOBJID_NATIVEOM() As UInteger
        Select Case Application.GetType
            Case GetType(Word.Application)
                Throw New NotImplementedException()
                'Return New Guid("")
            Case GetType(Excel.Application)
                Return &HFFFFFFF0UI
        End Select
        Return Nothing
    End Function

#End Region

#Region "Unmanaged Declarations"

    ''' <summary>
    ''' 'To get hold of the Excel Application object, this is what we’ll do. From the add-in assembly, we can get hold of the current process (Excel), and from that we can get Excel’s main window. Then we can walk the list of child windows to find one that supports Accessibility. Once we’ve found that, we can use the AccessibleObjectFromWindow API to get the Excel Application object. Here’s the import for AccessibleObjectFromWindow.
    ''' </summary>
    ''' <param name="hwnd"></param>
    ''' <param name="dwObjectID"></param>
    ''' <param name="riid"></param>
    ''' <param name="ptr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("Oleacc.dll")> _
    Public Shared Function AccessibleObjectFromWindow(hwnd As Integer, dwObjectID As UInteger, riid As Byte(), _
                                               ByRef ptr As Microsoft.Office.Interop.Word.Window) As Integer
    End Function

    <DllImport("User32.dll")> _
    Public Shared Function EnumChildWindows(hWndParent As Integer, lpEnumFunc As EnumChildCallback, ByRef lParam As Integer) As Boolean
    End Function


    ''' <summary>
    ''' For each window handle we're passed, we call the Win32 API function GetClassName to match the class name of the window against the known class name of an Excel window that supports accessibility ("EXCEL7")
    ''' </summary>
    ''' <param name="hWnd"></param>
    ''' <param name="lpClassName"></param>
    ''' <param name="nMaxCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DllImport("User32.dll")> _
    Public Shared Function GetClassName(hWnd As Integer, lpClassName As StringBuilder, nMaxCount As Integer) As Integer
    End Function

#End Region

#Region "Excel"
    'Public Function F2C(val As Double) As Double
    '    Dim constantFactor As Double = 32.0

    '    If xl IsNot Nothing Then
    '        Dim missing As Object = Type.Missing
    '        xl.Volatile(missing)

    '        Dim sheet As Excel.Worksheet = DirectCast(xl.ActiveSheet, Excel.Worksheet)
    '        If sheet IsNot Nothing Then
    '            constantFactor = CDbl(DirectCast(sheet.Cells(1, 1), Excel.Range).Value2)
    '        End If
    '    End If
    '    Return ((5.0 / 9.0) * (val - constantFactor))
    'End Function
#End Region

End Class
