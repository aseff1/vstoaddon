﻿Imports Microsoft.Office.Interop '.Word
Imports Microsoft.Office.Interop.Word
'Imports GlobalStuff '<--we might not need this since all it's members are anyway public shared...
'Imports other word stuff such as enums which are referenced implicitly in VBA
'consider creating a VS plugin which overrides Project > Add Module with adding an Office (Word) Module with all theses imports added automatically.

'I DON'T want this on here:     <System.Diagnostics.DebuggerStepThrough()> _
Module Module1

    Sub Test1()
        Application.Selection.MoveRight(WdUnits.wdCharacter, 2, WdMovementType.wdExtend)

    End Sub
End Module
