﻿Module EinMishpot
    Option Explicit

    Sub PutsFootnoteTextFromLittleMishaTitle2EinMishpot()
        Dim Ein$

        GoHome()
        'for each x
        With Selection
            Do While zzFind("^f", zWildOff, "Little Mishna Title")
                Ein = .Footnotes(1).Range.Text
                .Delete()
                'zzfind(
                .MoveUp(WdUnits.wdLine, 1)
                .HomeKey WdUnits.wdLine
                .TypeText Ein
                .TypeParagraph()
                .MoveUp(WdUnits.wdLine, 1, WdMovementType.wdExtend)
                .HomeKey(WdUnits.wdLine, WdMovementType.wdExtend)
                .style = "Ein Mishpot Bold"
                Clps wdCollapseEnd
                'MsgboxUnicode x
                'GoHome

            Loop
        End With
    End Sub

    Sub PutsRambamandShulchanOruchIntoEinMishpot()

        Selection.Font.Reset()
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = ChrW(1490) & ChrW(1490) & ChrW(1490) & ChrW(1490) & ChrW(1490 _
                ) & ChrW(1490) & ChrW(1490)
            .Replacement.Text = ChrW(1512) & ChrW(1502) & ChrW(1489) & """" & _
                ChrW(1501) & " " & ChrW(1492) & ChrW(1500) & "' ^p" & ChrW(1496) & _
                ChrW(1493) & ChrW(1512) & " " & ChrW(1493) & ChrW(1513) & ChrW(1493) _
                 & """" & ChrW(1506) & " " & ChrW(1497) & ChrW(1493) & """" & ChrW( _
                1491) & "/" & ChrW(1488) & ChrW(1492) & """" & ChrW(1506)
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
    End Sub


End Module
