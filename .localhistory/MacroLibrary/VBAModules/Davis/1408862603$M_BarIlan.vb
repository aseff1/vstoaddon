﻿Module M_BarIlan
    Option Explicit
    Sub BAR_ILAN() ' Alt-CTR-B
        Dim str As String
        Documents.Add DocumentType:=wdNewBlankDocument
        Selection.Paste()

        GetRidOfParsha()

        AbbreviateMesechtos()

        str = HE(PEY) & HE(REISH) & HE(KOOF) & " "
        zReplace(str, " ", False)

        str = " " & HE(PEY) & HE(SAMECH) & HE(vav) & HE(KOOF) & " "
        zReplace(str, ":", False)

        fixtanach()

        zReplace("[0-9]@.", "", True)
        zReplace("[0-9]@", "", True)
        zReplace(".", "", True)
        zReplace("^p", ",", False)
        zReplace(" \(*\)", "", True)

        FixBavli()

        str = HE(MEM) & HE(SHIN) & HE(NUN) & HE(HEY) & " " & HE(MEM) & HE(SAMECH) & HE(KAF) & HE(TAV) & " "
        zReplace(str, " ", False)

        str = " " & HE(MEM) & HE(SHIN) & HE(NUN) & HE(HEY) & " "
        zReplace(str, ":", False)

        str = ".  " & HE(TAV) & HE(LAMED) & HE(MEM) & HE(vav) & _
                HE(DALET) & " " & HE(BEIS) & HE(BEIS) & HE(LAMED) & HE(YUD) _
                 & " " & HE(MEM) & HE(SAMECH) & HE(KAF) & HE(TAV) & " "
        zReplace(str, " ", False)

        str = HE(DALET) & HE(FINAL_PEY) & " "
        zReplace(str, "", False)

        str = " " & HE(AYIN) & HE(MEM) & HE(vav) & HE(DALET) & " " & HE(ALEF)
        zReplace(str, ".", False)

        str = " " & HE(AYIN) & HE(MEM) & HE(vav) & HE(DALET) & " " & HE(BEIS)
        zReplace(str, ":", False)

        str = "( [" & HE(ALEF) & "-" & HE(TAV) & "]@ )(*)\1"


Loop2:
        If Not zFind(str, True, True) Then GoTo EndLoop2
        zReplace(str, "\1\2", True)
        GoTo Loop2
EndLoop2:

        zReplace(",", ", ", False)
        zReplace("[ ]{1,},", ",", True)
        zReplace("([ ,]){2,}", "\1", True)

        str = "([" & HE(ALEF) & "-" & HE(TAV) & "]@:)([" & HE(ALEF) & "-" & HE(TAV) & " ,]@, )\1"
Loop1:
        If Not zFind(str, True, True) Then GoTo EndLoop1
        zReplace(str, "\1\2", True)
        GoTo Loop1
EndLoop1:

        zReplace(", ^p", "", False)
        Selection.HomeKey Unit:=wdStory
        Selection.Delete(Unit:=wdCharacter, Count:=1)

        Selection.WholeStory()
        Selection.Cut()
        ActiveWindow.Close(Word.WdSaveOptions.wdDoNotSaveChanges)
        Selection.PasteAndFormat(wdFormatPlainText)
    End Sub
    Sub fixtanach() 'Used by Bar Ilan
        Rep("( " & HE(SHIN) & HE(MEM) & HE(vav) & HE(ALEF) & HE(LAMED) & " " & HE(ALEF) & ")(  )", "\1:", "", "", zWildon, zNoConfirm, zMain)
        Rep("( " & HE(SHIN) & HE(MEM) & HE(vav) & HE(ALEF) & HE(LAMED) & " " & HE(BEIS) & ")(  )", "\1:", "", "", zWildon, zNoConfirm, zMain)
        Rep("( " & HE(MEM) & HE(LAMED) & HE(KAF) & HE(YUD) & HE(FINAL_MEM) & " " & HE(ALEF) & ")(  )", "\1:", "", "", zWildon, zNoConfirm, zMain)
        Rep("( " & HE(MEM) & HE(LAMED) & HE(KAF) & HE(YUD) & HE(FINAL_MEM) & " " & HE(BEIS) & ")(  )", "\1:", "", "", zWildon, zNoConfirm, zMain)
        Rep("( " & HE(DALET) & HE(BEIS) & HE(REISH) & HE(YUD) & " " & HE(HEY) & HE(YUD) & HE(MEM) & HE(YUD) & HE(FINAL_MEM) & " " & HE(ALEF) & ")(  )", "\1:", "", "", zWildon, zNoConfirm, zMain)
        Rep("( " & HE(DALET) & HE(BEIS) & HE(REISH) & HE(YUD) & " " & HE(HEY) & HE(YUD) & HE(MEM) & HE(YUD) & HE(FINAL_MEM) & " " & HE(BEIS) & ")(  )", "\1:", "", "", zWildon, zNoConfirm, zMain)
    End Sub
    Sub GetRidOfParsha() 'Used by Bar Ilan
        'bereishis
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(BEIS) & HE(REISH) & HE(ALEF) & HE(SHIN) & HE(YUD) & HE(TAV), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(NUN) & HE(CHES), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(LAMED) & HE(FINAL_KAF) & ChrW(32) & HE(LAMED) & HE(FINAL_KAF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(YUD) & HE(REISH) & HE(ALEF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(CHES) & HE(YUD) & HE(YUD) & ChrW(32) & HE(SHIN) & HE(REISH) & HE(HEY), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(TAV) & HE(vav) & HE(LAMED) & HE(DALET) & HE(TAV), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(YUD) & HE(TSADI) & HE(ALEF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(YUD) & HE(SHIN) & HE(LAMED) & HE(CHES), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(YUD) & HE(SHIN) & HE(BEIS), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(MEM) & HE(KOOF) & HE(FINAL_TSADI), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(YUD) & HE(GIMEL) & HE(SHIN), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(YUD) & HE(CHES) & HE(YUD), "", "", "", zWildOff, zNoConfirm, zMain)

        'shmos
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(SHIN) & HE(MEM) & HE(vav) & HE(TAV), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(ALEF) & HE(REISH) & HE(ALEF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(BEIS) & HE(ALEF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(BEIS) & HE(SHIN) & HE(LAMED) & HE(CHES), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(YUD) & HE(TAV) & HE(REISH) & HE(vav), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(MEM) & HE(SHIN) & HE(PEY) & HE(TES) & HE(YUD) & HE(FINAL_MEM), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(TAV) & HE(REISH) & HE(vav) & HE(MEM) & HE(HEY), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(TAV) & HE(TSADI) & HE(vav) & HE(HEY), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(KAF) & HE(YUD) & ChrW(32) & HE(TAV) & HE(SHIN) & HE(ALEF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(YUD) & HE(KOOF) & HE(HEY) & HE(LAMED), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(PEY) & HE(KOOF) & HE(vav) & HE(DALET) & HE(YUD), "", "", "", zWildOff, zNoConfirm, zMain)

        'vayikra
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(YUD) & HE(KOOF) & HE(REISH) & HE(ALEF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(TSADI) & HE(vav), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(SHIN) & HE(MEM) & HE(YUD) & HE(NUN) & HE(YUD), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(TAV) & HE(ZAYIN) & HE(REISH) & HE(YUD) & HE(AYIN), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(MEM) & HE(TSADI) & HE(REISH) & HE(AYIN), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(ALEF) & HE(CHES) & HE(REISH) & HE(YUD) & ChrW(32) & HE(MEM) & HE(vav) & HE(TAV), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(KOOF) & HE(DALET) & HE(SHIN) & HE(YUD) & HE(FINAL_MEM), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(KOOF) & HE(DALET) & HE(vav) & HE(SHIN) & HE(YUD) & HE(FINAL_MEM), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(ALEF) & HE(MEM) & HE(REISH), "", "", "", zWildOff, zNoConfirm, zMain)

        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(BEIS) & HE(HEY) & HE(REISH), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(BEIS) & HE(CHES) & HE(KOOF) & HE(TAV) & HE(YUD), "", "", "", zWildOff, zNoConfirm, zMain)

        'bamidbar
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(BEIS) & HE(MEM) & HE(DALET) & HE(BEIS) & HE(REISH), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(NUN) & HE(SHIN) & HE(ALEF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(BEIS) & HE(HEY) & HE(AYIN) & HE(LAMED) & HE(TAV) & HE(FINAL_KAF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(BEIS) & HE(HEY) & HE(AYIN) & HE(LAMED) & HE(vav) & HE(TAV) & HE(FINAL_KAF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(SHIN) & HE(LAMED) & HE(CHES), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(KOOF) & HE(REISH) & HE(CHES), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(KOOF) & HE(vav) & HE(REISH) & HE(CHES), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(CHES) & HE(KOOF) & HE(TAV), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(CHES) & HE(vav) & HE(KOOF) & HE(TAV), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(BEIS) & HE(LAMED) & HE(KOOF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(PEY) & HE(YUD) & HE(NUN) & HE(CHES) & HE(SAMECH), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(MEM) & HE(TES) & HE(vav) & HE(TAV), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(MEM) & HE(SAMECH) & HE(AYIN) & HE(YUD), "", "", "", zWildOff, zNoConfirm, zMain)

        'divorim
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(DALET) & HE(BEIS) & HE(REISH) & HE(YUD) & HE(FINAL_MEM), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(ALEF) & HE(TAV) & HE(CHES) & HE(NUN) & HE(FINAL_NUN), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(AYIN) & HE(KOOF) & HE(BEIS), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(REISH) & HE(ALEF) & HE(HEY), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(SHIN) & HE(PEY) & HE(TES) & HE(YUD) & HE(FINAL_MEM), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(KAF) & HE(YUD) & ChrW(32) & HE(TAV) & HE(TSADI) & HE(ALEF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(KAF) & HE(YUD) & ChrW(32) & HE(TAV) & HE(BEIS) & HE(vav) & HE(ALEF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(NUN) & HE(TSADI) & HE(BEIS) & HE(YUD) & HE(FINAL_MEM), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(YUD) & HE(LAMED) & HE(FINAL_KAF), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(HEY) & HE(ALEF) & HE(ZAYIN) & HE(YUD) & HE(NUN) & HE(vav), "", "", "", zWildOff, zNoConfirm, zMain)
        Rep(" " & HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & " " & HE(vav) & HE(ZAYIN) & HE(ALEF) & HE(TAV) & ChrW(32) & HE(HEY) & HE(BEIS) & HE(REISH) & HE(KAF) & HE(HEY), "", "", "", zWildOff, zNoConfirm, zMain)

    End Sub
    Sub AbbreviateMesechtos() 'Used by Bar Ilan
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = HE(BEIS) & HE(BEIS) & HE(ALEF) & " " & HE(KOOF) & _
                HE(MEM) & HE(ALEF)
            .Replacement.Text = HE(BEIS) & """" & HE(KOOF)
            .Forward = True
            .Wrap = wdFindContinue
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        With Selection.Find
            .Text = HE(BEIS) & HE(BEIS) & HE(ALEF) & " " & HE(MEM) & _
                HE(TSADI) & HE(YUD) & HE(AYIN) & HE(ALEF)
            .Replacement.Text = HE(BEIS) & """" & HE(MEM)
            .Forward = True
            .Wrap = wdFindContinue
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        With Selection.Find
            .Text = HE(BEIS) & HE(BEIS) & HE(ALEF) & " " & HE(BEIS) & _
                HE(TAV) & HE(REISH) & HE(ALEF)
            .Replacement.Text = HE(BEIS) & """" & HE(BEIS)
            .Forward = True
            .Wrap = wdFindContinue
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        With Selection.Find
            .Text = HE(AYIN) & HE(BEIS) & HE(vav) & HE(DALET) & ChrW(HEY _
                ) & " " & HE(ZAYIN) & HE(REISH) & HE(HEY)
            .Replacement.Text = HE(AYIN) & """" & HE(ZAYIN)
            .Forward = True
            .Wrap = wdFindContinue
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        With Selection.Find
            .Text = HE(MEM) & HE(vav) & HE(AYIN) & HE(DALET) & " " & _
                HE(KOOF) & HE(TES) & HE(FINAL_NUN)
            .Replacement.Text = HE(MEM) & HE(vav) & """" & HE(KOOF)
            .Forward = True
            .Wrap = wdFindContinue
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        With Selection.Find
            .Text = HE(REISH) & HE(ALEF) & HE(SHIN) & " " & HE(HEY) & _
                HE(SHIN) & HE(NUN) & HE(HEY)
            .Replacement.Text = HE(REISH) & """" & HE(HEY)
            .Forward = True
            .Wrap = wdFindContinue
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
    End Sub
    Sub FixBavli() 'Used by Bar Ilan
        'Remove Talmud Bavli Meseces
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = HE(TAV) & HE(LAMED) & HE(MEM) & HE(vav) & ChrW(DALET _
                ) & " " & HE(BEIS) & HE(BEIS) & HE(LAMED) & HE(YUD) & " " & _
                HE(MEM) & HE(SAMECH) & HE(KAF) & HE(TAV)
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        'Remove daf
        With Selection.Find
            .Text = HE(DALET) & HE(FINAL_PEY) & " "
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        'Omud Aleph
        With Selection.Find
            .Text = " " & HE(AYIN) & HE(MEM) & HE(vav) & HE(DALET) & " " & HE(ALEF)
            .Replacement.Text = "."
            .Forward = True
            .Wrap = wdFindContinue
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        'Omud Bais
        With Selection.Find
            .Text = " " & HE(AYIN) & HE(MEM) & HE(vav) & HE(DALET) & " " & HE(BEIS)
            .Replacement.Text = ":"
            .Forward = True
            .Wrap = wdFindContinue
        End With
    End Sub


End Module
