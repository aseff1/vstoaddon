Attribute VB_Name = "M_Prepare4OizVihodor"
Option Explicit

Sub PutInEinMishpot()
    GoHome
    Do While 1 = 1
        Selection.Find.ClearFormatting
        Selection.Find.style = ActiveDocument.Styles("SECTION TITLE")
        With Selection.Find
            .Text = HE(SHIN) & HE(AYIN) & HE(REISH) & " " & HE(HEY) & _
                HE(TAV) & HE(LAMED) & HE(MEM) & HE(vav) & HE(DALET)
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindStop
            .Format = True
        End With
        Selection.Find.Execute
        Clps wdCollapseEnd
    
        Selection.TypeParagraph
        Selection.style = ActiveDocument.Styles("Ein Mishpot")
        Selection.TypeText Text:=HE(GIMEL) & HE(GIMEL) & HE(GIMEL) & HE(GIMEL) & HE(GIMEL) & HE(GIMEL) & HE(GIMEL)
       ' Selection.TypeText Text:=HE(REISH) & HE(MEM) & HE(BEIS) & """" & HE(FINAL_MEM) & " " _
        & HE(HEY) & HE(LAMED) & "' " & _
        HE(TES) & HE(VAV) & HE(MEM) & HE(ALEF) & HE(TAV) & " " & HE(ALEF) & HE(VAV) & HE(KAF) & HE(LAMED) & HE(YUD) & HE(FINAL_NUN) _
        & " : "

        If aCurrSection = ActiveDocument.Sections.Count Then
                Exit Sub
            Else
                Selection.GoToNext wdGoToSection
            End If
    Loop
    'adds daleds to gimels
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = ChrW(1490) & ChrW(1490) & ChrW(1490) & ChrW(1490) & ChrW(1490 _
            ) & ChrW(1490) & ChrW(1490)
        .Replacement.Text = "^&^p" & ChrW(1491) & ChrW(1491) & ChrW(1491) & _
            ChrW(1491) & ChrW(1491) & ChrW(1491)
        .Forward = True
        .Wrap = wdFindContinue
        .MatchWildcards = False
    End With
    
    'changes gimels to yerushalmi
    Selection.Find.Execute Replace:=wdReplaceAll
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    Selection.Find.Replacement.style = ActiveDocument.Styles("Ein Mishpot Bold")
    With Selection.Find
        .Text = ChrW(1490) & ChrW(1490) & ChrW(1490) & ChrW(1490) & ChrW(1490 _
            ) & ChrW(1490) & ChrW(1490)
        .Replacement.Text = ChrW(1497) & ChrW(1512) & ChrW(1493) & ChrW(1513) _
             & ChrW(1500) & ChrW(1502) & ChrW(1497)
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
'    'changes daleds to rambam  - run this in regular replace
'    Selection.Find.ClearFormatting
'    Selection.Find.Replacement.ClearFormatting
'    With Selection.Find
'        .Text = ChrW(1491) & ChrW(1491) & ChrW(1491) & ChrW(1491) & ChrW(1491) & ChrW(1491)
'        .Replacement.Text = ChrW(1512) & ChrW(1502) & ChrW(1489) & """" & _
'            ChrW(1501) & " " & ChrW(1492) & ChrW(1500) & "' " & ChrW(1513) & _
'            ChrW(1502) & ChrW(1496) & ChrW(1492) & "  :  "
'        .Forward = True
'        .Wrap = wdFindContinue
'        .MatchWildcards = False
'    End With
'    Selection.Find.Execute Replace:=wdReplaceAll
End Sub

Sub AAAPutMishnaNumberAfterSharHatalmud()
    Selection.HomeKey Unit:=wdStory
    Selection.Find.ClearFormatting
    Selection.Find.style = ActiveDocument.Styles("SECTION TITLE")
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = HE(SHIN) & HE(AYIN) & HE(REISH) & " " & HE(HEY) & _
            HE(TAV) & HE(LAMED) & HE(MEM) & HE(vav) & HE(DALET)
        .Replacement.Text = "^&^p" & HE(KOOF) & HE(KOOF) & HE(KOOF) & _
            HE(KOOF) & HE(KOOF) & " "
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    Selection.Find.Replacement.style = ActiveDocument.Styles("Little Mishna Title")
    With Selection.Find
        .Text = HE(KOOF) & HE(KOOF) & HE(KOOF) & HE(KOOF) & HE(KOOF)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    Selection.HomeKey Unit:=wdStory
    
    Do While zzFind(HE(KOOF) & HE(KOOF) & HE(KOOF) & HE(KOOF) & HE(KOOF) & " ", zWildOff, "")
        Selection.Collapse wdCollapseEnd
        Selection.Fields.Add Range:=Selection.Range, Type:=wdFieldEmpty, Text:="SECTION  \* hebrew1 ", PreserveFormatting:=True
    Loop
             
      Selection.HomeKey Unit:=wdStory
             
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = HE(KOOF) & HE(KOOF) & HE(KOOF) & HE(KOOF) & HE(KOOF)
        .Replacement.Text = HE(MEM) & HE(SHIN) & HE(NUN) & HE(HEY)
        .Forward = True
        .Wrap = wdFindContinue
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    Selection.WholeStory
    Selection.Fields.Unlink
    Selection.Collapse wdCollapseStart
    
    AAAMoveFootnoteFromBeginning2LaterMishna
    ' For Batch mDone
End Sub
Sub AAAPutMishnaLetterinMishna() 'do one perek at a time
Dim Flag As String
    Flag = String(6, HE(FINAL_MEM))
    
    cAY "", Flag & " ^&", "MISHNA", "", zWildOff, zNoConfirm, zMain
    cAY Flag, "", "", "Mishna Letter in Mishna", zWildOff, zNoConfirm, zMain

    Selection.HomeKey Unit:=wdStory
    
    Do While zzFind(Flag, zWildOff, "")
        Selection.Collapse wdCollapseEnd
        Selection.Fields.Add Range:=Selection.Range, Type:=wdFieldEmpty, Text:= _
        "SECTION  \* hebrew1 ", PreserveFormatting:=True
    Loop
    
    cAY Flag, "", "", "", zWildOff, zNoConfirm, zMain
    
    Selection.WholeStory
    Selection.Fields.Unlink
    Selection.Collapse wdCollapseStart
    mDone
End Sub
Sub AAAMoveLongFootnoteToEnd()
Dim docMain As Word.Document, doc2 As Word.Document
Dim aFN As Footnote, aLength1 As Integer, aLength2 As Integer
Const MAX_FOOTNOTE_LENGTH As Integer = 30
Dim FootnoteNumber As Integer, aText$

    aAttachTemplates
    Set docMain = ActiveDocument
    cAY " {2,}", " ", "", "", zWildOff, zNoConfirm, zFootnotesAndEndnotes 'Clean up footnotes
    GoEnd
    Selection.InsertBreak Type:=wdSectionBreakNextPage
    
    Set doc2 = Documents.Add
    docMain.Activate
   
    For Each aFN In ActiveDocument.Footnotes
        docMain.Activate
        aFN.Range.Select
        aLength1 = Len(aFN.Range.Text)
        If aLength1 < (MAX_FOOTNOTE_LENGTH + 5) Then GoTo NextFn
       
        
        FootnoteNumber = aFN.index
        Selection.Copy
            
        doc2.Activate
        Selection.WholeStory
        Selection.Delete
        Selection.Paste
        Selection.WholeStory
        Selection.RtlPara
        
        cAY "^p", "", "", ""
        cAY " ^p", "", "", ""
        cAY "", "====" & "^&" & "++++", "mokor small", "" 'Marks mokor small
        
        cAY ",++++", "++++,", "", "" 'fixes commas so they don't die
        cAY "(,)(?++++)", "\2\1", "", "", zWildon
        
        cAY "====*++++", "", "", "", zWildon 'Deletes mokor small
                
        Selection.WholeStory
        aLength2 = Len(Selection)
        
        If (aLength1 - aLength2) < MAX_FOOTNOTE_LENGTH Then GoTo NextFn
                
'        Selection.Extend
'        Selection.Extend
'        Selection.Extend
'        Selection.Font.Reset  this works even though it shouldn;t


'        Clps wdCollapseEnd
        'zStyleDelete "mokor small"
'        cAY ",", ",", "mokor small", "", zWildOff, znoconfirm, zMain
        
'        Selection.MoveLeft wdCharacter, 1, wdExtend
'        On Error Resume Next
'        Selection.ClearCharacterAllFormatting
'        Selection.ClearCharacterAllFormatting
'        Selection.ClearFormatting
'        Selection.ClearFormatting
'        On Error GoTo 0
        
        docMain.Activate
        aFN.Range.Select
        Selection.Cut
        
        GoEnd
        Selection.TypeText HE(DALET) & HE(FINAL_PEY) & " "
        Selection.InsertCrossReference ReferenceType:="Footnote", ReferenceKind:= _
            wdPageNumber, ReferenceItem:=FootnoteNumber, InsertAsHyperlink:=True, _
            IncludePosition:=False, SeparateNumbers:=False, SeparatorString:=" "
        
        Selection.TypeText ", " & HE(HEY) & HE(AYIN) & HE(REISH) & HE(HEY) & " "
        Selection.InsertCrossReference ReferenceType:="Footnote", ReferenceKind:= _
            wdFootnoteNumber, ReferenceItem:=FootnoteNumber, InsertAsHyperlink:=True, _
            IncludePosition:=False, SeparateNumbers:=False, SeparatorString:=" "
                
        Selection.TypeText ": "
        Selection.MoveLeft Unit:=wdCharacter, Count:=3
        Selection.Extend
        Selection.Extend
        Selection.Extend
        Selection.style = ActiveDocument.Styles("mokor oruch header")
        Clps wdCollapseEnd
    
        Selection.Paste
        Selection.TypeParagraph
        Selection.TypeParagraph
        
        doc2.Activate
        Selection.WholeStory
        aText = Trim(Selection.Text) 'new
        aText = Mid(aText, 1, Len(aText) - 1) & " " '& Chr(167)
        aCopy aText 'new
       'new Selection.MoveLeft wdCharacter, 1, wdExtend
        'new Selection.Cut
        
        docMain.Activate
        aFN.Range.Select
        Clps wdCollapseStart
        'Selection.MoveRight wdCharacter, 2
        Selection.Paste
        
        Application.Templates("C:\Users\" & Environ("username") & "\AppData\Roaming\Microsoft\Document Building Blocks\1033\14\Building Blocks.dotx" _
        ).BuildingBlockEntries("SYMBOL").Insert Where:=Selection.Range, RichText:=True
        
NextFn: Next aFN
    doc2.Activate
    ActiveWindow.Close (Word.WdSaveOptions.wdDoNotSaveChanges)
    mDone
End Sub

Sub AAATouchUpEnd()
    AAAMoveFootnoteFromBeginning2LaterMishna
    FixKDD
    ReplaceRosheiTeivosConfirm
    mDone
End Sub
Sub AAAMoveFootnoteFromBeginning2LaterMishna()
 GoHome
        
    Do While zzFind("^f", zWildOff, "mishna title")
        Selection.Cut
        zzFind "", zWildOff, "Little Mishna Title"
        Selection.EndKey Unit:=wdLine
        Selection.PasteAndFormat (wdFormatOriginalFormatting)
        GoHome
    Loop
End Sub
