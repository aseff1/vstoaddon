'Attribute VB_Name = "M_Commemerative"
Option Explicit
Sub SaveCurrentDocumentAsPDF_WithoutPrompt()
Dim NewName$
    NewName = Replace(ActiveDocument.FullName, ".doc", ".pdf")
    NewName = Replace(ActiveDocument.FullName, ".docx", ".pdf")
    
    ActiveDocument.ExportAsFixedFormat OutputFileName:= _
        NewName, ExportFormat:=wdExportFormatPDF, OpenAfterExport:=False, OptimizeFor:= _
        wdExportOptimizeForPrint, Range:=wdExportAllDocument, from:=1, To:=1, _
        Item:=wdExportDocumentContent, IncludeDocProps:=True, KeepIRM:=True, _
        CreateBookmarks:=wdExportCreateNoBookmarks, DocStructureTags:=True, _
        BitmapMissingFonts:=True, UseISO19005_1:=False
        
    mDone
        
End Sub
Sub Indexing() 'Puts all "tavla" into index
    GoHome
    Child_Indexing_Internal
    If GetIntoFootnotes = True Then Child_Indexing_Internal
    Child_CreateIndex
    mDone
End Sub
Sub Child_Indexing_Internal() 'Used by Indexing
Dim zwhat As String, zname As String
'    zwhat = he(TES) & he(BEIS) & he(LAMED) & he(ALEF) & "*" & he(DALET) & he(Final_PEY)
    zwhat = HE(TES) & HE(BEIS) & HE(LAMED) & HE(ALEF) & " "
    'Runs once in main text, and then once in footnotes:
    Do While zzFind(zwhat, zWildOff, "")
        Selection.Extend
        'Selection.MoveLeft Unit:=wdWord, Count:=7, Extend:=wdExtend
        Selection.MoveRight Unit:=wdWord, Count:=7
        zname = Selection.Text
        Selection.Collapse 'Direction:=wdCollapseStart
        ActiveDocument.Indexes.MarkEntry Range:=Selection.Range, Entry:=zname, EntryAutoText:=zname, CrossReference:="", CrossReferenceAutoText:="", _
            BookmarkName:="", Bold:=False, Italic:=False
        Selection.MoveRight Unit:=wdWord, Count:=3
        Selection.Collapse direction:=wdCollapseEnd
        Selection.EscapeKey
    Loop
End Sub
Sub Child_CreateIndex()  'Used by Indexing
    Selection.Collapse direction:=wdCollapseStart
    Selection.EscapeKey
    GoHome
    GoHome
    GoHome
    Selection.EndKey Unit:=wdStory
    Selection.InsertBreak Type:=wdSectionBreakNextPage
    With ActiveDocument
        .Indexes.Add Range:=Selection.Range, HeadingSeparator:= _
            wdHeadingSeparatorNone, Type:=wdIndexIndent, RightAlignPageNumbers:= _
            False, NumberOfColumns:=1, IndexLanguage:=wdHebrew
        .Indexes(1).TabLeader = wdTabLeaderDots
    End With
End Sub
Sub FixAllFilesBeforeCombinig()
    Dim J As Integer
    Dim sFile As String
    Dim D As Word.Document
    For J = 1 To 10
    sFile = "C:\Users\" & Environ("username") & "\Desktop\1\6 1 Kelim " & J & ".docx"
        If (Dir(sFile) > "") Then
            Set D = Documents.Open(sFile)
            D.Activate
            Child_UnlinkSectionFieldInHeaders
            Child_FixPageSizeAndMargins
            D.Close WdSaveOptions.wdSaveChanges
            End If
    Next
End Sub
Sub Child_FixPageSizeAndMargins()
    Selection.WholeStory
    With Selection.PageSetup
        .LineNumbering.Active = False
        .Orientation = wdOrientPortrait
        .TopMargin = InchesToPoints(1)
        .BottomMargin = InchesToPoints(1)
        .LeftMargin = InchesToPoints(0.75)
        .RightMargin = InchesToPoints(0.75)
        .Gutter = InchesToPoints(0)
        .HeaderDistance = InchesToPoints(0.5)
        .FooterDistance = InchesToPoints(0.5)
        .PageWidth = InchesToPoints(8.5)
        .PageHeight = InchesToPoints(11)
        .FirstPageTray = wdPrinterDefaultBin
        .OtherPagesTray = wdPrinterDefaultBin
        .SectionStart = wdSectionNewPage
        .OddAndEvenPagesHeaderFooter = False
        .DifferentFirstPageHeaderFooter = False
        .VerticalAlignment = wdAlignVerticalTop
        .SuppressEndnotes = False
        .MirrorMargins = False
        .TwoPagesOnOne = False
        .BookFoldPrinting = False
        .BookFoldRevPrinting = False
        .BookFoldPrintingSheets = 1
        .GutterPos = wdGutterPosRight
        .SectionDirection = wdSectionDirectionRtl
    End With
    Selection.Collapse
End Sub

Sub CombineFiles()
Dim J As Integer
Dim sFile As String
Dim aFile As String
    aFile = "C:\Users\" & Environ("username") & "\Desktop\1\6 4 Poro "
    Selection.EndKey Unit:=wdStory 'moves to end of document
    Selection.InsertBreak Type:=wdSectionBreakNextPage 'puts section break with new page between perokim
    For J = 2 To 30
        sFile = aFile & J & ".doc"
        If (Dir(sFile) > "") Then
            Selection.InsertFile FileName:=sFile, ConfirmConversions:=False
            Selection.TypeParagraph
            Selection.EndKey Unit:=wdStory 'moves to end of document
            Selection.InsertBreak Type:=wdSectionBreakNextPage 'puts section break with new page between perokim
        End If
        sFile = aFile & J & ".docx"
        If (Dir(sFile) > "") Then
            Selection.InsertFile FileName:=sFile, ConfirmConversions:=False
            Selection.TypeParagraph
            Selection.EndKey Unit:=wdStory 'moves to end of document
            Selection.InsertBreak Type:=wdSectionBreakNextPage 'puts section break with new page between perokim
        End If
    Next
    mDone
End Sub
Sub Child_UnlinkSectionFieldInHeaders()
    Dim sc As Section
    On Error GoTo ER
    
    Application.ScreenUpdating = False
    zzUnLinkHeaders
    
    ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
    GoHome
    
    ActiveWindow.ActivePane.View.SeekView = wdSeekPrimaryHeader
   
    ActiveWindow.View.ShowFieldCodes = True
    'Selection.Fields.ToggleShowCodes

    For Each sc In ActiveDocument.Sections
        ayFind "^d^wSECTION", False, False
        Selection.Fields.Unlink
        ActiveWindow.ActivePane.View.NextHeaderFooter
    Next sc
    
    ActiveWindow.ActivePane.View.SeekView = wdSeekMainDocument
    GoHome
    ActiveWindow.View.ShowFieldCodes = False
    Application.ScreenUpdating = True
    'mDone
    Exit Sub
    
ER:
    Select Case Err.Number
        Case 4605   'last section, can't go to next
            Resume Next
        Case Else
            Stop
            D Err.Number
            Resume Next
    End Select
    
End Sub
Sub Obs_AAA() ' i think this is junk
Dim oStory As Word.Range  'storyrange 'wdstorytype 'Object
Dim f As Field
'Dim oToc As Object
     
    Application.ScreenUpdating = False
    For Each oStory In ActiveDocument.StoryRanges
        Select Case oStory.StoryType
            Case 7, 10, 6
                For Each f In oStory.Fields
                    f
                    f.Unlink
                Next f
        End Select
    Next oStory
    
    Application.ScreenUpdating = True
    
End Sub
Sub Obs_AConvert_Field_2_Text_in_Header() ' I think this junk
'First copy one header
'Then set Mishna as Field Section
'Then Run this
Dim J As Integer
Dim K As Integer
UnLinkHeaders
    
    Dim f As Field
    f.LinkFormat
    
    J = ActiveDocument.Sections.Count
    
    For K = 1 To J
        Selection.HomeKey Unit:=wdLine
        Selection.Find.ClearFormatting
        With Selection.Find
            .Text = ":"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindAsk
            .MatchWildcards = False
        End With
        Selection.Find.Execute
        Selection.MoveRight Unit:=wdCharacter, Count:=1
        Selection.Extend
        Selection.MoveRight Unit:=wdCharacter, Count:=2
        Selection.Fields.Unlink
        
        If (K < J) Then ActiveWindow.ActivePane.View.NextHeaderFooter
              
    Next K
'            For K = 1 To ActiveDocument.Sections(J).Footers.Count
'                ActiveDocument.Sections(J).Footers(K).LinkToPrevious = True
'           Next K
''        Next j
'    End If
End Sub
