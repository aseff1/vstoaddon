Module WP_To_W_2


    'http://www.ammara.com/access_image_faq/code/browse_for_folder_code.txt
Private Type BROWSEINFO
  hOwner As Long
  pidlRoot As Long
  pszDisplayName As String
  lpszTitle As String
  ulFlags As Long
  lpfn As Long
  lParam As Long
  iImage As Long
End Type

    Private Declare Function SHGetPathFromIDList Lib "shell32.dll" Alias _
                "SHGetPathFromIDListA" (ByVal pidl As Long, _
                ByVal pszPath As String) As Long

    Private Declare Function SHBrowseForFolder Lib "shell32.dll" Alias _
                "SHBrowseForFolderA" (lpBrowseInfo As BROWSEINFO) _
                As Long

    Private Const BIF_RETURNONLYFSDIRS = &H1
    '***





    'http://www.ammara.com/access_image_faq/code/browse_for_folder_code.txt
    Public Function BrowseFolder(szDialogTitle As String) As String
        Dim x As Long, bi As BROWSEINFO, dwIList As Long
        Dim szPath As String, wPos As Integer

        With bi
            .hOwner = Application.hWnd  'hWndAccessApp
            .lpszTitle = szDialogTitle
            .ulFlags = BIF_RETURNONLYFSDIRS
        End With

        dwIList = SHBrowseForFolder(bi)
        szPath = Space$(512)
    x = SHGetPathFromIDList(ByVal dwIList, ByVal szPath)

        If x Then
            wPos = InStr(szPath, Chr(0))
            BrowseFolder = Left$(szPath, wPos - 1)
        Else
            BrowseFolder = vbNullString
        End If
    End Function




    Sub aRemoveTalmudHakdamaFootnote()
        'Stop
        Clps wdCollapseStart
        Selection.MoveRight(wdCharacter, 1)
        Selection.MoveRight(wdCharacter, 1, wdExtend)
        Selection.Cut()
        ActiveDocument.Bookmarks.Add(Range:=Selection.Range, name:="Q____Q____Q____Q____5Q")

        Selection.GoTo(What:=wdGoToSection, Which:=wdGoToFirst, Count:=aCurrSection, name:="")
        zzFind(HE(MEM) & HE(SHIN) & HE(NUN) & HE(HEY), False, "")
        Selection.EndKey Unit:=wdLine
        Selection.Paste()

        ActiveDocument.Bookmarks("Q____Q____Q____Q____5Q").Select()
        ActiveDocument.Bookmarks("Q____Q____Q____Q____5Q").Delete()
        Selection.HomeKey wdLine
        Selection.EndKey(wdLine, wdExtend)
        Selection.Delete()
        'aScrollDown
    End Sub
    Sub ResetFRParameters()
        On Error GoTo Handler
        With Selection.Find
            .Text = ""
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
            .Execute()
        End With
        Exit Sub
Handler:
    End Sub

    Sub aRemoveTalmudHakdama()
        'Stop
        Clps wdCollapseStart
        Selection.MoveRight(wdCharacter, 1)
        Selection.EndKey(wdLine, wdExtend)
        Selection.Delete()
        'aScrollDown
        'Stop
    End Sub


    Function Yerushalmi()
        Yerushalmi = HE(YUD) & HE(REISH) & HE(vav) & HE(SHIN) & HE(LAMED) & HE(MEM) & HE(YUD)
    End Function

    Function Bavli()
        Bavli = HE(BEIS) & HE(BEIS) & HE(LAMED) & HE(YUD)
    End Function


    Sub DoTheTalmudReferenceStuff(aRef As Boolean)
        Dim aBookMark$, se As Selection
        Dim PrevField, aCurrLetter$

        se = Selection


        Clps wdCollapseStart

        aBookMark = GetUniqueBookmarkName

        se.TypeText Text:=vbTab
        se.Fields.Add(Range:=se.Range, Type:=wdFieldEmpty, Text:="SEQ Talmud\* hebrew1")

        With se
            PrevField = .GoToPrevious(What:=wdGoToField)
            .MoveRight(Unit:=wdWord, Extend:=wdExtend)
            .Fields(1).Select()
            .style = "talmud aleph"
            aCurrLetter = .Text
        End With

        With ActiveDocument.Bookmarks
            .Add(Range:=se.Range, name:=aBookMark)
            .DefaultSorting = wdSortByName
        End With
        'Stop
        With se

            Clps wdCollapseEnd
            .ExtendMode = True
            zzFind(" ", zWildOff, "")
            .ExtendMode = False
            .MoveLeft(wdCharacter, 1, wdExtend)

            .TypeText GetTalmudRefChofToYudAlef(.Text)
            .MoveLeft(wdCharacter, 1, wdExtend)

            If .Text <> aCurrLetter Then GoTo errHand 'problem*******with 2 letters
DoThis:
            .TypeText Text:=vbTab
            .Delete()   'problem*******with 2 letters
            'Stop

            If Not aRef Then
                .ExtendMode = True
                .Extend ":"
                .style = "shar bold"
                .ExtendMode = False
                Clps wdCollapseStart
            End If

            'GoHome
            GoToBeginningOfSection()

            'zzFind aCurrLetter, zWildOff, "ay temp"
            FindInSectionForward(aCurrLetter, zWildOff, "ay temp")   'problem*******with 2 letters



            .Delete()


            .InsertCrossReference(ReferenceType:="Bookmark", ReferenceKind:= _
                wdContentText, ReferenceItem:=aBookMark, InsertAsHyperlink:=True, _
                IncludePosition:=False, SeparateNumbers:=False, SeparatorString:=" ")
            'Stop
            'if multiple references then copy it....
            'what to do if it can't find a matching reference...



            .GoTo(What:=wdGoToBookmark, name:=aBookMark)
            Clps wdCollapseEnd
        End With
        Exit Sub


errHand:
        M "Doesn't match!"
        Stop
        GoTo DoThis

    End Sub

    Function GetTalmudRefChofToYudAlef(aLetter$) As String

        Select Case aLetter
            Case HE(KAF)
                GetTalmudRefChofToYudAlef = HE(YUD) & HE(ALEF)
            Case HE(LAMED)
                GetTalmudRefChofToYudAlef = HE(YUD) & HE(BEIS)
            Case HE(MEM)
                GetTalmudRefChofToYudAlef = HE(YUD) & HE(GIMEL)
            Case HE(NUN)
                GetTalmudRefChofToYudAlef = HE(YUD) & HE(DALET)
            Case HE(SAMECH)
                GetTalmudRefChofToYudAlef = HE(TES) & HE(vav)
            Case HE(AYIN)
                GetTalmudRefChofToYudAlef = HE(TES) & HE(ZAYIN)
            Case HE(PEY)
                GetTalmudRefChofToYudAlef = HE(YUD) & HE(ZAYIN)
            Case HE(TSADI)
                GetTalmudRefChofToYudAlef = HE(YUD) & HE(CHES)
            Case HE(KOOF)
                GetTalmudRefChofToYudAlef = HE(YUD) & HE(TES)
            Case HE(REISH)
                GetTalmudRefChofToYudAlef = HE(KAF)
            Case HE(SHIN)
                GetTalmudRefChofToYudAlef = HE(KAF) & HE(ALEF)
            Case HE(TAV)
                GetTalmudRefChofToYudAlef = HE(KAF) & HE(BEIS)
            Case Else
                GetTalmudRefChofToYudAlef = aLetter
                'Stop
        End Select

    End Function




    Sub ayTempStyle()
        'sets "ay temp" style to current selection.
        'used for putting in mock footnotes or letters to be used for MokorStart stuff...

        'Stop
        If Selection.Start = Selection.End Then
            Selection.MoveRight(wdCharacter, 1, wdExtend)
        End If
        Selection.style = "ay temp"

    End Sub

    Sub b()
        DeleteAllFootnotesInSection2()
    End Sub

    Sub DeleteAllFootnotesInSection(Optional aSection% = -1)
        Dim ftn As Footnote
        'Don't why why but it doesn't work.  Use the next one.

        If aSection = -1 Then aSection = aCurrSection
        For Each ftn In ActiveDocument.Sections(aSection).Range.Footnotes
            M ActiveDocument.Sections(aSection).Range.Footnotes.Count
            ActiveDocument.Sections(aSection).Range.Select()
            ftn.Range.Select()
            'ftn.Delete
            'Application.ScreenRefresh
        Next ftn
        mDone()
    End Sub

    Sub GotoPreviousTable()
        Selection.GoTo(What:=WdGoToItem.wdGoToTable, Which:=wdGoToPrevious, Count:=1, name:="")
    End Sub

    Sub GotoNextTable()
        Selection.GoTo(What:=WdGoToItem.wdGoToTable, Which:=wdGoToNext, Count:=1, name:="")
    End Sub


    Sub DeleteAllFootnotesInSection2(Optional aSection% = -1)
        Dim x%, aTotal%


        If aSection = -1 Then aSection = aCurrSection
        aTotal = ActiveDocument.Sections(aSection).Range.Footnotes.Count
        For x = 1 To aTotal
            'M ActiveDocument.Sections(aSection).Range.Footnotes.count
            ActiveDocument.Sections(aSection).Range.Footnotes(1).Range.Select()
            ActiveDocument.Sections(aSection).Range.Footnotes(1).Delete()
            'ftn.Delete
            'Application.ScreenRefresh
        Next x
        mDone()
    End Sub



    Function GetUniqueBookmarkName(Optional Unique As Boolean = False) As String
        Dim xNow
        Dim OldTime As String
        Dim LoopTime As Integer
        Dim iRandom As Integer
        xNow = Now
        GetUniqueBookmarkName = CStr(Year(xNow) & Month(xNow) & Day(xNow) & Hour(xNow) & Minute(xNow) & Second(xNow) & Int(100 * (Timer - Int(Timer))))

        If Unique Then
            If GetUniqueBookmarkName <> OldTime Then
                OldTime = GetUniqueBookmarkName
                iRandom = 1
            Else
                iRandom = iRandom + 1
                GetUniqueBookmarkName = GetUniqueBookmarkName & CStr(iRandom)
            End If
        End If

        For LoopTime = 1 To Len(GetUniqueBookmarkName)
            Mid(GetUniqueBookmarkName, LoopTime, 1) = Chr(Val(Mid(GetUniqueBookmarkName, LoopTime, 1)) + 65)
        Next LoopTime
    End Function



    Sub KDD_Hakdomo()
        'sets "KDD HAKDOMO" to current selection

        With Selection
            .style = "KDD HAKDOMO"
            Clps wdCollapseStart
            .TypeText HE(HEY) & HE(KOOF) & HE(DALET) & HE(MEM) & HE(HEY) & ":" & Chr(13)
            .MoveUp wdLine
            .HomeKey wdLine
            .EndKey(wdLine, wdExtend)
            .style = "shar bold"
            Clps wdCollapseStart
        End With

    End Sub



    Function GetQuickPartTemplateName(aName$)
        Dim x%, y%, t As Template

        Templates.LoadBuildingBlocks()

        For y = 1 To Templates.Count
            For x = 1 To Templates(y).BuildingBlockEntries.Count
                'M t.BuildingBlockTypes(wdTypeQuickParts).Name
                'M t.BuildingBlockTypes(wdTypeCustomQuickParts).Name
                If aName = Templates(y).BuildingBlockEntries.Item(x).name Then
                    GetQuickPartTemplateName = Templates(y).FullName
                    'Stop
                    Exit Function
                End If
            Next x
        Next y

        M("QuickPart not found!", vbCritical, "We caught this error!")
        Stop

    End Function

    Sub ayCleanup(b4$, after$, aQuickPart$)
        Dim aTemplateName$, aFound As Boolean

        aTemplateName = GetQuickPartTemplateName(aQuickPart)
        Do 'While zzFind("444^p", False, "")
            aFound = zzFind(b4 & "^p", False, "")
            If Not aFound Then Exit Do
            On Error GoTo errHan
            Application.Templates(aTemplateName).BuildingBlockEntries(aQuickPart).Insert( _
                Where:=Selection.Range, RichText:=True)


            'Application.Templates( _
                "C:\Users\Avraham 2\AppData\Roaming\Microsoft\Templates\DavisNormal.dotm" _
                ).BuildingBlockEntries("KOL DEMOMO DAKA").Insert Where:=Selection.Range, _
                RichText:=True

        Loop
        Rep(after & "^p", "", "", "", zWildOff, zNoConfirm, zMain)
        Rep("(" & after & ")(^012)", "\2", "", "", zWildon, zNoConfirm, zMain)

        Exit Sub

errHan:
        If Err = -2147467259 Then Resume Next 'buggy.  I don't understand this, I'm just ignoring the error.  It appears to happen only in a table.
    End Sub

    Sub CreateTempStyles()

        On Error Resume Next
        ActiveDocument.Styles.Add(name:=TEMP_CHAR_STYLE_NAME, Type:=wdStyleTypeCharacter)
        With ActiveDocument.Styles(TEMP_CHAR_STYLE_NAME).Font
            .name = "Times New Roman"
            .UnderlineColor = wdColorWhite
            .Color = 16737792
            .Position = 1
            .SizeBi = 14
            .NameBi = "RmzFrank"
            With .Shading
                .Texture = wdTextureNone
                .ForegroundPatternColor = wdColorAutomatic
                .BackgroundPatternColor = wdColorBrightGreen
            End With
            .Borders(1).LineStyle = wdLineStyleNone
            .Borders.Shadow = False
        End With
        ActiveDocument.Styles(TEMP_CHAR_STYLE_NAME).LanguageID = wdHebrew


        '
        ' Macro1 Macro
        '
        '
        ActiveDocument.Styles.Add(name:=TEMP_PARA_STYLE_NAME, Type:=wdStyleTypeParagraph)
        With ActiveDocument.Styles(TEMP_PARA_STYLE_NAME).Font
            .name = "Arial"
            .UnderlineColor = wdColorWhite
            .Color = 16737792
            .Position = 1
            .SizeBi = 16
            .NameBi = "RmzFrank"
            With .Shading
                .Texture = wdTextureNone
                .ForegroundPatternColor = wdColorAutomatic
                .BackgroundPatternColor = wdColorAqua
            End With
            .Borders(1).LineStyle = wdLineStyleNone
            .Borders.Shadow = False
        End With
        ActiveDocument.Styles(TEMP_PARA_STYLE_NAME).LanguageID = wdHebrew

    End Sub

    '''InsertMachlokes
    'TALMUD
    '''RemoveTalmudHakdama
    'TalmudFootnoteAlephSmall
    'TalmudTabs
    'TalmudHardReturns
    'TalmudSoftReturns
    'Drashos888
    'ConvertMokorSmallInDroshosToSharThin
    'CheckNumberRemoval
    'ConvertBracketsInFootnotesToMokorSmall



    Sub KillTempStyles()
        On Error Resume Next
        ActiveDocument.Styles(TEMP_CHAR_STYLE_NAME).Delete()
        ActiveDocument.Styles(TEMP_PARA_STYLE_NAME).Delete()
    End Sub

    Sub CreateNewFootnote()
        With Selection
            With .FootnoteOptions
                .Location = wdBeneathText
                .NumberingRule = wdRestartContinuous
                .StartingNumber = 1
                .NumberStyle = wdNoteNumberStyleArabic
                .NumberingRule = wdRestartSection
            End With
            .Footnotes.Add(Range:=Selection.Range, Reference:="")
        End With
    End Sub

    Function GetCurrentMishnaWithoutNikkud()
        Dim aText$

        Selection.GoTo(What:=wdGoToSection, Which:=wdGoToFirst, Count:=aCurrSection, name:="")
        Selection.GoTo(What:=wdGoToField, Which:=wdGoToNext, Count:=1, name:="")

        Selection.Paragraphs(1).Range.Select()
        aText = Selection.Text
        Clps wdCollapseStart
        GetCurrentMishnaWithoutNikkud = RemoveNikkud(aText)

    End Function

    Sub GetAllMishnayosWithoutNikkud()
        Dim x%, y%, aText$, aDoc1 As Word.Document, aDoc2 As Word.Document

        aDoc1 = ActiveDocument
        'Documents.Add Template:="Normal", NewTemplate:=False, DocumentType:=0
        Documents.Add DocumentType:=wdNewBlankDocument
        aDoc2 = ActiveDocument
        aDoc1.Activate()

        y = aDoc1.Sections.Count
        For x = 1 To y
            Selection.GoTo(What:=wdGoToSection, Which:=wdGoToFirst, Count:=x, name:="")
            Selection.GoTo(What:=wdGoToField, Which:=wdGoToNext, Count:=1, name:="")
            Selection.Paragraphs(1).Range.Select()
            aText = Selection.Text
            Clps wdCollapseStart
            aDoc2.Activate()
            Selection.EndKey Unit:=wdStory
            'Stop
            Selection.TypeText(RemoveNikkud(aText))
            Selection.TypeParagraph()
            Selection.TypeParagraph()
            aDoc1.Activate()
        Next x

        GoHome()

        'Selection.WholeStory
        'RemoveNikkud Selection.Text

        'Selection.TypeText Text:=aText

    End Sub
    Sub GetCurrentMishnaWithoutNikkudInNewDoc()
        Dim aText$, aDocMain As Word.Document, aDocTemp As Word.Document

        aDocMain = ActiveDocument
        'Documents.Add Template:="Normal", NewTemplate:=False, DocumentType:=0
        Documents.Add DocumentType:=wdNewBlankDocument
        aDocTemp = ActiveDocument
        aDocMain.Activate()

        Selection.GoTo(What:=wdGoToSection, Which:=wdGoToFirst, Count:=aCurrSection, name:="")
        Selection.GoTo(What:=wdGoToField, Which:=wdGoToNext, Count:=1, name:="")

        Selection.Paragraphs(1).Range.Select()
        aText = Selection.Text
        Clps wdCollapseStart

        aDocTemp.Activate()
        Selection.TypeText(RemoveNikkud(aText))
        'EnlargeKDDmishna
    End Sub




End Module

