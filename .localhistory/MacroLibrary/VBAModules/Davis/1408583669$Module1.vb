Attribute VB_Name = "Module1"
Option Explicit

Sub Main()
    a1
    a2
    a3
    mDone
End Sub
Sub a1()
Dim x$, y$
    GoHome
    cAY "", "", "Little Mishna Title", "Ein Mishpot"
    x = "(" & HE(MEM) & HE(SHIN) & HE(NUN) & HE(HEY) & " )(*)(^09)(*)(^013)"
    y = "\4\5\2"
    cAY x, y, "Ein Mishpot", "", zWildon
End Sub
Sub a2()
    GoHome
    Do While zzFind("", zWildOff, "Ein Mishpot")
        Clps wdCollapseEnd
        Selection.MoveRight WdUnits.wdCharacter, 1
        Selection.MoveRight WdUnits.wdCharacter, 1, wdExtend
        Selection.style = "DropCap"
        Clps wdCollapseEnd
    Loop
End Sub
Sub a3()
    InsertMishnaNumberInShar HE(ALEF) & HE(GIMEL)
    InsertMishnaNumberInShar HE(DALET) & HE(REISH)
    mDone
End Sub
Sub InsertMishnaNumberInShar(Shar$)
    GoHome
    Do While zzFind(Shar, zWildOff, "SECTION TITLE")
        Selection.EndKey Unit:=wdLine
        Selection.MoveRight WdUnits.wdCharacter, 1
        
        MakeDropCap
        If aCurrSection = ActiveDocument.Sections.Count Then
            Exit Sub
        Else
            Selection.GoToNext wdGoToSection
        End If
    Loop
End Sub

Sub MakeDropCap()  'Alt-D
    Selection.Fields.Add Range:=Selection.Range, Type:=wdFieldEmpty, Text:="SECTION  \* hebrew1 ", PreserveFormatting:=True
    Selection.TypeParagraph
    Selection.MoveLeft Unit:=wdCharacter, Count:=1
    Selection.MoveLeft wdCharacter, 1, WdMovementType.wdExtend
    Selection.Fields(1).Unlink
    Selection.style = ActiveDocument.Styles("DropCap")
    Clps wdCollapseStart

End Sub
