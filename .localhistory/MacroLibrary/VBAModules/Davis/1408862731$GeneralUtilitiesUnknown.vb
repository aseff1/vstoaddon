﻿Module GeneralUtilitiesUnknown
    Option Explicit
Private Type BROWSEINFO
  hOwner As Long
  pidlRoot As Long
  pszDisplayName As String
  lpszTitle As String
  ulFlags As Long
  lpfn As Long
  lParam As Long
  iImage As Long
End Type

    Private Declare Function SHGetPathFromIDList Lib "shell32.dll" Alias _
                "SHGetPathFromIDListA" (ByVal pidl As Long, _
                ByVal pszPath As String) As Long

    Private Declare Function SHBrowseForFolder Lib "shell32.dll" Alias _
                "SHBrowseForFolderA" (lpBrowseInfo As BROWSEINFO) _
                As Long

    Private Const BIF_RETURNONLYFSDIRS = &H1
    Sub ConvertNumberInSelectionIntoHebrewNumber()
        Dim num As Integer, MyField As Word.Field
        num = Val(Selection.Text)
        If num = 0 Then Stop

        MyField = Selection.Fields.Add(Range:=Selection.Range, Type:=wdFieldEmpty, Text:= _
            "=" & str(num) & "  \* hebrew1 ", PreserveFormatting:=True)
        MyField.Update()
        MyField.Unlink()
    End Sub
    Sub ViewVBCode()
        'Replaces the built-in "ViewVBCode" Macro
        ' Shows the VB editing environment

        '    Stop
        Application.Keyboard(1033)
        'Application.Keyboard (1037)

        'not yet:  CheckForUpdatedVersionOfModules
        ShowVisualBasicEditor = True

    End Sub

    Sub a()
        Beep()
        mDone()
    End Sub

End Module
