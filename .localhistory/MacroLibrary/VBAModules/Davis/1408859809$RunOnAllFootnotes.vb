'Attribute VB_Name = "RunOnAllFootnotes"
Option Explicit
'These 3 macros go together:
        Sub ClearCharacterDirectFormattingForAllFootnotesInDocument()
            RunCodeOnAllFootnotesInDocument "aClearCharacterDirectFormatting"
        End Sub
   
        Sub RunCodeOnAllFootnotesInDocument(MacroName$)
        Dim f As Footnote
            For Each f In ActiveDocument.Footnotes
                f.Range.Select
                Application.Run MacroName
            Next f
        End Sub
        Sub aClearCharacterDirectFormatting()
            Selection.ClearCharacterDirectFormatting
        End Sub
'End 3
