﻿Module aRelocate_Footnotes

    Dim counter As Integer


    Public Rib As IRibbonUI
    Public MyTag As String

    Dim s As Selection


    Dim aCount As Long

    Private Enum direction
        Forward = CInt(True)
        Back = False
    End Enum

    Private Enum Afterwards
        Afterward = True
        Before = False
    End Enum

    Sub aaaaa()
        mDone()
    End Sub
    'Sub AutoExec()
    '    CreateMoveFootnotesTab
    'End Sub

Public Sub btnStartStop_Click(Optional control As IRibbonControl)
        'Stop
        Select Case MyTag
            Case "Not Running", "" 'start
                ActiveDocument.Bookmarks.Add(Range:=Selection.Range, name:="qqqqqqqqqqqaqq")

                'I think this make sense...:
                If GetBookmarkExists("relocate_temp") Then ActiveDocument.Bookmarks("relocate_temp").Delete()

                If Msgbox("Is this the first time running Move Footnotes on this file?", vbDefaultButton2 + vbYesNo + vbQuestion) = vbYes Then
                    'remember where we are (not really necesary, since first time):

                    '                'put HR at end of each table, to avoid bug:
                    '                Dim tb As Table
                    '                For Each tb In ActiveDocument.Tables
                    '                    tb.Select
                    '                    Clps wdCollapseEnd
                    '                    Selection.MoveLeft WdUnits.wdCharacter, 2, WdMovementType.wdMove
                    '                    Selection.TypeParagraph
                    '                Next tb

                    'Fix all footnote paragraph marks first:
                    CleanUpParagraphMarksInNotes()
                End If

                cAY("^f", "", "", "Footnote Reference", zWildOff, zNoConfirm, zMain) 'fixes all footnote references

                Selection.GoTo(What:=wdGoToBookmark, name:="qqqqqqqqqqqaqq")
                ActiveDocument.Bookmarks("qqqqqqqqqqqaqq").Delete()

                GoToNextFootnote()

            Case "Waiting"  'suspend
                MyTag = "Not Running"
                Rib.Invalidate() ': Stop

                Selection.ClearCharacterDirectFormatting()
                If GetBookmarkExists("relocate_temp") Then ActiveDocument.Bookmarks("relocate_temp").Delete()
        End Select

    End Sub


    Sub GoToNextFootnote()
        Dim i%, aFound As Boolean

        'MyTag = "Processing"
        'Rib.Invalidate ': Stop
        'Stop

        'go back to bookmark
        'On Error Resume Next
        If GetBookmarkExists("relocate_temp") Then
            Selection.GoTo(WdGoToItem.wdGoToBookmark, , , "relocate_temp")
            ActiveDocument.Bookmarks("relocate_temp").Delete()
        End If

        Select Case Err.Number
            Case 0, 4120, 5941   'bookmark doesn't exist
                Err.Clear()
            Case Else

                Msgbox Err.Number & vbNewLine And Err.Description
                Stop
        End Select


        'Goto next footnote after current location:
        'selection.GoTo what:=wdGoToFootnote, Which:=WdGoToDirection.wdGoToNext, Count:=1, Name:=""
        aFound = zzFind("^f", zWildOff, "", aForward)

        If Not aFound Then
            M "Done!"

            MyTag = "Not Running"
            Rib.Invalidate() ': Stop
            '        Stop
            Exit Sub
        End If

        If Selection.Tables.Count > 0 Then
            Clps wdCollapseEnd
            GoToNextFootnote()
        Else

            'Select the next character
            Selection.MoveRight(wdCharacter, 1, wdMove)
            Selection.MoveRight(wdCharacter, 1, wdExtend)

            Select Case Selection.Text
                Case " ", Chr$(13), ")", "(", Chr$(11), Chr$(12), "|"
                    'Good footnote
                    Clps wdCollapseStart
                    GoToNextFootnote()

                Case Else
                    'bad footnote
                    Selection.Collapse WdCollapseDirection.wdCollapseStart
                    'Selection.MoveLeft WdUnits.wdCharacter, 1, WdMovementType.wdMove    'to be safe, we'e going back a few (5) spaces, not just 1.  This solves a bug wherein the bookmark sometimes get deleted upon CutFootnote...
                    Selection.MoveLeft(WdUnits.wdCharacter, 5, WdMovementType.wdMove)    'going back one character works even for double-digit footnote references

                    'add bookmark b4 the footnote:
                    ActiveDocument.Bookmarks.Add(Range:=Selection.Range, name:="relocate_temp")

                    'Select footnote:
                    Selection.MoveRight(WdUnits.wdCharacter, 4, WdMovementType.wdMove)
                    Selection.MoveRight(WdUnits.wdCharacter, 1, WdMovementType.wdExtend)

                    'Make footnote easy to see:
                    Selection.Font.Color = WdColor.wdColorRed
                    For i = 1 To 7 : Selection.Font.Grow() : Next i

                    ''''''''            EnableControls True
                    ''''''''
                    ''''''''            'User can't go again until choosing a selection
                    ''''''''            Me.cmdNext.Enabled = False
                    ''''''''            Me.cmdUndo.Enabled = False

            End Select
        End If

        MyTag = "Waiting"

        Rib.Invalidate() ': Stop

    End Sub

    Sub aStart()
        frmRelocateFootnotes.Show 0
        frmRelocateFootnotes.Tag = "good"
    End Sub
    Sub bStart()
        btnStartStop_Click()
    End Sub
    Sub SelectNextFootnote()    'alt ctrl shift f
        Selection.Find.Execute "^f"
    End Sub

    Sub btnResetRibbon_Click(control As IRibbonControl)
        If Msgbox("Are you sure?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbYes Then
            MyTag = "Not Running"
            Rib.Invalidate()
        End If
    End Sub

    'Callback for customUI.onLoad
    Public Sub RibbonOnLoad(ribbon As IRibbonUI)
        '    Stop

        '    WordBasic.disableautomacros 1

        On Error Resume Next

        Rib = ribbon
        'If you want to run a macro below when you open the workbook
        'you can call the macro like this :
        'Call EnableControlsWithCertainTag3

        MyTag = "Not Running"


        s = Selection

        'WordBasic.disableautomacros 0
    End Sub

    Sub GetEnabledMacro(control As IRibbonControl, ByRef Enabled)
        '    Stop
        Select Case MyTag
            Case "Not Running" ', ""
                If control.ID = "btnStartStop" Then
                    Enabled = True
                Else
                    Enabled = False
                End If
            Case "Waiting"
                If control.ID = "btnUndo" Then
                    Enabled = False
                Else
                    Enabled = True
                End If
            Case "Processing"
                Enabled = False
        End Select

        'We no longer use "line", because it's "off"--on screen can be different than in print.  Instead, always user hard return:
        If control.ID = "btnLine" Then Enabled = False
        'This onse is also never used.  Mrs. Weitzner requested we remove it:  (10-5-2013)
        If control.ID = "btnBackSpace" Then Enabled = False

        'Reset button is alwasys enabled (that's the whole point...):
        If control.ID = "btnReset" Then Enabled = True

    End Sub


    Private Sub btnWord_Click(control As IRibbonControl)
        '    Stop
        MoveToWord(1, Forward)
    End Sub

    Private Sub btnColon_Click(control As IRibbonControl)
        GoToCharacter(":", Afterward)
    End Sub


    Private Sub btnComma_Click(control As IRibbonControl)
        GoToCharacter(",", Afterward)
    End Sub

    Private Sub btnBeforeEmDash_Click(control As IRibbonControl)
        GoToCharacter(" " & ChrW(8211), Before) 'or use Chr(150)
    End Sub

    Private Sub btnHR_Click(control As IRibbonControl)
        GoToCharacter(Chr$(13), Before)
    End Sub

    Private Sub btnLine_Click(control As IRibbonControl)
        CutFootnote()
        Selection.EndKey Unit:=WdUnits.wdLine
        PasteFootnote()
    End Sub

    Private Sub btnPeriod_Click(control As IRibbonControl)
        GoToCharacter(".", Afterward)
    End Sub

    Private Sub btnQuote_Click(control As IRibbonControl)
        GoToCharacter("""", Afterward)
    End Sub

    Sub AAA()
        Dim dtime

        dtime = Now + TimeValue("00:00:02")
        Application.OnTime(dtime, "FindFootnote")

    End Sub
    Sub FindFootnote()
        Dim counter As Integer
        Beep()
        counter = counter + 1
        If counter = 5 Then
            End
        Else
            a()
        End If

    End Sub


    Private Sub btnBackSpace_Click(control As IRibbonControl)
        CutFootnote()
        GoBackOverWhiteSpace()
        PasteFootnote()
    End Sub

    Private Sub btnSentence_Click(control As IRibbonControl)
        GoToCharacter(".", Afterward)
    End Sub


    Private Sub btnAfterParen_Click(control As IRibbonControl)
        GoToCharacter(")", Afterward)
    End Sub

    Private Sub btnBeforeParen_Click(control As IRibbonControl)
        GoToCharacter(")", Before)
    End Sub


    Private Sub btnUndo_Click(control As IRibbonControl)
        Dim i%

        zzFind("^f", zWildOff, "", aBackwards)
        Selection.Cut()
        Selection.GoTo(WdGoToItem.wdGoToBookmark, , , "relocate_temp")
        Selection.Paste()
        Selection.MoveLeft(wdCharacter, 1)

        Selection.MoveRight(wdCharacter, 1, WdMovementType.wdExtend)


        'Make footnote easy to see:
        Selection.Font.Color = WdColor.wdColorRed
        For i = 1 To 7 : Selection.Font.Grow() : Next i

        '    EnableControls True
        '
        '    'User can't go again until choosing a selection
        '    Me.cmdNext.Enabled = False
        '    Me.cmdUndo.Enabled = False
        '
        MyTag = "Waiting"
        Rib.Invalidate() ': Stop

        GoToNextFootnote()


    End Sub

    Sub a()
        M MyTag
    End Sub

    Private Sub btnSkip_Click(control As IRibbonControl)
        MoveToWord(0, Forward)

        '    CutFootnote
        '    ActiveDocument.Bookmarks("relocate_temp").Delete
        '    Selection.PasteAndFormat (WdPasteOptions.wdKeepSourceFormatting)
        '    Selection.TypeText "|"
        '    GoToNextFootnote


    End Sub





    Sub MoveToWordButton(control As IRibbonControl)
        Dim aDirection As direction, num%

        aDirection = IIf(Left(control.ID, 1) = "f", direction.Forward, direction.Back)
        num = Val(Right(control.ID, 1))

        'M num

        'Debug.Print num
        'Stop
        MoveToWord(num, aDirection)

    End Sub


    Public Sub CutFootnote()
        Dim x%
        '    Stop
        MyTag = "Processing"
        Rib.Invalidate() ': Stop

        'Selection.Font.Reset
        'clearformating
        'cleardirectformatting
        Selection.style = "Footnote Reference"
        'For x = 1 To 7: Selection.Font.Shrink: Next x
        'Selection.Font.Color = WdColor.wdColorAutomatic
        Selection.Cut()

        'needed due to bug in Word which randomly adds spaces after cutting:
        Selection.MoveRight(WdUnits.wdCharacter, 1, wdExtend)
        If Selection.Text = " " Then
            'Selection.Delete
            Clps wdCollapseEnd
        Else
            Clps wdCollapseStart
            Selection.TypeText " "
        End If

    End Sub

    Sub PasteFootnote(Optional skip As Boolean = False)

        If Not skip Then
            GoBackOverWhiteSpace()
        Else
            ActiveDocument.Bookmarks("relocate_temp").Delete()
        End If

        Selection.PasteAndFormat(WdPasteOptions.wdKeepSourceFormatting)

        If Not skip Then
            'needed due to bug wherein Word adds a space before pasting characters after punctuation.
            'This bug resulted in an infinite loop, since the space caused this recently fixed footnote to
            'become bad again...
            Selection.MoveLeft(WdUnits.wdCharacter, 1)
            Selection.MoveLeft(WdUnits.wdCharacter, 1, wdExtend)
            If Selection.Text = " " Then
                Selection.Delete()
            End If
        End If

        Clps wdCollapseEnd


        ''''''''    Me.cmdNext.Enabled = True
        ''''''''    Me.cmdUndo.Enabled = True

        'If Not skip Then
        '    Sleep 2000

        'This is used for backup purposes:  (no longer needed--only for remote debugging)
        '    MakeBackup

        GoToNextFootnote()


    End Sub

    'This is used for backup purposes:
    Sub MakeBackup()
        Dim aFileName$
        aFileName = Replace(Replace(Now, ":", "-"), "/", "-") & ".doc"
        'ActiveDocument.SaveAs2 FileName:=aFileName, FileFormat:=wdFormatDocument, CompatibilityMode:=0
        aFileName = Replace(Replace(ActiveDocument.name, ".docx", ""), ".doc", "") & " " & aFileName
        Dim fso As New FileSystemObject
        fso.CopyFile(ActiveDocument.FullName, ActiveDocument.Path & "\Backups\" & aFileName)


    End Sub

    '
    'Sub aaaa()
    ' Dim aFileName$
    '    aFileName = Replace(Replace(Now, ":", "-"), "/", "-") & ".docx"
    ''    ActiveDocument.SaveAs2 FileName:=aFileName, FileFormat:=wdFormatDocument, CompatibilityMode:=0
    '    aFileName = Replace(Replace(ActiveDocument.Name, ".docx", ""), ".doc", "") & " " & aFileName
    '    'D aFileName
    '     Dim fso As New FileSystemObject
    '    fso.CopyFile ActiveDocument.FullName, ActiveDocument.Path & "\Backups\" & aFileName
    '    mDone
    'End Sub

    'returns true if the passed bookmark name exists as a bookmark in the given document, ActiveDocument is default.
Function GetBookmarkExists(BookmarkName$, Optional doc As Word.Document) As Boolean
        Dim bkmrk As bookmark

        If doc Is Nothing Then doc = ActiveDocument

        For Each bkmrk In doc.Bookmarks
            If bkmrk.name = BookmarkName Then
                GetBookmarkExists = True
                Exit Function
            End If
        Next bkmrk

        GetBookmarkExists = False

    End Function





    Public Sub GoBackOverWhiteSpace()

        Do
            'select previous character:
            Selection.MoveLeft(Unit:=WdUnits.wdCharacter, Count:=1, Extend:=WdMovementType.wdExtend)

            'if it's a space then go back another character:
            If Selection.Text = " " Then
                Selection.MoveLeft(Unit:=WdUnits.wdCharacter, Count:=1)
            Else
                Exit Do
            End If
        Loop

        'go back (forwards) to original position:
        Selection.MoveRight(Unit:=WdUnits.wdCharacter, Count:=1)

    End Sub

    Public Sub GoBackOverNonCharacterSpace()

        'Same as previous procedure, except that, besides for checking for spaces, it also checks for soft and hard returns (ascii characters 11 & 13)
        Do
            Selection.MoveLeft(Unit:=WdUnits.wdCharacter, Count:=1, Extend:=WdMovementType.wdExtend)
            If Selection.Text = " " Or Asc(Selection.Text) = 11 Or Asc(Selection.Text) = 13 Then
                Selection.MoveLeft(Unit:=WdUnits.wdCharacter, Count:=1)
            Else
                Exit Do
            End If
        Loop

        Selection.MoveRight(Unit:=WdUnits.wdCharacter, Count:=1)

    End Sub


    Private Sub MoveToWord(ByVal aNumber As Integer, Optional ByVal direction As direction = direction.Forward)
        Dim a%


        CutFootnote()
        'Stop
        'Forward:
        If direction = Forward Then
            'go ahead x number of word
            For a = 1 To aNumber '+ 1

                'go ahead one word at a time:
                Selection.MoveRight(Unit:=WdUnits.wdWord, Count:=1)

                'get the next character:
                Selection.MoveRight(Unit:=WdUnits.wdCharacter, Count:=1, Extend:=WdMovementType.wdExtend)

                'if next character is a punctuation mark then go ahead another "word":
                Select Case Selection.Text
                    Case ".", ",", """", "'", ";", ":", Chr(13), Chr(12)
                        Selection.Collapse(WdCollapseDirection.wdCollapseEnd)
                        Selection.MoveRight(Unit:=WdUnits.wdWord, Count:=1)
                End Select
            Next a
            Selection.Collapse(WdCollapseDirection.wdCollapseStart)

            'Back:
        Else
            For a = 1 To aNumber
                Selection.MoveLeft(Unit:=WdUnits.wdWord, Count:=1)
                Selection.MoveRight(Unit:=WdUnits.wdCharacter, Count:=1, Extend:=WdMovementType.wdExtend)
                Select Case Selection.Text
                    Case ".", ",", """", "'", ";", ":", Chr(13)
                        Selection.Collapse(WdCollapseDirection.wdCollapseStart)
                        Selection.MoveLeft(Unit:=WdUnits.wdWord, Count:=1)
                    Case Else
                        Selection.Collapse(WdCollapseDirection.wdCollapseStart)
                End Select
            Next a
            Selection.Collapse(WdCollapseDirection.wdCollapseEnd)

            Selection.MoveRight(Unit:=WdUnits.wdWord, Count:=1)
            Selection.MoveLeft(Unit:=WdUnits.wdCharacter, Count:=1, Extend:=WdMovementType.wdExtend)
            If Selection.Text = " " Then
                Selection.Collapse(WdCollapseDirection.wdCollapseStart)
            Else
                Selection.Collapse(WdCollapseDirection.wdCollapseEnd)
            End If

        End If

        PasteFootnote Not CBool(aNumber)   'if anumber =0 then skip, so "not anumber" would= true and v.v.

    End Sub





    Private Sub GoToCharacter(ByVal TheCharacters As String, ByVal Afterwards As Afterwards, Optional ByVal zWild As Boolean = False)


        CutFootnote()

        If ayFind(TheCharacters, zWild, False) Then
            If Afterwards Then
                Selection.Collapse(WdCollapseDirection.wdCollapseEnd)
            Else
                Selection.Collapse(WdCollapseDirection.wdCollapseStart)
            End If
        Else
            Msgbox("Can't find!", vbApplicationModal + vbExclamation + vbOKOnly)
            Stop
        End If

        PasteFootnote()

    End Sub




    '2.0:

    Public Sub CreateMoveFootnotesTab()
        Dim myCustomUI As rxCustomUI

        myCustomUI = rxCustomUI.defaultInstance
        With myCustomUI
            ' Clear old state
            .Clear()
            ' Add a new tab
            With .ribbon.Tabs.Add(New rxTab)
                .Label = "My First Tab"
                ' Add a new group to our tab
                With .groups.Add(New rxGroup)
                    .Label = ""
                    ' Add a new button to our group
                    With .Buttons.Add(New rxButton)
                        .Label = "My Button"
                        .ID = "btnStartStop"
                        ' Hook up the control's onAction delegate
                        .OnAction = myCustomUI.make_delegate("MyCallback")
                    End With
                End With
            End With
            ' Render the UI
            .Refresh()
        End With

    End Sub


    Sub MyCallback(ByVal control As DynamicRibbonX.IRibbonControl)
        Msgbox "Hello, world!"
        mDone()

    End Sub


End Module
