Attribute VB_Name = "m_CleanUpInLimbo"
Sub REMOVE_CHARACTERFORMAT_FROM_FOOTNOTES()
Dim x%

    If Not GetIntoFootnotes Then
       Exit Sub
    Else
        For x = 1 To ActiveDocument.Footnotes.Count
            Selection.GoTo What:=wdGoToFootnote, Which:=wdGoToFirst, Count:=x, name:=""
            Selection.Extend Chr(13)
            Selection.MoveLeft WdUnits.wdCharacter, 1, wdExtend
            Selection.ClearCharacterDirectFormatting
            Selection.ClearCharacterDirectFormatting
        Next x
    End If
End Sub


Sub FixKDD()
'Fix KDD that Looks Like Kdd Small
'fix footnote numbers that are messed up by above
'fix footnote text that looks like mokor small

    Selection.Find.ClearFormatting
    Selection.Find.style = ActiveDocument.Styles("KDD")
    Selection.Find.Replacement.ClearFormatting
    Selection.Find.Replacement.style = ActiveDocument.Styles("kdd small")
    With Selection.Find
        .Font.SizeBi = 14
        .Text = ""
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    cAY "^f", "", "", "Footnote Reference", zWildOff, zNoConfirm, zMain
    
    If Not GetIntoFootnotes Then
        Exit Sub
    Else
        Selection.Find.ClearFormatting
        Selection.Find.Font.Color = wdColorRed
        Selection.Find.style = ActiveDocument.Styles("Footnote Text")
        Selection.Find.Replacement.ClearFormatting
        Selection.Find.Replacement.style = ActiveDocument.Styles("mokor small")
        With Selection.Find
            .Text = ""
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = False
        End With
    End If
    
End Sub

Sub AAAHeader()
Dim FLAG1$, FLAG2$, FLAG3$, FLAG4$, Prk$, Mshn$, Mesechta$, Perek$, PerekName$

    FLAG1 = String(6, HE(FINAL_MEM))
    Prk = String(6, HE(SHIN))
    FLAG2 = String(6, HE(FINAL_NUN))
    FLAG3 = String(6, HE(KOOF))
    Mshn = String(6, HE(NUN))
    FLAG4 = String(6, HE(MEM))
    
    Application.Keyboard (1033) 'switches to english
    Mesechta = LatinToHebrew(InputBox$("Enter Mesechta:"))
    Perek = LatinToHebrew(InputBox$("Enter Perek Number:"))
    PerekName = LatinToHebrew(InputBox$("Enter Perek Name:"))
    Application.Keyboard (1037) 'switches to hebrew
    cAY "", FLAG1 & "^p", "MISHNA TITLE", "", zWildOff, zNoConfirm, zMain
   
    cAY FLAG1, FLAG1 & Prk & FLAG2 & FLAG3 & Mshn & FLAG4, "", "", zWildOff, zNoConfirm, zMain
    cAY FLAG1, "", "", "Large Head", zWildOff, zNoConfirm, zMain
    cAY Prk, "", "", "Small Head", zWildOff, zNoConfirm, zMain
    cAY FLAG2, "", "", "Large Head", zWildOff, zNoConfirm, zMain
    cAY FLAG3, "", "", "Small Head", zWildOff, zNoConfirm, zMain
    cAY Mshn, "", "", "Small Head", zWildOff, zNoConfirm, zMain
    cAY FLAG4, "", "", "Large Head", zWildOff, zNoConfirm, zMain

    cAY FLAG1, Mesechta & " ", "", "", zWildOff, zNoConfirm, zMain
    cAY Prk, HE(PEY) & HE(REISH) & HE(KOOF) & " ", "", "", zWildOff, zNoConfirm, zMain
    cAY FLAG2, Perek & " ", "", "", zWildOff, zNoConfirm, zMain
    cAY FLAG3, "(" & PerekName & ") ", "", "", zWildOff, zNoConfirm, zMain
    cAY Mshn, HE(MEM) & HE(SHIN) & HE(NUN) & HE(HEY) & " ", "", "", zWildOff, zNoConfirm, zMain
    
     
    Selection.HomeKey Unit:=wdStory

    Do While zzFind(FLAG4, zWildOff, "")
        Selection.Collapse wdCollapseEnd
        Selection.Fields.Add Range:=Selection.Range, Type:=wdFieldEmpty, Text:= _
        "SECTION  \* hebrew1 ", PreserveFormatting:=True
    Loop

    cAY FLAG4, "", "", "", zWildOff, zNoConfirm, zMain

    Selection.WholeStory
    Selection.Fields.Unlink
    Selection.Collapse wdCollapseStart
    
    
End Sub

Sub CleanParshata() 'there is a long obsolete verion in obsolete
    cAY HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & HE(ALEF) & " " & HE(ALEF) & " ", _
            HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) _
             & HE(ALEF) & " " & HE(ALEF) & "' ", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
    
    cAY HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & ChrW(ALEF _
            ) & " " & HE(BEIS) & " ", _
        HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) _
             & HE(ALEF) & " " & HE(BEIS) & "' ", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
    
    cAY HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & ChrW(ALEF _
            ) & " " & HE(GIMEL) & " ", _
        HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) _
             & HE(ALEF) & " " & HE(GIMEL) & "' ", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
 
     cAY HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & ChrW(ALEF _
            ) & " " & HE(DALET) & " ", _
        .Replacement.Text = HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) _
             & HE(ALEF) & " " & HE(DALET) & "' ", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
        
    cAY HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & ChrW(ALEF _
            ) & " " & HE(HEY) & " ", _
        HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) _
             & HE(ALEF) & " " & HE(HEY) & "' ", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
    
    cAY HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & ChrW(ALEF _
            ) & " " & HE(vav) & " ", _
        HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) _
             & HE(ALEF) & " " & HE(vav) & "' ", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
   
     cAY HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & ChrW(ALEF _
            ) & " " & HE(ZAYIN) & " ", _
        HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) _
             & HE(ALEF) & " " & HE(ZAYIN) & "' ", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes

     cAY HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & ChrW(ALEF _
            ) & " " & HE(CHES) & " ", _
        HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) _
             & HE(ALEF) & " " & HE(CHES) & "' ", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
        
     cAY HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & ChrW(ALEF _
            ) & " " & HE(TES) & " ", _
        HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) _
             & HE(ALEF) & " " & HE(TES) & "' ", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
       
      cAY HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) & ChrW(ALEF _
            ) & " " & HE(YUD) & " ", _
        HE(PEY) & HE(REISH) & HE(SHIN) & HE(TAV) _
             & HE(ALEF) & " " & HE(YUD) & "' ", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
End Sub



Sub ClearDirectFormat(WholeDocument As Boolean, Optional zClearCharacterStyle As Boolean = False)
    
    If WholeDocument Then Selection.WholeStory  'else, just for current location
    
    Selection.ClearParagraphDirectFormatting
    Selection.ClearParagraphDirectFormatting
    Selection.ClearCharacterDirectFormatting
    Selection.ClearCharacterDirectFormatting
    If zClearCharacterStyle Then
        
        Selection.style = ActiveDocument.Styles("mokor small") 'Puts on Dumb Char code, if not next line can bomb
        Selection.clearcharacterstyle
    End If
End Sub




'--old:



Sub AAATouchUpAtStart()
'Dim zstyle As String
    'consider running AAASemiAutomateTheCleanupOf_UHOH_AAAAA by hand, one at a time.
    
    

    NTRG    'partially cleans up NTRG & ATRG
    
    CleanUpNumbers
    CleanUpOtherStuff
    
    CleanStyles
    RemoveAllWrongCharacterStylesFromKDD
    
    CleanParshata
    
'    zstyle = "KDD SPACE ON TOP"
'    If StyleExist(zstyle) = True Then FIX_KDD_SPACE_ON_TOP
    
    CleanPunctuation
    
        
    cAY "*", "", "talmud aleph", "", zWildon, zNoConfirm, zMain     'Delete Talmud Aleph
    cAY Chr(9) & Chr(9), "", "TALMUD", "", zWildOff, zNoConfirm, zMain  'Remove tabs from Talmud
    
'    RemoveTalmudAlephsinTalmud 'works but is dangerous only use if they are no longer fields
    
    ReplaceRosheiTeivos ' in footnotes
    SetMokorSmallforCertainWords
    FIX_COMMA_STYLE_In_Footnotes
    
    ClearCharacterDirectFormattingForAllFootnotesInDocument
    
    
    
 
    ' AAAPutMishnaNumberAfterSharHatalmud   ' only run if it is one perek
    
    ClearDirectFormat True, False
    EachMishnaNewSection
    'DeleteBavli 'we will not be using this for now; instead, we will be moving "Bavli" by hand to another place in the document.
    'Macro10 'don't know what this is   'last part bombs
    ConvertCommentaryIntoKDD_Small
    
    GetNameOfMesechta   'Calls "RemoveTalmudHakdama"
    'TalmudHardReturns  'we're not sure if we need these
    'TalmudSoftReturns
    
    mDone
End Sub







Sub CleanUpOtherStuff()
Dim f As Field
Dim b As bookmark

    For Each f In ActiveDocument.Fields 'DeleteAllCrossReferences
    If f.Type = wdFieldRef Then
            f.Delete
        End If
    Next f

    For Each b In ActiveDocument.Bookmarks 'Delete all bookmarks
        b.Delete
    Next b

    For Each f In ActiveDocument.Fields
        If f.Type = wdFieldSequence Then 'Delete all Seq fields
            f.Delete
        Else
            f.Select
            Stop
        End If
    Next f
End Sub














Sub Macro10()
Attribute Macro10.VB_ProcData.VB_Invoke_Func = "Normal.m_CleanUpInLimbo.Macro10"
    ColoRize
        Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = "(^12)(*)(SHAREND)"
        .Replacement.Text = "\2\1\3"
        .Forward = True
        .Wrap = wdFindContinue
        .MatchWildcards = True
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    Selection.Find.Replacement.style = ActiveDocument.Styles("TALMUD")
    With Selection.Find
        .Text = "555*SHAREND"
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchWildcards = True
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    Selection.Find.Replacement.style = ActiveDocument.Styles("SECTION TITLE")
    With Selection.Find
        .Text = "555"
        .Replacement.Text = HE(SHIN) & HE(AYIN) & HE(REISH) & " " & ChrW _
            (HEY) & HE(TAV) & HE(LAMED) & HE(MEM) & HE(vav) & ChrW( _
            DALET) & "555"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll

    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = "555^p666^p777^p888^p"
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = "SHAREND^p"
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
     Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = "999"
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
'    This bombs
'    Selection.Find.ClearFormatting
'    Selection.Find.Replacement.ClearFormatting
'    With Selection.Find
'        .Text = "@^13"
'        .Replacement.Text = "^p"
'        .Forward = True
'        .Wrap = wdFindContinue
'        .MatchWildcards = True
'    End With
'    Selection.Find.Execute Replace:=wdReplaceAll
    
End Sub


Sub TouchUpOlder()
    
    'Mishna
    cAY "^p", ":^&", "MISHNA", "", zzWildOff, zNoConfirm, zMain
    cAY "::", ":", "", "", zzWildOff, zNoConfirm, zMain
    cAY ChrW(SOF_PASUK) & ":", ":", "", "", zzWildOff, zNoConfirm, zMain
    
    'Talmud
    zFoldStyle "HALACHA", "shar bold"
    CleanTalmudFields
    
    'Sheorim
    zFoldStyle "SHAAR HEAD", "shar bold"
 
    'KDD
    zFoldStyle "KOL", "kdd small"
    Normal2KDD
    cAY ":", "", "KDD MACHLOKES", "", zzWildOff, zConfirm, zMain
    '    zReplaceInStyle "KDD MACHLOKES", ":", "", False
    Do While zFindStyle("KDD Breathing Space")
        Selection.Delete Unit:=wdCharacter, Count:=1
        Selection.style = "KDD SPACE ON TOP"
    Loop
    
'    zReplace "^=", " ^= ", False 'Puts space around n-dash
'    zReplace " ^=^p", "^p", False 'Remove n dash at end of line in KDD
'    zReplace " ^= ^p", "^p", False
'    zReplace " ^=^l", ".^l", False 'Remove n dash at end of line in Shaar
    
'    cAY "^=", " ^= ", "", "", zzWildOff, zNoConfirm, zMainFootnotesAndEndnotes 'Puts space around n-dash
'    cAY " ^=^p", "^p", "", "", zzWildOff, zConfirm, zMain 'Remove n dash at end of line in KDD
'    cAY " ^= ^p", "^p", "", "", zzWildOff, zConfirm, zMain
'    cAY " ^=^l", ".^l", "", "", zzWildOff, zConfirm, zMain 'Remove n dash at end of line in Shaar
    
    cAY " {2,}", " ", "", "", zzWildOn, zNoConfirm, zMainFootnotesAndEndnotes 'Remove multiple space
    cAY "([ ^013^11]" & HE(ALEF) & HE(vav) & "),", "\1", "", "", zzWildOn, zConfirm, zMainFootnotesAndEndnotes 'Remove comma after OH
    
'    zReplaceStyleofText "^f", "Footnote Reference", False 'Cleans up all footnote references ONLY RUN WHEN THEY ARE IN RIGHT PLACE

    CleanPunctuation
       
    'ay lParts " ^p", "^p", False Kills pictures by accident
    
    KillAllIndents
 '   ClearDirectFormat True
    MakeAllIndents
    
    TouchUpFootnotes
    CleanTalmudAleph
End Sub

Sub aCleanup()
    Selection.HomeKey Unit:=wdStory
    
    'Mishna
    cAY "^p", ":^&", "MISHNA", "", zzWildOff, zNoConfirm, zMain
    cAY "::", ":", "", "", zzWildOff, zNoConfirm, zMain
    
    'Rav
    zFoldStyle "rav-heading", "rav bold"
    zFoldStyle "Rav-Heading", "rav bold"
    
    'Talmud
    zFoldStyle "HALACHA", "shar bold"
    CleanTalmudFields
    
    'Sheorim
    zFoldStyle "SHAAR HEAD", "shar bold"
 
    'KDD
    zFoldStyle "KOL", "kdd small"
    Normal2KDD
    cAY ":", "", "KDD MACHLOKES", "", zzWildOff, zNoConfirm, zMain
'    zReplaceInStyle "KDD MACHLOKES", ":", "", False
    Do While zFindStyle("KDD Breathing Space")
        Selection.Delete Unit:=wdCharacter, Count:=1
        Selection.style = "KDD SPACE ON TOP"
    Loop
    
    zReplace "^=", " ^= ", False 'Puts space around n-dash
    zReplace " ^=^p", "^p", False 'Remove n dash at end of line in KDD
    zReplace " ^= ^p", "^p", False
    zReplace " ^=^l", ".^l", False 'Remove n dash at end of line in Shaar
      zReplaceInAllParts "([ ^013^11]" & HE(ALEF) & HE(vav) & "),", "\1", True 'Remove comma after OH
    
    zReplaceStyleofText "^f", "Footnote Reference", False 'Cleans up all footnote references ONLY RUN WHNE THEY ARE IN RIGHT PLACE

    CleanPunctuation
       
    'ay lParts " ^p", "^p", False Kills pictures by accident
    
    KillAllIndents
    ClearDirectFormat True
    MakeAllIndents
    
    AutoMoveFootnotesToEndOfCertainPhrases
End Sub


Sub FixOldRefCodes()
    ActiveWindow.View.ShowFieldCodes = Not ActiveWindow.View.ShowFieldCodes
    zReplace "^d REF", "ZZ", False
    zReplace ". ZZ", ".ZZ", False
    zReplace ", ZZ", ",ZZ", False
    zReplace "( ZZ", "(ZZ", False
    zReplace ") ZZ", ")ZZ", False
    ActiveWindow.View.ShowFieldCodes = Not ActiveWindow.View.ShowFieldCodes
    
    zReplaceInStyle "TALMUD", "", "AYAY^&", False
    zReplaceInStyle "TALMUD", "^p", "^&AYAY", False
    zReplaceInStyle "TALMUD", "AYAY", "AXAX", False
    zReplace "AYAY", "", False
        
    zFind "AXAX", False, True
    Do While Selection.Find.found
        Selection.Delete Unit:=wdCharacter, Count:=1
        AInsertParNum
        MakeSharHead
        zFind "AXAX", False
    Loop
    Part2
End Sub
Sub Part2()
    zReplace " ^t", "t", True
    zFind "^t*^t", True, True
    Do While Selection.Find.found
        Selection.HomeKey Unit:=wdLine
        Selection.Extend
        Selection.MoveRight Unit:=wdCharacter, Count:=3
        Selection.Font.Reset
        Selection.MoveLeft Unit:=wdCharacter, Count:=1
        Selection.MoveRight Unit:=wdCharacter, Count:=1
        Selection.Extend
        Selection.MoveRight Unit:=wdCharacter, Count:=1
        Selection.style = ActiveDocument.Styles("talmud aleph")
        Selection.EndKey Unit:=wdLine
        zFind "^t*^t", True
    Loop
End Sub
Sub FixStyleofAleph()
' Pull out space at begining of each Mishna
'Put at begining of set and keep hitting
    Selection.HomeKey Unit:=wdLine
    Selection.Extend
    Selection.MoveRight Unit:=wdCharacter, Count:=3
    Selection.Font.Reset
    Selection.MoveLeft Unit:=wdCharacter, Count:=1
    Selection.MoveRight Unit:=wdCharacter, Count:=1
    Selection.Extend
    Selection.MoveRight Unit:=wdCharacter, Count:=1
    Selection.style = ActiveDocument.Styles("talmud aleph")
    Selection.Find.ClearFormatting
    Selection.Collapse
    With Selection.Find
        .Text = "^p"
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindAsk
    End With
    Selection.Find.Execute
    Selection.MoveRight Unit:=wdCharacter, Count:=1
End Sub


