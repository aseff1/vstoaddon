'Attribute VB_Name = "RunOnAllFilesInFolder"
Option Explicit

Sub RunMacro(SourceFolder As String, SaveFolder As String, FileSpec As String, MacroName As String)
' Example: RunMacro "c:\zvi\filefolder\", "c:\zvi\savefolder\", "*.docx", "NewMacros.TestMacro"
Dim SourceFile As String
    If Right(SourceFolder, 1) <> "\" Then SourceFolder = SourceFolder & "\" ' test for "\" at end of dir name
    If Right(SaveFolder, 1) <> "\" Then SaveFolder = SaveFolder & "\"
    SourceFile = Dir(SourceFolder & FileSpec)
    
    Do
       If SourceFile = "" Then Exit Do
       Documents.Open SourceFolder & SourceFile
       Application.Run MacroName
       ActiveDocument.SaveAs SaveFolder & SourceFile
       Documents.Close
       SourceFile = Dir()
    Loop
    mDone
End Sub

Sub arun() 'sample use of this
'RunMacro "C:\Users\" & Environ("username") & "\Desktop\1", "C:\Users\HDavis\Desktop\2", "*.docX", "AAAAQuickCleanup"
RunMacro "C:\Users\" & Environ("username") & "\Desktop\1", "C:\Users\" & Environ("username") & "\Desktop\2", "*.docX", "AAAAQuickCleanUp"
End Sub


''
''Public Sub MasterMacro()
''Dim Directory As String
''Dim FType As String
''Dim FName As String
''
''
''    Directory = "C:\Users\" & Environ("username") & "\Desktop\Temp"   '<--change this
''  '  FType = "*.*"   '"*docx"
''     FType = "*.docx"
''    ChDir Directory
''    FName = Dir(FType)
''    ' for each file you find, run this loop
''    Do While FName <> ""
''        ' open the file
''        Documents.Open FileName:=FName
''
'''Put code here:
''        AUniversalCleanup
''        ' save and close the current document
''        ActiveDocument.Close wdSaveChanges
''
''        ' look for next matching file
''        FName = Dir
''    Loop
''End Sub
