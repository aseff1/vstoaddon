﻿Module GeneralHeaders

    Sub LinkHeaders() ' Link all headers to first
        Dim J As Integer
        Dim K As Integer
        '    If ActiveDocument.Sections.Count > 2 Then
        For J = 2 To ActiveDocument.Sections.Count
            For K = 1 To ActiveDocument.Sections(J).Headers.Count
                ActiveDocument.Sections(J).Headers(K).LinkToPrevious = True
            Next K
            '            For K = 1 To ActiveDocument.Sections(J).Footers.Count
            '                ActiveDocument.Sections(J).Footers(K).LinkToPrevious = True
            '           Next K
        Next J
        '    End If
    End Sub
    Sub UnLinkHeaders()
        Dim J As Integer
        Dim K As Integer
        '    If ActiveDocument.Sections.Count > 2 Then
        For J = 2 To ActiveDocument.Sections.Count
            For K = 1 To ActiveDocument.Sections(J).Headers.Count
                ActiveDocument.Sections(J).Headers(K).LinkToPrevious = False
            Next K
            '            For K = 1 To ActiveDocument.Sections(J).Footers.Count
            '                ActiveDocument.Sections(J).Footers(K).LinkToPrevious = True
            '           Next K
        Next J
        '    End If
    End Sub
    Sub RemoveHeaders()
        Dim oSec As Section
        Dim oHead As HeaderFooter
        '    Dim oFoot As HeaderFooter

        For Each oSec In ActiveDocument.Sections
            For Each oHead In oSec.Headers
                If oHead.Exists Then oHead.Range.Delete()
            Next oHead

            '        For Each oFoot In oSec.Footers
            '            If oFoot.Exists Then oFoot.Range.Delete
            '        Next oFoot
        Next oSec
    End Sub

End Module
