Module WP2WordConvert
    Sub Highlight()
        Dim zstyle As String

        Options.DefaultHighlightColorIndex = wdBrightGreen
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        Selection.Find.Replacement.Highlight = True
        With Selection.Find
            .Text = "ATRG*ATRG"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchAllWordForms = False
            .MatchSoundsLike = False
            .MatchWildcards = True
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        Options.DefaultHighlightColorIndex = wdRed
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        Selection.Find.Replacement.Highlight = True
        With Selection.Find
            .Text = "NTRG*NTRG"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchAllWordForms = False
            .MatchSoundsLike = False
            .MatchWildcards = True
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = "RRR"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        With Selection.Find
            .Text = "ATRG"
            .Replacement.Text = " "
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        With Selection.Find
            .Text = "NTRG"
            .Replacement.Text = " "
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = "111*444"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchAllWordForms = False
            .MatchSoundsLike = False
            .MatchWildcards = True
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = "1HEAD1"
            .Replacement.Text = "^m"
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll


        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = "555^p666^p777^p888^p999"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        zstyle = "H_chidush"
        If StyleExist(zstyle) = True Then

            Options.DefaultHighlightColorIndex = wdPink
            Selection.Find.ClearFormatting()
            Selection.Find.style = ActiveDocument.Styles("H_chidush")
            Selection.Find.Replacement.ClearFormatting()
            Selection.Find.Replacement.Highlight = True
            With Selection.Find
                .Text = ""
                .Replacement.Text = ""
                .Forward = True
                .Wrap = wdFindContinue
                .Format = True
                .MatchCase = False
                .MatchWholeWord = False
                .MatchKashida = False
                .MatchDiacritics = False
                .MatchAlefHamza = False
                .MatchControl = False
                .MatchWildcards = False
                .MatchSoundsLike = False
                .MatchAllWordForms = False
            End With
            Selection.Find.Execute Replace:=wdReplaceAll

        End If

        zstyle = "Posuk"
        If StyleExist(zstyle) = True Then
            Options.DefaultHighlightColorIndex = wdYellow
            Selection.Find.ClearFormatting()
            Selection.Find.style = ActiveDocument.Styles("Posuk")
            Selection.Find.Replacement.ClearFormatting()
            Selection.Find.Replacement.Highlight = True
            With Selection.Find
                .Text = ""
                .Replacement.Text = ""
                .Forward = True
                .Wrap = wdFindContinue
                .Format = True
                .MatchCase = False
                .MatchWholeWord = False
                .MatchKashida = False
                .MatchDiacritics = False
                .MatchAlefHamza = False
                .MatchControl = False
                .MatchWildcards = False
                .MatchSoundsLike = False
                .MatchAllWordForms = False
            End With
            Selection.Find.Execute Replace:=wdReplaceAll
        End If



        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = "^p^p"
            .Replacement.Text = "^p"
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.Find.Execute Replace:=wdReplaceAll
    End Sub

    Sub InsertChartFootnotes()
        Dim str As String
        zReplaceInStyle("z#Talmud", "", "REF^&REF", False, True)
        zReplaceInStyle("z#Rav", "", "REF^&REF", False, True)
        zReplaceInStyle("z#Mishna", "", "REF^&REF", False, True)
        zReplaceInStyle("z#RefBig", "", "REF^&REF", False, True)
        zReplaceInStyle("zalephMishna", "", "REF^&REF", False, True)
        zReplace("(NTRG[0-9]@NTRG*)(^013)", "\1AAAAA\2", True, True)

        zReplace("(1HEAD1)(*)(2HEAD2)(*)(3HEAD3)", "\4 \2", True)

        MoveSection("DROSHOSTART", "SHAREND", "888", "999")
        MoveSection("SHARSTART", "SHAREND", "666", "777")
Loop1:
        If Not zFind("NTRG[0-9]@NTRG", True, True) Then GoTo Loop2
        Selection.Copy()
        str = GetFromClipboard
        str = Replace(str, "NTRG", "")
        If Not zFind("REF" & str & "REF", False, True) Then
            zReplace("NTRG" & str & "NTRG", "UHOH" & str, False)
            GoTo Loop1
        End If
        If Not zFind("NTRG" & str & "NTRG*^013", True, True) Then
            zReplace("NTRG" & str & "NTRG", "UHOH" & str, False)
            GoTo Loop1
        End If
        Selection.Cut()
        zFind("REF" & str & "REF", False, True)
        ActiveDocument.Footnotes.Add Range:=Selection.Range
        Selection.Paste()
        zReplace("AAAAA^013", "", False, False)
        zReplace("NTRG" & str & "NTRG", "", False, False)
        Selection.GoTo(What:=wdGoToPage, Which:=wdGoToNext, name:="1")
        zReplace("REF" & str & "REF", "", False, True)
        GoTo Loop1
Loop2:
        If Not zFind("ATRG*ATRG", True, True) Then GoTo EndLoop2
        Selection.Copy()
        str = GetFromClipboard
        str = Replace(str, "ATRG", "")
        If Not zFind("REF" & str & "REF", False, True) Then
            zReplace("ATRG" & str & "ATRG", "UHOH" & str, False)
            GoTo Loop2
        End If
        Selection.TypeText "ZZ"
        zFind("ATRG" & str & "ATRG", False, True)
        Selection.Delete()
        FastInsertParNum()
        GoTo Loop2
EndLoop2:
    End Sub
    Sub ChartConvert()
        If ActiveWindow.View.SplitSpecial = wdPaneNone Then
            ActiveWindow.ActivePane.View.Type = wdPrintView
        Else
            ActiveWindow.View.Type = wdPrintView
        End If

        UpdateTemplate()

        Cleanup()

        ConvertNumbers()
        FixTables()
        Posuk()

        InsertChartFootnotes()

        FormatSections()
        ClearDirectFormat, True
        zReplaceInStyle("TALMUD", "RRRRRR", "RRR", False)
        zReplaceInStyle("DROSHOS", "RRRRRR", "RRR", False)
        zReplaceInStyle("TALMUD", "^p[RRR]", "^l", False) 'Turns the RRR into a line break
        zReplaceInStyle("DROSHOS", "^p[RRR]", "^l", False) 'Must be before RRR to Indent
        RRR2Indent()

        zReplaceStyleofText("([1-9])\1\1", "JUNK", True)
        zReplace("MOKORSTART[^013]@SHAREND", "", True)
        CleanSpaceHrt()

        Selection.WholeStory()
        Selection.Fields.Update()
        Selection.HomeKey Unit:=wdStory

        UnfreezeScreen()
        '    MsgBox "Z Finished"
    End Sub
    Sub AConvert()
        Dim StartPerek As Integer
        If ActiveWindow.View.SplitSpecial = wdPaneNone Then
            ActiveWindow.ActivePane.View.Type = wdPrintView
        Else
            ActiveWindow.View.Type = wdPrintView
        End If
        '    StartPerek = InputBox("Starting Perek #","", 1)
        StartPerek = 1
        '    FreezeScreen 'speed up macro

        UpdateTemplate()

        Cleanup()

        ConvertNumbers()
        Ravtitle()
        FixTables()
        Posuk()


        BreakAt999() 'Section break
        MishnaAndPerekNumber(StartPerek) ' careful this locks all fields, must be after update template
        TalmudresetFieldAt00000()

        InsertFootnotes()
        ParseMishna()

        FormatSections()
        ClearDirectFormat, True
        zReplaceInStyle("TALMUD", "RRRRRR", "RRR", False)
        zReplaceInStyle("DROSHOS", "RRRRRR", "RRR", False)
        zReplaceInStyle("TALMUD", "^p[RRR]", "^l", False) 'Turns the RRR into a line break
        zReplaceInStyle("DROSHOS", "^p[RRR]", "^l", False) 'Must be before RRR to Indent
        RRR2Indent()

        zReplaceStyleofText("([1-9])\1\1", "JUNK", True)
        zReplaceStyleofText("00000", "JUNK", True)
        zReplace("MOKORSTART[^013]@SHAREND", "", True)
        CleanSpaceHrt()

        Selection.WholeStory()
        Selection.Fields.Update()
        Selection.HomeKey Unit:=wdStory

        UnfreezeScreen()
        '    MsgBox "Z Finished"
    End Sub
    Sub InsertFootnotes()
        Dim str As String
        zReplaceInStyle("z#Talmud", "", "REF^&REF", False, True)
        zReplaceInStyle("z#Rav", "", "REF^&REF", False, True)
        zReplaceInStyle("z#Mishna", "", "REF^&REF", False, True)
        zReplaceInStyle("z#RefBig", "", "REF^&REF", False, True)
        zReplaceInStyle("zalephMishna", "", "REF^&REF", False, True)
        zReplace("(NTRG[0-9]@NTRG*)(^013)", "\1AAAAA\2", True, True)

        zReplace("00000", "JJJJJ", False)
        zReplace("999", "KKKKK", False)

Loop0:
        If Not zFind("JJJJJ*KKKKK", True, True) Then Exit Sub 'This is a Mishna
        Selection.Cut()
        Documents.Add DocumentType:=wdNewBlankDocument
        Selection.Paste()
        zReplace("JJJJJ", "00000", False)
        zReplace("KKKKK", "999", False)
        zReplace("(1HEAD1)(*)(2HEAD2)(*)(3HEAD3)", "\4 \2", True)

        MoveSection("DROSHOSTART", "SHAREND", "888", "999")
        MoveSection("SHARSTART", "SHAREND", "666", "777")
Loop1:
        If Not zFind("NTRG[0-9]@NTRG", True, True) Then GoTo Loop2
        Selection.Copy()
        str = GetFromClipboard
        str = Replace(str, "NTRG", "")
        If Not zFind("REF" & str & "REF", False, True) Then
            zReplace("NTRG" & str & "NTRG", "UHOH" & str, False)
            GoTo Loop1
        End If
        If Not zFind("NTRG" & str & "NTRG*^013", True, True) Then
            zReplace("NTRG" & str & "NTRG", "UHOH" & str, False)
            GoTo Loop1
        End If
        Selection.Cut()
        zFind("REF" & str & "REF", False, True)
        ActiveDocument.Footnotes.Add Range:=Selection.Range
        Selection.Paste()
        zReplace("AAAAA^013", "", False, False)
        zReplace("NTRG" & str & "NTRG", "", False, False)
        Selection.GoTo(What:=wdGoToPage, Which:=wdGoToNext, name:="1")
        zReplace("REF" & str & "REF", "", False, True)
        GoTo Loop1
Loop2:
        If Not zFind("ATRG*ATRG", True, True) Then GoTo EndLoop2
        Selection.Copy()
        str = GetFromClipboard
        str = Replace(str, "ATRG", "")
        If Not zFind("REF" & str & "REF", False, True) Then
            zReplace("ATRG" & str & "ATRG", "UHOH" & str, False)
            GoTo Loop2
        End If
        Selection.TypeText "ZZ"
        zFind("ATRG" & str & "ATRG", False, True)
        Selection.Delete()
        FastInsertParNum()
        GoTo Loop2
EndLoop2:
        Selection.WholeStory()
        Selection.Cut()
        ActiveWindow.Close(Word.WdSaveOptions.wdDoNotSaveChanges)
        Selection.Paste()
        GoTo Loop0
    End Sub
    Sub MoveSection(MarkerStart As String, MarkerEnd As String, NumStart As String, NumEnd As String)
        zFind(MarkerStart & "*" & MarkerEnd, True, True)
        Do While Selection.Find.found
            Selection.Cut()
            zFind(NumEnd, False)
            Selection.MoveUp(Unit:=wdLine, Count:=1)
            Selection.EndKey Unit:=wdLine
            Selection.TypeParagraph()
            Selection.Paste()
            zReplace("(" & NumStart & "*)(" & MarkerStart & ")(*)(" & MarkerEnd & ")", "\1\3", True)
            zFind(MarkerStart & "*" & MarkerEnd, True, True)
        Loop
    End Sub
    Sub FastInsertParNum()
        Dim bookmark As String
        Dim PrevField
        bookmark = UniqueBookmark(True)

        Selection.TypeText Text:=vbTab
        Selection.Fields.Add(Range:=Selection.Range, Type:=wdFieldEmpty, Text:= _
            "SEQ Talmud\* hebrew1")
        With Selection
            PrevField = .GoToPrevious(What:=wdGoToField)
            .MoveRight(Unit:=wdWord, Extend:=wdExtend)
            .Fields(1).Select()
        End With
        With ActiveDocument.Bookmarks
            .Add(Range:=Selection.Range, name:=bookmark)
            .DefaultSorting = wdSortByName
        End With
        Selection.MoveRight(Unit:=wdCharacter, Count:=1)
        Selection.TypeText Text:=vbTab

        zFind("ZZ", False, True)
        Selection.InsertCrossReference(ReferenceType:="Bookmark", ReferenceKind:= _
            wdContentText, ReferenceItem:=bookmark, InsertAsHyperlink:=True, _
            IncludePosition:=False, SeparateNumbers:=False, SeparatorString:=" ")
    End Sub
    Sub Cleanup()
        zReplaceInStyle("zero", "*", "", True, True) ' kill pair style and content
        zReplaceInStyle("zmove #", "*", "", True, True)
        zReplaceInStyle("zmove aleph", "*", "", True, True)
        zReplaceInStyle("zmove drosho", "*", "", True, True)
        zReplaceInStyle("zalephdrasha", "*", "^013", True, True)
        zReplaceInStyle("zalephdrosho", "*", "^013", True, True)
        zReplaceInStyle("zdroshoMishn", "*", "", True, True)
        zReplaceInStyle("H-Mishna #", "*", "", True, True)
        zReplaceInStyle("H-Hemshech M", "*", "", True, True)
        zReplaceInStyle("zMishna Box", "*", "", True, True)
        zReplaceInStyle("zMishna1Box", "*", "", True, True)

        zReplaceInStyle("Real Brackets", "", "[^&]", False, True) 'kill pair style, modify content
        zReplaceInStyle("H_Chidush", "", "{^&}", False, True)
        zReplaceInStyle("Hakdama", "", HE(HEY) & HE(KOOF) & HE(DALET) & HE(MEM) & HE(HEY) & ": ^&^p", False, True)

        zFoldStyle("1tiny", "kdd small", True) 'update pair styles
        zFoldStyle("0small", "kdd small", True)
        zFoldStyle("KOL", "kdd small", True)
        zFoldStyle("1H-small", "mokor small", True)
        zFoldStyle("Rav-Heading", "rav bold", True)
        zFoldStyle("H-Hagdara", "shar bold", True)
        zFoldStyle("Hagdara", "shar bold", True)
        zFoldStyle("H-Mokoros", "shar bold", True)

        zReplaceInStyle("rav bold", "", "^&: ", False)
        zReplaceInStyle("shar bold", "", "^&:", False)

        zReplace("333.", ".333", False)
        zReplace("^p333", "333", False)
        zReplace("^p555.", ".^p555", False)
        zReplace(".", ". ", False)
        zReplace("^+", "^=", False) 'Replace m-dash with n-dash
    End Sub

End Module
