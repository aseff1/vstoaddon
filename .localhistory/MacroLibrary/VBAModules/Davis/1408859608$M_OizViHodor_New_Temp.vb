'Attribute VB_Name = "M_OizViHodor_New_Temp"
Option Explicit
Sub Temp_Remove_Period_At_End_Of_Footnote()
Dim f As Footnote
    
    For Each f In ActiveDocument.Footnotes
        If Right(f.Range.Text, 1) = "." Then
            f.Range.Select
            Selection.Collapse WdCollapseDirection.wdCollapseEnd
            Selection.TypeBackspace
            '.Text = Left(f.Range.Text, Len(f.Range.Text) - 1)
        End If
    Next f
    
End Sub
Sub Temp_CLEAN_TAG_MARKS()
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    Selection.Find.Replacement.style = ActiveDocument.Styles("mokor small")
    With Selection.Find
        .Text = "(<" & ChrW(1502) & ChrW(1502) & ">)(*)(<" & ChrW(1488) & _
            ChrW(1502) & ChrW(1502) & ">)"
        .Replacement.Text = "\2"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchWildcards = True
    End With
    Selection.Find.Execute Replace:=wdReplaceAll

    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = "<>"
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
End Sub
Sub DoesntWorkConvertFootnoteToShaarSmall()   'Alt + U  (for "Up")
Dim fn As Integer

    'GetOutOfFootnotes
    With Selection
        
        If .Information(wdInFootnote) = False Then
            zzFind "^f", zWildOff, ""
            fn = .Footnotes(1).index
            GoHome
            GoIntoFootnotes
            .GoTo What:=wdGoToFootnote, Which:=wdGoToFirst, Count:=fn, name:=""
        End If
        
    'Cursor must already be in a footnote:
    
        fn = .Footnotes(1).index
        .Footnotes(1).Range.Cut
        
        GoHome
        .GoTo What:=wdGoToFootnote, Which:=wdGoToFirst, Count:=fn, name:=""
        .TypeText " ("
        .Paste
        Selection.MoveLeft WdUnits.wdCharacter, 1, WdMovementType.wdExtend
        If Selection.Text = " " Then Selection.Delete
        Clps wdCollapseEnd
        
        .TypeText ")"
        .MoveLeft WdUnits.wdCharacter, 1
        GetStuffInParentheses
        .style = "shaar small"
        Clps wdCollapseEnd
        zzFind "^f", zWildOff, ""
        .Delete
    End With
End Sub
