﻿Module Halacha
    Option Explicit
    Sub H_Find_Number_Marker() ' ALT-2 Used in Halach to find page marker for Manuscript
        zFind("^#", False, True)
    End Sub
    Sub H_BreakAtComma() 'ALT-Comma used in Halacha, ALT-C
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
        zFind(",", False)
        Selection.TypeParagraph()
        Selection.Delete(Unit:=wdCharacter, Count:=1)
        Indent()
    End Sub
    Sub H_BreakAtSemiColon() 'CTR-Semicolon
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
        zFind(";", False)
        Selection.TypeParagraph()
        Selection.Delete(Unit:=wdCharacter, Count:=1)
        '    Indent
    End Sub
    Sub H_RAMO() 'ALT-3
        GetStuffInParentheses()
        Selection.style = ActiveDocument.Styles("RAMO")
        Selection.Collapse wdCollapseEnd
    End Sub
    Sub H_SHULCHAN_ORUCH_CleanUp()
        zReplace("^p ", "^p", False)
        zReplace(". ", "^p", False)
        zReplace("; ", "^p", False)

        zClearFind()
        Selection.Find.Replacement.style = ActiveDocument.Styles("RAMO")
        With Selection.Find
            .Text = HE(HEY) & HE(GIMEL) & HE(HEY) & "*^013"
            .Replacement.Text = "^&"
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = True
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        zClearFind()
        Selection.Find.Replacement.style = ActiveDocument.Styles("ADDITION")
        With Selection.Find
            .Text = "\(*\)"
            .Replacement.Text = "^&"
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = True
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        zReplace("  ", " ", False)
        zReplace("( ", "(", False)
    End Sub
    Sub ObsFindNextComma() 'ALT _C or Comma
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
        zFind(",", False)
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
        HALACHA_NEW_LINE()
    End Sub
    Sub HALACHA_NEW_LINE()
        Selection.TypeParagraph()
        Selection.Delete(Unit:=wdCharacter, Count:=2)
        Indent()
    End Sub

End Module
