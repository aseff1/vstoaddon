'Attribute VB_Name = "NewMacros"
Option Explicit
Sub Macro2()
Attribute Macro2.VB_ProcData.VB_Invoke_Func = "Normal.NewMacros.Macro2"
'
' Macro2 Macro
'
'
    ActiveDocument.TrackRevisions = Not ActiveDocument.TrackRevisions
    With Options
        .InsertedTextMark = wdInsertedTextMarkUnderline
        .InsertedTextColor = wdByAuthor
        .DeletedTextMark = wdDeletedTextMarkStrikeThrough
        .DeletedTextColor = wdByAuthor
        .RevisedPropertiesMark = wdRevisedPropertiesMarkNone
        .RevisedPropertiesColor = wdByAuthor
        .RevisedLinesMark = wdRevisedLinesMarkOutsideBorder
        .RevisedLinesColor = wdAuto
        .CommentsColor = wdByAuthor
        .RevisionsBalloonPrintOrientation = wdBalloonPrintOrientationPreserve
    End With
    ActiveWindow.View.RevisionsMode = wdMixedRevisions
    With Options
        .MoveFromTextMark = wdMoveFromTextMarkDoubleStrikeThrough
        .MoveFromTextColor = wdGreen
        .MoveToTextMark = wdMoveToTextMarkDoubleUnderline
        .MoveToTextColor = wdGreen
        .InsertedCellColor = wdCellColorLightBlue
        .MergedCellColor = wdCellColorLightYellow
        .DeletedCellColor = wdCellColorPink
        .SplitCellColor = wdCellColorLightOrange
    End With
    With ActiveDocument
        .TrackMoves = True
        .TrackFormatting = False
    End With
End Sub
