'Attribute VB_Name = "M_Nekudos_trop"
Option Explicit
Dim CurrentMishna%
Dim docMain As Word.Document, docMishna As Word.Document
Dim se1 As Selection, se2 As Selection
'Removes Trop from string parameter passed, or the current selection in ActiveDocument
Function RemoveTropExclusive(Optional aString$ = "") As String    'Keeps all characters EXCEPT Trop
Dim x%, y%, aText$, aCurr$
    
    'Selection.WholeStory
    'y = Selection.Characters.count
    'Selection.HomeKey Unit:=wdStory
    
    If aString = "" Then aString = Selection.Text
    
    For x = 1 To Len(aString)
        aCurr = Mid$(aString, x, 1)
        Select Case AscW(aCurr)
'        Case 1488 To 1514, 32
'            'aBox AscW(aCurr)
'            'Stop
'            aText = aText & aCurr
        Case MORE_UNCD.ETNAHTA To MORE_UNCD.aCIRCLE
        Case Else
            aText = aText & aCurr
            'M AscW(aCurr)
            'Stop
        End Select
    Next x
    
    RemoveTropExclusive = aText
    'Stop
End Function


' %Integer $String &Long
Sub FixNekudosWord() ' ALT-F
    Selection.Extend
    Selection.Extend
    Selection.Font.Reset
    Selection.MoveLeft Unit:=wdCharacter, Count:=1
    Selection.TypeBackspace
End Sub
Sub FindWordWithNekudosInKddSmall()
    Dim s$, x%
    For x = 1456 To 1474
        s = s & ChrW(x)
    Next x
    Do While zzFind("[" & s & "]", zWildon, "kdd small")
'        If MsgBox("replace?", vbYesNo + vbQuestion) = vbYes Then
            FixNekudosWord
'        Else
'            Exit Sub
'        End If
    Loop
End Sub
Sub PutNekudosIntoKDD_FromMishna()
Dim y%, x%, MainWord$, MishnaWord$, MishnaWordNoNikud$
Dim MAX_NUM_WORDS_TO_TRY As Integer

    'Application.ScreenUpdating = False

    cAY "", "^p^&", "KDD SPACE", "KDD" 'Remove KDD SPACE style
    zzFoldStyle "KDD SPACE", "KDD", True
    cAY "", "^p^&", "KDD SPACE ON TOP", "KDD" 'Remove KDD SPACE style
    zzFoldStyle "KDD SPACE ON TOP", "KDD", True

    Set docMain = ActiveDocument
    Set se1 = Selection
    MarkAllKDD_Not_kddsmall
    
    
    Set docMishna = Documents.Add
    Set se2 = Selection
        
    'Loop through each Mishna:
    For CurrentMishna = 1 To docMain.Sections.Count
    CopyMishnaWithDotsIntoDocMishna
    MAX_NUM_WORDS_TO_TRY = 5
        
        Do While zzFind("%%%%" & "*" & "####", zWildon, "")
            If aCurrSection > CurrentMishna Then GoTo Cleanup
            
            MainWord = se1.Text
            MainWord = Mid(MainWord, 5, Len(MainWord) - 8)
           
            docMishna.Activate
            GoHome
            
            For y = 1 To MAX_NUM_WORDS_TO_TRY
                Clps wdCollapseEnd
                se2.MoveRight wdWord, 1, wdExtend
                MishnaWord = Trim(se2.Text)
                
                For x = 1 To 4
                    If x = 1 Then MishnaWordNoNikud = RemoveNikkud(MishnaWord)
                    If x = 2 Then MishnaWordNoNikud = RemoveNikkud(MishnaWord, False)
                    If x = 3 Then MishnaWordNoNikud = RemoveNikkud(BeheadedWordWithNekudos(MishnaWord))
                    If x = 4 Then MishnaWordNoNikud = RemoveNikkud(BeheadedWordWithNekudos(MishnaWord), False)
                    If Len(MishnaWordNoNikud) < 2 Then GoTo NextY
                    If MainWord = MishnaWordNoNikud Then GoTo CutPaste
                    If Choser(MainWord, 1) = MishnaWordNoNikud Then GoTo CutPaste
                    If Choser(MainWord, 2) = MishnaWordNoNikud Then GoTo CutPaste
                    If Choser2(MainWord) = MishnaWordNoNikud Then GoTo CutPaste
                    If MainWord = HE(GIMEL) & HE(BEIS) & HE(vav) & HE(HEY) And _
                        MishnaWordNoNikud = HE(GIMEL) & HE(BEIS) & HE(vav) & HE(HEY) & HE(HEY) _
                        Then GoTo CutPaste
                Next x
                
'                For x = 1 To 2
'                    MishnaWordNoNikud = RemoveNikkud(MishnaWord)
'                    If Len(MishnaWordNoNikud) < 2 Then GoTo NextY
'                    If MainWord = MishnaWordNoNikud Then GoTo CutPaste
'                    If Choser(MainWord, 1) = MishnaWordNoNikud Then GoTo CutPaste
'                    If Choser(MainWord, 2) = MishnaWordNoNikud Then GoTo CutPaste
'                    If Choser2(MainWord) = MishnaWordNoNikud Then GoTo CutPaste
'                    If MainWord = he(GIMEL) & he(BEIS) & he(VAV) & he(HEY) And _
'                        MishnaWordNoNikud = he(GIMEL) & he(BEIS) & he(VAV) & he(HEY) & he(HEY) _
'                        Then GoTo CutPaste
'                    MishnaWord = BeheadedWordWithNekudos(MishnaWord)
'                Next x
NextY:       Next y
                
            'The following code is only run if match was not found within MAX_NUM_WORDS_TO_TRY:
            MAX_NUM_WORDS_TO_TRY = MAX_NUM_WORDS_TO_TRY + 1
            docMain.Activate
            se1.Range.HighlightColorIndex = WdColorIndex.wdPink
            Clps wdCollapseStart
            Selection.Delete Unit:=wdCharacter, Count:=4
            zzFind "#", zWildOff, ""
            Selection.Delete Unit:=wdCharacter, Count:=4
            GoTo LoopIt
            
CutPaste:       se2.Delete
                docMain.Activate
                aCopy MishnaWord
                se1.PasteAndFormat wdFormatOriginalFormatting
                If x = 2 Then se1.Range.HighlightColorIndex = WdColorIndex.wdPink 'new line
                Clps wdCollapseStart
                'se1.TypeBackspace
LoopIt:     Loop

Cleanup:    CleanupMishnaDoc
            
    Next CurrentMishna
    
    CleanUpMishnaAtEnd
    
    FindWordWithNekudosInKddSmall
    
    Application.ScreenUpdating = True
   ' Batch M "Done!"
End Sub
Function Choser(aWord$, x%) As String
Dim Internal As String, WhereInWord As Integer, aTestLetter As String
    Choser = aWord
    If Len(aWord) <= 2 Then Exit Function
    If x <> 1 And x <> 2 Then Exit Function
    If x = 1 Then aTestLetter = HE(YUD)
    If x = 2 Then aTestLetter = HE(vav)
    Internal = Mid(aWord, 2, Len(aWord) - 2)
    WhereInWord = InStr(Internal, aTestLetter)
    If WhereInWord = 0 Then Exit Function
    Choser = Mid(Choser, 1, WhereInWord) & Mid(Choser, WhereInWord + 2)
End Function
Function Choser2(aWord$) As String
    Choser2 = Choser(aWord, 1)
    Choser2 = Choser(Choser2, 2)
End Function
Function BeheadedWordWithNekudos(aWord$) As String

    aWord = Mid(aWord, 2)
    
    Do
        Select Case AscW(Mid(aWord, 1, 1))
            Case ALEF To 1514
                BeheadedWordWithNekudos = aWord
                Exit Function
            Case Else
                aWord = Mid(aWord, 2)
        End Select
    Loop

End Function
Sub CleanupMishnaDoc()  'Cleanup any words which may be left in Mishna
    docMishna.Activate
    se2.WholeStory
    docMain.Activate
    If Len(se2.Text) < 3 Then Exit Sub 'has space and paragraph marker
    se2.Font.ColorIndex = wdBrightGreen
    se2.Cut
    GoHome
    se1.GoTo What:=wdGoToSection, Which:=wdGoToFirst, Count:=CurrentMishna, name:=""
    zzFind "", zWildOff, "KDD"
    
    'If Not zzFind("", zWildOff, "Little Mishna Title") Then Stop
    Clps wdCollapseStart
'    se1.TypeText " "
    se1.Paste
    Clps wdCollapseEnd
End Sub
Sub CleanUpMishnaAtEnd()
    docMishna.Activate
    ActiveWindow.Close (Word.WdSaveOptions.wdDoNotSaveChanges)
    
    docMain.Activate
'    GoHome
'    Selection.WholeStory
'    Selection.ClearCharacterDirectFormatting
'    Selection.ClearCharacterDirectFormatting
    
    cAY "( )(^2)", "\2", "", "", zWildon, zNoConfirm, zMain 'Removes space before footnote
    'cAY "%%%%", "", "", ""
    'cAY "####", "", "", ""
End Sub
Sub MarkAllKDD_Not_kddsmall()
    cAY "[A-Z]", "", "KDD", "kdd small", zWildon 'Change English into kdd small
    
    cAY "", "====" & "^&" & "++++", "kdd small", "", zWildOff, zNoConfirm, zMain, aMatchOnlyWholeWords '====kdd small++++
    cAY "", "%%%%" & "^&" & "####", "KDD", "", zWildOff, zNoConfirm, zMain, aMatchOnlyWholeWords   '%%%%KDD####
    cAY "====%%%%", "", "", "", zWildOff, zNoConfirm, zMain, aMatchNotJustWholeWords 'these 2 lines unmark kdd small (all of which is also in KDD)
    cAY "####++++", "", "", "", zWildOff, zNoConfirm, zMain, aMatchNotJustWholeWords


    cAY "", "====^&", "kdd small", "", zWildOff, zNoConfirm, zMain, aMatchNotJustWholeWords 'now do it for even not whole words
    cAY "", "^&++++", "kdd small", "", zWildOff, zNoConfirm, zMain, aMatchNotJustWholeWords

    cAY "([!" & HE(ALEF) & "-" & HE(TAV) & "])(====)", "\1", "", "", zWildon 'remove ==== from non text ====
    cAY "(====)([!" & HE(ALEF) & "-" & HE(TAV) & "])", "\2", "", "", zWildon 'remove ==== from ==== non text
    cAY "([!" & HE(ALEF) & "-" & HE(TAV) & "])(++++)", "\1", "", "", zWildon 'remove ++++ from non text ++++
    cAY "(++++)([!" & HE(ALEF) & "-" & HE(TAV) & "])", "\2", "", "", zWildon 'remove ++++ from ++++ non text
    
    cAY "(%%%%)(?)(++++)", "\2\1", "", "", zWildon 'Turns a one letter kdd small at beginning to look only at KDD part
    cAY "(%%%%)(?)(====)(*)(####)", "\2\4", "", "", zWildon 'Turns a one letter KDD at beginning to ignore
    
    cAY "(%%%%)(?)(####)", "\2", "", "", zWildon 'Gets rid of one letter KDD
    
    cAY "====", "", "", ""
    cAY "++++", "", "", ""
End Sub
Sub CopyMishnaWithDotsIntoDocMishna()
    ColoRize
    docMain.Activate
    GoHome
    se1.GoTo What:=wdGoToSection, Which:=wdGoToFirst, Count:=CurrentMishna, name:=""
    If Not zzFind("", zWildOff, "MISHNA") Then Stop
    se1.Copy
    docMishna.Activate
    se2.WholeStory
    se2.RtlPara
    se2.Paste
    se2.WholeStory
    se2.style = "KDD"
    cAY "^p", "", "", "" 'Remove extra paragraph marks
    cAY ":", "", "", "" 'Remove :  this is a new line
    cAY "[\" & ChrW(1475) & "\(\)]", "", "", "", zWildon 'Remove up non letters
    docMain.Activate
    GoHome
End Sub
