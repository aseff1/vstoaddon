Attribute VB_Name = "M_Shortcuts"
Option Explicit
Global Blatt$
Sub InserShaarHadroshos_WithoutQuickPart() 'control+shift+D

    Selection.ClearCharacterDirectFormatting
    Selection.ClearCharacterDirectFormatting

    Selection.TypeText Text:=ChrW(1513) & ChrW(1506) & ChrW(1512) & " " & ChrW _
        (1491) & ChrW(1512) & ChrW(1513) & ChrW(1493) & ChrW(1514)
    Selection.MoveLeft Unit:=wdWord, Count:=1
    Selection.TypeText Text:=ChrW(1492)
    Selection.EndKey Unit:=wdLine
    Selection.TypeParagraph
    Selection.MoveUp Unit:=wdLine, Count:=1
    Selection.HomeKey Unit:=wdLine, Extend:=wdExtend
    Selection.EndKey Unit:=wdLine, Extend:=wdExtend
    Selection.style = ActiveDocument.Styles("SECTION TITLE")
    Selection.MoveDown Unit:=wdLine, Count:=1
    Selection.EndKey Unit:=wdLine, Extend:=wdExtend
    Selection.style = ActiveDocument.Styles("DROSHOS")
    Clps wdCollapseStart
    
End Sub

Sub FixEndofDroshosFootnote() 'Alt-H
    TurnFootnoteStuff2SmallFont
    Selection.MoveLeft Unit:=wdCharacter, Count:=4
    Clps wdCollapseStart
    Selection.Delete Unit:=wdCharacter, Count:=1 'Deltes Comma
    Selection.MoveRight Unit:=wdCharacter, Count:=1
    Selection.TypeText Text:="("
    Selection.EndKey Unit:=wdLine
    Selection.TypeText Text:=")"
    Selection.MoveLeft Unit:=wdCharacter, Count:=1
    GetStuffInParentheses
    FootnoteOrDocumentTextStyle
    Selection.MoveRight Unit:=wdCharacter, Count:=3
    Clps
End Sub
Sub Halacha() 'ALT-V Puts in Vichain Halacha
    Selection.TypeText Text:=HE(vav) & HE(KAF) & HE(FINAL_NUN) & " " & HE(HEY) & HE(LAMED) & HE(KAF) & HE(HEY) & "."
    Selection.Extend
    Selection.MoveLeft Unit:=wdCharacter, Count:=9
    Emphasis
    With Selection
        With .FootnoteOptions
            .Location = wdBeneathText
            .NumberingRule = wdRestartContinuous
            .StartingNumber = 1
            .NumberStyle = wdNoteNumberStyleArabic
            .NumberingRule = wdRestartSection
        End With
        .Footnotes.Add Range:=Selection.Range, Reference:=""
    End With
End Sub
Sub TurnFootnoteStuff2SmallFont() 'Alt-N
    ayFind " ", False
    Clps wdCollapseStart
    Selection.EndKey Unit:=wdLine, Extend:=wdExtend
    
    If InStr(Selection, ",") > 0 Then
        Clps wdCollapseStart
        Selection.Extend
        Selection.Extend Character:=","
    End If
    
    FootnoteOrDocumentTextStyle
'    Selection.EndKey Unit:=wdLine
    
    Selection.MoveRight Unit:=wdCharacter, Count:=3
    Clps
        
End Sub
Sub FixPosukReference() 'alt-M(okor)
    Selection.EndKey Unit:=wdLine
    Selection.TypeText Text:=" "
    Selection.HomeKey Unit:=wdLine
    
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = HE(PEY) & HE(REISH) & HE(KOOF) & " "
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindAsk
        .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceOne
        
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = ", "
        .Replacement.Text = ":"
        .Forward = True
        .Wrap = wdFindAsk
          .MatchWildcards = False
    End With
    Selection.Find.Execute Replace:=wdReplaceOne
    
    Selection.Collapse direction:=wdCollapseEnd
     
    Selection.Find.ClearFormatting
    With Selection.Find
        .Text = " "
        .Replacement.Text = ":"
        .Forward = False
        .Wrap = wdFindAsk
        .MatchWildcards = False
    End With
    Selection.Find.Execute
    
    Selection.Extend
    Selection.Extend Character:=" "
    Selection.style = ActiveDocument.Styles("mokor small")
    Selection.Collapse direction:=wdCollapseEnd
End Sub
Sub Emphasis() 'ALT-B Puts bold style depending where we are
    
'    Selection.style = "Emphasis"
'    Exit Sub
    
    
    ClearDirectFormat False, True
    If Selection.style = "KDD HAKDOMO" Then
        Selection.style = "shar bold"
    ElseIf Selection.style = "TALMUD" Then
        Selection.style = "shar bold"
'    ElseIf Selection.style = "TALMUD HAKDAMA" Then
'        Selection.style = "shar bold"
    ElseIf Selection.style = "Agadah" Then
        Selection.style = "shar bold"
'    ElseIf Selection.style = "SHAAR" Then
'        Selection.style = "shar bold"
'    ElseIf Selection.style = "SHAR" Then
'        Selection.style = "shar bold"
'    ElseIf Selection.style = "SHAR SPACE" Then
'        Selection.style = "shar bold"
    ElseIf Selection.style = "DROSHOS" Then
        Selection.style = "shar bold"
'    ElseIf Selection.style = "PSHAT" Then
'        Selection.style = "shar bold"
    ElseIf Selection.style = "RAV" Then
        Selection.style = "rav bold"
    ElseIf Selection.style = "Tables" Then
        Selection.style = "Tables Bold"
    ElseIf Selection.style = "Ein Mishpot" Then
        Selection.style = "Ein Mishpot Bold"
'   ElseIf Selection.Style = "KDD" Then Selection.Style = "kdd bold"
    Else
        Msgbox ("No bold defined here")
        Exit Sub
    End If
    Selection.MoveRight Unit:=wdCharacter, Count:=1
    Selection.Font.Reset
End Sub
Sub MakeSharHead()  'Alt-: Bolds begining of line to : used in shaar head
    Selection.HomeKey Unit:=wdLine
    If Selection.style = "RAV" Then Exit Sub
'    If Selection.Style = "TALMUD" Then Selection.MoveRight Unit:=wdCharacter, Count:=3
    Selection.Extend
    zClearFind
    With Selection.Find
        .Text = ":"
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
    End With
    Selection.Find.Execute
    RemoveNikudFromSelection
    Emphasis
End Sub
Sub BracketsStyleThin() 'ALT-[ ALT-]
    GetStuffInBrackets
    Selection.style = ActiveDocument.Styles("shar thin")
    'Selection.EndKey Unit:=wdLine
End Sub
Sub QUOTE_OPEN1() 'ALT-"
    Selection.TypeText Text:=""""
    'Selection.InsertSymbol Font:="David", CharacterNumber:=8222, Unicode:=True
'    Selection.InsertSymbol CharacterNumber:=8222, Unicode:=True
'    Selection.MoveRight Unit:=wdCharacter, Count:=1
'    Selection.Extend
'    Selection.MoveLeft Unit:=wdCharacter, Count:=3
'    Selection.Font.Reset
'    Selection.Collapse direction:=wdCollapseEnd
'    Selection.MoveLeft Unit:=wdCharacter, Count:=1
End Sub
Sub QUOTE_END2() 'CTR-"
    Selection.TypeText Text:=""""
    'Selection.InsertSymbol Font:="David", CharacterNumber:=8221, Unicode:=True
'    Selection.InsertSymbol CharacterNumber:=8221, Unicode:=True
'    Selection.MoveRight Unit:=wdCharacter, Count:=1
'    Selection.Extend
'    Selection.MoveLeft Unit:=wdCharacter, Count:=3
'    Selection.Font.Reset
'    Selection.Collapse direction:=wdCollapseEnd
'    Selection.MoveLeft Unit:=wdCharacter, Count:=1
End Sub
Sub Rambam() ' ALT-R
 Application.Keyboard (1037)
    Selection.TypeText Text:=HE(REISH) & HE(MEM) & HE(BEIS) & """" & _
        HE(FINAL_MEM) & " "
    Selection.style = ActiveDocument.Styles("mokor small")
    Selection.TypeText Text:=HE(PEY) & HE(HEY) & """" & HE(MEM)
        
'    Selection.TypeText Text:="���" & """" & HE(FINAL_MEM) & " "
'    Selection.style = ActiveDocument.Styles("mokor small")
'    Selection.TypeText Text:=HE(PEY) & HE(HEY) & """" & HE(MEM)
End Sub
Sub FootnoteOrDocumentTextStyle() 'ALT-X  Original Macro created 5/10/2009 by Rajeev Maskey
    On Error Resume Next
    
    Selection.Font.Reset ' new addition
    
    If Selection.style = "KDD" Then
        Selection.style = "kdd small"
    ElseIf Selection.Information(wdInFootnoteEndnotePane) Then
         Selection.style = "mokor small" 'footnote style
    ElseIf Selection.style = "Agadah" Then
        Selection.style = "shaar small"
    ElseIf Selection.style = "DROSHOS" Then
        Selection.style = "shaar small"
    ElseIf Selection.style = "TALMUD" Then
        Selection.style = "shaar small"
    Else
        Selection.style = "KOL DEMOMO DAKA"        'regular document
    End If
   
    
    Select Case Err
        Case 0      'no error
        Case 5834   'error generated when style doesn't exist
            Selection.style = "Kol Demomo Daka"
        Case Else   'some other error
            Msgbox "Error #" & str(Err.Number) & ":  " & Err.Description
            Stop
    End Select
    
End Sub
Sub RAMBAM_HILCHOS() ' ALT-E
    Selection.TypeText Text:=HE(REISH) & HE(MEM) & HE(BEIS) & """" & _
        HE(FINAL_MEM) & " "
    FootnoteOrDocumentTextStyle
    Rambam_Yad_Finish 'in blatt
'    Selection.TypeText Text:=he(HEY) & he(LAMED) & "' " & _
'    he(TES) & he(VAV) & he(MEM) & he(ALEF) & he(TAV) & " " & he(MEM) & he(TAV) & " :"
'    Selection.MoveLeft Unit:=wdCharacter, Count:=1
End Sub
Sub VI_HILCHOS() ' CTR-E
'    Selection.TypeText Text:=he(VAV) & he(HEY) & he(LAMED) & "' " & _
'    he(TES) & he(VAV) & he(MEM) & he(ALEF) & he(TAV) & " " & he(MEM) & he(TAV) & " :"
'    Selection.MoveLeft Unit:=wdCharacter, Count:=1
    Selection.TypeText Text:=HE(vav)
    Rambam_Yad_Finish 'in Blatt
End Sub
Sub RASH_ROSH() 'ALT-W
    Selection.Font.Reset
    Selection.TypeText Text:=HE(REISH) & """" & HE(SHIN) & ", " & ChrW(REISH _
        ) & HE(ALEF) & """" & HE(SHIN)
End Sub
Sub Rambam_with_Yad() 'CTR-R
    Selection.TypeText Text:=HE(REISH) & HE(MEM) & HE(BEIS) & """" & _
        HE(FINAL_MEM) & " "
    FootnoteOrDocumentTextStyle
    Selection.TypeText Text:=HE(PEY) & HE(HEY) & """" & HE(MEM) & " " & HE(vav)
    Rambam_Yad_Finish 'in Blatt
'    Selection.TypeText Text:=he(PEY) & he(HEY) & """" & he(MEM) & " " _
'         & he(VAV) & he(HEY) & he(LAMED) & "' " & _
'    he(TES) & he(VAV) & he(MEM) & he(ALEF) & he(TAV) & " " & he(MEM) & he(TAV) & " :"
'    Selection.MoveLeft Unit:=wdCharacter, Count:=1
End Sub
Sub RAVAD() 'ALT-D needs fixing
    Selection.Font.Reset
    Selection.TypeText Text:=HE(REISH) & HE(ALEF) & HE(BEIS) & """" & _
        HE(DALET) & " "
    FootnoteOrDocumentTextStyle
    Selection.TypeText Text:=HE(HEY) & HE(LAMED) & "' " & HE(TES) & _
        HE(vav) & HE(MEM) & HE(ALEF) & HE(TAV) & " " & HE(MEM) & _
        HE(TAV) & " : "
    Selection.MoveLeft Unit:=wdCharacter, Count:=2
End Sub
Sub GRA() 'ALT-G
    Selection.Font.Reset
    Selection.TypeText Text:=HE(GIMEL) & HE(REISH) & """" & HE(ALEF) & " "
    FootnoteOrDocumentTextStyle
    Selection.TypeText Text:=HE(ALEF) & """" & HE(REISH)
End Sub
Sub MakeSuperscript()  'Alt-S   needs fixing since we now have endnotes
'   Selection.MoveLeft Unit:=wdCharacter, Count:=4
    zFind "^2", False
    Selection.style = "Footnote Reference"
    Selection.Collapse
    Selection.MoveRight Unit:=wdCharacter, Count:=1
End Sub
Sub Rambam_Yad_Finish()
'Selection.TypeText Text:=he(HEY) & he(LAMED) & "' " & _
    he(TES) & he(VAV) & he(MEM) & he(ALEF) & he(TAV) & " " & he(TSADI) & he(REISH) & he(AYIN) & he(TAV) & " "
'Selection.TypeText Text:=HE(HEY) & HE(LAMED) & "' " & HE(PEY) & HE(REISH) & HE(HEY) & " "
'Selection.TypeText Text:=HE(HEY) & HE(LAMED) & "' " & _
'    HE(TES) & HE(VAV) & HE(MEM) & HE(ALEF) & HE(TAV) & " " & HE(MEM) & HE(TAV) & " "
    
Selection.TypeText Text:=HE(HEY) & HE(LAMED) & "' " & _
    HE(SHIN) & HE(MEM) & HE(YUD) & HE(TES) & HE(HEY) & " "

If Blatt <> "" Then
        Selection.TypeText Blatt
    Else
        Selection.TypeText Text:=":"
        Selection.MoveLeft Unit:=wdCharacter, Count:=1
    End If
End Sub
Sub SetBlatt() 'CTR-B
    Application.Keyboard (1033) 'switches to english
    Blatt = LatinToHebrew(InputBox$("Enter Blatt:"))
    Application.Keyboard (1037) 'switches to hebrew
    End Sub
