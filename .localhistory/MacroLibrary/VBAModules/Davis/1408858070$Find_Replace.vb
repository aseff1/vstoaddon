﻿Module Find_Replace
    Option Explicit
    Dim NothingFoundGoingForward As Boolean
    '*******This is great stuff...All related to the cAY procedure:
    Sub cAY(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, Optional zzWild As zWild = zWildOff, _
        Optional Confirm As ConfirmAll = zNoConfirm, Optional WhatPart As TheStory = zMainFootnotesAndEndnotes, _
        Optional WholeWordsOnly As aMatchWholeWords = False)
        'If zFrom = "" And ZfromStyle = "" Then Exit Sub 'Checks for error--user left out zFrom and zFromStyle
        If StyleExist(ZfromStyle) = False Then Exit Sub
        If StyleExist(ZtoStyle) = False Then Exit Sub

        If Confirm Then
            Select Case WhatPart
                Case zMain, zHeader
                    zReplaceConfirm(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, WhatPart, WholeWordsOnly)
                Case zFootnotesAndEndnotes
                    zReplaceConfirm(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, wdFootnotesStory, WholeWordsOnly)
                    zReplaceConfirm(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, wdEndnotesStory, WholeWordsOnly)
                Case zMainFootnotesAndEndnotes
                    zReplaceConfirm(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, wdMainTextStory, WholeWordsOnly)
                    zReplaceConfirm(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, wdFootnotesStory, WholeWordsOnly)
                    zReplaceConfirm(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, wdEndnotesStory, WholeWordsOnly)
            End Select
        Else
            Select Case WhatPart
                Case zMain, zHeader
                    zReplaceAll(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, WhatPart, WholeWordsOnly)
                Case zFootnotesAndEndnotes
                    zReplaceAll(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, wdFootnotesStory, WholeWordsOnly)
                    zReplaceAll(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, wdEndnotesStory, WholeWordsOnly)
                Case zMainFootnotesAndEndnotes
                    zReplaceAll(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, wdMainTextStory, WholeWordsOnly)
                    zReplaceAll(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, wdFootnotesStory, WholeWordsOnly)
                    zReplaceAll(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, wdEndnotesStory, WholeWordsOnly)
            End Select
        End If

        Clps wdCollapseEnd

    End Sub

    Sub ShowHebrewReplaceForm()
        HebrewReplaceForm.Show()
    End Sub
    Sub zClearFind()
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.Text = ""
        Selection.Find.Replacement.ClearFormatting()
    End Sub
    Sub ChangeMokorSmallinMain2BracketsandSharthin()
        cAY("", "[^&]", "mokor small", "shar thin", zWildon, zNoConfirm, zMain)
    End Sub


    'needs work
    Function FindInSectionForward(zwhat As String, ByVal zWild As zWild, zstyle As String) As Boolean
        'attempts to find forwards, but only within current section:
        Dim aSection%, x%

        aSection = Selection.Information(wdActiveEndSectionNumber)

        x = zzFind(zwhat, zWild, zstyle)

        If x = True And Selection.Information(wdActiveEndSectionNumber) = aSection Then
            FindInSectionForward = True
        Else
            FindInSectionForward = False
        End If

    End Function

Function FindInSection(zwhat As String, ByVal zWild As zWild, zstyle As String, Optional aRestart As aRestartOption) As Boolean
        'attempts to find forwards, if not, then backwards, but only within current section:
        Dim aSection%, x%

        aSection = Selection.Information(wdActiveEndSectionNumber)
        ActiveDocument.Bookmarks.Add(Range:=Selection.Range, name:="qaqa_qaqa")

        If aRestart = True Then NothingFoundGoingForward = False

        x = zzFind(zwhat, zWild, zstyle)
        If x = True And Selection.Information(wdActiveEndSectionNumber) = aSection _
            And (NothingFoundGoingForward = False) Then

            FindInSection = True
        Else
            ActiveDocument.Bookmarks("qaqa_qaqa").Select()
            Clps wdCollapseStart
            x = zzFind(zwhat, zWild, zstyle, aBackwards)
            If x = True And Selection.Information(wdActiveEndSectionNumber) = aSection Then
                FindInSection = True
            Else
                FindInSection = False
                NothingFoundGoingForward = True
            End If
        End If

        ActiveDocument.Bookmarks("qaqa_qaqa").Delete()

    End Function

    'zzzz
    Function FindNextInSection(zwhat As String, ByVal zWild As zWild, zstyle As String) As Boolean
        'attempts to find forwards, if not, then backwards, but only within current section:
        Dim aSection%, x%

        aSection = Selection.Information(wdActiveEndSectionNumber)
        ActiveDocument.Bookmarks.Add(Range:=Selection.Range, name:="qaqa_qaqa")

        x = zzFind(zwhat, zWild, zstyle)
        If x = True And Selection.Information(wdActiveEndSectionNumber) = aSection Then
            FindNextInSection = True
        Else
            ActiveDocument.Bookmarks("qaqa_qaqa").Select()
            Clps wdCollapseStart
            x = zzFind(zwhat, zWild, zstyle, aBackwards)
            If x = True And Selection.Information(wdActiveEndSectionNumber) = aSection Then
                FindNextInSection = True
            Else
                FindNextInSection = False
            End If
        End If

        ActiveDocument.Bookmarks("qaqa_qaqa").Delete()

    End Function

    Function Find2Styles(zwhat$, zWild As zWild, zCharStyle$, zParagraphStyle$, _
        Optional zDirection As aFindDirection = aForward, Optional WholeWordsOnly As aMatchWholeWords = False) As Boolean
        'finds text which contains 2 styles.
        Dim se As Selection
        'Stop
        se = Selection
        'GoHome
        'application.ScreenRefresh
        'Application.ScreenUpdating = False
        Do While zzFind(zwhat, zWild, zCharStyle, zDirection, WholeWordsOnly)
            If se.Range.ParagraphStyle = zParagraphStyle Then
                Find2Styles = True
                Application.ScreenUpdating = True
                Exit Function
            End If
        Loop

        Find2Styles = False
        Application.ScreenUpdating = True

    End Function

    ''
    ''Sub FindAndHighlight()
    ''
    ''    Options.DefaultHighlightColorIndex = WdColorIndex.wdBrightGreen
    ''
    ''    Selection.Find.ClearFormatting
    ''    Selection.Find.style = ActiveDocument.Styles("KDD")
    ''    Selection.Find.Replacement.ClearFormatting
    ''    Selection.Find.Replacement.Highlight = True
    ''    With Selection.Find
    ''        .Text = ""
    ''        .Replacement.Text = ""
    ''        .Forward = True
    ''        .Wrap = wdFindContinue
    ''        .Format = True
    ''        .MatchCase = False
    ''        .MatchWholeWord = False
    ''        .MatchKashida = False
    ''        .MatchDiacritics = False
    ''        .MatchAlefHamza = False
    ''        .MatchControl = False
    ''        .MatchWildcards = False
    ''        .MatchSoundsLike = False
    ''        .MatchAllWordForms = False
    ''    End With
    ''    Selection.Find.Execute Replace:=wdReplaceAll
    ''    Application.Keyboard (1037)
    ''
    ''End Sub

    ''Function FindAndReplace2Styles(zwhat$, zWild As zWild, zCharStyle$, zParagraphStyle$, _
    ''    Optional zDirection As aFindDirection = aForward, Optional WholeWordsOnly As aMatchWholeWords = False) As Boolean
    '''finds text which contains 2 styles.
    ''Dim se As Selection
    '''Stop
    ''    Set se = Selection
    ''    'GoHome
    ''    'application.ScreenRefresh
    ''    'Application.ScreenUpdating = False
    ''    Do While zzFind(zwhat, zWild, zCharStyle, zDirection, WholeWordsOnly)
    ''        If se.Range.ParagraphStyle = zParagraphStyle Then
    ''            Find2Styles = True
    ''            Application.ScreenUpdating = True
    ''            Exit Function
    ''        End If
    ''    Loop
    ''
    ''    Find2Styles = False
    ''    Application.ScreenUpdating = True
    ''
    ''End Function

    Function Find2StylesInSection(zwhat$, ByVal zWild As zWild, zCharStyle$, zParagraphStyle$) As Boolean
        'attempts to find forwards, if not, then backwards, but only within current section:
        Dim aSection%, x%

        aSection = Selection.Information(wdActiveEndSectionNumber)
        ActiveDocument.Bookmarks.Add(Range:=Selection.Range, name:="qaqa_qaqa")

        x = Find2Styles(zwhat, zWild, zCharStyle$, zParagraphStyle$)
        If x = True And Selection.Information(wdActiveEndSectionNumber) = aSection Then
            Find2StylesInSection = True
        Else
            ActiveDocument.Bookmarks("qaqa_qaqa").Select()
            Clps wdCollapseStart
            x = Find2Styles(zwhat, zWild, zCharStyle$, zParagraphStyle$, aBackwards)
            If x = True And Selection.Information(wdActiveEndSectionNumber) = aSection Then
                Find2StylesInSection = True
            Else
                Find2StylesInSection = False
            End If
        End If

        ActiveDocument.Bookmarks("qaqa_qaqa").Delete()

    End Function


    'optional:
    Function Find2StylesPrompt() As Boolean
        'finds text which contains 2 styles.
        Dim se As Selection, zCharStyle$, zParagraphStyle$
        'Stop
        se = Selection
        'GoHome
        zCharStyle = Inp("Character style:")
        zParagraphStyle = Inp("Paragraph style:")
        Do While zzFind("", False, zCharStyle)
            If se.Range.ParagraphStyle = zParagraphStyle Then
                Find2StylesPrompt = True
                M "yes"
                Exit Function
            End If
        Loop

        Find2StylesPrompt = False
        M "no"

    End Function
    Function zFindBack(zwhat As String, zWild As Boolean) As Boolean
        Selection.Find.ClearFormatting()
        With Selection.Find
            .Text = zwhat
            .Replacement.Text = ""
            .MatchWildcards = zWild
            .Forward = False
        End With
        Selection.Find.Execute()
        zFindBack = Len(Selection.Text) > 1
    End Function
    Function zFindInPart(zwhat As String, zWild As Boolean, zPart As String)
        Dim myStoryRange As Range
        Selection.HomeKey Unit:=wdStory
        For Each myStoryRange In ActiveDocument.StoryRanges
            If myStoryRange.StoryType = zPart Then
                myStoryRange.Find.ClearFormatting()
                With myStoryRange.Find
                    .Text = zwhat
                    .Replacement.Text = ""
                    .MatchWildcards = zWild
                    .Forward = True
                    .Wrap = wdFindStop
                    .Execute()
                End With
            End If
        Next myStoryRange
        zFindInPart = Len(Selection.Text) > 1
    End Function
    Function zFindStyle(zstyle As String, Optional Go2Top As Boolean = False) As Boolean
        If Go2Top Then Selection.HomeKey Unit:=wdStory

        Selection.Find.ClearFormatting()
        Selection.Find.style = ActiveDocument.Styles(zstyle)
        With Selection.Find
            .Text = ""
            .Replacement.Text = ""
            .MatchWildcards = False
            .Forward = True
            '        .Wrap = wdFindContinue
            .Wrap = wdFindStop
        End With
        Selection.Find.Execute()
        zFindStyle = Len(Selection.Text) > 1
    End Function
    Sub zFoldStyle(ZfromStyle As String, ZtoStyle As String, Optional zKill As Boolean = True) 'Folds 1 style into another
        If StyleExist(ZfromStyle) = False Then Exit Sub

        If StyleExist(ZtoStyle) = False Then
            zReplaceStyle(ZfromStyle, ZtoStyle, False)
            Exit Sub
        End If
        Selection.HomeKey Unit:=wdStory
        Selection.Find.ClearFormatting()
        Selection.Find.style = ActiveDocument.Styles(ZfromStyle)
        Selection.Find.Replacement.ClearFormatting()
        Selection.Find.Replacement.style = ActiveDocument.Styles(ZtoStyle)
        With Selection.Find
            .Text = ""
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        If zKill Then ActiveDocument.Styles(ZfromStyle).Delete()
    End Sub
    Sub zMakeAllSeqFieldsSuperscript()
        Selection.WholeStory()
        Selection.Fields.ToggleShowCodes()

        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find.Replacement.Font
            .Superscript = True
            .Subscript = False
        End With
        With Selection.Find
            .Text = "^d^wSEQ"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.WholeStory()
        Selection.Fields.ToggleShowCodes()
        Selection.Fields.Update()
        Selection.EscapeKey()
    End Sub
    Sub zReplace(zFrom As String, zTo As String, zWild As Boolean, Optional Go2Top As Boolean = True)
        If Go2Top Then Selection.HomeKey Unit:=wdStory

        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            '        .Format = True
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.HomeKey Unit:=wdLine
    End Sub
    Sub Replace1(zFrom As String, zTo As String, zWild As zWild, Optional Go2Top As Boolean = True)
        If Go2Top Then Selection.HomeKey Unit:=wdStory

        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            '        .Format = True
        End With
        Selection.Find.Execute Replace:=wdReplaceOne
        Selection.HomeKey Unit:=wdLine
    End Sub
    Sub qReplaceInAllParts()
        Dim myStoryRange As Range
        Dim zFrom As String
        Dim zTo As String
        Dim zWild As Boolean
        zFrom = InputBox("from")
        zTo = InputBox("to")
        zWild = InputBox("wild", , False)

        Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges
            myStoryRange.Find.ClearFormatting()
            myStoryRange.Find.Replacement.ClearFormatting()
            With myStoryRange.Find
                .Text = zFrom
                .Replacement.Text = zTo
                .MatchWildcards = zWild
                .Forward = True
                .Wrap = wdFindContinue
                .Execute Replace:=wdReplaceAll
            End With
        Next myStoryRange
    End Sub
    Sub zReplaceInAllParts(zFrom As String, zTo As String, zWild As Boolean)
        Dim myStoryRange As Range
        Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges
            myStoryRange.Find.ClearFormatting()
            myStoryRange.Find.Replacement.ClearFormatting()
            With myStoryRange.Find
                .Text = zFrom
                .Replacement.Text = zTo
                .MatchWildcards = zWild
                .Forward = True
                .Wrap = wdFindContinue
                .Execute Replace:=wdReplaceAll
            End With
        Next myStoryRange
    End Sub
    Sub zReplaceInFootnotes(zFrom As String, zTo As String, zWild As Boolean)
        zReplaceInPart(zFrom, zTo, zWild, wdFootnotesStory)
    End Sub
    Sub zReplaceInPart(zFrom As String, zTo As String, zWild As Boolean, zPart As String)
        Dim myStoryRange As Range
        Selection.HomeKey Unit:=wdStory
        For Each myStoryRange In ActiveDocument.StoryRanges
            If myStoryRange.StoryType = zPart Then
                myStoryRange.Find.ClearFormatting()
                myStoryRange.Find.Replacement.ClearFormatting()
                With myStoryRange.Find
                    .Text = zFrom
                    .Replacement.Text = zTo
                    .MatchWildcards = zWild
                    .Forward = True
                    .Wrap = wdFindContinue
                    .Execute Replace:=wdReplaceAll
                End With
            End If
        Next myStoryRange
    End Sub
    Sub zReplaceInStyle(zstyle As String, zFrom As String, zTo As String, zWild As Boolean, Optional zKill As Boolean = False)
        If StyleExist(zstyle) = False Then Exit Sub

        Selection.HomeKey Unit:=wdStory
        Selection.Find.ClearFormatting()
        Selection.Find.style = ActiveDocument.Styles(zstyle)
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = zFrom
            .Replacement.Text = zTo
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = zWild
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

        If zKill Then ActiveDocument.Styles(zstyle).Delete()
    End Sub
    Sub zReplaceStyle(zFrom As String, zstyle As String, zWild As Boolean)
        If StyleExist(zstyle) = False Then Exit Sub

        Selection.HomeKey Unit:=wdStory
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        Selection.Find.Replacement.style = ActiveDocument.Styles(zstyle)
        With Selection.Find
            .Text = zFrom
            .Replacement.Text = ""
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
    End Sub

    'Sub zReplaceConfirm(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, zzWild As zWild, _
    WhatPart As TheStory, Optional WholeWordsOnly As aMatchWholeWords = False)
    Sub zReplaceConfirm(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, zzWild As zWild, _
        WhatPart As WdStoryType, Optional WholeWordsOnly As aMatchWholeWords = False)

        Dim x%, ThereIsAMatch As Boolean
        Dim oRange As Range

        oRange = ActiveDocument.StoryRanges(WhatPart)
        oRange.Select()
        Clps wdCollapseStart

        Do
            ThereIsAMatch = zzFind(zFrom, zzWild, ZfromStyle, aForward, WholeWordsOnly)
            If ThereIsAMatch = True Then
                x = MsgboxUnicode("Do you want to replace the current selection """ & zFrom & """ to """ & zTo & """?", vbYesNoCancel)
                Select Case x
                    Case vbCancel
                        Exit Do
                    Case vbNo
                        Clps wdCollapseEnd 'user chose not to replace, so do nothing, just loop to the next find
                    Case vbYes
                        Clps wdCollapseStart 'Since we just did a Find, the match will be highlighted, so it's necessary to collapse the selection
                        zReplaceOne(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, WholeWordsOnly)
                        Clps wdCollapseEnd  'I don't think you need this, beacuse the next found is not automatically highlighted without another find.execute
                End Select
            End If
        Loop While ThereIsAMatch = True

        Clps wdCollapseEnd

    End Sub

    Function zzFind(zwhat As String, ByVal zWild As zWild, ByVal zstyle As String, _
        Optional zDirection As aFindDirection = aFindDirection.aForward, Optional WholeWordsOnly As aMatchWholeWords = False) As Boolean
        zClearFind()
        With Selection.Find
            .Text = zwhat
            If zstyle <> "" Then .style = ActiveDocument.Styles(zstyle)
            'M .Replacement.Text
            'aBox .Replacement.Text
            'Stop
            .MatchWildcards = zWild
            .Forward = zDirection
            .Wrap = wdFindStop
            .MatchWholeWord = WholeWordsOnly
            .Execute()
        End With
        zzFind = Selection.Find.found
    End Function

    Sub zReplaceOne(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, ByVal zzWild As Boolean, Optional WholeWordsOnly As aMatchWholeWords = False)
        zClearFind()
        With Selection.Find
            If ZfromStyle <> "" Then .style = ActiveDocument.Styles(ZfromStyle)
            If ZtoStyle <> "" Then .Replacement.style = ActiveDocument.Styles(ZtoStyle)
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zzWild
            .Forward = True
            .Wrap = wdFindStop  'redundant?
            .MatchWholeWord = WholeWordsOnly
            .Execute Replace:=wdReplaceOne
        End With
    End Sub

    Sub zReplaceAll(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, ByVal zzWild As Boolean, TheStory As WdStoryType, _
        Optional WholeWordsOnly As aMatchWholeWords = False)

        Dim myStoryRange As Range

        On Error GoTo errHandler
        'On Error Resume Next
        Selection.HomeKey Unit:=wdStory
        zClearFind()
        myStoryRange = ActiveDocument.StoryRanges(TheStory)
        With myStoryRange.Find
            If ZfromStyle <> "" Then .style = ActiveDocument.Styles(ZfromStyle)
            If ZtoStyle <> "" Then .Replacement.style = ActiveDocument.Styles(ZtoStyle)
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zzWild
            .Forward = True
            .Wrap = wdFindContinue
            .MatchWholeWord = WholeWordsOnly
            .Execute Replace:=wdReplaceAll
        End With

        Exit Sub

errHandler:
        If Err = 5941 Then  'The requested member of the collection does not exist.
            Exit Sub
        Else
            Stop
            M Err.Description
            Resume Next
        End If

    End Sub

    Sub AAA()
        'HebrewReplaceForm.Show
        cAY("", "", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes)
    End Sub

    '********
    Sub bbAY(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, zzWild As zWild, Confirm As ConfirmAll, Optional WhatPart As TheStory = zMainFootnotesAndEndnotes)
        If zFrom = "" And ZfromStyle = "" Then Exit Sub 'Checks for error--user left out zFrom and zFromStyle
        'If StyleExist(ZfromStyle) = False Then Exit Sub
        'If StyleExist(ZtoStyle) = False Then Exit Sub

        If Confirm Then
            If WhatPart = zMainFootnotesAndEndnotes Then
                zReplaceConfirm(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, zMain)
                zReplaceConfirm(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, zFootnotesAndEndnotes)
            Else
                zReplaceConfirm(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild, WhatPart)
            End If
        Else
            If WhatPart = zMainFootnotesAndEndnotes Then
                zReplaceAll(zFrom, zTo, ZfromStyle$, ZtoStyle$, zzWild, zMain)
                zReplaceAll(zFrom, zTo, ZfromStyle$, ZtoStyle$, zzWild, zFootnotesAndEndnotes)
            Else
                zReplaceAll(zFrom, zTo, ZfromStyle$, ZtoStyle$, zzWild, WhatPart)
            End If
        End If
        Selection.Collapse direction:=wdCollapseEnd
    End Sub

    Sub zzReplaceConfirm(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, ByVal zzWild As zWild, WhatPart As TheStory)
        Dim x%, ThereIsAMatch As Boolean
        Dim oRange As Range

        oRange = ActiveDocument.StoryRanges(WhatPart)
        oRange.Select()
        Selection.Collapse direction:=wdCollapseStart

        Do
            ThereIsAMatch = zzFind(zFrom, zzWild, ZfromStyle)
            If ThereIsAMatch = True Then
                x = Msgbox("Do you want to replace the current selection """ & zFrom & """ to """ & zTo & """?", vbYesNoCancel)
                Select Case x
                    Case vbCancel
                        Exit Do
                    Case vbNo
                        Selection.Collapse direction:=wdCollapseEnd 'user chose not to replace, so do nothing, just loop to the next find
                    Case vbYes
                        Selection.Collapse direction:=wdCollapseStart 'Since we just did a Find, the match will be highlighted, so it's necessary to collapse the selection
                        zReplaceOne(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild)
                        Selection.Collapse direction:=wdCollapseEnd  'I don't think you need this, beacuse the next found is not automatically highlighted without another find.execute
                End Select
            End If
        Loop While ThereIsAMatch = True
        Selection.Collapse direction:=wdCollapseEnd
    End Sub
    Function zzzFind(zwhat As String, ByVal zzWild As Boolean, zstyle As String) As Boolean
        With Selection.Find
            .ClearFormatting()
            .Text = zwhat
            .style = ActiveDocument.Styles(zstyle)
            .Replacement.ClearFormatting()
            .MatchWildcards = zzWild
            .Forward = True
            .Wrap = wdFindStop
            .Execute()
        End With
        zzzFind = Selection.Find.found
    End Function
    Function azzFind(zwhat As String, ByVal zzWild As Boolean, zstyle As String) As Boolean
        zClearFind()
        With Selection.Find
            .Text = zwhat
            If zstyle <> "" Then .style = ActiveDocument.Styles(zstyle)
            .MatchWildcards = zzWild
            .Forward = True
            .Wrap = wdFindStop
            .Execute()
        End With
        azzFind = Selection.Find.found
    End Function
    Sub zzReplaceOne(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, ByVal zzWild As Boolean)
        zClearFind()
        With Selection.Find
            If ZfromStyle <> "" Then .style = ActiveDocument.Styles(ZfromStyle)
            If ZtoStyle <> "" Then .Replacement.style = ActiveDocument.Styles(ZtoStyle)
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zzWild
            .Forward = True
            .Wrap = wdFindStop  'redundant?
            .Execute Replace:=wdReplaceOne
        End With
    End Sub



    Sub a()
        frmRelocateFootnotes.Show 1

        'ayReplaceInPart_Comprehensive "","",
    End Sub


    '***These procedures have NOT been tested, unless specified.






    '-------------
    '***Part 1:
    '***REPLACE
    '-------------



    '*******
    Sub ayReplaceInPart_Comprehensive(zFrom$, zTo$, Optional TheRangeStory As DifferentTypesOfStories = ayAll, Optional zWild As Boolean = False)     '%=integer; $=string; &=long
        Dim rngStory As Word.Range, lngJunk&, oShp As Shape

        'Fix the skipped blank Header/Footer problem
        lngJunk = ActiveDocument.Sections(1).Headers(1).Range.StoryType

        'Iterate through all story types in the current document
        For Each rngStory In ActiveDocument.StoryRanges
            'Iterate through all linked stories
            Do
                If rngStory.StoryType = TheRangeStory Or TheRangeStory = ayAll Then
                    aySearchAndReplaceInStory(rngStory, zFrom, zTo, zWild)
                    On Error Resume Next
                    Select Case rngStory.StoryType
                        Case 6, 7, 8, 9, 10, 11
                            If rngStory.ShapeRange.Count > 0 Then
                                For Each oShp In rngStory.ShapeRange
                                    If oShp.TextFrame.HasText Then
                                        aySearchAndReplaceInStory(oShp.TextFrame.TextRange, zFrom, zTo, zWild)
                                    End If
                                Next
                            End If
                        Case Else
                            'Do Nothing
                    End Select

                    On Error GoTo 0
                End If

                'Get next linked story (if any)
                rngStory = rngStory.NextStoryRange

            Loop Until rngStory Is Nothing
        Next


    End Sub


    '******
    Sub aySearchAndReplaceInStory(rngStory As Word.Range, zFrom$, zTo$, Optional zWild As Boolean = False)

        With rngStory.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            .Execute Replace:=wdReplaceAll
        End With

        'MsgBox Str$(rngStory.StoryType)




        '1 wdMainTextStory
        '2 wdFootnotesStory
        '3 wdEndnotesStory
        '4 wdCommentsStory
        '5 wdTextFrameStory
        '6 wdEvenPagesHeaderStory
        '7 wdPrimaryHeaderStory
        '8 wdEvenPagesFooterStory
        '9 wdPrimaryFooterStory
        '10 wdFirstPageHeaderStory
        '11 wdFirstPageFooterStory
        '12 wdFootnoteSeparatorStory
        '13 wdFootnoteContinuationSeparatorStory
        '14 wdFootnoteContinuationNoticeStory
        '15 wdEndnoteSeparatorStory
        '16 wdEndnoteContinuationSeparatorStory
        '17 wdEndnoteContinuationNoticeStory

    End Sub


    Sub ayReplace(zFrom As String, zTo As String, zWild As Boolean, Optional Go2Top As Boolean = True)
        'G

        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            REM .Replacement.ClearFormatting.Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zWild
            '.Format = True
            .Forward = True
            .Wrap = wdFindContinue
            .Execute Replace:=wdReplaceAll
        End With

        Selection.HomeKey Unit:=wdLine

    End Sub

    Sub ayReplace1(zFrom As String, zTo As String, zWild As Boolean, Optional Go2Top As Boolean = True)

        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zWild
            '.Format = True
            .Forward = True
            .Wrap = wdFindContinue
            .Execute Replace:=wdReplaceOne
        End With

        Selection.HomeKey Unit:=wdLine

    End Sub

    Sub ay_Q_ReplaceInAllParts()

        'ayReplaceInAllParts InputBox("From:"), InputBox("To:"), Val(InputBox("zWild: (Enter: True, False, 0, or 1)", , "False"))

    End Sub


    Sub ayReplaceInFootnotes(zFrom As String, zTo As String, zWild As Boolean)

        ayReplaceInPartAbridged(zFrom, zTo, zWild, wdFootnotesStory)

    End Sub


    '**************
    Sub ayReplaceInAllPartsAbridged(zFrom As String, zTo As String, zWild As Boolean)
        Dim myStoryRange As Range

        REM Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges
            With myStoryRange.Find
                .ClearFormatting()
                .Replacement.ClearFormatting()
                .Text = zFrom
                .Replacement.Text = zTo
                .MatchWildcards = zWild
                .Forward = True
                .Wrap = wdFindContinue
                .Execute Replace:=wdReplaceAll
            End With
        Next myStoryRange

    End Sub



    '*********
    Sub ayReplaceInPartAbridged(zFrom$, zTo$, Optional zWild As Boolean = False, Optional zPart As DifferentTypesOfStories = ayAll)
        Dim myStoryRange As Range

        REM Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges
            If myStoryRange.StoryType = zPart Then
                With myStoryRange.Find
                    .ClearFormatting()
                    .Replacement.ClearFormatting()
                    .Text = zFrom
                    .Replacement.Text = zTo
                    .MatchWildcards = zWild
                    .Forward = True
                    .Wrap = wdFindContinue
                    .Execute Replace:=wdReplaceAll
                End With
            End If
        Next myStoryRange

    End Sub














    '-------------
    '***Part 2:
    '***FIND
    '-------------






    Function ayFind(zwhat As String, ByVal zWild As Boolean, Optional ByVal Go2Top As Boolean = False) As Boolean
        'G

        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .Text = zwhat
            .Replacement.Text = ""
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            .Wrap = wdFindStop
            ayFind = .Execute
        End With

    End Function

    Function ayFindInPartAbridged(zwhat As String, zWild As Boolean, zPart As String) As Boolean
        Dim myStoryRange As Range

        REM Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges

            If myStoryRange.StoryType = zPart Then
                With myStoryRange.Find
                    .ClearFormatting()
                    .Text = zwhat
                    .Replacement.Text = ""
                    .MatchWildcards = zWild
                    .Forward = True
                    .Wrap = wdFindStop
                    .Execute()
                End With
            End If

        Next myStoryRange

        ayFindInPartAbridged = Len(Selection.Text) > 1

    End Function









    '-------------
    '***Part 3:
    '***zzz
    '-------------






    Function zzFindBack(zwhat As String, zWild As Boolean) As Boolean

        With Selection.Find
            .ClearFormatting()
            .Text = zwhat
            .Replacement.Text = ""
            .MatchWildcards = zWild
            .Forward = False
            'zFindBack = .Execute
        End With

    End Function

    Function zzFindInPart(zwhat As String, zWild As Boolean, zPart As String)
        Dim myStoryRange As Range

        REM Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges
            If myStoryRange.StoryType = zPart Then
                myStoryRange.Find.ClearFormatting()
                With myStoryRange.Find
                    .Text = zwhat
                    .Replacement.Text = ""
                    .MatchWildcards = zWild
                    .Forward = True
                    .Wrap = wdFindStop
                    .Execute()
                End With
            End If
        Next myStoryRange

        zzFindInPart = Len(Selection.Text) > 1

    End Function



    Sub zzReplace(zFrom As String, zTo As String, zWild As Boolean, Optional Go2Top As Boolean = True)

        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            '.Format = True
            .Execute Replace:=wdReplaceAll
        End With

        Selection.HomeKey Unit:=wdLine

    End Sub

    Sub zzReplace1(zFrom As String, zTo As String, zWild As Boolean, Optional Go2Top As Boolean = True)

        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            '.Format = True
            .Execute Replace:=wdReplaceOne
        End With

        Selection.HomeKey Unit:=wdLine

    End Sub

    Sub zzReplaceInAllParts(zFrom As String, zTo As String, zWild As Boolean)
        Dim myStoryRange As Range

        Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges

            With myStoryRange.Find
                .ClearFormatting()
                .Replacement.ClearFormatting()
                .Text = zFrom
                .Replacement.Text = zTo
                .MatchWildcards = zWild
                .Forward = True
                .Wrap = wdFindContinue
                .Execute Replace:=wdReplaceAll
            End With

        Next myStoryRange

    End Sub

    Sub zzReplaceInFootnotes(zFrom As String, zTo As String, zWild As Boolean)

        zzReplaceInPart(zFrom, zTo, zWild, wdFootnotesStory)

    End Sub

    Sub zzReplaceInPart(zFrom As String, zTo As String, zWild As Boolean, zPart As String)
        Dim myStoryRange As Range

        Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges
            If myStoryRange.StoryType = zPart Then
                With myStoryRange.Find
                    .ClearFormatting()
                    .Replacement.ClearFormatting()
                    .Text = zFrom
                    .Replacement.Text = zTo
                    .MatchWildcards = zWild
                    .Forward = True
                    .Wrap = wdFindContinue
                    .Execute Replace:=wdReplaceAll
                End With
            End If
        Next myStoryRange

    End Sub









    '-------------------------------------
    '***Part 4 (Last part):
    '***Needs work, or is not relevant.
    '-------------------------------------







    Sub ayReplaceInStyle(zstyle As String, zFrom As String, _
        zTo As String, zWild As Boolean, Optional zKill As Boolean = False)
        If StyleExist(zstyle) = False Then Exit Sub

        Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .style = ActiveDocument.Styles(zstyle)
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = zWild
            .Execute Replace:=wdReplaceAll
        End With

        If zKill Then ActiveDocument.Styles(zstyle).Delete()

    End Sub

    Sub ayReplaceStyle(zFrom As String, zstyle As String, zWild As Boolean)

        If StyleExist(zstyle) = False Then Exit Sub

        Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Replacement.style = ActiveDocument.Styles(zstyle)
            .Text = zFrom
            .Replacement.Text = ""
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .Execute Replace:=wdReplaceAll
            .ClearFormatting()
            .Replacement.ClearFormatting()
        End With

    End Sub

    Function ayFindStyle(zstyle As String, Optional Go2Top As Boolean = False) As Boolean

        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .style = ActiveDocument.Styles(zstyle)
            .Text = ""
            .Replacement.Text = ""
            .MatchWildcards = False
            .Forward = True
            '.Wrap = wdFindContinue
            .Wrap = wdFindStop
        End With

        ayFindStyle = Selection.Find.Execute
        REM ayFindStyle = Len(Selection.Text) > 1

    End Function

    Function ay_zzFindInPartAbridged(zwhat As String, zWild As Boolean, zPart As String)
        Dim myStoryRange As Range

        Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges
            If myStoryRange.StoryType = zPart Then
                With myStoryRange.Find
                    .ClearFormatting()
                    .Text = zwhat
                    .Replacement.Text = ""
                    .MatchWildcards = zWild
                    .Forward = True
                    .Wrap = wdFindStop
                    ay_zzFindInPartAbridged = .Execute
                End With
            End If
        Next myStoryRange

    End Function

    Sub zzMakeSame() ' Link all headers to first
        Dim J As Integer
        Dim K As Integer
        '    If ActiveDocument.Sections.Count > 2 Then
        For J = 2 To ActiveDocument.Sections.Count
            For K = 1 To ActiveDocument.Sections(J).Headers.Count
                ActiveDocument.Sections(J).Headers(K).LinkToPrevious = True
            Next K


            '            For K = 1 To ActiveDocument.Sections(J).Footers.Count
            '                ActiveDocument.Sections(J).Footers(K).LinkToPrevious = True
            '           Next K
        Next J
        'mDone

        '    End If
    End Sub

    Sub zzUnLinkHeaders()
        Dim J As Integer
        Dim K As Integer
        '    If ActiveDocument.Sections.Count > 2 Then
        For J = 2 To ActiveDocument.Sections.Count
            For K = 1 To ActiveDocument.Sections(J).Headers.Count
                ActiveDocument.Sections(J).Headers(K).LinkToPrevious = False
            Next K
            '            For K = 1 To ActiveDocument.Sections(J).Footers.Count
            '                ActiveDocument.Sections(J).Footers(K).LinkToPrevious = True
            '           Next K
        Next J
        '    End If
        'mDone

    End Sub

    Function zzFindStyle(zstyle As String, Optional Go2Top As Boolean = False) As Boolean

        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .style = ActiveDocument.Styles(zstyle)
            .Text = ""
            .Replacement.Text = ""
            .MatchWildcards = False
            .Forward = True
            '.Wrap = wdFindContinue
            .Wrap = wdFindStop
            zzFindStyle = .Execute
        End With

    End Function

    Sub zzFoldStyle(ZfromStyle As String, ZtoStyle As String, Optional zKill As Boolean = True) 'Folds 1 style into another

        If StyleExist(ZfromStyle) = False Then Exit Sub

        If StyleExist(ZtoStyle) = False Then : zReplaceStyle(ZfromStyle, ZtoStyle, False) : Exit Sub

            Selection.HomeKey Unit:=wdStory

            With Selection.Find
                .ClearFormatting()
                .style = ActiveDocument.Styles(ZfromStyle)
                .Replacement.ClearFormatting()
                .Replacement.style = ActiveDocument.Styles(ZtoStyle)
                .Text = ""
                .Replacement.Text = ""
                .Forward = True
                .Wrap = wdFindContinue
                .Format = True
                .MatchWildcards = False
                .Execute Replace:=wdReplaceAll
            End With

            If zKill Then ActiveDocument.Styles(ZfromStyle).Delete()

    End Sub

    Sub zzMakeAllSeqFieldsSuperscript()

        With Selection
            .WholeStory()
            .Fields.ToggleShowCodes()
            With .Find
                .ClearFormatting()
                With .Replacement
                    .ClearFormatting()
                    With .Font
                        .Superscript = True
                        .Subscript = False
                    End With
                End With
                .Text = "^d^wSEQ"
                .Replacement.Text = ""
                .Forward = True
                .Wrap = wdFindContinue
                .Format = True
                .Execute Replace:=wdReplaceAll
            End With
            .WholeStory()
            .Fields.ToggleShowCodes()
            .Fields.Update()
            .EscapeKey()
        End With

    End Sub

    Sub zzReplaceInStyle(zstyle As String, zFrom As String, zTo As String, zWild As Boolean, Optional zKill As Boolean = False)

        If StyleExist(zstyle) = False Then Exit Sub

        Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .style = ActiveDocument.Styles(zstyle)
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = zWild
            .Execute Replace:=wdReplaceAll
        End With

        If zKill Then ActiveDocument.Styles(zstyle).Delete()

    End Sub

    Sub zzReplaceStyle(zFrom As String, zstyle As String, zWild As Boolean)

        If StyleExist(zstyle) = False Then Exit Sub

        Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Replacement.style = ActiveDocument.Styles(zstyle)
            .Text = zFrom
            .Replacement.Text = ""
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .Execute Replace:=wdReplaceAll
            .ClearFormatting()
            .Replacement.ClearFormatting()
        End With

    End Sub


    Sub MySampleStuff()

        Selection.Find.ClearFormatting()
        With Selection.Find
            .Text = "^f"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindAsk
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With

        Selection.Find.ClearFormatting()
        With Selection.Find
            .Text = "^f"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindAsk
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With


        Selection.HomeKey Unit:=wdStory
        Selection.Find.ClearFormatting()
        With Selection.Find
            .Text = "^f"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With

        Selection.Find.Execute()


        Selection.Find.ClearFormatting()
        With Selection.Find
            .Text = "[ ^013]^02"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchAllWordForms = False
            .MatchSoundsLike = False
            .MatchWildcards = True
        End With

        Selection.InlineShapes.AddOLEControl ClassType:="Forms.TextBox.1"
        Selection.MoveRight Unit:=wdCell
        Selection.TypeText Text:="gallonn"
        Selection.MoveRight Unit:=wdCell
        Selection.TypeText Text:="$49.99"
        Selection.MoveRight Unit:=wdCell
        Selection.TypeText Text:="on"
        Selection.TypeParagraph()
        Selection.TypeBackspace()
        Selection.InsertRowsBelow 1
        Selection.MoveRight(Unit:=wdCharacter, Count:=1)
        Selection.MoveUp(Unit:=wdLine, Count:=2)
        Selection.EndKey Unit:=wdLine
        Selection.EndKey Unit:=wdStory
        Selection.MoveUp(Unit:=wdLine, Count:=10)
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
        Selection.InsertRows 1
        Selection.Collapse direction:=wdCollapseStart
        Selection.EndKey Unit:=wdLine
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
        Selection.MoveDown(Unit:=wdLine, Count:=2)
        Selection.TypeText Text:="sd"
        Selection.MoveRight(Unit:=wdCharacter, Count:=1)
        Selection.InsertRows 1
        Selection.Collapse direction:=wdCollapseStart
        With Selection.Font
            .name = "+Body"
            .Size = 14
            .Bold = False
            .Italic = False
            .Underline = wdUnderlineNone
            .UnderlineColor = wdColorAutomatic
            .StrikeThrough = False
            .DoubleStrikeThrough = False
            .Outline = False
            .Emboss = False
            .Shadow = False
            .Hidden = True
            .SmallCaps = False
            .AllCaps = False
            .Color = wdColorAutomatic
            .Engrave = False
            .Superscript = False
            .Subscript = False
            .Spacing = 0
            .Scaling = 100
            .Position = 0
            .Kerning = 0
            .Animation = wdAnimationNone
            .SizeBi = 14
            .NameBi = "+Body CS"
            .BoldBi = False
            .ItalicBi = False
        End With
        Selection.TypeText Text:="5"
        With Selection.Font
            .name = "+Body"
            .Size = 14
            .Bold = False
            .Italic = False
            .Underline = wdUnderlineNone
            .UnderlineColor = wdColorAutomatic
            .StrikeThrough = False
            .DoubleStrikeThrough = False
            .Outline = False
            .Emboss = False
            .Shadow = False
            .Hidden = False
            .SmallCaps = False
            .AllCaps = False
            .Color = wdColorAutomatic
            .Engrave = False
            .Superscript = False
            .Subscript = False
            .Spacing = 0
            .Scaling = 100
            .Position = 0
            .Kerning = 0
            .Animation = wdAnimationNone
            .SizeBi = 14
            .NameBi = "+Body CS"
            .BoldBi = False
            .ItalicBi = False
        End With
        Selection.MoveRight Unit:=wdCell
        Selection.TypeText Text:="aaaa"
        Selection.MoveRight Unit:=wdCell
        Selection.InlineShapes.AddOLEControl ClassType:="Forms.TextBox.1"
        Selection.MoveRight Unit:=wdCell
        Selection.TypeText Text:="aas"
        Selection.MoveRight Unit:=wdCell
        Selection.Font.Bold = wdToggle
        Selection.Font.BoldBi = wdToggle
        Selection.Font.Color = 10498160
        Selection.TypeText Text:="848"
        Selection.MoveRight Unit:=wdCell
        Selection.Font.Color = wdColorAutomatic
        Selection.TypeText Text:="hh"
        Selection.MoveRight(Unit:=wdCharacter, Count:=1)
        Selection.InsertRows 1
        Selection.Collapse direction:=wdCollapseStart

        Selection.MoveUp(Unit:=wdLine, Count:=1)
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
        Selection.MoveUp(Unit:=wdLine, Count:=2)
        Selection.MoveLeft(Unit:=wdCharacter, Count:=2)
        Selection.MoveRight(Unit:=wdCharacter, Count:=1)
        Selection.MoveDown(Unit:=wdLine, Count:=1)
        Selection.GoTo(What:=wdGoToBookmark, name:="NumberOfProducts")
        With ActiveDocument.Bookmarks
            .DefaultSorting = wdSortByName
            .ShowHidden = True
        End With

        Selection.MoveRight(Unit:=wdCharacter, Count:=2)
        Selection.TypeParagraph()
        Selection.TypeText Text:=Chr(11)

        Selection.MoveUp(Unit:=wdLine, Count:=1)
        Selection.MoveDown(Unit:=wdLine, Count:=1)
        Selection.MoveRight(Unit:=wdCharacter, Count:=18)
        Selection.MoveRight(Unit:=wdCharacter, Count:=12)

        'Stop
        ' aaaaaapp Macro
        '
        '

        'qqqqqqqqqqqqqqqq
        'qqqqqqqqqqqqqq
        'qqqqqqqqqqqqqqq
        'qqqqqqqqqqqqqq
        'qqqqqqqqqqqqq
        '
        Dim x
        'MsgBox wdMainTextStory
        'x = zFindInPart("^013^f", False, wdMainTextStory)
        'x = ayFind("a", False, False)
        x = zFindInPart("a", False, wdMainTextStory)
        'x = zFindInPart("ù", False, wdTextFrameStory)

    End Sub







Sub zReplaceAnything_Confirm(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, zzWild As zWild, Optional WhatPart As TheStory)     ' = zMainFootnotesAndEndnotes)
        Dim x%, ThereIsAMatch As Boolean

        'Checks for error--user left out zFrom and zFromStyle:
        If zFrom = "" And ZfromStyle = "" Then Exit Sub

        If WhatPart = zMain Or WhatPart = zFootnotesAndEndnotes Then
            zReplaceAnything_Confirm_QQQQ(zFrom, zTo, ZfromStyle, ZtoStyle, zWildOff, WhatPart)
        Else
            zReplaceAnything_Confirm_QQQQ(zFrom, zTo, ZfromStyle, ZtoStyle, zWildOff, zMain)
            zReplaceAnything_Confirm_QQQQ(zFrom, zTo, ZfromStyle, ZtoStyle, zWildOff, zFootnotesAndEndnotes)
        End If

        Selection.Collapse direction:=wdCollapseEnd
    End Sub
    Sub zReplaceAnything_Confirm_QQQQ(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, zzWild As zWild, WhatPart As TheStory)
        Dim x%, ThereIsAMatch As Boolean
        Dim oRange As Range

        oRange = ActiveDocument.StoryRanges(WhatPart)
        oRange.Select()
        Selection.Collapse direction:=wdCollapseStart

        Do
            ThereIsAMatch = ayFind(zFrom, zzWild, ZfromStyle)
            If ThereIsAMatch = True Then

                x = Msgbox("Do you want to replace the current selection """ & zFrom & """ to """ & zTo & """?", vbYesNoCancel)
                Select Case x
                    Case vbCancel
                        Exit Do
                    Case vbNo
                        'user chose not to replace, so do nothing, just loop to the next find
                        Selection.Collapse direction:=wdCollapseEnd
                    Case vbYes
                        'Since we just did a Find, the match will be highlighted, so it's necessary to collapse the selection.
                        Selection.Collapse direction:=wdCollapseStart
                        zReplaceOne(zFrom, zTo, ZfromStyle, ZtoStyle, zzWild)
                        Selection.Collapse direction:=wdCollapseEnd  'I don't think you need this, beacuse the next found is not automatically highlighted without another find.execute
                End Select
            End If
        Loop While ThereIsAMatch = True

        Selection.Collapse direction:=wdCollapseEnd
    End Sub
Sub zReplaceAnything_No_Confirm(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, zzWild As zWild, Optional WhatPart As TheStory)  '= zMainFootnotesAndEndnotes)
        If zFrom = "" And ZfromStyle = "" Then Exit Sub 'Checks for error--user left out zFrom and zFromStyle:

        If WhatPart = zMain Or WhatPart = zFootnotesAndEndnotes Then
            zReplaceAll(zFrom, zTo, ZfromStyle$, ZtoStyle$, zzWild, WhatPart)
        Else
            zReplaceAll(zFrom, zTo, ZfromStyle$, ZtoStyle$, zzWild, zMain)
            zReplaceAll(zFrom, zTo, ZfromStyle$, ZtoStyle$, zzWild, zFootnotesAndEndnotes)
        End If
    End Sub









    Sub aayReplaceInPart_Comprehensive(zFrom$, zTo$, Optional TheRangeStory As DifferentTypesOfStories = ayAll, Optional zWild As Boolean = False)     '%=integer; $=string; &=long
        Dim rngStory As Word.Range, lngJunk&, oShp As Shape


        'Iterate through all story types in the current document
        For Each rngStory In ActiveDocument.StoryRanges
            'Iterate through all linked stories
            Do
                If rngStory.StoryType = TheRangeStory Or TheRangeStory = ayAll Then
                    aySearchAndReplaceInStory(rngStory, zFrom, zTo, zWild)
                End If

                'Get next linked story (if any)
                rngStory = rngStory.NextStoryRange

            Loop Until rngStory Is Nothing
        Next


    End Sub


    '******
    Sub aaySearchAndReplaceInStory(rngStory As Word.Range, zFrom$, zTo$, Optional zWild As Boolean = False)

        With rngStory.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            .Execute Replace:=wdReplaceAll
        End With

        'MsgBox Str$(rngStory.StoryType)




        '1 wdMainTextStory
        '2 wdFootnotesStory
        '3 wdEndnotesStory
        '4 wdCommentsStory
        '5 wdTextFrameStory
        '6 wdEvenPagesHeaderStory
        '7 wdPrimaryHeaderStory
        '8 wdEvenPagesFooterStory
        '9 wdPrimaryFooterStory
        '10 wdFirstPageHeaderStory
        '11 wdFirstPageFooterStory
        '12 wdFootnoteSeparatorStory
        '13 wdFootnoteContinuationSeparatorStory
        '14 wdFootnoteContinuationNoticeStory
        '15 wdEndnoteSeparatorStory
        '16 wdEndnoteContinuationSeparatorStory
        '17 wdEndnoteContinuationNoticeStory

    End Sub



    Sub aqq()

        Dim oRange As Range
        oRange = ActiveDocument.StoryRanges(Trim$(str$(InputBox$("Story type?"))))
        oRange.Select()
        Selection.Collapse direction:=wdCollapseStart

        'Selection.GoTo what:=WdGoToItem.wdGoToObject name:=wd
        'Selection.GoTo ActiveDocument.StoryRanges(wdFootnotesStory)


    End Sub


    'This should "replace" all previous zReplace procedures:
Sub zReplaceAnything(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, zWild As zWild, Go2Top As Go2Top, Confirm As ConfirmAll, _
    Optional WhatPart As TheStory)  ' = zMainFootnotesAndEndnotes)
        Dim x%

        If zFrom = "" And ZfromStyle = "" Then
            Msgbox "!?" & ChrW$(SHIN) & ChrW$(1497) & ChrW$(1497) & ChrW$(1499) & ChrW$(vav) & ChrW$(1514)
            Exit Sub
        End If

        If Not Confirm Then Application.ScreenUpdating = False

        If Go2Top Then Selection.HomeKey Unit:=wdStory

        Do
            With Selection.Find
                .ClearFormatting()
                .Replacement.ClearFormatting()
                .Text = zFrom
                .style = ActiveDocument.Styles(ZfromStyle)
                .MatchWildcards = zWild
                '.Format = True
                .Forward = True
                .Wrap = wdFindStop          'DON'T go back to beginning.  We already did Go2Top
                'first find, without replacing
                REM  .Execute2007  'what's this??

                If .Execute And (Selection.StoryType = WhatPart Or WhatPart = zMainFootnotesAndEndnotes) Then       ' a match was found in the right story
                    x = vbYes
                    If Confirm Then x = Msgbox("Do you want to replace the current selection """ & zFrom & """ to """ & zTo & """?", vbYesNoCancel)
                    Select Case x
                        Case vbCancel
                            Exit Do
                        Case vbNo
                            'user chose not to replace, so do nothing, just loop to the next find
                            Selection.Collapse direction:=wdCollapseEnd 'MoveRight Unit:=wdCharacter, count:=1
                        Case vbYes
                            'manual replace
                            'Selection.TypeText Text:=zTo

                            'on 2nd thought, do a real replace:
                            .Replacement.style = ActiveDocument.Styles(ZtoStyle)
                            .Replacement.Text = zTo

                            Selection.Collapse direction:=wdCollapseStart
                            .Execute Replace:=wdReplaceOne
                            '.Collapse Direction:=wdCollapseStart  I don't think you need this, beacuse the next found is not automatically highlighted without another find.execute


                    End Select

                Else    ' a match was not found, or not in the right story.
                    'no message
                    'MsgBox "There are no " & AtLeastOnce & "instances of """ & zFrom & """ in the current document."
                End If
            End With
        Loop


        Selection.Collapse direction:=wdCollapseEnd 'MoveLeft Unit:=wdCharacter, count:=1

        Application.ScreenUpdating = True

    End Sub







    '.Replacement.Text = "ZZZZZZ^l^l"


    Sub TryThisOut()
        'Dim r As Object

        'Set r = CreateObject("Wscript.Shell")

        'zReplaceAnything "", "", "", "", zWildOff, zYesGo2Top, zFootnotesAndEndnotes, zPrompt_ON, zReplaceAll
        'r.echo "Hello"

        'MsgBox wdReplaceOne  '1
        'MsgBox wdReplaceAll '2
        'MsgBox wdReplaceNone '0

        'MSForms.

        Msgbox wdCollapseEnd

        Exit Sub

        Msgbox wdFindAsk '2
        Msgbox wdFindContinue '1
        Msgbox wdFindStop '0


        'Selection.StoryType

        '1 wdMainTextStory
        '2 wdFootnotesStory
        '3 wdEndnotesStory
        '4 wdCommentsStory
        '5 wdTextFrameStory
        '6 wdEvenPagesHeaderStory
        '7 wdPrimaryHeaderStory
        '8 wdEvenPagesFooterStory
        '9 wdPrimaryFooterStory
        '10 wdFirstPageHeaderStory
        '11 wdFirstPageFooterStory
        '12 wdFootnoteSeparatorStory
        '13 wdFootnoteContinuationSeparatorStory
        '14 wdFootnoteContinuationNoticeStory
        '15 wdEndnoteSeparatorStory
        '16 wdEndnoteContinuationSeparatorStory
        '17 wdEndnoteContinuationNoticeStory


    End Sub
    'This should "replace" all previous zReplace procedures:
    'So far, every argument is mandatory, with no defaults.
    Sub zReplaceAnything__GoodButClunky(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, _
        zWild As zWild, Go2Top As Go2Top, _
        WhatPart As TheStory, Confirm As ConfirmAll)   ', Optional HowManyTimes As DoAll) ' = zReplaceAll)  'not necessary


        ' the " _" at the end of the previous line means, "continue on next line."  You can add it all together if you want.  My screen is smaller.
        Dim x%, AtLeastOnce$


        If zFrom = "" And ZfromStyle = "" Then   'my excel chart of all 16 binary possibilities can be confusing.  It really just boils down to these 2 conditions
            Msgbox SHAYCHUS
            Exit Sub
        End If


        'not needed anymore because go2top is set to wdFindContinue or wdFindStop, so it's used as .Wrap
        'If Go2Top Then Selection.HomeKey Unit:=wdStory

        'MsgBox Selection.StoryType

        If Not Confirm Then Application.ScreenUpdating = False

        Do
            With Selection.Find
                .ClearFormatting()
                .Text = zFrom
                '.Replacement.ClearFormatting.Text = zFrom
                '.Replacement.Text = zTo
                '.Replacement.Text = ""
                .MatchWildcards = zWild
                '.Format = True
                .Forward = True
                .Wrap = Go2Top
                .Execute()        'first find, without replacing
                REM  .Execute2007  'what's this??

                If .found And (Selection.StoryType = WhatPart Or WhatPart = zMainFootnotesAndEndnotes) Then       ' a match was found in the right story
                    x = vbYes
                    If Confirm Then x = Msgbox("Do you want to replace the current selection """ & zFrom & """ to """ & zTo & """?", vbYesNoCancel)
                    Select Case x       'select case is a fancy if...then...elseif statement.  In the second condition of an elseif--it could be a different condition.  For eg., if a=5...elseif b=8.  In contrast select case is always evaluating the same expression, in our case, x.  If x=a...if x=b...if x=c.
                        Case vbCancel
                            Exit Do
                        Case vbNo
                            'user chose not to replace, so do nothing, just loop to the next find
                        Case vbYes
                            'replace
                            '.Execute Replace:=wdReplaceOne  'instead of "HowManyTimes"
                            'manual replace
                            Selection.TypeText Text:=zTo
                            'Selection.Find.Execute Replace:=wdReplaceOne
                            AtLeastOnce = "more "
                    End Select

                Else                                                                        ' a match was not found, or not in the chosen story.
                    Msgbox "There are no " & AtLeastOnce & "instances of """ & zFrom & """ in the current document."
                End If
            End With
        Loop 'While Selection.Find.Found


        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)     'clears selection

        Application.ScreenUpdating = True

    End Sub



    Sub haw()
        Dim myStoryRange As Word.Range
        Dim zFrom, zTo, zWild, zPart, zwhat, Go2Top, zstyle, zKill





        For Each myStoryRange In ActiveDocument.StoryRanges
            With myStoryRange.Find
                .ClearFormatting()
                .Replacement.ClearFormatting()
                .Text = zFrom
                .Replacement.Text = zTo
                .MatchWildcards = zWild
                .Forward = True
                .Wrap = wdFindContinue
                .Execute Replace:=wdReplaceAll
            End With
        Next myStoryRange


        For Each myStoryRange In ActiveDocument.StoryRanges
            If myStoryRange.StoryType = zPart Then
                With myStoryRange.Find
                    .ClearFormatting()
                    .Replacement.ClearFormatting()
                    .Text = zFrom
                    .Replacement.Text = zTo
                    .MatchWildcards = zWild
                    .Forward = True
                    .Wrap = wdFindContinue
                    .Execute Replace:=wdReplaceAll
                End With
            End If
        Next myStoryRange



        'find:

        REM Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges

            If myStoryRange.StoryType = zPart Then
                With myStoryRange.Find
                    .ClearFormatting()
                    .Text = zwhat
                    .Replacement.Text = ""
                    .MatchWildcards = zWild
                    .Forward = True
                    .Wrap = wdFindStop
                    .Execute()
                End With
            End If

        Next myStoryRange

        'ayFindInPartAbridged = Len(Selection.Text) > 1

        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            '.Format = True
            .Execute Replace:=wdReplaceAll
        End With

        Selection.HomeKey Unit:=wdLine




        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            '.Format = True
            .Execute Replace:=wdReplaceOne
        End With

        Selection.HomeKey Unit:=wdLine



        If StyleExist(zstyle) = False Then Exit Sub

        Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .style = ActiveDocument.Styles(zstyle)
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = zWild
            .Execute Replace:=wdReplaceAll
        End With

        If zKill Then ActiveDocument.Styles(zstyle).Delete()



        If StyleExist(zstyle) = False Then Exit Sub

        Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Replacement.style = ActiveDocument.Styles(zstyle)
            .Text = zFrom
            .Replacement.Text = ""
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .Execute Replace:=wdReplaceAll
            .ClearFormatting()
            .Replacement.ClearFormatting()
        End With


        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .style = ActiveDocument.Styles(zstyle)
            .Text = ""
            .Replacement.Text = ""
            .MatchWildcards = False
            .Forward = True
            '.Wrap = wdFindContinue
            .Wrap = wdFindStop
        End With

        'ayFindStyle = Selection.Find.Execute
        REM ayFindStyle = Len(Selection.Text) > 1





        'bbbbbbbbbbbb then


        'function ay_zzFindInPartAbridged(zWhat As String, zWild As Boolean, zPart As String)


        Selection.HomeKey Unit:=wdStory

        For Each myStoryRange In ActiveDocument.StoryRanges
            If myStoryRange.StoryType = zPart Then
                With myStoryRange.Find
                    .ClearFormatting()
                    .Text = zwhat
                    .Replacement.Text = ""
                    .MatchWildcards = zWild
                    .Forward = True
                    .Wrap = wdFindStop
                    'zFindInPart = .Execute
                End With
            End If
        Next myStoryRange
    End Sub


    Function azzFindStyle(zstyle As String, Optional Go2Top As Boolean = False) As Boolean

        If Go2Top Then Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .style = ActiveDocument.Styles(zstyle)
            .Text = ""
            .Replacement.Text = ""
            .MatchWildcards = False
            .Forward = True
            '.Wrap = wdFindContinue
            .Wrap = wdFindStop
            azzFindStyle = .Execute
        End With

    End Function

    Sub aazzFoldStyle(ZfromStyle As String, ZtoStyle As String, Optional zKill As Boolean = True) 'Folds 1 style into another

        If StyleExist(ZfromStyle) = False Then Exit Sub

        If StyleExist(ZtoStyle) = False Then : zReplaceStyle(ZfromStyle, ZtoStyle, False) : Exit Sub

            Selection.HomeKey Unit:=wdStory

            With Selection.Find
                .ClearFormatting()
                .style = ActiveDocument.Styles(ZfromStyle)
                .Replacement.ClearFormatting()
                .Replacement.style = ActiveDocument.Styles(ZtoStyle)
                .Text = ""
                .Replacement.Text = ""
                .Forward = True
                .Wrap = wdFindContinue
                .Format = True
                .MatchWildcards = False
                .Execute Replace:=wdReplaceAll
            End With

            If zKill Then ActiveDocument.Styles(ZfromStyle).Delete()

    End Sub


    Sub qzzReplaceInStyle(zstyle As String, zFrom As String, zTo As String, zWild As Boolean, Optional zKill As Boolean = False)

        If StyleExist(zstyle) = False Then Exit Sub

        Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .style = ActiveDocument.Styles(zstyle)
            .Replacement.ClearFormatting()
            .Text = zFrom
            .Replacement.Text = zTo
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = zWild
            .Execute Replace:=wdReplaceAll
        End With

        If zKill Then ActiveDocument.Styles(zstyle).Delete()

    End Sub

    Sub wzzReplaceStyle(zFrom As String, zstyle As String, zWild As Boolean)

        If StyleExist(zstyle) = False Then Exit Sub

        Selection.HomeKey Unit:=wdStory

        With Selection.Find
            .ClearFormatting()
            .Replacement.ClearFormatting()
            .Replacement.style = ActiveDocument.Styles(zstyle)
            .Text = zFrom
            .Replacement.Text = ""
            .MatchWildcards = zWild
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .Execute Replace:=wdReplaceAll
            .ClearFormatting()
            .Replacement.ClearFormatting()
        End With

    End Sub


    Sub dMySampleStuff()

        Selection.Find.ClearFormatting()
        With Selection.Find
            .Text = "^f"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindAsk
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With

        Selection.Find.ClearFormatting()
        With Selection.Find
            .Text = "^f"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindAsk
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With


        Selection.HomeKey Unit:=wdStory
        Selection.Find.ClearFormatting()
        With Selection.Find
            .Text = "^f"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
        End With

        Selection.Find.Execute()


        Selection.Find.ClearFormatting()
        With Selection.Find
            .Text = "[ ^013]^02"
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = False
            .MatchKashida = False
            .MatchDiacritics = False
            .MatchAlefHamza = False
            .MatchControl = False
            .MatchAllWordForms = False
            .MatchSoundsLike = False
            .MatchWildcards = True
        End With

        Selection.InlineShapes.AddOLEControl ClassType:="Forms.TextBox.1"
        Selection.MoveRight Unit:=wdCell
        Selection.TypeText Text:="gallonn"
        Selection.MoveRight Unit:=wdCell
        Selection.TypeText Text:="$49.99"
        Selection.MoveRight Unit:=wdCell
        Selection.TypeText Text:="on"
        Selection.TypeParagraph()
        Selection.TypeBackspace()
        Selection.InsertRowsBelow 1
        Selection.MoveRight(Unit:=wdCharacter, Count:=1)
        Selection.MoveUp(Unit:=wdLine, Count:=2)
        Selection.EndKey Unit:=wdLine
        Selection.EndKey Unit:=wdStory
        Selection.MoveUp(Unit:=wdLine, Count:=10)
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
        Selection.InsertRows 1
        Selection.Collapse direction:=wdCollapseStart
        Selection.EndKey Unit:=wdLine
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
        Selection.MoveDown(Unit:=wdLine, Count:=2)
        Selection.TypeText Text:="sd"
        Selection.MoveRight(Unit:=wdCharacter, Count:=1)
        Selection.InsertRows 1
        Selection.Collapse direction:=wdCollapseStart
        With Selection.Font
            .name = "+Body"
            .Size = 14
            .Bold = False
            .Italic = False
            .Underline = wdUnderlineNone
            .UnderlineColor = wdColorAutomatic
            .StrikeThrough = False
            .DoubleStrikeThrough = False
            .Outline = False
            .Emboss = False
            .Shadow = False
            .Hidden = True
            .SmallCaps = False
            .AllCaps = False
            .Color = wdColorAutomatic
            .Engrave = False
            .Superscript = False
            .Subscript = False
            .Spacing = 0
            .Scaling = 100
            .Position = 0
            .Kerning = 0
            .Animation = wdAnimationNone
            .SizeBi = 14
            .NameBi = "+Body CS"
            .BoldBi = False
            .ItalicBi = False
        End With
        Selection.TypeText Text:="5"
        With Selection.Font
            .name = "+Body"
            .Size = 14
            .Bold = False
            .Italic = False
            .Underline = wdUnderlineNone
            .UnderlineColor = wdColorAutomatic
            .StrikeThrough = False
            .DoubleStrikeThrough = False
            .Outline = False
            .Emboss = False
            .Shadow = False
            .Hidden = False
            .SmallCaps = False
            .AllCaps = False
            .Color = wdColorAutomatic
            .Engrave = False
            .Superscript = False
            .Subscript = False
            .Spacing = 0
            .Scaling = 100
            .Position = 0
            .Kerning = 0
            .Animation = wdAnimationNone
            .SizeBi = 14
            .NameBi = "+Body CS"
            .BoldBi = False
            .ItalicBi = False
        End With
        Selection.MoveRight Unit:=wdCell
        Selection.TypeText Text:="aaaa"
        Selection.MoveRight Unit:=wdCell
        Selection.InlineShapes.AddOLEControl ClassType:="Forms.TextBox.1"
        Selection.MoveRight Unit:=wdCell
        Selection.TypeText Text:="aas"
        Selection.MoveRight Unit:=wdCell
        Selection.Font.Bold = wdToggle
        Selection.Font.BoldBi = wdToggle
        Selection.Font.Color = 10498160
        Selection.TypeText Text:="848"
        Selection.MoveRight Unit:=wdCell
        Selection.Font.Color = wdColorAutomatic
        Selection.TypeText Text:="hh"
        Selection.MoveRight(Unit:=wdCharacter, Count:=1)
        Selection.InsertRows 1
        Selection.Collapse direction:=wdCollapseStart

        Selection.MoveUp(Unit:=wdLine, Count:=1)
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
        Selection.MoveUp(Unit:=wdLine, Count:=2)
        Selection.MoveLeft(Unit:=wdCharacter, Count:=2)
        Selection.MoveRight(Unit:=wdCharacter, Count:=1)
        Selection.MoveDown(Unit:=wdLine, Count:=1)
        Selection.GoTo(What:=wdGoToBookmark, name:="NumberOfProducts")
        With ActiveDocument.Bookmarks
            .DefaultSorting = wdSortByName
            .ShowHidden = True
        End With

        Selection.MoveRight(Unit:=wdCharacter, Count:=2)
        Selection.TypeParagraph()
        Selection.TypeText Text:=Chr(11)

        Selection.MoveUp(Unit:=wdLine, Count:=1)
        Selection.MoveDown(Unit:=wdLine, Count:=1)
        Selection.MoveRight(Unit:=wdCharacter, Count:=18)
        Selection.MoveRight(Unit:=wdCharacter, Count:=12)

        'Stop
        ' aaaaaapp Macro
        '
        '

        'qqqqqqqqqqqqqqqq
        'qqqqqqqqqqqqqq
        'qqqqqqqqqqqqqqq
        'qqqqqqqqqqqqqq
        'qqqqqqqqqqqqq
        '
        Dim x
        'MsgBox wdMainTextStory
        'x = zFindInPart("^013^f", False, wdMainTextStory)
        'x = ayFind("a", False, False)
        x = zFindInPart("a", False, wdMainTextStory)
        'x = zFindInPart("ù", False, wdTextFrameStory)

    End Sub
    Function StyleExist(ByVal zstyle As String) As Boolean
        Dim sty As style
        StyleExist = True

        If zstyle = "" Then Exit Function

        For Each sty In ActiveDocument.Styles
            If sty = zstyle Then Exit Function
        Next

        StyleExist = False
        'Stop
    End Function
    Function zFind(zwhat As String, zWild As Boolean, Optional Go2Top As Boolean = False) As Boolean
        If Go2Top Then Selection.HomeKey Unit:=wdStory

        zClearFind()
        With Selection.Find
            .Text = zwhat
            .Replacement.Text = ""
            .MatchWildcards = zWild
            .Forward = True
            '        .Wrap = wdFindContinue
            .Wrap = wdFindStop
        End With
        Selection.Find.Execute()
        zFind = Len(Selection.Text) > 1
    End Function





End Module
