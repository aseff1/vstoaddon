﻿Module GeneralUtilities
    Option Explicit
    Sub RemoveAllHighlighting()

        'Selection.Range.HighlightColorIndex = wdNoHighlight

        Selection.Find.ClearFormatting()
        Selection.Find.Highlight = True
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = ""
            .Replacement.Text = "^&"
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll

    End Sub
    Sub PrintAutoCorrect()
        Dim a As AutoCorrectEntry

        Selection.ParagraphFormat.TabStops.ClearAll()
        Selection.ParagraphFormat.TabStops.Add(Position:=72, _
          Alignment:=wdAlignTabLeft, Leader:=wdTabLeaderSpaces)

        For Each a In Application.AutoCorrect.Entries
            Selection.TypeText a.name & vbTab & a.Value & " " & vbCr
        Next
    End Sub

    Sub DeleteAllComments()
        Dim aComment
        For Each aComment In ActiveDocument.Comments
            aComment.Delete()
        Next
    End Sub

    Sub PrintCustomStyles()
        Dim docThis As Document
        Dim styItem As style
        Dim sUserDef(499) As String
        Dim iStyleCount As Integer
        Dim J As Integer

        ' Ref the active document
        docThis = ActiveDocument

        iStyleCount = 0
        For Each styItem In docThis.Styles
            'see if being used
            If styItem.InUse Then
                'make sure not built in
                If Not styItem.BuiltIn Then
                    iStyleCount = iStyleCount + 1
                    sUserDef(iStyleCount) = styItem.NameLocal
                End If
            End If
        Next styItem

        If iStyleCount > 0 Then
            ' Create the output document
            Documents.Add()

            Selection.TypeText "User-defined Styles In Use"
            Selection.TypeParagraph()
            For J = 1 To iStyleCount
                Selection.TypeText sUserDef(J)
                Selection.TypeParagraph()
            Next J
            Selection.TypeParagraph()
            Selection.TypeParagraph()
        Else
            Msgbox "No custom styles in use."
        End If
    End Sub

    'Sub ReplaceWithAUTOTEXTII()
    'Dim oRng As Word.Range
    'Dim strFind() As String
    'Dim i As Long
    'strFind() = Split("student|your name", "|")
    'For i = 0 To UBound(strFind)
    '  Set oRng = ActiveDocument.Range
    '  With oRng.Find
    '    .ClearFormatting
    '    .Replacement.ClearFormatting
    '    .Text = strFind(i)
    '    .Forward = True
    '    .Wrap = wdFindContinue
    '    .Format = False
    '    .MatchCase = False
    '    .MatchWholeWord = False
    '    .MatchWildcards = False
    '    .MatchSoundsLike = False
    '    .MatchAllWordForms = False
    '    While .Execute
    '      ActiveDocument.AttachedTemplate.BuildingBlockEntries("Confidential").Insert Where:=oRng, RichText:=True
    '    Wend
    '  End With
    'Next
    'End Sub


    'useful code
    'Application.ScreenUpdating = False
    'Application.ScreenUpdating = true



    'Sub PrintAutoCorrect()
    '    Dim a As AutoCorrectEntry
    '
    '    Selection.ParagraphFormat.TabStops.ClearAll
    '    Selection.ParagraphFormat.TabStops.Add Position:=72, _
    '      Alignment:=wdAlignTabLeft, Leader:=wdTabLeaderSpaces
    '
    '    For Each a In Application.AutoCorrect.Entries
    '        Selection.TypeText a.Name & vbTab & a.Value & " " & vbCr
    '    Next
    'End Sub

    Sub ChangeLooksLikeKddSmalltoKddSmall()
        Dim c As Word.Range
        For Each c In ActiveDocument.Characters
            If c.Font.Shading.BackgroundPatternColor = RGB(226, 242, 246) Then
                c.style = "kdd small"
            End If
        Next c
    End Sub
    Sub ChangeLookLikeMokorSmalltoMokorSmall()
        If Not GetIntoFootnotes Then
            Exit Sub
        Else
            Selection.Find.ClearFormatting()
            Selection.Find.Font.Color = wdColorRed
            Selection.Find.Replacement.ClearFormatting()
            Selection.Find.Replacement.style = ActiveDocument.Styles("mokor small")
            With Selection.Find
                .Text = ""
                .Replacement.Text = ""
                .Forward = True
                .Wrap = wdFindContinue
                .Format = True
                .MatchWildcards = False
            End With
            Selection.Find.Execute Replace:=wdReplaceAll
        End If
    End Sub


    Sub SetDefaults()
        ActiveDocument.ShowSpellingErrors = True

        'puts footnotes directly bneath text:
        With ActiveDocument.Range(Start:=ActiveDocument.Content.Start, End:=ActiveDocument.Content.End).FootnoteOptions
            .Location = wdBeneathText
            .NumberingRule = wdRestartContinuous
            .StartingNumber = 1
            .NumberStyle = wdNoteNumberStyleArabic
            .NumberingRule = wdRestartSection
        End With

    End Sub


    Sub RemoveAutoUpdate() 'AutoUpdate is Fatal to Styles
        Dim s As style
        For Each s In ActiveDocument.Styles
            If s.Type = wdStyleTypeParagraph Then
                s.AutomaticallyUpdate = False
            End If
        Next s
    End Sub
    Sub ChangeTemplates()
        Dim strDocPath As String
        Dim strTemplateB As String
        Dim strCurDoc As String
        Dim docCurDoc As Document

        ' set document folder path and template strings
        strDocPath = "C:\path to document folder\"
        strTemplateB = "C:\path to template\templateB.dotx"

        ' get first doc - only time need to provide file spec
        strCurDoc = Dir(strDocPath & "*.doc?")

        ' ready to loop (for as long as file found)
        Do While strCurDoc <> ""
            ' open file
            docCurDoc = Documents.Open(FileName:=strDocPath & strCurDoc)
            ' change the template
            docCurDoc.AttachedTemplate = strTemplateB
            ' save and close
            docCurDoc.Close wdSaveChanges
            ' get next file name
            strCurDoc = Dir
        Loop
        Msgbox "Finished"
    End Sub
    Sub aaShortCutList()
        Dialogs(wdDialogListCommands).Show()
    End Sub
    Sub ListStylesInUse()
        Dim sty As style
        For Each sty In ActiveDocument.Styles
            If sty.InUse = True Then
                Selection.HomeKey Unit:=wdStory
                zClearFind()
                Selection.Find.style = ActiveDocument.Styles(sty)
                With Selection.Find
                    .Text = ""
                    .Replacement.Text = ""
                    .Forward = True
                    .Wrap = wdFindStop
                    .Format = True
                End With
                Selection.Find.Execute()
                If Selection.Find.found = True Then
                    Selection.EndKey Unit:=wdStory
                    Selection.InsertAfter Text:=sty.NameLocal
                    Selection.InsertParagraphAfter()
                End If
            End If
        Next sty
    End Sub
    Public Sub ListFontsInDoc()
        Dim FontList(199) As String
        Dim FontCount As Integer
        Dim FontName As String
        Dim J As Integer, K As Integer, L As Integer
        Dim x As Long, y As Long
        Dim FoundFont As Boolean
        Dim rngChar As Range
        Dim strFontList As String

        FontCount = 0
        x = ActiveDocument.Characters.Count
        y = 0
        ' For-Next loop through every character
        For Each rngChar In ActiveDocument.Characters
            y = y + 1
            FontName = rngChar.Font.name
            StatusBar = y & ":" & x
            ' check if font used for this char already in list
            FoundFont = False
            For J = 1 To FontCount
                If FontList(J) = FontName Then FoundFont = True
            Next J
            If Not FoundFont Then
                FontCount = FontCount + 1
                FontList(FontCount) = FontName
            End If
        Next rngChar

        ' sort the list
        StatusBar = "Sorting Font List"
        For J = 1 To FontCount - 1
            L = J
            For K = J + 1 To FontCount
                If FontList(L) > FontList(K) Then L = K
            Next K
            If J <> L Then
                FontName = FontList(J)
                FontList(J) = FontList(L)
                FontList(L) = FontName
            End If
        Next J

        StatusBar = ""
        ' put in new document
        Documents.Add()
        Selection.TypeText Text:="There are " & _
          FontCount & " fonts used in the document, as follows:"
        Selection.TypeParagraph()
        Selection.TypeParagraph()
        For J = 1 To FontCount
            Selection.TypeText Text:=FontList(J)
            Selection.TypeParagraph()
        Next J
    End Sub
    Sub CheckParens() 'checks for mismatched parentheses
        ' Is this also checking on footnotes?
        ' Optional Simplify: when not balanced, simply type text message
        Dim WorkPara As String
        Dim CheckP() As Boolean
        Dim NumPara As Integer, J As Integer
        Dim LeftParens As Integer, RightParens As Integer
        Dim LeftBracket As Integer, RightBracket As Integer
        Dim LeftBrace As Integer, RightBrace As Integer
        Dim MsgText As String

        NumPara = ActiveDocument.Paragraphs.Count
        ReDim CheckP(NumPara)

        MsgText = "***Unbalanced parens in the next paragraph"
        For J = 1 To NumPara
            CheckP(J) = False
            WorkPara = ActiveDocument.Paragraphs(J).Range.Text
            If Len(WorkPara) <> 0 Then
                LeftParens = CountChars(WorkPara, "(")
                RightParens = CountChars(WorkPara, ")")

                LeftBracket = CountChars(WorkPara, "[")
                RightBracket = CountChars(WorkPara, "]")

                LeftBrace = CountChars(WorkPara, "{")
                RightBrace = CountChars(WorkPara, "}")

                If LeftBracket <> RightBracket Then CheckP(J) = True
                If LeftParens <> RightParens Then CheckP(J) = True
                If LeftBrace <> RightBrace Then CheckP(J) = True
            End If
        Next J

        For J = NumPara To 1 Step -1
            If CheckP(J) Then
                Selection.HomeKey(Unit:=wdStory, Extend:=wdMove)
                If J > 1 Then
                    Selection.MoveDown(Unit:=wdParagraph, _
                      Count:=(J - 1), Extend:=wdMove)
                End If
                Selection.InsertParagraphBefore()
                Selection.MoveLeft(Unit:=wdCharacter, Count:=1)
                Selection.style = "Normal"
                Selection.TypeText Text:=MsgText
            End If
        Next J
    End Sub
    Private Function CountChars(a As String, c As String) As Integer 'Used by Check Parens
        Dim Count As Integer
        Dim found As Integer

        Count = 0
        found = InStr(a, c)
        While found <> 0
            Count = Count + 1
            found = InStr(found + 1, a, c)
        End While
        CountChars = Count
    End Function
    Sub ClearAllTabStops()
        Selection.WholeStory()
        Selection.ParagraphFormat.TabStops.ClearAll()
    End Sub
    Function GetFromClipboard() As String
        On Error GoTo errorhandler
        Dim DtObj As DataObject
        DtObj = New DataObject
        DtObj.GetFromClipboard()
        GetFromClipboard = DtObj.GetText
        Exit Function

errorhandler:
        GetFromClipboard = ""
    End Function
    Sub Nextchar()
        Dim Nextchar$
    Nextchar$ = str(Asc(WordBasic.[Selection$]()))
        WordBasic.Msgbox( _
        "The code for the next character is " + Nextchar$ + ".", _
        "Next Character")
    End Sub
    Sub RemoveBuiltinStylesfromQuikstyle()
        Dim i%
        On Error Resume Next
        For i = 1 To ActiveDocument.Styles.Count
            If ActiveDocument.Styles(i).BuiltIn Then
                ActiveDocument.Styles(i).QuickStyle = False
            Else
                ActiveDocument.Styles(i).QuickStyle = True
            End If
        Next i
        Beep()
    End Sub

    Sub Shortcuts() 'Alt-5
        Shell("notepad " & "C:\Documents and Settings\Zvi\Application Data\Microsoft\Templates\shortcuts.txt", vbNormalFocus)
    End Sub
    Sub DeleteUnusedStyles2() ' this is a duplicate
        ' We took care of this Careful - this may delete styles that exist in footnotes, Put footnote style into Doc
        Dim sty As style

        'M ActiveDocument.Styles.Count

        On Error Resume Next
        For Each sty In ActiveDocument.Styles
            If sty.BuiltIn = False Then
                If sty.InUse = False Then
                    If sty <> "mokor small" Then
                        sty.Delete()
                    Else
                        sty.QuickStyle = True
                    End If
                Else
                    '                Stop
                    Selection.HomeKey Unit:=wdStory
                    If Not zzFind("", zWildOff, sty) And sty <> "mokor small" Then
                        sty.Delete()
                    Else
                        sty.QuickStyle = True
                    End If
                End If
            Else
                sty.QuickStyle = False
            End If
        Next sty
        Selection.HomeKey Unit:=wdStory

        '    M ActiveDocument.Styles.Count


    End Sub

    Sub DeleteUnusedStyles() 'CTR-ALT-D
        ' Careful - this may delete styles that exist in footnotes, It does NOT delete mokor small (or put style in Doc)
        Dim sty As style
        On Error Resume Next
        For Each sty In ActiveDocument.Styles
            If sty.BuiltIn = False Then
                If sty.InUse = False Then
                    If sty <> "mokor small" Then
                        sty.Delete()
                    Else
                        sty.QuickStyle = True
                    End If
                Else
                    Selection.HomeKey Unit:=wdStory
                    If Not zzFind("", zWildOff, sty) And sty <> "mokor small" Then
                        sty.Delete()
                    Else
                        sty.QuickStyle = True
                    End If
                End If
            Else
                sty.QuickStyle = False
            End If
        Next sty
        Selection.HomeKey Unit:=wdStory
        '    mDone
    End Sub


    Sub DeleteUnusedStyles3()
        ' Careful - this may delete styles that exist in footnotes, Put footnote style into Doc
        Dim sty As style
        On Error Resume Next
        For Each sty In ActiveDocument.Styles
            If sty.BuiltIn = False Then
                If sty.InUse = False Then
                    If sty <> "mokor small" Then
                        sty.Delete()
                    Else
                        sty.QuickStyle = True
                    End If
                Else
                    Selection.HomeKey Unit:=wdStory

                    zClearFind()
                    Selection.Find.style = ActiveDocument.Styles(sty)
                    With Selection.Find
                        .Text = ""
                        .Replacement.Text = ""
                        .Forward = True
                        .Wrap = wdFindStop
                        .Format = True
                    End With
                    Selection.Find.Execute()
                    If Selection.Find.found = False And sty <> "mokor small" Then
                        sty.Delete()
                    Else
                        sty.QuickStyle = True
                    End If
                End If
            Else
                sty.QuickStyle = False
            End If
        Next sty
        Selection.HomeKey Unit:=wdStory
        mDone()

    End Sub
    Sub zDeleteUnusedStyles()
        'this is from Alan Wyat
        Dim oStyle As style, x%

        'This doesn't cycle through all the stories.  Use mvps...
        For Each oStyle In ActiveDocument.Styles
            'Only check out non-built-in styles
            If oStyle.BuiltIn = False Then
                With ActiveDocument.Content.Find
                    .ClearFormatting()
                    .style = oStyle.NameLocal
                    .Execute(FindText:="", Format:=True)
                    'If .Found = False Then
                    oStyle.Delete()
                    x = x + 1
                    'End If
                End With
            End If
        Next oStyle
        M x & " style(s) deleted."
    End Sub
    Function GoIntoFootnotes()
        GoIntoFootnotes = GetIntoFootnotes
    End Function
    Function GoToFootnotes()
        GoToFootnotes = GetIntoFootnotes
    End Function

    Function GetIntoFootnotes() As Boolean
        GetIntoFootnotes = False
        If ActiveDocument.Footnotes.Count = 0 Then Exit Function
        ActiveDocument.StoryRanges(wdFootnotesStory).Select()
        Clps wdCollapseStart
        GetIntoFootnotes = True
    End Function

    Function GetIntoEndnotes() As Boolean
        GetIntoEndnotes = False
        If ActiveDocument.Endnotes.Count = 0 Then Exit Function
        ActiveDocument.StoryRanges(wdEndnotesStory).Select()
        Clps wdCollapseStart
        GetIntoEndnotes = True
    End Function

    Sub GetOutOfFootnotes(Optional GoUp As Boolean = False)
        Do Until Selection.Information(wdInFootnote) = False
            Selection.MoveDown(wdLine, 1)
        Loop
    End Sub

    Sub ToggleBookmarks()
        ActiveWindow.View.ShowBookmarks = Not ActiveWindow.View.ShowBookmarks
    End Sub
    Sub DeleteAllFootnotes()
        cAY("^f", "", "", "")
    End Sub
    Sub DeleteAllEndnotes()
        cAY("^e", "", "", "")
    End Sub
    Sub zStyleDelete(zstyle As String)
        If StyleExist(zstyle) = False Then Exit Sub
        zClearFind()
        Selection.Find.style = ActiveDocument.Styles(zstyle)
        With Selection.Find
            .Text = ""
            .Replacement.Text = ""
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = False
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
    End Sub
    Sub RemoveHyperlinks()
        Dim oField As Field
        For Each oField In ActiveDocument.Fields
            If oField.Type = wdFieldHyperlink Then
                oField.Unlink()
            End If
        Next
        oField = Nothing
        mDone()
    End Sub

    Function ExistsStyle(ThisStyle As String) As Boolean
        'Function cycles through entire list of styles. If style wanted is not there, return false
        Dim i As Integer
        For i = 1 To Application.ActiveDocument.Styles.Count
            If Application.ActiveDocument.Styles(i) = ThisStyle Then
                ExistsStyle = True
                Exit Function
            End If
        Next
        ' MsgBox "ERROR: Style " & ThisStyle & "does not exist.", vbCritical
        ExistsStyle = False
    End Function
    Sub UpdateAllFields2()
        ActiveDocument.PrintPreview()
        ActiveDocument.ClosePrintPreview()
    End Sub

    'Did you change the keybord layout to Hebrew becuase you wanted to type in Hebrew, but you then started typing in a program such as VBE
    'or notepad which doesn't support Hebrew and you ended up with characters like ùâëâëé÷øì ?  No problem, just use this procedure:
    Function GibberishToHebrew(aGibberish$) As String
        Dim x%

        For x = 1 To Len(aGibberish$)
            Select Case LCase$(Mid$(aGibberish, x, 1))
                Case "/" 'q
                    GibberishToHebrew = GibberishToHebrew & "/"
                Case "'" 'w
                    GibberishToHebrew = GibberishToHebrew & "'"
                Case "÷" 'e
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("e")
                Case "ø" 'r
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("r")
                Case "à" 't
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("t")
                Case "è" 'y
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("y")
                Case "å" 'u
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("u")
                Case "ï" 'i
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("i")
                Case "í" 'o
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("o")
                Case "ô" 'p
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("p")
                Case "ù" 'a
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("a")
                Case "ã" 's
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("s")
                Case "â" 'd
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("d")
                Case "ë" 'f
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("f")
                Case "ò" 'g
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("g")
                Case "é" 'h
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("h")
                Case "ç" 'j
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("j")
                Case "ì" 'k
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("k")
                Case "ê" 'l
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("l")
                Case "ó" ';
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew(";")
                Case "," ''
                    GibberishToHebrew = GibberishToHebrew & ","
                Case "æ" 'z
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("z")
                Case "ñ" 'x
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("x")
                Case "á" 'c
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("c")
                Case "ä" 'v
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("v")
                Case "ð" 'b
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("b")
                Case "î" 'n
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("n")
                Case "ö" 'm
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew("m")
                Case "ú" ',
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew(",")
                Case "õ" '.
                    GibberishToHebrew = GibberishToHebrew & LatinToHebrew(".")
                Case "." '/
                    GibberishToHebrew = GibberishToHebrew & "."
                Case Else
                    '                Stop
                    GibberishToHebrew = GibberishToHebrew & Mid$(aGibberish, x, 1)

            End Select
        Next x

    End Function
    Function LatinToHebrew(aLatin$)
        Dim x%
        'Stop
        For x = 1 To Len(aLatin)
            Select Case LCase$(Mid$(aLatin, x, 1))
                Case "q"
                    LatinToHebrew = LatinToHebrew & "/"
                Case "w"
                    LatinToHebrew = LatinToHebrew & "'"
                Case "e"
                    LatinToHebrew = LatinToHebrew & HE(KOOF)
                Case "r"
                    LatinToHebrew = LatinToHebrew & HE(REISH)
                Case "t"
                    LatinToHebrew = LatinToHebrew & HE(ALEF)
                Case "y"
                    LatinToHebrew = LatinToHebrew & HE(TES)
                Case "u"
                    LatinToHebrew = LatinToHebrew & HE(vav)
                Case "i"
                    LatinToHebrew = LatinToHebrew & HE(FINAL_NUN)
                Case "o"
                    LatinToHebrew = LatinToHebrew & HE(FINAL_MEM)
                Case "p"
                    LatinToHebrew = LatinToHebrew & HE(PEY)
                Case "a"
                    LatinToHebrew = LatinToHebrew & HE(SHIN)
                Case "s"
                    LatinToHebrew = LatinToHebrew & HE(DALET)
                Case "d"
                    LatinToHebrew = LatinToHebrew & HE(GIMEL)
                Case "f"
                    LatinToHebrew = LatinToHebrew & HE(KAF)
                Case "g"
                    LatinToHebrew = LatinToHebrew & HE(AYIN)
                Case "h"
                    LatinToHebrew = LatinToHebrew & HE(YUD)
                Case "j"
                    LatinToHebrew = LatinToHebrew & HE(CHES)
                Case "k"
                    LatinToHebrew = LatinToHebrew & HE(LAMED)
                Case "l"
                    LatinToHebrew = LatinToHebrew & HE(FINAL_KAF)
                Case ";"
                    'LatinToHebrew = LatinToHebrew & he(Final_PEY)
                    LatinToHebrew = LatinToHebrew & Mid$(aLatin, x, 1)
                Case "'"
                    LatinToHebrew = LatinToHebrew & ","
                Case "z"
                    LatinToHebrew = LatinToHebrew & HE(ZAYIN)
                Case "x"
                    LatinToHebrew = LatinToHebrew & HE(SAMECH)
                Case "c"
                    LatinToHebrew = LatinToHebrew & HE(BEIS)
                Case "v"
                    LatinToHebrew = LatinToHebrew & HE(HEY)
                Case "b"
                    LatinToHebrew = LatinToHebrew & HE(NUN)
                Case "n"
                    LatinToHebrew = LatinToHebrew & HE(MEM)
                Case "m"
                    LatinToHebrew = LatinToHebrew & HE(TSADI)
                Case ","
                    LatinToHebrew = LatinToHebrew & HE(TAV)
                Case "."
                    'LatinToHebrew = LatinToHebrew & he(Final_TSADI)
                    LatinToHebrew = LatinToHebrew & Mid$(aLatin, x, 1)
                Case "/"
                    LatinToHebrew = LatinToHebrew & "."
                Case Else
                    LatinToHebrew = LatinToHebrew & Mid$(aLatin, x, 1)
            End Select
        Next x
    End Function

    Sub HideUnusedBuiltInStyles()
        'Dialogs(wdDialogStyleManagement).Show
        'sendkeys %r %b %h ok
        Dim oStyle As style, x%

        ActiveDocument.FormattingShowFilter = 0
        ActiveDocument.FormattingShowFilter = 1
        ActiveDocument.FormattingShowFilter = 3

        ActiveDocument.FormattingShowFilter = 5
        ActiveDocument.FormattingShowFilter = 6
        'oStyle.QuickStyle = False
        For Each oStyle In ActiveDocument.Styles
            If oStyle.BuiltIn = True Then
                'If Not oStyle.InUse Then
                'Stop
                'oStyle.Visibility = False
                'don't know why this seems to work backwards!!!
                oStyle.Visibility = True
                '                oStyle.QuickStyle = False
                On Error Resume Next
                oStyle.QuickStyle = True
                'oStyle.QuickStyle = True
                'oStyle.UnhideWhenUsed = True
                'oStyle.UnhideWhenUsed = False
                x = x + 1
                'End If
            End If
        Next oStyle
        '    M "Altogether there are " & x & " hidden style(s)."


        ActiveDocument.StyleSortMethod = wdStyleSortByName

    End Sub

End Module
