﻿Module General_Shortcuts
    Option Explicit
    Sub PasteTextOnly() 'CTR-T
        Selection.PasteAndFormat(wdFormatPlainText)
    End Sub
    Sub PasteOriginalFormat() 'CTR-O
        Selection.PasteAndFormat(wdFormatOriginalFormatting)
    End Sub
    Sub PasteNewFormat() 'CTR-N
        Selection.PasteAndFormat(wdUseDestinationStylesRecovery)
    End Sub
    Sub Delete2EndofLine() ' CTR-END
        Selection.Extend()
        Selection.EndKey Unit:=wdLine
        Selection.MoveLeft(Unit:=wdCharacter, Count:=1, Extend:=wdExtend)
        Selection.Delete(Unit:=wdCharacter, Count:=1)
    End Sub
    Sub DeleteRestOfSentance() 'ALT . (not used)
        Selection.Extend()
        Selection.Extend Character:="."
        Selection.Delete(Unit:=wdCharacter, Count:=1)
    End Sub
    Sub DeleteWord() ' CTR-Backspace
        Selection.Extend()
        Selection.Extend()

        Selection.MoveRight(WdUnits.wdCharacter, 1, WdMovementType.wdExtend)

        If Asc(Right(Selection.Text, 1)) = 34 Then
            Selection.MoveRight(WdUnits.wdCharacter, 1, WdMovementType.wdExtend)
        Else
            Selection.MoveLeft(WdUnits.wdCharacter, 1, WdMovementType.wdExtend)
        End If

        Selection.MoveRight(WdUnits.wdCharacter, 1, WdMovementType.wdExtend)
        If Asc(Right(Selection.Text, 1)) = 44 Then
        Else
            Selection.MoveLeft(WdUnits.wdCharacter, 1, WdMovementType.wdExtend)
        End If
        Selection.Delete(Unit:=wdCharacter, Count:=1)
        '    Selection.MoveLeft Unit:=wdCharacter, Count:=1
    End Sub
    Sub GetStuffInParentheses() ' Alt-)  Alt-(
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = "("
            .Replacement.Text = ""
            .Forward = False
            .Wrap = wdFindAsk
            .MatchWildcards = False
        End With
        Selection.Find.Execute()
        Selection.Extend()
        Selection.Extend Character:=")"
    End Sub
    Sub GetStuffInSquiggly() ' CTR-{  CTR-}
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = "{"
            .Replacement.Text = ""
            .Forward = False
            .Wrap = wdFindAsk
            .MatchWildcards = False
        End With
        Selection.Find.Execute()
        Selection.Extend()
        Selection.Extend Character:="}"
    End Sub
    Sub GetStuffInBrackets() ' Alt-[  Alt-]  NO SHORTUCT
        Selection.Find.ClearFormatting()
        Selection.Find.Replacement.ClearFormatting()
        With Selection.Find
            .Text = "["
            .Replacement.Text = ""
            .Forward = False
            .Wrap = wdFindAsk
            .MatchWildcards = False
        End With
        Selection.Find.Execute()
        Selection.Extend()
        Selection.Extend Character:="]"
    End Sub
    Sub PrintPage() ' ALT-P
        ActiveDocument.Save()
    ActiveDocument.Bookmarks.Add String(10, "z")
        UnColor()
        ChangeLooksLikeKddSmalltoKddSmall()
        ChangeLookLikeMokorSmalltoMokorSmall()
    Selection.GoTo What:=wdGoToBookmark, name:=String(10, "z")
        Application.PrintOut(FileName:="", Range:=wdPrintCurrentPage, Item:= _
            wdPrintDocumentContent, Copies:=1, Pages:="", PageType:=wdPrintAllPages, _
            ManualDuplexPrint:=False, Collate:=True, Background:=True, PrintToFile:= _
            False, PrintZoomColumn:=0, PrintZoomRow:=0, PrintZoomPaperWidth:=0, _
            PrintZoomPaperHeight:=0)
        ColoRize()
    Selection.GoTo What:=wdGoToBookmark, name:=String(10, "z")
    ActiveDocument.Bookmarks(String(10, "z")).Delete

    End Sub
    Sub Print_Current_Section() ' CTR-P
        Dim CurrentSection&, aSection$
        ActiveDocument.Save()
        Selection.MoveRight(WdUnits.wdCharacter, 2, WdMovementType.wdExtend) 'or else aCurrSection works on and off
        CurrentSection = aCurrSection
        Clps wdCollapseStart
        UnColor()
        ChangeLooksLikeKddSmalltoKddSmall()
        ChangeLookLikeMokorSmalltoMokorSmall()
        aSection = "s" & St(CurrentSection)
        Application.PrintOut(FileName:="", Range:=wdPrintRangeOfPages, Item:= _
        wdPrintDocumentWithMarkup, Copies:=1, Pages:=aSection, PageType:= _
        wdPrintAllPages, Collate:=True, Background:=True, PrintToFile:=False)
        ColoRize()
    End Sub

    Sub OBSOLETE_Print_Current_Section_Page_at_a_Time()
        Dim CurrentSection&, CurrentPage&
        ActiveDocument.Save()
    ActiveDocument.Bookmarks.Add String(10, "z")
        UnColor()
    Selection.GoTo What:=wdGoToBookmark, name:=String(10, "z")

        Selection.MoveRight(WdUnits.wdCharacter, 2, wdExtend)
        CurrentSection = Selection.Information(wdActiveEndSectionNumber)

        Selection.GoTo(What:=wdGoToSection, Which:=wdGoToFirst, Count:=CurrentSection, name:="")
        CurrentPage = Selection.Information(wdActiveEndPageNumber)

1:      Application.PrintOut(FileName:="", Range:=wdPrintCurrentPage, Item:= _
                wdPrintDocumentWithMarkup, Copies:=1, Pages:="", PageType:= _
                wdPrintAllPages, Collate:=True, Background:=True)

        Selection.GoTo(What:=wdGoToPage, Which:=wdGoToNext, Count:=1, name:="")
        Selection.MoveRight(WdUnits.wdCharacter, 2, wdExtend)
        If CurrentPage = Selection.Information(wdActiveEndPageNumber) Then GoTo 2
        If CurrentSection <> Selection.Information(wdActiveEndSectionNumber) Then GoTo 2
        CurrentPage = Selection.Information(wdActiveEndPageNumber)
        GoTo 1
2:      ColoRize()
    Selection.GoTo What:=wdGoToBookmark, name:=String(10, "z")
    ActiveDocument.Bookmarks(String(10, "z")).Delete

    End Sub


End Module
