Module Module2


    Private Const MB_USERICON = &H80&

    Private Structure MsgBoxParams
        Dim cbSize As Long
        Dim hWndOwner As Long
        Dim hInstance As Long
        Dim lpszText As Long
        Dim lpszCaption As Long
        Dim dwStyle As Long
        Dim lpszIcon As Long
        Dim dwContextHelpId As Long
        Dim lpfnMsgBoxCallback As Long
        Dim dwLanguageId As Long
    End Structure

    Private Declare Function MessageBoxIndirectW Lib "user32" (lpMsgBoxParams As MsgBoxParams) As Long

    Public Function LenB() As Long
        Throw New NotImplementedException

    End Function
    ' note: I didn't bother to go ahead and start hacking with HelpFile and Context
Public Function aMsgBox(ByVal Prompt As String, Optional ByVal Buttons As VbMsgBoxStyle = vbOKOnly, Optional ByVal Title As String, Optional ByVal ResourceIcon As String, Optional ByVal hWndOwner As Long) As VbMsgBoxResult
        Dim udtMsgBox As MsgBoxParams
        ' if no owner is specified, try to use the active form
        hWndOwner = GetWordHandle
        '
        '    If hWndOwner = 0 Then
        '        If Not Screen.ActiveForm Is Nothing Then
        '            hWndOwner = Screen.ActiveForm.hWnd
        '        End If
        '    End If

        With udtMsgBox
            .cbSize = Len(udtMsgBox)
            ' important to set owner to get behavior similar to the native MsgBox
            .hWndOwner = hWndOwner
            '.hInstance = App.hInstance
            ' set the message
            .lpszText = StrPtr(Prompt)
            ' if no title is given, use the application title like the native MsgBox
            If LenB(Title) = 0 Then Title = Application.ActiveWindow.Caption 'App.Title
            .lpszCaption = StrPtr(Title)
            ' thought this would be a nice feature addition
            If LenB(ResourceIcon) = 0& Then
                .dwStyle = Buttons
            Else
                .dwStyle = (Buttons Or MB_USERICON) And Not (&H70&)
                .lpszIcon = StrPtr(ResourceIcon)
            End If
        End With
        ' show the message box
        aMsgBox = MessageBoxIndirectW(udtMsgBox)
    End Function

End Module