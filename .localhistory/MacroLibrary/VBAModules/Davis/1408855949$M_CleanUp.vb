'Attribute VB_Name = "M_CleanUp"
Option Explicit
Global Const zConfirm As Boolean = False  '<--Change as needed
Global NAME_OF_MESECHTA$
Global Const BUILDING_BLOCKS = "C:\Users\Avraham\Dropbox\Mishna\Misc\Templates\Building Blocks (1).dotx" '"C:\Users\Avraham 2\AppData\Roaming\Microsoft\Document Building Blocks\1033\14\Building Blocks.dotx"
Global Const DAVID_NORMAL = "C:\Users\Avraham\Dropbox\Mishna\Misc\Templates\New Davis Normal.dotm" '"C:\Users\Avraham 2\AppData\Roaming\Microsoft\Templates\DavisNormal.dotm"
Global Const TEMP_CHAR_STYLE_NAME = "my temp char style"
Global Const TEMP_PARA_STYLE_NAME = "my temp para style"
Global Const STYLES_TEMPLATE = "Z:\_Our Template_REMOTE.dotm"       'C:\Users\aaaaa\Desktop\6 2 Oholos 2 .docx"
Sub a()
  cAY "^f", "", "", "Footnote Reference", zWildOff, zNoConfirm, zMain
End Sub


Sub AAAAQuickCleanUp()

    ActiveDocument.TrackRevisions = False
    ActiveDocument.ShowRevisions = False
       
    aAttachTemplates 'OK
    DeleteUnusedStyles
    GetTheRightStyles 'OK
    HideUnusedBuiltInStyles
    SetDefaults
    
    FixWordperfectTables
    
    CleanPunctuation
    
    CleanUpAlephs 'OK Gets rid of Talmud aleph style
    RemoveHebrewSuperscripts
    ConvertMultipleIndentsIntoSingleIndent
    
    zzFoldStyle "PEREK TITLE", "Heading 1", True  'Used in Table of Contents

    zReplaceInAllParts " {2,}", " ", True 'Remove multiple space NoConfirm
    
    CleanRavTitleStyle
  
    cAY "", "^p^&", "KDD SPACE", "KDD" 'Remove KDD SPACE style
    zzFoldStyle "KDD SPACE", "KDD", True
    cAY "", "^p^&", "KDD SPACE ON TOP", "KDD" 'Remove KDD SPACE style
    zzFoldStyle "KDD SPACE ON TOP", "KDD", True
    
    cAY HE(KOOF) & HE(KOOF) & HE(KOOF), "???", "", ""
    
    cAY "^f", "", "", "Footnote Reference", zWildOff, zNoConfirm, zMain
    
    FoldOldStyles
    ConvertMokorSmallInDroshosToSharThin
    ConvertSquareBracketsInFootnotesToMokorSmall
    
    ConvertAllCommasAfterMokorSmallIntoMokorSmall
    SetMokorSmallforCertainWords
    
    RemoveDirectFormatting
      
        EachMishnaNewSection
    'NOT CLEAR WHY IT BOMBS ActiveDocument.PageSetup.SectionStart = wdSectionNewPage 'converts all sections into start new page at section
    
    cAY "<", "{", "", ""
    cAY ">", "}", "", ""
    
    'ConvertCommentaryIntoKDD_Small  need to fix UHOH first
    
    AutoMoveFootnotesToEndOfCertainPhrases 'moves many footnotes into correct position
    CleanUpParagraphMarksInNotes
    MakeMishnaAchronaBig
    
    UnColor
    ChangeLooksLikeKddSmalltoKddSmall
    ChangeLookLikeMokorSmalltoMokorSmall
    ColoRize
    'AAAPutMishnaNumberAfterSharHatalmud
    If Not ReplaceRosheiTeivos Then
        M ("All done.  However, ROSHEI TEIVOS were NOT Fixed!") 'in footnotes
    Else
        ' FOR BATCH mDone
    End If
    
    CleanUpNumbers
    
    AAAPutMishnaNumberAfterSharHatalmud
    
    PutInEinMishpot
    
    PutNekudosIntoKDD_FromMishna
    
End Sub

Sub CleanUpNumbers()    'don't do until everything else is already fixed.
Dim x%
    cAY "00000", "", "", "", zWildOff, zNoConfirm, zMain
    cAY "000", "", "JUNK", "", zWildOff, zNoConfirm, zMain
    'turn 111 into colon:
    cAY("111", ChrW(1475), "", "MISHNA", zWildOff, zNoConfirm, zMain)
    cAY(ChrW(1475) & "{2,}", ChrW(1475), "", "MISHNA", zWildon, zNoConfirm, zMain)
    
    cAY "222" & HE(REISH) & HE(AYIN) & """" & HE(BEIS) & " ", _
        "222" & HE(REISH) & HE(AYIN) & """" & HE(BEIS) & " ", _
        "", ActiveDocument.Styles("rav title"), zWildOff, zNoConfirm, zMain
            
        '.Text = "222" & HE(REISH) & HE(AYIN) & """" & HE(BEIS) & " "

    cAY("222" & HE(REISH) & HE(AYIN) & """" & HE(BEIS) & Chr(13), "", "", "", zWildOff, zNoConfirm, zMain)     'if no rav, delete "rav"
    'cAY "222" & HE(REISH) & HE(AYIN) & """" & HE(BEIS) & chr(13), "", "", "", zWildOff, zNoConfirm, zMain
    cAY "222", "", "", "", zWildOff, zNoConfirm, zMain
    cAY "333", "", "", "", zWildOff, zNoConfirm, zMain
    cAY HE(REISH) & HE(AYIN) & """" & HE(BEIS) & " ", "^&", "RAV", "rav title", zWildOff, zNoConfirm, zMain

    'Selection.Fields.ToggleShowCodes
    ActiveWindow.View.ShowFieldCodes = True
    GoHome
    cAY "^d", "", "KDD", "talmud aleph", zWildOff, zNoConfirm, zMain
    cAY "^d", "", "KDD HAKDOMO", "talmud aleph", zWildOff, zNoConfirm, zMain
    cAY "^d", "", "KDD SPACE ON TOP", "talmud aleph", zWildOff, zNoConfirm, zMain
    cAY "^d", "", "KDD1", "talmud aleph", zWildOff, zNoConfirm, zMain
    cAY "444", "444^p", "", "", zWildOff, zNoConfirm, zMain
    cAY "444^p^p", "444^p", "", "", zWildOff, zNoConfirm, zMain
    
    'For x = 1 To 10: ayCleanup "444", "555", "KOL DEMOMO DAKA": Next x
    For x = 1 To 25: ayCleanup "444", "555", "KOL DEMOMO DAKA": Next x
    ActiveWindow.View.ShowFieldCodes = False
    
    'cAY "666^p777^p", "", "", "", zWildOff, zNoConfirm, zMain
    For x = 1 To 25: ayCleanup "666", "777", "SHAAR HATALMUD": Next x
    
    'cAY "888^p999^p", "", "", "", zWildOff, zNoConfirm, zMain
    For x = 1 To 25: ayCleanup "888", "999", "SHAR HADROSHOS": Next x
    cAY "999", "", "JUNK", "", zWildOff, zNoConfirm, zMain
    
    'CheckNumberRemoval
    If zzFind("00000", False, "") Then M ("00000")
    If zzFind("111", False, "") Then M ("111")
    If zzFind("222", False, "") Then M ("222")
    If zzFind("333", False, "") Then M ("333")
    If zzFind("444", False, "") Then M ("444")
    If zzFind("555", False, "") Then M ("555")
    If zzFind("666", False, "") Then M ("666")
    If zzFind("777", False, "") Then M ("777")
    If zzFind("888", False, "") Then M ("888")
    If zzFind("999", False, "") Then M ("999")
End Sub


Sub AAA_UniversalCleanup()
    AQuickCleanUp
   ' RemoveHRb4Mishna
   
   FixKDD 'Fix KDD that Looks Like Kdd Small
    
    REMOVE_CHARACTERFORMAT_FROM_FOOTNOTES
    FIX_COMMA_STYLE
    ReplaceBracketsWithMokorSmallStyleInFootnotes
    
End Sub
Sub aAttachTemplates() 'OK
'    AddIns("D:\Davis\Davis.dotm").Installed = True
'    AddIns(DAVID_NORMAL).Installed = True
    'AddIns("C:\Documents and Settings\Zvi\Application Data\Microsoft\Document Building Blocks\1033\14\Building Blocks.dotx").Installed = True
    'AddIns.Add "C:\Users\" & Environ("username") & "\Desktop\Dropbox\Mishna\Misc\Templates\Building Blocks.dotx"
    AddIns.Add "C:\Users\" & Environ("username") & "\AppData\Roaming\Microsoft\Document Building Blocks\1033\14\Building Blocks.dotx"
'    With ActiveDocument
'        .UpdateStylesOnOpen = True
'        .AttachedTemplate = "Normal.dotm"
'        .XMLSchemaReferences.AutomaticValidation = False
'        .XMLSchemaReferences.AllowSaveAsXMLWithoutValidation = False
'    End With
End Sub
Sub RemoveDirectFormatting()
    Selection.WholeStory
    Selection.ClearCharacterDirectFormatting
    Selection.ClearCharacterDirectFormatting
    Selection.ClearParagraphDirectFormatting
    Selection.ClearParagraphDirectFormatting
    Selection.HomeKey Unit:=wdStory
End Sub

Sub zChangeMultipleTabs2SingleTab()
    cAY "^t^t", "^t", "", "", zWildOff, zNoConfirm, zMainFootnotesAndEndnotes
    cAY "^t^t", "^t", "", "", zWildOff, zNoConfirm, zMainFootnotesAndEndnotes
    cAY "^t^t", "^t", "", "", zWildOff, zNoConfirm, zMainFootnotesAndEndnotes
    cAY "^t^t", "^t", "", "", zWildOff, zNoConfirm, zMainFootnotesAndEndnotes
    cAY "^t^t", "^t", "", "", zWildOff, zNoConfirm, zMainFootnotesAndEndnotes
End Sub

Sub CleanUpAlephs()  'OK
'Gets rid of Talmud aleph style
zStyleDelete ("talmud aleph")

'Gets rid of fields
    Selection.WholeStory
    Selection.Fields.ToggleShowCodes
    Selection.Collapse
    
    cAY "^d", "", "", ""
    cAY "^t^t", "", "", ""
    
End Sub
Function ReplaceRosheiTeivos() As Boolean
Dim xl As Excel.Application, sprd As Excel.Workbook
Dim i%, aFrom$, aTo$, aPath$

    'get directory based on user name:
'    Select Case Environ("USERNAME")
'        Case "Avraham"
'            aPath = "C:\Users\Avraham\Dropbox\Mishna\Roshei Teivos.xlsm"
'        Case "Zvi"
'            aPath = "C:\Documents and Settings\Zvi\Desktop\Dropbox\Mishna\Roshei Teivos.xlsm"
'        Case Else
'            aPath = BrowseFolder("Where is the directory?") & "\" & "Roshei Teivos.xlsm"
'    End Select
    aPath = Environ("DROPBOX")
    If aPath = "" Then
        ReplaceRosheiTeivos = False
        Exit Function
    End If
    
    aPath = aPath & "\Mishna\Roshei Teivos\Roshei Teivos.xlsm"
          
    Set xl = New Excel.Application
    Set sprd = xl.Workbooks.Open(FileName:=aPath)
    xl.Visible = True
    
    i = 2
    aFrom = sprd.ActiveSheet.Range("A" & St(i))
        
    Do Until aFrom = ""
        aTo = sprd.ActiveSheet.Range("B" & St(i))
        'sprd.ActiveSheet.Range("B" & St(i)).Select
        cAY aFrom, aTo, "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
        i = i + 1
        aFrom = sprd.ActiveSheet.Range("A" & St(i))
    Loop
    
    sprd.Close
    Set sprd = Nothing
    xl.Quit
    Set xl = Nothing
    
    ReplaceRosheiTeivos = True
    
End Function

Sub MakeMishnaAchronaBig()
    GoHome
    If Not GetIntoFootnotes Then
        Exit Sub
    Else

        Do While zzFind(HE(MEM) & HE(SHIN) & """" & HE(ALEF), zWildOff, "")
            If Selection.style = "mokor small" Then Selection.clearcharacterstyle
        Loop
    End If
End Sub
Sub ConvertMultipleIndentsIntoSingleIndent()
Dim pa As Paragraph

    For Each pa In ActiveDocument.Paragraphs
        If pa.LeftIndent > 40 Then pa.LeftIndent = 36  'InchesToPoints(.5)
    Next pa
    
    ActiveWindow.ActivePane.View.ShowAll = Not ActiveWindow.ActivePane.View. _
        ShowAll
    ActiveWindow.ActivePane.View.ShowAll = Not ActiveWindow.ActivePane.View. _
    ShowAll

End Sub
Sub AutoMoveFootnotesToEndOfCertainPhrases() 'OK   'moves many footnotes into correct position
Dim StrFrom As String, StrTo As String

    zReplaceInAllParts " {2,}", " ", True 'Remove multiple space NoConfirm
        
    StrFrom = "(^2)(" & HE(SHIN) & HE(NUN) & HE(ALEF) & HE(MEM) & HE(REISH) & "*:)"
    StrTo = "\2\1"
    zReplace StrFrom, StrTo, True, True 'Moves footnotes in Droshos from before Shenamr, to end of :
    
    StrFrom = "(^2)(" & HE(NUN) & HE(ALEF) & HE(MEM) & HE(REISH) & "*:)"
    StrTo = "\2\1"
    zReplace StrFrom, StrTo, True, True 'Moves footnotes in Droshos from before Neemar, to end of :
    
    StrFrom = "(" & HE(LAMED) & HE(PEY) & HE(YUD) & " " & ")(^2)(*:)"
    StrTo = "\1 \3\2 "
    zReplace StrFrom, StrTo, True, True 'Moves footnotes from before LEFI to end of :
        
    StrFrom = "(^2)(" & HE(LAMED) & HE(PEY) & HE(YUD) & ")(*:)"
    StrTo = "\2 \3\1 "
    zReplace StrFrom, StrTo, True, True 'Moves footnotes from before LEFI to end of :
    
    StrFrom = "(^2)(" & HE(vav) & HE(KAF) & HE(FINAL_NUN) & " " & HE(HEY) & HE(LAMED) & HE(KAF) & HE(HEY) & ".)"
    StrTo = " \2\1"
    zReplace StrFrom, StrTo, True, True 'Moves footnote from before Vichain Halacha to after period
    
    StrFrom = "(^2)(" & HE(ALEF) & HE(vav) & HE(MEM) & HE(REISH) & HE(YUD) & HE(FINAL_MEM) & ":)"
    StrTo = " \2\1"
    zReplace StrFrom, StrTo, True, True 'Moves footnote from before OMRIM to after :
    
    'can also be written like this:
'    cAY "(^2)(" & HE(LAMED) & HE(PEY) & HE(YUD) & "*:)", "\2\1", "", "", zWildOn, znoConfirm
'    cAY "(" & HE(LAMED) & HE(PEY) & HE(YUD) & " )(^2)(*:)", "\1\3\2", "", "", zWildOn, znoConfirm
'    cAY "(" & HE(SHIN) & HE(NUN) & HE(ALEF) & HE(MEM) & HE(REISH) & " )(^2)(*:)", _
'        "\1\3\2", "", "", zWildOn, znoConfirm
'    cAY "(^2)(" & HE(SHIN) & HE(NUN) & HE(ALEF) & HE(MEM) & HE(REISH) & "*:)", "\2\1", "", "", zWildOn, znoConfirm


End Sub
Sub GetTheRightStyles() 'gets styles from Oholos
Dim x%, aStylesDoc  As Word.Document, aMainDoc As Word.Document

    ColoRize
    Exit Sub
    
    
    ResizeWindowDebuggingMode
    
    Set aMainDoc = ActiveDocument
    Set aStylesDoc = Documents.Open(FileName:=STYLES_TEMPLATE, Visible:=False)
    ''used for debugging, precluding the need to kill the process
    'aStylesDoc.Windows(1).Visible = True
    
    For x = 1 To aStylesDoc.Styles.Count       'UBound(aStyles)
        If Not aStylesDoc.Styles(x).BuiltIn Then
            Application.OrganizerCopy STYLES_TEMPLATE, aMainDoc.FullName, aStylesDoc.Styles(x).NameLocal, wdOrganizerObjectStyles
        End If
    Next x
    Application.OrganizerCopy STYLES_TEMPLATE, aMainDoc.FullName, aStylesDoc.Styles("Footnote Reference").NameLocal, wdOrganizerObjectStyles
    Application.OrganizerCopy STYLES_TEMPLATE, aMainDoc.FullName, aStylesDoc.Styles("Footnote Text").NameLocal, wdOrganizerObjectStyles
    
    aStylesDoc.Close
    Set aStylesDoc = Nothing
    
    
End Sub
Sub CleanPunctuation()
    zReplaceInAllParts " {2,}", " ", True 'Remove multiple space NoConfirm
    
    Selection.HomeKey Unit:=wdStory
'    zReplace " ^f", "^f", False 'Removes space before footnote
    cAY "(^02)(\))", "\2\1", "", "", zWildon, zConfirm, zMain
    cAY "(^02)(\?)", "\2\1", "", "", zWildon, zConfirm, zMain
    cAY "(^02)(.)", "\2\1", "", "", zWildon, zConfirm, zMain
    cAY "(^02)(,)", "\2\1", "", "", zWildon, zConfirm, zMain
    cAY "(^02)(:)", "\2\1", "", "", zWildon, zConfirm, zMain 'PutReferenceAfterColon
'    cAY "(^02)(\))", "\2\1", "", "", zWildOn, zConfirm, zMain
'    cAY "(^02)(\?)", "\2\1", "", "", zWildOn, zConfirm, zMain
'    cAY "(^02)(.)", "\2\1", "", "", zWildOn, zConfirm, zMain
'    cAY "(^02)(,)", "\2\1", "", "", zWildOn, zConfirm, zMain
'    cAY "(^02)(:)", "\2\1", "", "", zWildOn, zConfirm, zMain
    
    cAY "(.)(\))", "\2\1", "", "", zWildon, zConfirm, zMain
    cAY "(,)(\))", "\2\1", "", "", zWildon, zConfirm, zMain
    cAY "(\?)(\))", "\2\1", "", "", zWildon, zConfirm, zMain
    
    
    cAY "^t", "", "", "", zWildOff, zConfirm, zMainFootnotesAndEndnotes 'remove all tabs
    cAY "<", "{", "", "", zWildOff, zConfirm, zMainFootnotesAndEndnotes
    cAY ">", "}", "", "", zWildOff, zConfirm, zMainFootnotesAndEndnotes
    cAY HE(vav) & HE(KAF) & HE(FINAL_NUN) & " " & HE(HEY) & HE(LAMED) & HE(KAF) & HE(HEY), "", "", "shar bold", zWildOff, zConfirm, zMain
    'makes vechain halacha bold
    
    zzFoldStyle "Foonote reference", "Footnote Reference", True
    cAY "^f", "", "", "Footnote Reference", zWildOff, zNoConfirm, zMain 'fix style of footnote numbers in text
'   cAY "^f", "", "", "Footnote Reference", zWildOff, znoConfirm, zMain 'fix style of footnote numbers in text
           
    cAY "^=", " ^= ", "", "", zWildOff, zConfirm, zMainFootnotesAndEndnotes 'Puts space around n-dash
' not sure   cAY " ^=^p", "^p", "", "", zWildOff, zConfirm, zMain 'Remove n dash at end of line in KDD
    cAY " ^= ^p", "^p", "", "", zWildOff, zConfirm, zMain
    cAY " ^=^l", ".^l", "", "", zWildOff, zConfirm, zMain 'Remove n dash at end of line in Shaar
    cAY "::", ":", "", "", zWildOff, zConfirm, zMainFootnotesAndEndnotes
            
    cAY Chr(132) & " ", Chr(132), "", "", zWildOff, zConfirm 'removes space after opening quote
' can't be before footnote moved only at end    cAY "( )(^2)", "\2", "", "", zWildOn, zConfirm, zMain 'Clean space before superscripts (only has to be run once since multiple spaces were removed earlier)
    cAY " ^p", "^p", "", "", zWildOff, zConfirm, zMainFootnotesAndEndnotes 'removes space at end of line
    cAY "^p ", "^p", "", "", zWildOff, zConfirm, zMainFootnotesAndEndnotes 'removes space at beginning of line
    cAY " ^l", "^l", "", "", zWildOff, zConfirm, zMainFootnotesAndEndnotes 'removes space at end line break
    cAY "^l ", "^l", "", "", zWildOff, zConfirm, zMainFootnotesAndEndnotes 'removes space at beginning of line break
    cAY "( ", "(", "", "", zWildOff, zConfirm 'remove space after open parenthesis
    cAY " )", ")", "", "", zWildOff, zConfirm 'remove space before end parenthesis
    cAY "[ ", "[", "", "", zWildOff, zConfirm 'remove space after open bracket
    cAY " ]", "]", "", "", zWildOff, zConfirm 'remove space before end bracket
    cAY "{ ", "{", "", "", zWildOff, zConfirm 'remove space after open squiggle
    cAY " }", "}", "", "", zWildOff, zConfirm 'remove space before end squiggle
    cAY " ,", ",", "", "", zWildOff, zConfirm 'remove space before comma
    cAY ". ,", ".,", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes 'remove space between period (amud aleph) and comma
    cAY ". )", ".)", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes 'remove space between period (amud aleph) and end parenthsis
    cAY ".US", "US", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
    cAY "US.", "US", "", "", zWildOff, zConfirm, zFootnotesAndEndnotes
    
   ' RemoveHRb4Mishna 'NoConfirm
   
    cAY " {2,}", " ", "", "", zWildon, zConfirm, zMainFootnotesAndEndnotes 'Remove multiple space
    
End Sub
Sub CleanUpParagraphMarksInNotes()
'suuuuuper important to run before footnote move
    
    If Not GetIntoFootnotes Then
        GoTo Part2
    Else
          Do While zzFind("^p", zWildOff, "mokor small")
        ' Do While zzFind("^p", zWildOff, "")
             Selection.Font.Reset
         Loop
    End If
Part2:
    If Not GetIntoEndnotes Then
        Exit Sub
    Else
          Do While zzFind("^p", zWildOff, "mokor small")
             Selection.Font.Reset
         Loop
    End If
End Sub
Sub KillAllTables()
Dim tbl As Table
    For Each tbl In ActiveDocument.Tables
        tbl.Rows.ConvertToText Separator:=wdSeparateByParagraphs, NestedTables:=True
    Next tbl
End Sub

Sub FixWordperfectTables()
Dim tbl As Table
    For Each tbl In ActiveDocument.Tables
        tbl.Rows.Alignment = wdAlignRowCenter
        tbl.Rows.LeftIndent = InchesToPoints(0)
        tbl.Rows.WrapAroundText = False
        tbl.Rows.ConvertToText Separator:=wdSeparateByParagraphs, _
        NestedTables:=True
    Next tbl
End Sub


Sub FoldOldStyles()
    zzFoldStyle "KDD1", "KDD", True
    zzFoldStyle "KDD2", "KDD", True
    zzFoldStyle "KDD3", "KDD", True
    zzFoldStyle "KDD4", "KDD", True
    zzFoldStyle "KDD5", "KDD", True
    zzFoldStyle "KDD6", "KDD", True
    zzFoldStyle "KDD7", "KDD", True
    zzFoldStyle "KDD8", "KDD", True
    zzFoldStyle "KDD9", "KDD", True
    zzFoldStyle "hakdomo bold", "shar bold", True
End Sub



Sub ConvertMokorSmallInDroshosToSharThin()
'works beautifully
Dim se As Selection
Set se = Selection

    GoHome
    Do While Find2Styles("", False, "mokor small", "DROSHOS")
        'Stop
        Clps wdCollapseStart
        se.TypeText "["
        zzFind "", False, "mokor small"
        Clps wdCollapseEnd
        se.TypeText "]"
        se.MoveLeft Unit:=wdCharacter, Count:=2
        GetStuffInBrackets
        Selection.style = "shar thin"
        Clps wdCollapseEnd
        Selection.EscapeKey
        'Stop
    Loop
End Sub

Sub ConvertSquareBracketsInFootnotesToMokorSmall()
'works beautifully
Dim se As Selection
Set se = Selection

    GoHome
    Do While se.Information(wdInFootnote) = False
        se.MoveDown wdLine, 1
    Loop
    ''Puts cursor in footnote section:
    'zzFind "", False, "Footnote Text"
        
    Do While zzFind("[", False, "Footnote Text")
        'Stop
        se.MoveRight Unit:=wdCharacter, Count:=2
        GetStuffInBrackets
        Selection.style = "mokor small"
        Clps wdCollapseEnd
        Selection.EscapeKey
'        Stop
    Loop
End Sub

Sub SetMokorSmallforCertainWords()
Dim xl As Excel.Application, sprd As Excel.Workbook
Dim i%, aWord$, aPath$

    If Not GetIntoFootnotes Then
        Exit Sub
    Else
        'get directory based on user name:
        aPath = GetDropBoxPath & "\Mishna\Make Mokor Small.xlsm"
            
        Set xl = New Excel.Application
        Set sprd = xl.Workbooks.Open(FileName:=aPath)
        xl.Visible = True
        
        i = 2
        aWord = sprd.ActiveSheet.Range("A" & St(i))
            Do Until aWord = ""
                cAY aWord, "^&", "", "mokor small", zWildOff, zNoConfirm, zFootnotesAndEndnotes
                i = i + 1
                aWord = sprd.ActiveSheet.Range("A" & St(i))
            Loop
            
        sprd.Close
        Set sprd = Nothing
        xl.Quit
        Set xl = Nothing
    End If
    '
    '    Selection.Find.ClearFormatting
    '    Selection.Find.Replacement.ClearFormatting
    '    Selection.Find.Replacement.style = ActiveDocument.Styles("mokor small")
    '    With Selection.Find
    '        .Text = he(PEY) & he(HEY) & """" & he(MEM) & ","
    '        .Replacement.Text = ""
    '        .Forward = True
    '        .Wrap = wdFindContinue
    '        .Format = True
    '        .MatchCase = False
    '        .MatchWholeWord = False
    '        .MatchKashida = False
    '        .MatchDiacritics = False
    '        .MatchAlefHamza = False
    '        .MatchControl = False
    '        .MatchWildcards = False
    '        .MatchSoundsLike = False
    '        .MatchAllWordForms = False
    '    End With
    '    Selection.Find.Execute Replace:=wdReplaceAll
End Sub
Sub ConvertAllCommasAfterMokorSmallIntoMokorSmall()
Dim Flag As String
    Flag = String(6, HE(FINAL_MEM))
    
    CleanUpParagraphMarksInNotes
    cAY "", "^&" & Flag, "mokor small", "mokor small", zWildOff, zNoConfirm, zFootnotesAndEndnotes
    cAY Flag & ",", ",", "", "mokor small", zWildOff, zNoConfirm, zFootnotesAndEndnotes
    cAY Flag, "", "", "", zWildOff, zNoConfirm, zFootnotesAndEndnotes
End Sub


Sub EachMishnaNewSection()
Dim x%

    If ActiveDocument.Sections.Count > 1 Then Exit Sub

    x = 1
    Do While zzFind("", False, "MISHNA TITLE")
        If Selection.Information(wdActiveEndSectionNumber) = x Then
            Selection.InsertBreak Type:=wdSectionBreakNextPage
            x = x + 1
        End If
    Loop
    
End Sub
Sub RemoveHebrewSuperscripts()
    Selection.Find.ClearFormatting
    With Selection.Find.Font
        .Superscript = True
        .Subscript = False
    End With
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = ChrW(1488)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1489)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1490)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1491)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1492)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1493)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1494)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1495)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1496)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1497)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1499)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    With Selection.Find
        .Text = ChrW(1500)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
End Sub

'not sure if this belongs here:
Sub ChangeAllBrackets2MokorSmallinFootnotes()
    If Not GetIntoFootnotes Then
        Exit Sub
    Else
        Selection.Find.ClearFormatting
        Selection.Find.Replacement.ClearFormatting
        Selection.Find.Replacement.style = ActiveDocument.Styles("mokor small")
        With Selection.Find
            .Text = "(\[)(*)(\])"
            .Replacement.Text = "\2"
            .Forward = True
            .Wrap = wdFindContinue
            .Format = True
            .MatchWildcards = True
        End With
        Selection.Find.Execute Replace:=wdReplaceAll
    End If
End Sub


