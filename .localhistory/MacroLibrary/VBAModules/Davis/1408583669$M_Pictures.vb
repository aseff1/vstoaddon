Attribute VB_Name = "M_Pictures"
Option Explicit
Sub ShrinkPicture()
    If Selection.InlineShapes.Count = 1 Then
        Selection.InlineShapes(1).ScaleHeight = 66
        Selection.InlineShapes(1).ScaleWidth = 66
    End If
    
'1) Add to each of the above the code that if the picture is Square Wrap text shape do the same (i.e. change to height 35 or 100)
    If Selection.ShapeRange.Count = 1 Then
        Selection.ShapeRange(1).ScaleHeight 0.66, msoTrue
        Selection.ShapeRange(1).ScaleWidth 0.66, msoTrue
        Selection.ShapeRange(1).WrapFormat.Side = wdWrapRight
    End If
    
End Sub
'4) A Macro to change all Inline shapes in a file to square wrap
Sub ConvertAllInlineShapesToShapes()
Dim ils As InlineShape, sh As Shape

    For Each ils In Ac.InlineShapes
        Set sh = ils.ConvertToShape
        sh.WrapFormat.Type = wdWrapSquare
        sh.WrapFormat.Side = wdWrapRight
    Next ils

End Sub
Sub ShrinkAllPictures()
'2) A Macro to Shrink all (Inline and Square Wrap) in a file
Dim ils As InlineShape, sh As Shape

    For Each ils In Ac.InlineShapes
        ils.ScaleHeight = 66
        ils.ScaleWidth = 66
    Next ils
    
    For Each sh In Ac.Shapes
        sh.ScaleHeight 0.66, msoTrue
        sh.ScaleWidth 0.66, msoTrue
    Next sh

End Sub

Sub ResetAllPictures()
'3) A Macro to Reset all (Inline and Square Wrap) in a file
Dim ils As InlineShape, sh As Shape

    For Each ils In Ac.InlineShapes
        ils.ScaleHeight = 100
        ils.ScaleWidth = 100
    Next ils
    
    For Each sh In Ac.Shapes
        sh.ScaleHeight 1, msoTrue
        sh.ScaleWidth 1, msoTrue
    Next sh

End Sub
Sub ResetPicture()

    If Selection.InlineShapes.Count = 1 Then
        Selection.InlineShapes(1).ScaleHeight = 100
        Selection.InlineShapes(1).ScaleWidth = 100
    End If
    
'1) Add to each of the above the code that if the picture is Square Wrap text shape do the same (i.e. change to height 35 or 100)
    If Selection.ShapeRange.Count = 1 Then
        Selection.ShapeRange(1).ScaleHeight 1, msoTrue
        Selection.ShapeRange(1).ScaleWidth 1, msoTrue
    End If
    
End Sub
