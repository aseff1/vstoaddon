'Attribute VB_Name = "M_CleanUpNonBatch"
Option Explicit
Declare Function keybd_event Lib "user32.dll" (ByVal vKey As Long, bScan As Long, ByVal Flag As Long, ByVal exInfo As Long) As Long
Const VK_LWIN = 91
'These procedures are used for "TouchUp" but cannot be run in a batch.  They require more attention...



Sub A0SemiAutomaticCleanupNTRG_AAAAA()
Dim num$, i%
Dim Win1 As Word.Window
Dim Win2 As Word.Window
    
    If Documents.Count > 1 Or Windows.Count > 2 Then
        M "Please close all windows except for 1 and run again.", vbInformation, "Microsoft Word"
        Exit Sub
    End If

    If zFind("+NTRG", False, True) = False Then
        cAY "REF[" & HE(ALEF) & "-" & HE(TAV) & "]{1,2}REF", "", "", "", zWildon
        cAY "REF*REF", "(^&)", "", "", zWildon
        cAY "(NTRG*NTRG)(*)(AAAAA)", "\1+\2+\3", "", "", zWildon
        
        cAY "+{2,}", "+", "", "", zWildon
        cAY "\({2,}", "(", "", "", zWildon
        cAY "\){2,}", ")", "", "", zWildon

        cAY "UHOH[" & ChrW(1488) & "-" & ChrW(1514) & "]", "", "TALMUD", "TALMUD", zWildOff, zNoConfirm, zMain
        
    End If

    'Open 2 windows:
    Set Win1 = ActiveWindow
    'AeroSnapLeft
    
    If Windows.Count = 1 Then
        NewWindow
        Set Win2 = ActiveWindow
        MsgboxModalless "You can adjust both windows.  Then press OK.", vbOKOnly + vbInformation, "Microsoft Word"
        'MsgboxUnicode "You can adjust both windows.  Then press OK.", vbInformation, "Lotus 123"
    Else
        Set Win2 = GetOtherWindow
    End If
    
    'AeroSnapRight
    'Dim OtherWindowNumber  As Integer
    'OtherWindowNumber = IIf(Right(ActiveWindow.Caption, 1) = 1, 2, 1)
    'Windows.CompareSideBySideWith Left(ActiveWindow.Caption, Len(ActiveWindow.Caption) - 1) & OtherWindowNumber  '  "6 11 Yodayim.docx [Compatibility Mode]:1"
    'Windows.CompareSideBySideWith GetOtherWindow.Caption
    'Windows.SyncScrollingSideBySide = False
    
    Win1.Activate
    Do While zFind("REF[0-9]@REF", True, True)
        'Make Reference easy to see:
        Selection.Font.Color = WdColor.wdColorRed
        For i = 1 To 7: Selection.Font.Grow: Next i
                
        num = Replace(UCase(Selection.Text), "REF", "")
        Win1.Activate
        Win2.Activate
        If Not zFind("NTRG" & num & "NTRG+", True, True) Then Exit Do
        If MsgboxModalless("Is this the correct match?", vbYesNo + vbDefaultButton1 + vbQuestion) = vbYes Then
            'Selection.Information(wdActiveEndPageNumber)
            Win2.Activate
            Clps wdCollapseEnd
            GetStuffInPlus
            Selection.Cut
            Win1.Activate
            GetStuffInParentheses
            Selection.Delete
            Selection.Footnotes.Add Range:=Selection.Range, Reference:=""
            Selection.PasteAndFormat wdFormatOriginalFormatting
            
            cAY "+", "", "", "", zWildOff, zNoConfirm, zFootnotesAndEndnotes
            cAY "NTRG[0-9]@NTRGA{5}", "", "", "", zWildon
            'ActiveDocument.Save
        Else
            Msgbox "Change REF to REFx or remove ""+"" from NRTG, then kill one window, then start again.", vbInformation, "Microsoft Word."
            Exit Do
        End If
        Win1.Activate
    Loop

End Sub
Sub AeroSnap(direction$)
    keybd_event VK_LWIN, 0, 0, 0
    SendKeys "{up}"
    keybd_event VK_LWIN, 0, 2, 0
    keybd_event VK_LWIN, 0, 0, 0
    SendKeys "{" & direction$ & "}"
    keybd_event VK_LWIN, 0, 2, 0
End Sub
Sub AeroSnapLeft()
    AeroSnap "left"
End Sub
Sub AeroSnapRight()
    AeroSnap "right"
End Sub
Function GetOtherWindow() As Word.Window
    If Windows.Count = 2 Then
        Set GetOtherWindow = Windows(((ActiveWindow.index) Mod 2) + 1)
    ElseIf ActiveDocument.Windows.Count = 2 Then
        Dim MyWin As Word.Window
        For Each MyWin In Windows
            If MyWin.Active = False And MyWin.Document Is ActiveDocument Then
                Set GetOtherWindow = MyWin 'ActiveDocument.Windows(((ActiveDocument.Windows(ActiveWindow.Caption).Index) Mod 2) + 1)
                Exit For
            End If
        Next MyWin
    End If
End Function


''''''''''Sub A0SemiAutomaticCleanupNTRG_AAAAA()
''''''''''Dim Num$, i%
''''''''''
''''''''''    If (zFind("+NTRG", False, True) = False) Then
''''''''''        cAY "REF[" & HE(ALEF) & "-" & HE(TAV) & "]{1,2}REF", "", "", "", zWildOn
''''''''''        cAY "REF*REF", "(^&)", "", "", zWildOn
''''''''''        cAY "(NTRG*NTRG)(*)(AAAAA)", "\1+\2+\3", "", "", zWildOn
''''''''''        cAY "++++", "+", "", ""
''''''''''        cAY "+++", "+", "", ""
''''''''''        cAY "++", "+", "", ""
''''''''''        cAY "((((", "(", "", ""
''''''''''        cAY "(((", "(", "", ""
''''''''''        cAY "((", "(", "", ""
''''''''''        cAY "))))", ")", "", ""
''''''''''        cAY ")))", ")", "", ""
''''''''''        cAY "))", ")", "", ""
''''''''''        Selection.Find.ClearFormatting
''''''''''        Selection.Find.style = ActiveDocument.Styles("TALMUD")
''''''''''        Selection.Find.Replacement.ClearFormatting
''''''''''        With Selection.Find
''''''''''            .Text = "UHOH[" & ChrW(1488) & "-" & ChrW(1514) & "]"
''''''''''            .Replacement.Text = ""
''''''''''            .Forward = True
''''''''''            .Wrap = wdFindContinue
''''''''''            .Format = True
''''''''''            .MatchWildcards = True
''''''''''        End With
''''''''''        Selection.Find.Execute Replace:=wdReplaceAll
''''''''''    End If
''''''''''
''''''''''    'Open 2 windows:
''''''''''    Dim Win1 As Word.Window
''''''''''    Dim Win2 As Word.Window
''''''''''
''''''''''    If Documents.Count > 1 Then Stop 'Must kill 2nd window
''''''''''
''''''''''    Set Win1 = ActiveWindow
''''''''''    NewWindow
''''''''''
''''''''''    Set Win2 = ActiveWindow
''''''''''    Dim OtherWindowNumber  As Integer
''''''''''    OtherWindowNumber = IIf(Right(ActiveWindow.Caption, 1) = 1, 2, 1)
''''''''''    Windows.CompareSideBySideWith Left(ActiveWindow.Caption, Len(ActiveWindow.Caption) - 1) & OtherWindowNumber  '  "6 11 Yodayim.docx [Compatibility Mode]:1"
''''''''''    Windows.SyncScrollingSideBySide = False
''''''''''
''''''''''    Win1.Activate
''''''''''    Do While zFind("REF[0-9]@REF", True, True)
''''''''''        'Make Reference easy to see:
''''''''''        Selection.Font.Color = WdColor.wdColorRed
''''''''''        For i = 1 To 7: Selection.Font.Grow: Next i
''''''''''
''''''''''        Num = Replace(UCase(Selection.Text), "REF", "")
''''''''''        Win1.Activate
''''''''''        Win2.Activate
''''''''''        If Not zFind("NTRG" & Num & "NTRG+", True, True) Then Exit Do
''''''''''        If MsgBox("Is this the correct match?", vbYesNo + vbDefaultButton1 + vbQuestion) = vbYes Then
''''''''''            Win2.Activate
''''''''''            Clps wdCollapseEnd
''''''''''            GetStuffInPlus
''''''''''            Selection.Cut
''''''''''            Win1.Activate
''''''''''            GetStuffInParentheses
''''''''''            Selection.Delete
''''''''''            Selection.Footnotes.Add Range:=Selection.Range, Reference:=""
''''''''''            Selection.PasteAndFormat wdFormatOriginalFormatting
''''''''''
''''''''''            cAY "+", "", "", "", zWildOff, zNoConfirm, zFootnotesAndEndnotes
''''''''''            cAY "NTRG[0-9]@NTRGA{5}", "", "", "", zWildOn
''''''''''
''''''''''        Else
''''''''''            Exit Do
''''''''''        End If
''''''''''        Win1.Activate
''''''''''    Loop
''''''''''
''''''''''    If MsgBox("Change REF to REFx or remove +from NRTG, then kill one window, then start again") = vbOK Then Stop
''''''''''
''''''''''End Sub
Sub GetStuffInPlus()
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = "+"
        .Replacement.Text = ""
        .Forward = False
        .Wrap = wdFindAsk
        .MatchWildcards = False
    End With
    Selection.Find.Execute
    Selection.Extend
    Selection.Extend Character:="+"
End Sub

'Sub RemoveNearestRefNumberRef()
'    Selection.MoveLeft WdUnits.wdCharacter, 8
'    Replace1 "REF[*]REF", "", zWildOn
'End Sub

'Sub CallSemiAutomaticCleanupNTRG_AAAAA()
'
'    cAY "REF[" & HE(ALEF) & "-" & HE(TAV) & "]{1,2}REF", "", "", "", zWildOn
'    'cAY "ATRG*ATRG", "", "", "", zWildOn, zNoConfirm, zMain
'    cAY "REF*REF", "(^&)", "", "", zWildOn
'    cAY "(NTRG*NTRG)(*)(AAAAA)", "\1+\2+\3", "", "", zWildOn
'
'
'    SemiAutomaticCleanupNTRG_AAAAA
'
'
'End Sub

'Kill this routine
'Sub ManualCleanupNTRG_AAAAA() 'used in conjuction with previous macro
'Dim Num$, OtherWindowNumber%, Win1 As Word.Window, Win2 As Word.Window
'
'    'Start in window with reference (Win1)
'    If MsgBox("Are you sure you're in the window with REF", vbDefaultButton1 + vbYesNo + vbQuestion) = vbNo Then Exit Sub
'
'    Set Win1 = ActiveWindow
'    OtherWindowNumber = IIf(Right(ActiveWindow.Caption, 1) = 1, 2, 1)
'    Windows(Left(ActiveWindow.Caption, Len(ActiveWindow.Caption) - 1) & OtherWindowNumber).Activate
'    Set Win2 = ActiveWindow
'
'    'Win2.Activate
'    GetStuffInPlus
'    Selection.Cut
'    Win1.Activate
'    GetStuffInParentheses
'    Selection.Delete
'    Selection.Footnotes.Add Range:=Selection.Range, Reference:=""
'    Selection.PasteAndFormat wdFormatOriginalFormatting
'
'    cAY "+", "", "", "", zWildOff, zNoConfirm, zFootnotesAndEndnotes
'    cAY "NTRG[0-9]@NTRGA{5}", "", "", "", zWildOn
'
'    SemiAutomaticCleanupNTRG_AAAAA
'
'End Sub


Sub CreateFootnotesOutOfExistingText()  'used for charts or other things where the footnote references were lost in convrsion from WP
'The Sub AFTER this one takes two windows of the same document.  THIS procedure, however, takes two separate documents, "Document 1"
'having JUST footnotes, and nothing else.  It's also assumed that each footnote is separated by an apostrophe (asc 39) (which happens
'to be in superscript)
'
'1-cursor must start by being at the beginning of the "footnote" part of the document (not really footnotes), at THE BEGINNING OF THE LINE
'2-cursor must start by being where you want the footnote reference to be in document 2
'3-document 2 must be the active window.
Dim DocumentFootnote As Word.Document, DocumentText As Word.Document, s As Selection
    
    If Documents.Count <> 2 Then Stop
    
    Set DocumentText = ActiveDocument
    If DocumentText = Documents(1) Then
        Set DocumentFootnote = Documents(2)
    Else
        Set DocumentFootnote = Documents(1)
    End If

    D DocumentFootnote.Windows(1).Caption
    D DocumentText.Windows(1).Caption

    DocumentFootnote.Activate
    Selection.Extend Character:=Chr(39)
    Selection.Cut
    
    DocumentText.Activate
    Selection.Footnotes.Add Range:=Selection.Range, Reference:=""    'Same code as the built-in InsertFootnoteNow
    Selection.PasteAndFormat wdFormatPlainText
    Selection.TypeBackspace
    
End Sub

'see comment to sub above.
Sub AAASemiAutomateTheCleanupOf_UHOH_AAAAA() 'Alt-U
'1-cursor must start by being between the UHOH and the AAAAA in window 1
'2-cursor must start by being where you want the footnote reference to be in
'document 2
'3-document 1 must be the active window.
Dim a As Word.Window, b As Word.Window, s As Selection
    
    If Windows.Count <> 2 Then Stop
    
'    Select Case Windows.Count
'        Case 1
'            NewWindow
'            Windows.CompareSideBySideWith "6 4 Poro 1.doc [Compatibility Mode]:1"
'            Windows.SyncScrollingSideBySide = False
'        Case 2
'        Case Else
'            Stop
'    End Select

    Set a = ActiveWindow
    If a = Windows(1) Then
        Set b = Windows(2)
    Else
        Set b = Windows(1)
    End If

    D a.Caption
    D b.Caption

'    'Stop
    
    Set s = Selection
            
    Clps wdCollapseStart
    
    'in the doc, it looks like [0-9]UHOH, but in Hebrew, it's the opposite...We're not stupid...
    If Not zzFind("UHOH[0-9]{1,}", zWildon, "", aBackwards) Then Stop
    
    'Clps wdCollapseStart
    
    's.MoveLeft WdUnits.wdCharacter, 1
    
    Selection.Delete
    
    s.MoveRight WdUnits.wdCharacter, 1, wdExtend
    
    Selection.Extend
    Selection.Extend Character:="A"
    Selection.Cut
    
    Replace1 "AAAA^p", "", False, False
    
    b.Activate
    'insertfootnotenow
    Selection.Footnotes.Add Range:=Selection.Range, Reference:=""
   
    Selection.PasteAndFormat wdFormatPlainText
   
    Selection.TypeBackspace
    
    
End Sub


Sub MoveParenthesis()
    zFind "^f", False, False
    Selection.Cut
    zFind " ", False, False
    PasteOriginalFormat
    Selection.MoveRight Unit:=wdCharacter, Count:=1
    Selection.MoveLeft Unit:=wdCharacter, Count:=1
    Selection.TypeText Text:=" "
    Selection.MoveLeft Unit:=wdCharacter, Count:=3
    Selection.Delete Unit:=wdCharacter, Count:=1
End Sub

Sub MoveDafNumberIntoMishna() 'Alt-M
'Start by going to first daf
'
    Selection.Extend
    Selection.EndKey Unit:=wdLine
    Selection.MoveLeft Unit:=wdCharacter, Count:=1
    Selection.Cut
    Selection.Delete Unit:=wdCharacter, Count:=1
    Selection.Find.ClearFormatting
    Selection.Find.style = ActiveDocument.Styles("MISHNA TITLE")
    With Selection.Find
        .Text = HE(MEM) & HE(SHIN) & HE(NUN) & HE(HEY)
        .Replacement.Text = ""
        .Forward = False
        .Wrap = wdFindAsk
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute
    Selection.EscapeKey
    Selection.EscapeKey
    Selection.EndKey Unit:=wdLine
    With Selection
        With .FootnoteOptions
            .Location = wdBeneathText
            .NumberingRule = wdRestartContinuous
            .StartingNumber = 1
            .NumberStyle = wdNoteNumberStyleArabic
            .NumberingRule = wdRestartSection
        End With
        .Footnotes.Add Range:=Selection.Range, Reference:=""
    End With
    PasteTextOnly

    Selection.GoToNext wdGoToSection
    Selection.Find.ClearFormatting
    With Selection.Find
        .Text = HE(SHIN) & HE(AYIN) & HE(REISH) & " " & HE(HEY) & _
            HE(TAV) & HE(LAMED) & HE(MEM) & HE(vav) & HE(DALET)
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindAsk
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute
    Selection.MoveDown Unit:=wdLine, Count:=1
    Selection.HomeKey Unit:=wdLine
End Sub
Sub UHOH_Sophisticated()
'must first manually put in the footnote references in their right places
Dim aMainDoc As Document
Dim aTempDoc As Document
Dim se1 As Selection
Dim se2 As Selection
Dim aCurrFootnote%, aCurrFootnoteInDoc%, aCurrentSection%

    'ResizeWindowDebuggingMode
    
    
    Set aMainDoc = ActiveDocument
    Set aTempDoc = Documents.Add(DocumentType:=wdNewBlankDocument)
    
    aMainDoc.Activate
    Set se1 = Selection
    
    GoHome
    
    Do While zzFind("MOKORSTART", zWildOff, "")
        Stop
        aCurrentSection = aCurrSection
        se1.ExtendMode = True
        zzFind "SHAREND", zWildOff, ""
        se1.ExtendMode = False
        'Stop
        se1.Cut
        'se1.Copy
        se1.TypeText "���"
        aTempDoc.Activate
        'ResizeWindowDebuggingMode
        
        Set se2 = Selection
        se2.Paste
        cAY "AAAAA", "", "", "", zWildOff, zNoConfirm, zMain
        cAY "UHOH", " ", "", "", zWildOff, zNoConfirm, zMain
        cAY "MOKORSTART", " ", "", "", zWildOff, zNoConfirm, zMain
        cAY "SHAREND", " ", "", "", zWildOff, zNoConfirm, zMain
        cAY "NTRG", " ", "", "", zWildOff, zNoConfirm, zMain
        GoHome
        'Stop
        
        Do While zzFind("[0-9]{1,}", zWildon, "")
            Stop
            aCurrFootnote = IIf(Len(Trim(se2.Text)) = 1, Val(Trim(se2.Text)), Val(St(Right$(Trim(se2.Text), 1)) & St(Left$(Trim(se2.Text), 1))))    'switch around backwards numbers
            se2.Delete
            se2.ExtendMode = True
            se2.Extend VBA.Chr(13)
            se2.MoveLeft wdCharacter, wdExtend
            se2.ExtendMode = False
            se2.Cut
            
            aMainDoc.Activate
            Selection.GoTo What:=wdGoToSection, Which:=wdGoToFirst, Count:=aCurrentSection, name:=""    'goes to beginning of section--in main story
          
            Do While FindInSectionForward("", zWildOff, "ay temp")
                If se1.Text = St(aCurrFootnote) Then
                    'Stop
                    se1.Delete
                    'Stop
                    aInsertFootnote
                    se1.Paste
                    'Application.GoBack
                    'Application.GoBack
                    Selection.GoTo What:=wdGoToSection, Which:=wdGoToFirst, Count:=aCurrentSection, name:=""    'goes to beginning of section--in main story
                    '*********Put in something to do if it cannot find the reference at least once******
                End If
            Loop
            'Stop
            
            aTempDoc.Activate
        Loop
        Stop
        se2.WholeStory
        se2.Delete
        
        aMainDoc.Activate
        'Do While se1.Information(wdInFootnote) = True
        '    se1.MoveDown wdLine, 1
        'Loop
        Selection.GoTo What:=wdGoToSection, Which:=wdGoToFirst, Count:=aCurrentSection, name:=""    'goes to beginning of section--in main story
        'Stop
    Loop
    
    aTempDoc.Close WdSaveOptions.wdDoNotSaveChanges
    
    
    Set aMainDoc = Nothing
    Set aTempDoc = Nothing
    Set se1 = Nothing
    Set se2 = Nothing
    
    mDone
End Sub

Sub CleanCharts()
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = ChrW(64298)
        .Replacement.Text = HE(SHIN)
        .Forward = True
        .Wrap = wdFindContinue
    End With
    Selection.Find.Execute Replace:=wdReplaceAll

    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = HE(TAV) & HE(vav) & HE(SAMECH) & HE(PEY) & ChrW(vav _
            ) & HE(TAV)
        .Replacement.Text = HE(TAV) & HE(vav) & HE(SAMECH) & "'"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    With Selection.Find
        .Text = HE(BEIS) & HE(BEIS) & HE(ALEF) & " " & HE(KOOF) & _
            HE(MEM) & HE(ALEF)
        .Replacement.Text = HE(BEIS) & """" & HE(KOOF)
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    With Selection.Find
        .Text = HE(BEIS) & HE(BEIS) & HE(ALEF) & " " & HE(BEIS) & _
            HE(TAV) & HE(REISH) & HE(ALEF)
        .Replacement.Text = HE(BEIS) & """" & HE(BEIS)
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    With Selection.Find
        .Text = HE(HEY) & HE(LAMED) & HE(KAF) & HE(vav) & HE(TAV)
        .Replacement.Text = HE(HEY) & HE(LAMED) & "'"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    With Selection.Find
        .Text = HE(TAV) & HE(vav) & HE(REISH) & HE(TAV) & " " & _
            HE(KAF) & HE(HEY) & HE(NUN) & HE(YUD) & HE(FINAL_MEM)
        .Replacement.Text = HE(TAV) & HE(vav) & """" & HE(KAF)
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    With Selection.Find
        .Text = HE(PEY) & HE(YUD) & HE(REISH) & HE(vav) & ChrW(SHIN _
            ) & " " & HE(HEY) & HE(MEM) & HE(SHIN) & HE(NUN) & ChrW( _
            HEY)
        .Replacement.Text = HE(PEY) & HE(HEY) & """" & HE(MEM)
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    With Selection.Find
        .Text = HE(YUD) & HE(vav) & HE(REISH) & HE(HEY) & " " & _
            HE(DALET) & HE(AYIN) & HE(HEY)
        .Replacement.Text = HE(YUD) & HE(vav) & """" & HE(DALET)
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    With Selection.Find
        .Text = HE(SHIN) & HE(vav) & HE(LAMED) & HE(CHES) & ChrW(1503 _
            ) & " " & HE(AYIN) & HE(REISH) & HE(vav) & HE(FINAL_KAF)
        .Replacement.Text = HE(SHIN) & HE(vav) & """" & HE(AYIN)
        .Forward = True
        .Wrap = wdFindContinue
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    With Selection.Find
        .Text = HE(SHIN) & HE(LAMED) & HE(CHES) & HE(FINAL_NUN) & " " & _
            HE(AYIN) & HE(REISH) & HE(vav) & HE(FINAL_KAF)
        .Replacement.Text = HE(SHIN) & HE(vav) & """" & HE(AYIN)
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = HE(SHIN) & HE(LAMED) & HE(CHES) & HE(FINAL_NUN) & " " & _
            HE(AYIN) & HE(REISH) & HE(vav) & HE(FINAL_KAF)
        .Replacement.Text = HE(SHIN) & HE(vav) & """" & HE(AYIN)
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll

    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = HE(SHIN) & HE(LAMED) & HE(CHES) & HE(FINAL_NUN) & " " & _
            HE(AYIN) & HE(REISH) & HE(vav) & HE(FINAL_KAF)
        .Replacement.Text = HE(SHIN) & HE(vav) & """" & HE(AYIN)
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll

    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = "|"
        .Replacement.Text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    With Selection.Find
        .Text = ">"
        .Replacement.Text = "{"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    With Selection.Find
        .Text = "<"
        .Replacement.Text = "}"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchCase = False
        .MatchWholeWord = False
        .MatchKashida = False
        .MatchDiacritics = False
        .MatchAlefHamza = False
        .MatchControl = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
    
    AbbreviateMesechtos
End Sub

Sub GetNameOfMesechta()
    frmGetNameOfMesechta.Show False
End Sub

Sub RemoveTalmudHakdama(NAME_OF_MESECHTA$)
'OK, now it works automatically, first removing any footnotes
'must fix ntrg stuff first. ??

   '*******This line has to be changed manually for each mesechta:******
   '--------------------------------------------------------------------
   'Taharos:
   'NAME_OF_MESECHTA = HE(TES) & HE(HEY) & HE(REISH) & VBA.ChrW (VAV) & HE(TAV)
   'vba.chrW(YUD) & VBA.ChrW (DALET) & VBA.ChrW(YUD) & VBA.ChrW(Final_MEM)

   'NAME_OF_MESECHTA = he(KOOF) & he(NUN) & he(YUD) & he (Final_MEM)
   'MsgboxUnicode NAME_OF_MESECHTA

   'ResizeWindowDebuggingMode

   cAY "^f", "", "", "Footnote Reference", zWildOff, zNoConfirm, zMain
    Stop
   ResetFRParameters
   GoHome
   Do While zzFind("^013^02" & NAME_OF_MESECHTA & "????^013", zWildon, "")
'
       aRemoveTalmudHakdamaFootnote
   Loop

   GoHome
   Do While zzFind("^013^02" & NAME_OF_MESECHTA & "?????^013", zWildon, "")
'
       aRemoveTalmudHakdamaFootnote
   Loop

   GoHome
   Do While zzFind("^013^02" & NAME_OF_MESECHTA & "??????^013", zWildon, "")       '
       aRemoveTalmudHakdamaFootnote
   Loop


'    Stop
   Do While zzFind("^013" & NAME_OF_MESECHTA & "????^013", zWildon, "")   '
       aRemoveTalmudHakdama
   Loop
'    Stop
   GoHome
   Do While zzFind("^013" & NAME_OF_MESECHTA & "?????^013", zWildon, "")
       aRemoveTalmudHakdama
   Loop

   GoHome
   Do While zzFind("^013" & NAME_OF_MESECHTA & "??????^013", zWildon, "")
       aRemoveTalmudHakdama
   Loop

   'Stop
   

End Sub




Sub NTRG() 'Do not use this
'formerly called "a"

'fixes missing footnotes from droshos or mekoros
cAY "ATRG*ATRG", "", "", "", zWildon, zNoConfirm, zMain

Dim str As String
Loop1:
    If Not ayFind("NTRG[0-9]{1,3}NTRG", True, True) Then GoTo Loop2
    Selection.Copy
    str = GetFromClipboard
    
    str = Replace(str, "NTRG", "")
    If Not ayFind("REF" & str & "REF", False, True) Then
        zReplace "NTRG" & str & "NTRG", "UHOH" & str, False
        GoTo Loop1
    End If
    'Stop
    If Not ayFind("NTRG" & str & "NTRG*^013", True, True) Then
        zReplace "NTRG" & str & "NTRG", "UHOH" & str, False
        GoTo Loop1
    End If
    Selection.Cut
    ayFind "REF" & str & "REF", False, True
    ActiveDocument.Footnotes.Add Range:=Selection.Range
    Selection.Paste
    zReplace "AAAAA^013", "", False, False
    zReplace "NTRG" & str & "NTRG", "", False, False
    Selection.GoTo What:=wdGoToPage, Which:=wdGoToNext, name:="1"
    zReplace "REF" & str & "REF", "", False, True
    GoTo Loop1
Loop2:
'Stop
'If Not ayFind("ATRG*ATRG", True, True) Then GoTo EndLoop2
    
'    Selection.Copy
'    str = GetFromclipboard
'    str = Replace(str, "ATRG", "")
'    If Not ayFind("REF" & str & "REF", False, True) Then
'        zReplace "ATRG" & str & "ATRG", "UHOH" & str, False
'        GoTo Loop2
'    End If
'    Selection.TypeText "ZZ"
'    ayFind "ATRG" & str & "ATRG", False, True
'    Selection.Delete
'    AInsertParNum
'    GoTo Loop2
EndLoop2:

'''
'''Dim str As String
'''Loop1:
'''    If Not ayFind("NTRG[0-9]{1,3}NTRG", True, True) Then GoTo Loop2
'''    Selection.Copy
'''    str = GetFromclipboard
'''
'''    str = Replace(str, "NTRG", "")
'''    If Not ayFind("REF" & str & "REF", False, True) Then
'''        zReplace "NTRG" & str & "NTRG", "UHOH" & str, False
'''        GoTo Loop1
'''    End If
'''    'Stop
'''    If Not ayFind("NTRG" & str & "NTRG*^013", True, True) Then
'''        zReplace "NTRG" & str & "NTRG", "UHOH" & str, False
'''        GoTo Loop1
'''    End If
'''        Selection.Cut
'''        ayFind "REF" & str & "REF", False, True
'''        ActiveDocument.Footnotes.Add Range:=Selection.Range
'''        Selection.Paste
'''        zReplace "AAAAA^013", "", False, False
'''        zReplace "NTRG" & str & "NTRG", "", False, False
'''        Selection.GoTo what:=wdGoToPage, Which:=wdGoToNext, Name:="1"
'''        zReplace "REF" & str & "REF", "", False, True
'''    GoTo Loop1
'''Loop2:
''''Stop
'''If Not ayFind("ATRG*ATRG", True, True) Then GoTo EndLoop2
'''    Selection.Copy
'''    str = GetFromclipboard
'''    str = Replace(str, "ATRG", "")
'''    If Not ayFind("REF" & str & "REF", False, True) Then
'''        zReplace "ATRG" & str & "ATRG", "UHOH" & str, False
'''        GoTo Loop2
'''    End If
'''        Selection.TypeText "ZZ"
'''        ayFind "ATRG" & str & "ATRG", False, True
'''        Selection.Delete
'''        AInsertParNum
'''    GoTo Loop2
'''EndLoop2:
'''
''''Stop
End Sub



Sub ConvertCommentaryIntoKDD_Small()
'We know what this is.  It works.  It's great.  DOn't delete.  For assistance try ay@seff.co

    IfNotInMishna  'needs a little bit of fixing...
    TextInParentheses
    Lifee
    'after ^f
    'after ^d
    'if not in mishna
    'more stuff here...
End Sub

Sub TextInParentheses()
'this works beautifully!!
'MUST FIX ALL UHOH's b4 this procedure or they will be formated INappropriately:
    
    
    
    cAY "\(*\)", "", "KDD", "kdd small", zWildon
    Exit Sub    'I don't think the rest is necessary


Dim se As Selection
    Set se = Selection
GoHome
    
    Do While zzFind("(", False, "KDD")
        se.MoveRight Unit:=wdCharacter, Count:=2
        GetStuffInParentheses
        'Stop
        se.style = "kdd small"
        Clps wdCollapseEnd
        Selection.EscapeKey
    Loop
    
End Sub

Sub Lifee()
Dim se As Selection
    Set se = Selection
    
    GoHome
    Do While zzFind(VBA.ChrW(LAMED) & VBA.ChrW(PEY) & VBA.ChrW(YUD) & " ", False, "KDD")
        Selection.EndKey Unit:=wdLine, Extend:=wdExtend
        se.style = "kdd small"
        Clps wdCollapseEnd
    Loop
        
End Sub



Sub IfNotInMishna()
'very few false positives, only false negatives
'have to fix talmud reference first
'Stop
Dim x%, aMishna$, aCurrWord$, se As Selection
    Set se = Selection

    'Selection.GoTo What:=wdGoToSection, Which:=wdGoToFirst, count:=aCurrSection, Name:=""
    'Selection.GoTo What:=wdGoToFootnote, Which:=wdGoToFirst, count:=1, Name:= _

    For x = 1 To ActiveDocument.Sections.Count
        Selection.GoTo What:=wdGoToSection, Which:=wdGoToFirst, Count:=x, name:=""
        aMishna = GetCurrentMishnaWithoutNikkud
        'GetCurrentMishnaWithoutNikkudInNewDoc
        If zzFind("", False, "KDD") Then
            Do While se.Range.ParagraphStyle = "KDD" Or (se.Range.ParagraphStyle = "Normal")
                If aCurrSection > x Then Exit Do
                Clps wdCollapseStart
                se.MoveRight wdWord, 1, wdExtend
                'Stop
                If InStr(1, aMishna, se.Text) < 1 Then
                    If Len(se.Text) < 25 Then       'not the whole column of a table
                        Select Case se.Text
                            Case VBA.Chr$(13), VBA.ChrW(8211) & " ", VBA.Chr$(9)
                            Case Else
                                Stop
                                se.style = "kdd small"
                        End Select
                    End If
                End If
                Clps wdCollapseEnd
                'M se.Range.ParagraphStyle
            Loop
        End If
    Next x
    
End Sub
Sub RemoveAllWrongCharacterStylesFromKDD()

    Do While zzFind("", zWildOff, "KDD", aForward, aMatchOnlyWholeWords)
        Select Case Selection.style
            Case "KDD", "kdd small", "Footnote Reference" ',"Default Paragraph Font"
            Case Else
                Selection.clearcharacterstyle
                
                
        End Select
        Clps wdCollapseEnd
        
    Loop
    Stop
End Sub
