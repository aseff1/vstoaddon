'Attribute VB_Name = "M_Shortcuts_Old"
Option Explicit
Sub SidreiTaharosOruch() 'Alt-O
    SidreiTaharos HE(ALEF) 'he(ALEF)
End Sub
Sub SidreiTaharosHaKotzor() 'Alt-K
    SidreiTaharos HE(KOOF) 'he(KOOF)
End Sub
Sub SidreiTaharos(aText$)
    Selection.Font.Reset
    Application.Keyboard (1037)    'Makes input language Hebrew.
    Selection.TypeText Text:=HE(SAMECH) & HE(DALET) & """" & HE(TES) & " "
    FootnoteOrDocumentTextStyle
    Selection.TypeText Text:=aText & "' "
    
    If Blatt <> "" Then
        Selection.TypeText Blatt
        Selection.TypeText Text:=" " & HE(DALET) & """" & HE(HEY) & " "
    Else
        Selection.TypeText Text:=" " & HE(DALET) & """" & HE(HEY) & " "
        Selection.MoveLeft wdCharacter, 5
    End If
    Exit Sub
End Sub
Sub Outdent() ' Alt-Right  not obsolete
    Selection.Paragraphs.Outdent
End Sub
Sub Indent() ' Alt-Left    not obsolete
    Selection.Paragraphs.Indent
End Sub
