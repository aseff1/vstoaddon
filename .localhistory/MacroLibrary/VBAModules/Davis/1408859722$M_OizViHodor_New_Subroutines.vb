'Attribute VB_Name = "M_OizViHodor_New_Subroutines"
Option Explicit
Sub ConvertFootnotesInRavAndMishaNumberIntoEndnotes()
    GoHome
    Do While Find2Styles("", zWildOff, "Footnote Reference", "RAV", aForward)
        ToggleFootnoteEndnote
        GoHome
    Loop
    Do While Find2Styles("", zWildOff, "Footnote Reference", "Little Mishna Title", aForward)
        ToggleFootnoteEndnote
        GoHome
    Loop
End Sub
Sub A_Fix_Peirush_Kol_Demomo_Daka()
    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    With Selection.Find
        .Text = "(" & ChrW(1506) & ChrW(1501) & " )(" & ChrW(1511) & ChrW( _
            1493) & ChrW(1500) & " " & ChrW(1491) & ChrW(1502) & ChrW(1502) & _
            ChrW(1492) & ")"
        .Replacement.Text = ChrW(1506) & ChrW(1501) & " " & ChrW(1508) & ChrW _
            (1497) & ChrW(1512) & ChrW(1493) & ChrW(1513) & " \2"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = False
        .MatchWildcards = True
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
End Sub
'Sub ConvertFootnoteToEndnoteOrViceVersa()
Sub ToggleFootnoteEndnote()     '(alt + f)
    With Selection
        Select Case .StoryType
            Case WdStoryType.wdEndnotesStory
                .Endnotes.Convert
            Case WdStoryType.wdFootnotesStory
                .Footnotes.Convert
            Case Else
                If .Footnotes.Count > 0 And .Endnotes.Count = 0 Then
                    .Footnotes.Convert
                ElseIf .Footnotes.Count = 0 And .Endnotes.Count > 0 Then
                    .Endnotes.Convert
                Else
                    M "Hmmm, you've selected footnotes AND endnotes, so I can't figure out which one you wanted..."
                End If
        End Select
    End With
End Sub


'If not in footnotes, go to next footnote reference and then go to its corresponding footnote.
Sub GetCurrentOrNextFootnote()
Dim fn As Integer

    With Selection
        If .Information(wdInFootnote) = False Then
            zzFind "^f", zWildOff, ""
            fn = .Footnotes(1).index
            GoHome
            GoIntoFootnotes
            .GoTo What:=wdGoToFootnote, Which:=wdGoToFirst, Count:=fn, name:=""
        End If
    End With
    
End Sub

Sub A_SetNoteOptions()
    With ActiveDocument.Range(Start:=ActiveDocument.Content.Start, End:= _
        ActiveDocument.Content.End).EndnoteOptions
        .Location = wdEndOfSection
        .NumberingRule = wdRestartSection
        .StartingNumber = 1
'        .NumberStyle = wdNoteNumberStyleHebrewLetter1
        .NumberStyle = wdNoteNumberStyleArabic
    End With
    
    With ActiveDocument.PageSetup
        .SuppressEndnotes = False
    End With
    
    With ActiveDocument.Range(Start:=ActiveDocument.Content.Start, End:= _
        ActiveDocument.Content.End).FootnoteOptions
        .Location = wdBeneathText
        .NumberingRule = wdRestartSection
        .StartingNumber = 1
'        .NumberStyle = wdNoteNumberStyleArabic
        .NumberStyle = wdNoteNumberStyleHebrewLetter1
    End With
    A_Insert_Shaar_Mekoros_and_Heoros
End Sub
Sub ConvertToShaarSmall()   '(alt + k)

    With Selection
        .Font.Reset
        .Text = "(" & Trim(.Text) & ") "
        .style = "shaar small"
    End With
    
End Sub

Sub Tosefta()
Dim t1 As String, t2 As String, t3 As String, t4 As String, t5 As String
    t1 = "(" & HE(TAV) & HE(vav) & HE(SAMECH) & HE(PEY) & HE(TAV) & HE(ALEF) & " )"
    t2 = "([" & HE(ALEF) & "-" & HE(TAV) & "]{1,2})"
    t3 = "([(])"
    t4 = "([)])"
    t5 = t1 & t2 & "(:)" & t2 & t3 & t2 & t4
    Rep t5, "\1\2\3\6", "", "", zWildon
End Sub
Sub TurnEnglishIntoEndnote()
    GoHome
    Do While zzFind("[A-Z]{1,} [A-Z]{1,} [A-Z]{1,} [A-Z]{1,} [A-Z]{1,} [A-Z]{1,}", zWildon, "")
        With Selection
            .Cut
            .Endnotes.Add Range:=Selection.Range, Reference:=""
            .PasteAndFormat (wdFormatPlainText)
        End With
        GoHome
    Loop
    Do While zzFind("[A-Z]{1,} [A-Z]{1,} [A-Z]{1,} [A-Z]{1,} [A-Z]{1,}", zWildon, "")
        With Selection
            .Cut
            .Endnotes.Add Range:=Selection.Range, Reference:=""
            .PasteAndFormat (wdFormatPlainText)
        End With
        GoHome
    Loop
    Do While zzFind("[A-Z]{1,} [A-Z]{1,} [A-Z]{1,} [A-Z]{1,}", zWildon, "")
        With Selection
            .Cut
            .Endnotes.Add Range:=Selection.Range, Reference:=""
            .PasteAndFormat (wdFormatPlainText)
        End With
        GoHome
    Loop
    Do While zzFind("[A-Z]{1,} [A-Z]{1,} [A-Z]{1,}", zWildon, "")
        With Selection
            .Cut
            .Endnotes.Add Range:=Selection.Range, Reference:=""
            .PasteAndFormat (wdFormatPlainText)
        End With
        GoHome
    Loop
    Do While zzFind("[A-Z]{1,} [A-Z]{1,}", zWildon, "")
        With Selection
            .Cut
            .Endnotes.Add Range:=Selection.Range, Reference:=""
            .PasteAndFormat (wdFormatPlainText)
        End With
        GoHome
    Loop
    Do While zzFind("[A-Z]{1,}", zWildon, "")
        With Selection
            .Cut
            .Endnotes.Add Range:=Selection.Range, Reference:=""
            .PasteAndFormat (wdFormatPlainText)
        End With
        GoHome
    Loop
        
End Sub


'Don't call this directly.
'FootnoteNumber is from the beginning of doc, not of section, ����.
Sub MoveItUp(NoteNumber As Integer, EncloseInParenthesis As Boolean, Optional IsEndnote As Boolean = False)
    With Selection
        GoHome
        .GoTo What:=IIf(IsEndnote, wdGoToEndnote, wdGoToFootnote), Which:=wdGoToFirst, Count:=NoteNumber, name:=""
        If EncloseInParenthesis Then
            .TypeText " ("
        Else
            zzFind ".", zWildOff, ""
            .MoveLeft WdUnits.wdCharacter, 1
            .TypeText " "
        End If
        .Paste
        If EncloseInParenthesis Then .TypeText ")"
        'To solve problem with space
        'put parenthesis in footnote and then cut into top
            
        .MoveLeft WdUnits.wdCharacter, 1
        GetStuffInParentheses
        .style = "shaar small"
        .ExtendMode = False
        Clps wdCollapseEnd
        
        'Remove extra space that Word� randomly puts in:
        .MoveLeft Unit:=wdCharacter, Count:=1
        RemoveExtraSpace True
    End With
End Sub

Sub RemoveExtraSpace(GoLeft As Boolean)
    Selection.MoveLeft Unit:=wdCharacter, Count:=IIf(GoLeft = True, 1, -1), Extend:=wdExtend
    If Selection.Text = " " Then Selection.Delete
    Clps wdCollapseEnd
End Sub

Function CountOfSubstring(Main$, Child$)
    CountOfSubstring = (Len(Main) - Len(Replace(Main, Child, ""))) / Len(Child)
End Function
