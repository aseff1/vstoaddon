'Attribute VB_Name = "M_Cleanup_Obsolete"
Option Explicit
Sub OBSRemoveHRb4Mishna() 'this makes a lot of trouble - do not run
Dim x%, i%
    For i = 1 To ActiveDocument.Sections.Count
        Selection.GoTo What:=wdGoToSection, Which:=wdGoToFirst, Count:=i, name:=""
        'Selection.GoTo What:=wdGoToSection, Which:=wdGoToNext, count:=1, Name:=""
        Selection.MoveRight Unit:=wdCharacter, Count:=1, Extend:=wdExtend
        If Selection.Text = Chr$(13) Then
            'x = M("Delete?", vbYesNoCancel)
            'Select Case x
            '    Case vbYes
            Selection.Delete(Unit:=wdCharacter, Count:=1)
            '    Case vbNo
            '        'Loop around
            '    Case vbCancel
            '        E
            'End Select
        End If
    Next i
End Sub


Sub OBSReplaceFromWP() 'this is junk
Dim fWP As Word.Document, fWord As Word.Document
Dim x As ComDlg, TheText$ ' fn As Variant

    Set x = New ComDlg
    x.AllowMultiSelect = False
    x.DialogTitle = "Choose WP file..."
    x.Directory = "C:\Documents and Settings\Zvi\Desktop\Dropbox\ok"
    x.ShowOpen
    If x.FileNames.Count = 0 Then Exit Sub
    
    Set fWP = Documents.Open(x.FileNames(1))
    
    Set x = Nothing
    Set x = New ComDlg
    x.AllowMultiSelect = False
    x.DialogTitle = "Choose Word� file..."
    x.Directory = "C:\Documents and Settings\Zvi\Desktop\Dropbox\Mishna"
    x.ShowOpen
    If x.FileNames.Count = 0 Then Exit Sub
    
    Set fWord = Documents.Open(x.FileNames(1))
    
    fWP.Activate
    Do While ayFindStyle("Posuk", False)
        TheText = Selection.Text
        fWord.Activate
        
        If MsgboxUnicode("Replace " & TheText & "?", vbYesNo) = vbYes Then
            cAY TheText, ChrW(8222) & "^&" & ChrW(8221), "", "", zWildOff, zNoConfirm, zMainFootnotesAndEndnotes
        End If
        
        fWP.Activate
        Selection.Delete
    Loop
    
    Do While ayFindStyle("H_chidush", False)
        TheText = Selection.Text
        fWord.Activate
        
        If MsgboxUnicode("Replace " & TheText & "?", vbYesNo) = vbYes Then
            cAY TheText, "{" & "^&" & "}", "", "", zWildOff, zNoConfirm, zMainFootnotesAndEndnotes
        End If
        
        fWP.Activate
        Selection.Delete
    Loop
    
    'fWord.Save
    Set fWord = Nothing
    Set fWP = Nothing
    
    mDone
    
End Sub

Sub OBSCleanRavTitleStyle()
'This may be the same as "RAV".  Needs testing...
    Selection.Find.ClearFormatting
    Selection.Find.style = ActiveDocument.Styles("RAV")
    Selection.Find.Replacement.ClearFormatting
    Selection.Find.Replacement.style = ActiveDocument.Styles("rav title")
    With Selection.Find
        .Text = HE(REISH) & HE(AYIN) & """" & HE(BEIS)
        .Replacement.Text = "^&"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
End Sub

Sub OBSConvertBracketsInFootnotesToMokorSmall2()
'This appears to do the same as ConvertBracketsInFootnotesToMokorSmall, but doesn't
'indicate that it's only in the footnotes.
'For now, just use ConvertBracketsInFootnotesToMokorSmall1

    Selection.Find.ClearFormatting
    Selection.Find.Replacement.ClearFormatting
    Selection.Find.Replacement.style = ActiveDocument.Styles("mokor small")
    With Selection.Find
        .Text = "(\[)(*)(\])"
        .Replacement.Text = "\2"
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchWildcards = True
    End With
    Selection.Find.Execute Replace:=wdReplaceAll
End Sub

Sub OBSTalmudHardReturns()  'Worked well, but I don't think it's relevant anymore
Dim se As Selection
'Stop
    Set se = Selection
    GoHome
    Do While Find2Styles("", False, "shar bold", "TALMUD")
        'Stop
        Clps wdCollapseStart
        Selection.MoveLeft Unit:=wdCharacter, Count:=1, Extend:=wdExtend
        If se.Text <> Chr(9) And se.Text <> Chr(13) Then
            'Stop
            Clps wdCollapseEnd
            se.TypeParagraph()

            Selection.EndKey(Unit:=wdLine, Extend:=wdExtend)
            Selection.ParagraphFormat.LeftIndent = InchesToPoints(0.5)

            Clps wdCollapseEnd
        Else
            Clps wdCollapseEnd
            zzFind("", False, "shar bold")
        End If
    Loop
    Clps wdCollapseEnd
    
    'Stop
    GoHome
    Do While zzFind("^l", zWildOff, "TALMUD")
        Clps wdCollapseEnd
        se.MoveRight wdCharacter, 1, wdExtend
        Select Case se.Text
            Case Chr(9)
                Clps wdCollapseStart
                se.MoveLeft(wdCharacter, 1, wdExtend)
                se.TypeParagraph()
            Case Chr(13)
                se.Delete()
            Case Else
                Clps wdCollapseEnd
        End Select
    Loop
    
    
    'M se.Style
    'M ActiveDocument.Paragraphs(4).Style
    'M se.Range.ParagraphStyle
End Sub

Sub OBSTalmudSoftReturns()
Dim se As Selection
'this works beautifully!!
'Stop
    'causes infinite loop at EOF
    Set se = Selection
    
    GoHome
    Do While zzFind("^p", False, "TALMUD")
        'Stop
        Clps wdCollapseEnd
        se.MoveRight Unit:=wdCharacter, Count:=1, Extend:=wdExtend
        If se.Text <> Chr(9) _
            And se.style <> "shar bold" _
            And se.Text <> Chr(12) _
            And se.Text <> "U" _
            And se.Text <> ChrW(BEIS) _
            And se.Range.ParagraphStyle = "TALMUD" Then    '12 is section break at end of talmud section if it's the end of that mishna; "U" is for UhOh which is a new note so it retains its hard return
            'Stop
            Clps wdCollapseStart
            se.MoveLeft(Unit:=wdCharacter, Count:=1, Extend:=wdExtend)
            se.TypeText Text:=Chr(11)
        Else
            Clps wdCollapseEnd
        End If
    Loop
    Clps wdCollapseEnd
    
End Sub
Sub OBSDeleteBavli()
    cAY "^013" & HE(BEIS) & HE(BEIS) & HE(LAMED) & HE(YUD) & "*^013", "^013", "", "", zWildon, zNoConfirm, zMain
    'Some shar hatalmuds became normal style.  Undo:
    cAY HE(SHIN) & HE(AYIN) & HE(REISH) & " " & HE(HEY) & HE(TAV) & HE(LAMED) & HE(MEM) & HE(vav) & HE(DALET), _
        HE(SHIN) & HE(AYIN) & HE(REISH) & " " & HE(HEY) & HE(TAV) & HE(LAMED) & HE(MEM) & HE(vav) & HE(DALET), _
        "", "SECTION TITLE", zWildOff, zNoConfirm, zMain
End Sub
