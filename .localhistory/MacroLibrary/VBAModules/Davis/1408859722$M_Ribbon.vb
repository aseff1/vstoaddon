'Attribute VB_Name = "M_Ribbon"
Option Explicit
Sub RemoveNikudFromSelection()
    Selection = RemoveNikkudExclusive
End Sub
Sub GotoPreviousPerek()
    zzFoldStyle "PEREK TITLE", "Heading 1", True
    Child_GetOutOfFootnotes
    zzFind "", False, "Heading 1", aBackwards
    Selection.Collapse wdCollapseStart
End Sub
Sub GotoNextPerek()
    zzFoldStyle "PEREK TITLE", "Heading 1", True
    If Selection.Range.ParagraphStyle = "Heading 1" Then Selection.MoveDown Unit:=wdLine, Count:=3
    Child_GetOutOfFootnotes
    zzFind "", False, "Heading 1"
    Selection.Collapse wdCollapseStart
End Sub
Sub Child_GetOutOfFootnotes()
    'based on GetIntoFootnotes in GeneralUitlites
    Do Until Selection.Information(wdInFootnote) = False
        Selection.MoveUp wdLine, 1
    Loop
End Sub

'More elegant way
'Sub GotoPreviousPerek()
'    GotoPerek aBackwards
'End Sub
'
'Sub GotoNextPerek()
'    GotoPerek aForward
'End Sub
'
'Sub GotoPerek(zDirection As aFindDirection)
'    zzFoldStyle "PEREK TITLE", "Heading 1", True
'    If aDirection = aForwards Then
'        If Selection.Range.ParagraphStyle = "Heading 1" Then Selection.MoveDown Unit:=wdLine, Count:=3
'    End If
'    GetOutOfFootnotes
'    zzFind "", False, "Heading 1", zDirection
'    Selection.Collapse wdCollapseStart
'End Sub

Sub InsertShaarAgada()
    Selection.TypeParagraph
    aAttachTemplates
    Application.Templates("C:\Users\" & Environ("username") & "\AppData\Roaming\Microsoft\Document Building Blocks\1033\14\Building Blocks.dotx" _
        ).BuildingBlockEntries("SHAR HAGADAH").Insert Where:=Selection.Range, RichText:=True
End Sub
Sub InsertShaarDroshos()
'Dim a As Template 'AddIn
    Selection.TypeParagraph
    aAttachTemplates
    Application.Templates("C:\Users\" & Environ("username") & "\AppData\Roaming\Microsoft\Document Building Blocks\1033\14\Building Blocks.dotx" _
        ).BuildingBlockEntries("SHAR HADROSHOS").Insert Where:=Selection.Range, RichText:=True
    
'    Set a = Application.Templates("C:\Users\" & Environ("username") & "\Desktop\Dropbox\Mishna\Misc\Templates\Building Blocks.dotx")
'    'M a.BuildingBlockEntries.Count
'    Dim x%
'
'    For x = 1 To a.BuildingBlockEntries.Count
'        Selection.TypeText a.BuildingBlockEntries(x).Name & vbNewLine
'    Next x
    
    '(he(SHIN) & he(AYIN) & he(REISH) & " " & he(HEY) & he(DALET) & he(REISH) & he(SHIN) & he(VAV) & he(TAV)). _
        Insert Where:=Selection.Range, RichText:=True
        
   ' Application.Templates( _
        "C:\Users\" & Environ("username") & "\AppData\Roaming\Microsoft\Document Building Blocks\1033\14\Building Blocks.dotx" _
        ).BuildingBlockEntries(he(SHIN) & he(AYIN) & he(REISH) & " " & ChrW( _
        HEY) & he(DALET) & he(REISH) & he(SHIN) & he(VAV) & he(TAV)). _
        Insert Where:=Selection.Range, RichText:=True
End Sub
Sub UnColor()
    With ActiveDocument
        .UpdateStylesOnOpen = True
        
        '.AttachedTemplate = "C:\Documents and Settings\Zvi\Application Data\Microsoft\Templates\Nocolor.dotx"
        .AttachedTemplate = "C:\Users\" & Environ("username") & "\Desktop\Dropbox\Templates\nocolor.dotx"
        .XMLSchemaReferences.AutomaticValidation = False
        .XMLSchemaReferences.AllowSaveAsXMLWithoutValidation = False
    End With
End Sub
Sub ColoRize()
    With ActiveDocument
        .UpdateStylesOnOpen = True
        '.AttachedTemplate = "C:\Documents and Settings\Zvi\Application Data\Microsoft\Templates\Color.dotx"
        .AttachedTemplate = "C:\Users\" & Environ("username") & "\Desktop\Dropbox\Templates\color.dotx"
        .XMLSchemaReferences.AutomaticValidation = False
        .XMLSchemaReferences.AllowSaveAsXMLWithoutValidation = False
    End With
End Sub
Sub HighlightYellow()
    'Options.DefaultHighlightColorIndex = wdYellow
    Selection.Range.HighlightColorIndex = wdYellow
End Sub
Sub HighlightBrightGreen()
    Selection.Range.HighlightColorIndex = wdBrightGreen
End Sub
Sub HighlightTurquoise()
    Selection.Range.HighlightColorIndex = wdTurquoise
End Sub
Sub HighlightPink()
    Selection.Range.HighlightColorIndex = wdPink
End Sub
Sub HighlightBlue()
    Selection.Range.HighlightColorIndex = wdBlue
End Sub
Sub HighlightRed()
    Selection.Range.HighlightColorIndex = wdRed
End Sub
Sub HighlightDarkBlue()
    Selection.Range.HighlightColorIndex = wdDarkBlue
End Sub
Sub HighlightTeal()
    Selection.Range.HighlightColorIndex = wdTeal
End Sub
Sub HighlightGreen()
    Selection.Range.HighlightColorIndex = wdGreen
End Sub
Sub HighlightViolet()
    Selection.Range.HighlightColorIndex = wdViolet
End Sub
Sub HighlightDarkRed()
    Selection.Range.HighlightColorIndex = wdDarkRed
End Sub
Sub HighlightDarkYellow()
    Selection.Range.HighlightColorIndex = wdDarkYellow
End Sub
Sub HighlightGray50()
    Selection.Range.HighlightColorIndex = wdGray50
End Sub
Sub HighlightGray25()
    Selection.Range.HighlightColorIndex = wdGray25
End Sub
Sub HighlightBlack()
    Selection.Range.HighlightColorIndex = wdBlack
End Sub
Sub HighlightNoHighlight()
    Selection.Range.HighlightColorIndex = wdNoHighlight
End Sub
