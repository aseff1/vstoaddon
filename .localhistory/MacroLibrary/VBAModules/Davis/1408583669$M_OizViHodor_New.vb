Attribute VB_Name = "M_OizViHodor_New"
Option Explicit
Sub A_Put_Parentheses_in_Notes()
    CleanUpParagraphMarksInNotes
    cAY "", "(^&)", "mokor small", "", zWildOff, zNoConfirm, zFootnotesAndEndnotes
    zReplaceInAllParts " {2,}", " ", True 'Remove multiple space NoConfirm
    cAY " )", ") ", "", "", zWildOff, zNoConfirm, zFootnotesAndEndnotes 'Fix ending parenthesis
    cAY ",)", "),", "", "", zWildOff, zNoConfirm, zFootnotesAndEndnotes
    cAY "( ", " (", "", "", zWildOff, zNoConfirm, zFootnotesAndEndnotes 'Fix opening parenthesis
    cAY "()", "", "", ""
    cAY "( )", " ", "", ""
End Sub
Sub A_ReplaceforOizVehodor()
Dim xl As Excel.Application, sprd As Excel.Workbook
Dim i%, aFrom$, aTo$, aPath$, DoConfirm As ConfirmAll, strDoConfirm As String
 
    aPath = Environ("DROPBOX")
    If aPath = "" Then Exit Sub
'        ReplaceRosheiTeivos = False
'        Exit Function
'    End If
    aPath = aPath & "\Mishna\Roshei Teivos\Roshei Teivos.xlsm"

    Set xl = New Excel.Application
    Set sprd = xl.Workbooks.Open(FileName:=aPath)
'    xl.Visible = False
    xl.Visible = True
    xl.ActiveWindow.WindowState = xlMinimized

    i = 2
    With sprd.ActiveSheet
        
        aFrom = .Range("A" & St(i))
    
        Do Until aFrom = ""
            aTo = .Range("B" & St(i))
            strDoConfirm = .Range("C" & St(i))
            Select Case LCase(strDoConfirm)
                Case "skip"
                    GoTo QWERTY
                Case "1", "-1", "confirm", "yes", "true"
                    DoConfirm = ConfirmAll.zConfirm
                Case Else   'anything else, including blanks:
                    DoConfirm = ConfirmAll.zNoConfirm
            End Select
            
'            cAY_OizVeHodor aFrom, aTo, "", "", zWildOff, DoConfirm, zMainFootnotesAndEndnotes
            cAY_OizVeHodor aFrom, aTo, "", "", zWildOff, DoConfirm
QWERTY: i = i + 1
        aFrom = .Range("A" & St(i))
        Loop
        
    End With
        
    sprd.Close
        
    Set sprd = Nothing
    xl.Quit
    Set xl = Nothing

'    ReplaceRosheiTeivos = True

End Sub
Sub cAY_OizVeHodor(zFrom$, zTo$, ZfromStyle$, ZtoStyle$, Optional zzWild As zWild = zWildOff, _
    Optional Confirm As ConfirmAll = zNoConfirm, Optional WhatPart As TheStory = zMainFootnotesAndEndnotes, _
    Optional WholeWordsOnly As aMatchWholeWords = False)  'called by above routine
    'If zFrom = "" And ZfromStyle = "" Then Exit Sub 'Checks for error--user left out zFrom and zFromStyle
    If StyleExist(ZfromStyle) = False Then Exit Sub
    If StyleExist(ZtoStyle) = False Then Exit Sub
    
    If Confirm Then
'        zReplaceConfirm zFrom, zTo, "shaar small", "", zzWild, wdMainTextStory, WholeWordsOnly
        zReplaceConfirm zFrom, zTo, "", "", zzWild, wdFootnotesStory, WholeWordsOnly
        zReplaceConfirm zFrom, zTo, "", "", zzWild, wdEndnotesStory, WholeWordsOnly
    Else
'        zReplaceAll zFrom, zTo, "shaar small", "", zzWild, wdMainTextStory, WholeWordsOnly
        zReplaceAll zFrom, zTo, "", "", zzWild, wdFootnotesStory, WholeWordsOnly
        zReplaceAll zFrom, zTo, "", "", zzWild, wdEndnotesStory, WholeWordsOnly
    End If
    
    Clps wdCollapseEnd
    
End Sub
Sub A_CleanStyleofFootnoteandEndnoteReference()
    
    GoHome
    Do While zzFind("^f", zWildOff, "")
        Selection.style = "Footnote Reference"
        Selection.ClearCharacterDirectFormatting
    Loop
    
    GoHome
    Do While zzFind("^e", zWildOff, "")
        Selection.style = "Endnote Reference"
        Selection.ClearCharacterDirectFormatting
    Loop
    
    'cAY "(^02)", "\1", "", "Footnote Reference"
    
End Sub
Sub A_CleanforOzVehodor()
    A_ZCleanforOzVehodor
'    cay "{", "", "", "" 'Removes curly brackets
'    cay "}", "", "", ""
    cAY ChrW(8222), """", "", "" 'Fixes quotation marks
    cAY ChrW(8221), """", "", ""
    cAY "..", " � ", "", ""
    cAY "�", " � ", "", ""
    cAY "�", HE(vav) & HE(GIMEL) & HE(vav) & "'", "DROSHOS", ""  'Fixes ellipse in DROSHOS
    cAY "�", HE(vav) & HE(KAF) & HE(vav) & "'", "", ""  'Fixes ellipse in other places
    cAY "([,.])("")", "\2\1", "", "", zWildon
    cAY "([,.:])(^2)", "\2\1", "", "", zWildon
    TurnEnglishIntoEndnote
    Tosefta
    A_SetNoteOptions
    ConvertFootnotesInRavAndMishaNumberIntoEndnotes
    zReplaceInAllParts " {2,}", " ", True
End Sub
Sub A_ZCleanforOzVehodor()
    zReplaceInAllParts " {2,}", " ", True 'Remove multiple space NoConfirm
    cAY " )", ")", "", ""
End Sub
Sub A_Insert_Shaar_Mekoros_and_Heoros()

    If ActiveWindow.View.SplitSpecial = wdPaneNone Then
        ActiveWindow.ActivePane.View.Type = wdNormalView
    Else
        ActiveWindow.View.Type = wdNormalView
    End If
    If ActiveWindow.ActivePane.View.Type = wdPrintView Or ActiveWindow. _
        ActivePane.View.Type = wdWebView Or ActiveWindow.ActivePane.View.Type = _
        wdPrintPreview Then
        ActiveWindow.View.SeekView = wdSeekFootnotes
    Else
        ActiveWindow.View.SplitSpecial = wdPaneFootnotes
    End If
    ActiveWindow.View.SplitSpecial = wdPaneFootnoteSeparator
    Selection.Extend
    Selection.EndKey Unit:=wdLine
    Selection.Delete Unit:=wdCharacter, Count:=1
    Selection.Font.BoldBi = wdToggle
    Selection.ParagraphFormat.Alignment = wdAlignParagraphCenter
    Selection.Font.NameBi = "David"
    Selection.Font.SizeBi = 18
    Selection.TypeText Text:=ChrW(1513) & ChrW(1506) & ChrW(1512) & " " & ChrW _
        (1492) & ChrW(1492) & ChrW(1506) & ChrW(1512) & ChrW(1493) & ChrW(1514)
    
    ActiveWindow.View.SplitSpecial = wdPaneEndnotes
    ActiveWindow.View.SplitSpecial = wdPaneEndnoteSeparator
    Selection.Extend
    Selection.EndKey Unit:=wdLine
    Selection.Delete Unit:=wdCharacter, Count:=1
    Selection.Font.NameBi = "David"
    Selection.Font.SizeBi = 18
    Selection.ParagraphFormat.Alignment = wdAlignParagraphCenter
    Selection.Font.BoldBi = wdToggle
    Selection.TypeText Text:=ChrW(1513) & ChrW(1506) & ChrW(1512) & " " & ChrW _
        (1492) & ChrW(1502) & ChrW(1511) & ChrW(1493) & ChrW(1512) & ChrW(1493) & _
         ChrW(1514)
         
    ActiveWindow.View.Type = wdPrintView

End Sub
Sub ConvertShaarSmallToEndnote()   '  Alt + D  (for "Down")
Dim f As Endnote

    With Selection
        'If we're outside of the parentheses, goto the next closest parentheses (forward) and take the whole thing:
        If .style <> "shaar small" Then
            zFind "(", False
            Selection.MoveRight WdUnits.wdCharacter, 1
        End If
        
        GetStuffInParentheses
        .Cut
        RemoveExtraSpace True
        RemoveExtraSpace True
        
        Set f = .Endnotes.Add(Range:=Selection.Range, Reference:="")
     
        .EndKey WdUnits.wdLine
        .PasteAndFormat wdFormatPlainText
        .TypeBackspace
        .HomeKey WdUnits.wdLine
        zFind "(", False
        .Delete Unit:=wdCharacter, Count:=1
    End With
End Sub
Sub ConvertNoteToShaarSmall()   'Alt + U  (for "Up")
Dim n As Integer ', InEndnote As Boolean
' Const BookmarkName$ = "temp_ConvertFootnoteToShaarSmall"

    With Selection
        'Cursor must already be in a footnote:
        'GetCurrentOrNextFootnote
        If .Information(wdInFootnote) = True Then
            'InEndnote = False
            n = .Footnotes(1).index
            .Footnotes(1).Range.Cut
            MoveItUp n, True
            zzFind "^f", zWildOff, ""
            .Delete
            If ActiveDocument.Footnotes.Count >= n Then
                If MsgboxModalless("Go to next footnote?", vbYesNo) = vbYes Then
                    GoIntoFootnotes
                    Selection.GoTo WdGoToItem.wdGoToFootnote, wdGoToAbsolute, n
                End If
            Else
                M "No more footnotes."
            End If
            
        ElseIf .Information(wdInEndnote) = True Then
            'InEndnote = False
            n = .Endnotes(1).index
            .Endnotes(1).Range.Cut
            MoveItUp n, True, True
            zzFind "^e", zWildOff, ""
            .Delete
            If ActiveDocument.Endnotes.Count >= n Then
                If MsgboxModalless("Go to next endnote?", vbYesNo) = vbYes Then
                    GetIntoEndnotes
                    Selection.GoTo WdGoToItem.wdGoToEndnote, wdGoToAbsolute, n
                End If
            Else
                M "No more endnotes."
            End If
            
'            Selection.GoToNext wdGoToEndnote
'            zzFind "^e", zWildOff, ""
'            Clps wdCollapseEnd
'
'            CreateBookmarkInCurrentLocationOverrideIfExists BookmarkName
'            Selection.GoToPrevious wdGoToEndnote
'            ToggleFootnoteEndnote   'purple elephant :)
'            ConvertFootnoteToShaarSmall
            'InEndnote = True
        Else
            M "You must either be in a footnote or an endnote."
        End If
        
'        If InEndnote Then
'            If MsgboxModalless("Go to next note?", vbYesNo) = vbYes Then
'                Selection.GoTo What:=wdGoToBookmark, name:=BookmarkName
'            End If
'        End If
        
        
    End With
    
End Sub

Sub SplitFootnoteIntoTwoShaarSmalls()  'work on this with Rav Avrohom
Dim t As String, fn As Integer

    GetCurrentOrNextFootnote
    
    With Selection
        t = .Footnotes(1).Range.Text
        fn = .Footnotes(1).index
        If CountOfSubstring(t, "(") <> 1 Or CountOfSubstring(t, ")") <> 1 Then
            M "there must be one, and only one, set of parentheses for this to work!"
            Exit Sub
        End If
        
        .HomeKey WdUnits.wdLine
        zzFind "^f", zWildOff, ""
        .MoveRight WdUnits.wdCharacter, 1
        
        .Extend "("
        .MoveLeft WdUnits.wdCharacter, 2, WdMovementType.wdExtend
        .Cut
        
        MoveItUp fn, True
        
        GetIntoFootnotes
        .GoTo What:=wdGoToFootnote, Which:=wdGoToFirst, Count:=fn, name:=""
        .Footnotes(1).Range.Select
        .Cut
        
        MoveItUp fn, False
        
        GoHome
        .GoTo What:=wdGoToFootnote, Which:=wdGoToFirst, Count:=fn, name:=""
        .Delete
    
    End With
End Sub

