﻿Imports Microsoft.Office.Interop

Public Module GlobalStuff

    Private _Main As Main

    Public ReadOnly Property Application As Word.Application
        Get
            If IsNothing(_Main) Then _Main = New Main
            'Dont worry about this--it's taken care of by the MacroManager
            'If IsNothing(Main.Application) Then Dim MyMain As New Main(Main.CreateIfDoesntExist.No)
            Return _Main.Application
        End Get
    End Property

    'http://msdn.microsoft.com/en-us/library/office/ff193414(v=office.15).aspx

#Region "Global Methods"

    Public ReadOnly Property Selection As Word.Selection
        Get
            Return Application.Selection
        End Get
    End Property

    Public ReadOnly Property ActiveDocument As Word.Document
        Get
            Return Application.ActiveDocument
        End Get
    End Property

    Public ReadOnly Property Dialogs As Word.Dialogs
        Get
            Return Application.Dialogs
        End Get
    End Property

    Public ReadOnly Property Documents As Word.Documents
        Get
            Return Application.Documents
        End Get
    End Property


    Public ReadOnly Property ActiveWindow As Word.Window
        Get
            Return Application.ActiveWindow
        End Get
    End Property

    ''' <summary>
    ''' Returns a unique number for the specified key combination.
    ''' </summary>
    ''' <remarks></remarks>
    Public Function BuildKeyCode(Arg1 As Word.WdKey,
                                 Optional ByRef Arg2 As Word.WdKey = Word.WdKey.wdNoKey,
                                 Optional ByRef Arg3 As Word.WdKey = Word.WdKey.wdNoKey,
                                 Optional ByRef Arg4 As Word.WdKey = Word.WdKey.wdNoKey) As Long
        Return Application.BuildKeyCode(Arg1, Arg2, Arg3, Arg4)
    End Function

    'CentimetersToPoints	Converts a measurement from centimeters to points (1 cm = 28.35 points). Returns the converted measurement as a Single.
    'ChangeFileOpenDirectory	Sets the folder in which Word searches for documents. .
    'CheckSpelling	Checks a string for spelling errors. Returns a Boolean to indicate whether the string contains spelling errors. True if the string has no spelling errors.
    'CleanString	Removes nonprinting characters (character codes 1 â€“ 29) and special Word characters from the specified string or changes them to spaces (character code 32), as described in the "Remarks" section. Returns the result as a String.
    'DDEExecute	Sends a command or series of commands to an application through the specified dynamic data exchange (DDE) channel.
    'DDEInitiate	Opens a dynamic data exchange (DDE) channel to another application, and returns the channel number.
    'DDEPoke	Uses an open dynamic data exchange (DDE) channel to send data to an application.
    'DDERequest	Uses an open dynamic data exchange (DDE) channel to request information from the receiving application, and returns the information as a string.
    'DDETerminate	Closes the specified dynamic data exchange (DDE) channel to another application.
    'DDETerminateAll	Closes all dynamic data exchange (DDE) channels opened by Microsoft Word. .
    'GetSpellingSuggestions	Returns a SpellingSuggestions collection that represents the words suggested as spelling replacements for a given word.
    'Help	Displays on-line Help information.
    'InchesToPoints	Converts a measurement from inches to points (1 inch = 72 points). Returns the converted measurement as a Single.
    'KeyString	Returns the key combination string for the specified keys (for example, CTRL+SHIFT+A).
    'LinesToPoints	Converts a measurement from lines to points (1 line = 12 points). Returns the converted measurement as a Single.
    'MillimetersToPoints	Converts a measurement from millimeters to points (1 mm = 2.85 points). Returns the converted measurement as a Single.
    'NewWindow	Opens a new window with the same document as the specified window. Returns a Window object.
    'PicasToPoints	Converts a measurement from picas to points (1 pica = 12 points). Returns the converted measurement as a Single.
    'PixelsToPoints	Converts a measurement from pixels to points. Returns the converted measurement as a Single.
    'PointsToCentimeters	Converts a measurement from points to centimeters (1 centimeter = 28.35 points). Returns the converted measurement as a Single.
    'PointsToInches	Converts a measurement from points to inches (1 inch = 72 points). Returns the converted measurement as a Single.
    'PointsToLines	Converts a measurement from points to lines (1 line = 12 points). Returns the converted measurement as a Single.
    'PointsToMillimeters	Converts a measurement from points to millimeters (1 millimeter = 2.835 points). Returns the converted measurement as a Single.
    'PointsToPicas	Converts a measurement from points to picas (1 pica = 12 points). Returns the converted measurement as a Single.
    'PointsToPixels	Converts a measurement from points to pixels. Returns the converted measurement as a Single.
    'Repeat	Repeats the most recent editing action one or more times. Returns True if the commands were repeated successfully.

#End Region

#Region "Global Properties"

    ''' <summary>
    ''' Returns a Document object that represents the active document (the document with the focus). Read-only.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ActiveDocument As Word.Document
        Get
            Return Application.ActiveDocument
        End Get
    End Property


    'ActivePrinter	Returns or sets the name of the active printer. Read/write String.
    'ActiveProtectedViewWindow	Returns a ProtectedViewWindow object that represents the active protected view window (the protected view window with the focus). Read-only.
    'ActiveWindow	Returns a Window object that represents the active window (the window with the focus). Read-only.
    'AddIns	Returns an AddIns collection that represents all available add-ins, regardless of whether they're currently loaded. Read-only.
    'Application	Returns an Application object that represents the Microsoft Word application.
    'AutoCaptions	Returns an AutoCaptions collection that represents the captions that are automatically added when items such as tables and pictures are inserted into a document. Read-only.
    'AutoCorrect	Returns an AutoCorrect object that contains the current AutoCorrect options, entries, and exceptions. Read-only.
    'AutoCorrectEmail	Returns an AutoCorrect object that represents automatic corrections made to e-mail messages.
    'CaptionLabels	Returns a CaptionLabels collection that represents all the available caption labels. Read-only.
    'CommandBars	Returns a CommandBars collection that represents the menu bar and all the toolbars in Microsoft Word.
    'Creator	Returns a 32-bit integer that indicates the application in which the specified object was created. Read-only Long.
    'CustomDictionaries	Returns a Dictionaries object that represents the collection of active custom dictionaries. Read-only.
    'CustomizationContext	Returns or sets a Template or Document object that represents the template or document in which changes to menu bars, toolbars, and key bindings are stored. Read/write. .
    'Dialogs	Returns a Dialogs collection that represents all the built-in dialog boxes in Word. Read-only.
    'Documents	Returns a Documents collection that represents all the open documents. Read-only.
    'FileConverters	Returns a FileConverters collection that represents all the file converters available to Microsoft Word. Read-only.
    'FindKey	Returns a KeyBinding object that represents the specified key combination. Read-only.
    'FontNames	Returns a FontNames object that includes the names of all the available fonts. Read-only.
    'HangulHanjaDictionaries	Returns a HangulHanjaConversionDictionaries collection that represents all the active custom conversion dictionaries.
    'IsObjectValid	True if the specified variable that references an object is valid. Read-only Boolean.
    'IsSandboxed	True if the application window is a protected view window. Read-only.
    'KeyBindings	Returns a KeyBindings collection that represents customized key assignments, which include a key code, a key category, and a command.
    'KeysBoundTo	Returns a KeysBoundTo object that represents all the key combinations assigned to the specified item.
    'LandscapeFontNames	Returns a FontNames object that includes the names of all the available landscape fonts.
    'Languages	Returns a Languages collection that represents the proofing languages listed in the Language dialog box.
    'LanguageSettings	Returns a LanguageSettings object, which contains information about the language settings in Microsoft Word.
    'ListGalleries	Returns a ListGalleries collection that represents the three list template galleries (Bulleted, Numbered, and Outline Numbered).
    'MacroContainer	Returns a Template or Document object that represents the template or document in which the module that contains the running procedure is stored.
    'Name	Returns name of the specified object. Read-only String.
    'NormalTemplate	Returns a Template object that represents the Normal template.
    'Options	Returns an Options object that represents application settings in Microsoft Word.
    'Parent	Returns an Object that represents the parent object of the specified Global object.
    'PortraitFontNames	Returns a FontNames object that includes the names of all the available portrait fonts.
    'PrintPreview	True if print preview is the current view. Read/write Boolean.
    'ProtectedViewWindows	Returns a ProtectedViewWindows object that represents the open protected view windows. Read-only.
    'RecentFiles	Returns a RecentFiles collection that represents the most recently accessed files.
    'Selection	Returns a Selection object that represents a selected range or the insertion point. Read-only.
    'ShowVisualBasicEditor	True if the Visual Basic Editor window is visible. Read/write Boolean.
    'StatusBar	This property is no longer supported in Microsoft Word Visual Basic for Applications.
    'SynonymInfo	Returns a SynonymInfo object that contains information from the thesaurus on synonyms, antonyms, or related words and expressions for the specified word or phrase.
    'System	Returns a System object, which can be used to return system-related information and perform system-related tasks.
    'Tasks	Returns a Tasks collection that represents all the applications that are running.
    'Templates	Returns a Templates collection that represents all the available templatesâ€”global templates and those attached to open documents.
    'VBE	Returns a VBE object that represents the Visual Basic Editor.
    'Windows	Returns a Windows collection that represents all open document windows. Read-only.
    'WordBasic	Returns an Automation object (Word.Basic) that includes methods for all the WordBasic statements and functions available in Word version 6.0 and Word for Windows 95. Read-only.

#End Region

End Module
