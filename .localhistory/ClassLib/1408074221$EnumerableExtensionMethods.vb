﻿Imports System.Runtime.CompilerServices
Imports System.Math
Imports System.Threading

Namespace System.Linq

    Public Module EnumerableExtensionMethods

        <Extension> _
        Public Function ForEach(Of T)(source As IEnumerable(Of T), action As Action(Of T)) As IEnumerable(Of T)
            If action Is Nothing Then Throw New ArgumentNullException("action")

            For Each item As T In source
                action(item)
            Next

            'This allows the caller to chain additional calls to the IEnumerable:
            'http://mfelicio.wordpress.com/2010/01/13/c-tips-ienumerablet-foreach-extension-method/
            '--
            'eg: 
            'Dim persons = RetrievePersons()
            'Dim maxAge As Integer = persons
            '   .Where(Function(p) p.Country = "Portugal")
            '   .ForEach(Function(p) Console.WriteLine("Name: {0} ; Age: {1}", p.Name, p.Age))
            '   .Max(Function(p) p.Age)
            '--
            Return source
        End Function


        ''' <summary>
        ''' http://stackoverflow.com/a/521894/1378356
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="ie"></param>
        ''' <param name="action"></param>
        ''' <remarks></remarks>
        <Extension> _
        Public Sub ForEach(Of T)(ie As IEnumerable(Of T), action As Action(Of T, Integer))
            Dim i = 0
            For Each e As T In ie
                action(e, Max(Interlocked.Increment(i), i - 1))
            Next
        End Sub


    End Module
End Namespace
