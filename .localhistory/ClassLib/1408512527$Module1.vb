﻿Imports System.Linq
Imports System.Text
Imports System.Collections.Generic
Imports System.Reflection
'CreateVBAModuleTemplateTemplate
Module Module1
    Sub a()
        Dim MyAssembly As Assembly = Assembly.Load("Microsoft.Office.Interop.Word")
        MyAssembly.GetTypes.Where(Function(n) n.IsEnum).Select(Function(n) n.Name).ToList.ForEach( _
            Sub(b) Console.WriteLine("Imports " & b))

    End Sub

    Public Function ReloadMacroLibrary(MacroLibraryPath As String)
        'Throw New NotImplementedException
        Dim MyAssembly As Assembly = Assembly.LoadFile(MacroLibraryPath)
        Dim typeMain As Type = MyAssembly.GetType("MacroLibrary.Main")
        Return Activator.CreateInstance(typeMain)
    End Function

End Module
