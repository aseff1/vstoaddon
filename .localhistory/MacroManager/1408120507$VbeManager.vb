﻿Imports Microsoft.Vbe.Interop
Imports pk = Microsoft.Vbe.Interop.vbext_ProcKind
Imports ct = Microsoft.Vbe.Interop.vbext_ComponentType

Module VbeManager
    Public Function GetGenerateMacrosText() As StringBuilder
        'we have to have a prebuild thing to prevent users from having a macro called CallMacro.
        'we can also use this technique to prevent the aabbcc issue with the dictionary

        'we also need a post build to put the dll in the proper path which the MacroManage is looking for.
        'we then need to refresh/reload the MacroManager's version of MacroLibrary.
        'Or, we can just hijack Macro Run and CustomizeKeyboard and do it then, every time...

        'I just realized..Extensibility has to be turned on.  Is there a way for VSTO to turn it on automatically??

        Dim query = From m In MacroDictionary.Keys Select GetGenerateMacroText(m)

        Return New StringBuilder(String.Join("", query))

    End Function

    ''' <summary>
    ''' Just for one macro
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetGenerateMacroText(MacroName As String) As String
        Return "Sub " & MacroName & "()" _
                & Environment.NewLine & vbTab & "CallMacro """ & MacroName & """" _
                & Environment.NewLine & "End Sub" & Environment.NewLine

    End Function

    Private ReadOnly Property MacroCallerModule As CodeModule
    
        Get
            Const MACRO_CALLERS_MODULE_NAME As String = "MacroCallers"

            Dim md = App.NormalTemplate.VBProject.VBComponents.OfType(Of VBComponent). _
                Where(Function(n) n.Type = vbext_ComponentType.vbext_ct_StdModule AndAlso n.Name = MACRO_CALLERS_MODULE_NAME)

            If md.Count = 1 Then
                Return md.Single
            Else
                Try
                    Dim VBComp As VBComponent = App.NormalTemplate.VBProject.VBComponents.Add(ct.vbext_ct_StdModule)
                    VBComp.Name = MACRO_CALLERS_MODULE_NAME
                    'The above better work, or the below will be an infinite loop:
                    Return MacroCallerModule
                Catch ex As Exception
                    'I think sometimes the COM errors fail silently, so I just want to make sure that we really get it so that there's no infinite loop.
                    Throw New Exception("Error creating the " & MACRO_CALLERS_MODULE_NAME & " module in VBA.", ex)
                End Try
            End If
        End Get
    End Property


    ''' <summary>
    ''' will this remove all keyboard shortcuts??
    ''' </summary>
    ''' <remarks></remarks>
    Sub UpdateVBA1()
        AddCodeToVBA_Module(GetGenerateMacrosText.ToString)
    End Sub

    Sub AddCodeToVBA_Module(MultiLineCode As String)

        With MacroCallerModule
            .DeleteLines(1, .CountOfLines)
            .InsertLines(.CountOfLines + 1, MultiLineCode)
        End With

    End Sub

    ''' <summary>
    ''' This way is more logical, and prevents the removal of existing keyboard shortcuts
    ''' It's like an upsert and delsert.
    ''' </summary>
    ''' <remarks></remarks>
    Sub UpdateVBA2()
        Dim MacroListFromOffice As List(Of String) = GetMacroListFromOffice()

        'Delete what's in VBA and not in MacroLibrary:
        MacroListFromOffice.Except(MacroDictionary.Keys.OfType(Of String)).ForEach(Sub(m) DeleteMacroFromModule(m))

        'Insert into VBA what's not there yet but that's MacroLibrary:
        MacroDictionary.Keys.OfType(Of String).Except(MacroListFromOffice).ForEach(Sub(m) AddCodeToVBA_Module(m))


    End Sub

    ''' <summary>
    ''' http://www.cpearson.com/excel/vbe.aspx
    ''' </summary>
    ''' <remarks></remarks>
    Function GetMacroListFromOffice() As List(Of String)
        Dim MyList As New List(Of String)
        Dim LineNum, NumLines As Long
        Dim ProcName As String
        Dim ProcKind As vbext_ProcKind

        With MacroCallerModule
            NumLines = .CountOfDeclarationLines + 1
            Do Until LineNum >= .CountOfLines
                ProcName = .ProcOfLine(LineNum, ProcKind)
                'Rng.Value = ProcName
                MyList.Add(ProcName)
                'Rng(1, 2).Value = ProcKindString(ProcKind)
                LineNum = .ProcStartLine(ProcName, ProcKind) + .ProcCountLines(ProcName, ProcKind) + 1
                'Rng = Rng(2, 1)
            Loop
        End With

        Return MyList.Distinct.ToList

    End Function

    Sub DeleteMacroFromModule(MacroName As String)

        With MacroCallerModule
            .DeleteLines(.ProcStartLine(MacroName, pk.vbext_pk_Proc), .ProcCountLines(MacroName, pk.vbext_pk_Proc))
        End With
    End Sub

    Function ProcKindString(ProcKind As vbext_ProcKind) As String
        Select Case ProcKind
            Case pk.vbext_pk_Get : Return "Property Get"
            Case pk.vbext_pk_Let : Return "Property Let"
            Case pk.vbext_pk_Set : Return "Property Set"
            Case pk.vbext_pk_Proc : Return "Sub Or Function"
            Case Else : Return "Unknown Type: " & CStr(ProcKind)
        End Select
    End Function
End Module
