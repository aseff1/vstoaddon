﻿Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices


Module Main

    Public App As Word.Application = Globals.ThisAddIn.Application
    Public MyMacroManager As MacroLibrary.Main = New MacroLibrary.Main(App)

    '<System.Diagnostics.DebuggerStepThrough()> _

    'this has to be com visible
    Public Sub CallMacro(MacroName As String)
        MyMacroManager.CallMacro(MacroName)
    End Sub

End Module

