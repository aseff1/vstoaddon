﻿#Region "Imports"
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices

#End Region


Public Class Main

#Region "Global"
    Public Shared Application As Word.Application

#End Region

#Region "Constructors"
    ''' <summary>
    ''' This overload accepts an existing Word.Application instance
    ''' </summary>
    ''' <param name="wrd"></param>
    ''' <remarks></remarks>
    Public Sub New(wrd As Word.Application)
        Application = wrd
    End Sub

    Public Enum CreateIfDoesntExist
        Yes
        No
    End Enum

    ''' <summary>
    ''' This overload is used when there's exactly 1 or 0 existing instances of Word already in memory, so the system will figure out which on it is if it exists.  If it doesn't exist, you have the option of creating a new instance.
    ''' </summary>
    ''' <param name="CreateIfDoesntExist"></param>
    ''' <remarks></remarks>
    Public Sub New(CreateIfDoesntExist As CreateIfDoesntExist)
        Dim WordProcesses As Process() = Process.GetProcessesByName("word")

        Select Case WordProcesses.Count
            Case 0
                If CreateIfDoesntExist = Main.CreateIfDoesntExist.Yes Then
                    Application = New Word.Application
                    Application.Visible = True
                Else
                    Application = Nothing
                End If
            Case 1
                Application = DirectCast(Microsoft.VisualBasic.Interaction.GetObject(Nothing, "Word.Application"), Application)
            Case Else
                Throw New Exception("You're assuming that there's only one instance of said Office application running right now, but the truth is that there are more than one, so I don't know which one you want.  To solve this problem, make sure that there's only one instance running, or call one of the other overloads to this procedure.")

        End Select

    End Sub

    ''' <summary>
    ''' This overload accepts the ID of an existing Word application
    ''' </summary>
    ''' <param name="ProcessID"></param>
    ''' <remarks></remarks>
    Public Sub New(ProcessID As Integer)
        Dim WordProcess As Process = Process.GetProcessesByName("word").Where(Function(n) n.Id = ProcessID).Single
        Dim hwnd As Integer = CType(WordProcess.MainWindowHandle, System.Int32)
        Application = GetWordApp(hwnd)     '593458
    End Sub

#End Region

#Region "Unamanaged Window Management"
    Public Delegate Function EnumChildCallback(hwnd As Integer, ByRef lParam As Integer) As Boolean
    Private cb As EnumChildCallback


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="hwnd">Handle of Main Window</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetWordApp(hwnd As Integer) As Word.Application
        ' First, get word's main window handle.
        'Dim hwnd As Integer = CInt(Process.GetCurrentProcess().MainWindowHandle)

        ' We need to enumerate the child windows to find one that
        ' supports accessibility. To do this, instantiate the
        ' delegate and wrap the callback method in it, then call
        ' EnumChildWindows, passing the delegate as the 2nd arg.
        If hwnd <> 0 Then
            Dim hwndChild As Integer = 0
            cb = New EnumChildCallback(AddressOf EnumChildProc)     'added AddressOf 
            Dim AccessibilityClassName As String = GetAccessibilityClassName()
            EnumChildWindows(hwnd, cb, hwndChild)

            ' If we found an accessible child window, call
            ' AccessibleObjectFromWindow, passing the constant
            ' OBJID_NATIVEOM (defined in winuser.h) and
            ' IID_IDispatch - we want an IDispatch pointer
            ' into the native object model.
            If hwndChild <> 0 Then
                Dim OBJID_NATIVEOM As UInteger = GetOBJID_NATIVEOM() '&HFFFFFFF0UI
                Dim IID_IDispatch As Guid = GetGUID()  '00020400-0000-0000-C000-000000000046
                Dim ptr As Word.Window = Nothing

                Dim hr As Integer = AccessibleObjectFromWindow(hwndChild, OBJID_NATIVEOM, IID_IDispatch.ToByteArray(), ptr)
                If hr >= 0 Then
                    'If we successfully got a native OM IDispatch pointer, we can QI this for an word Application 
                    '(using the implicit cast operator supplied in the PIA).
                    Return ptr.Application
                End If
            End If
        End If
        Return Nothing
    End Function

    Public Function EnumChildProc(hwndChild As Integer, ByRef lParam As Integer) As Boolean
        Dim buf As New StringBuilder(128)
        GetClassName(hwndChild, buf, 128)
        If buf.ToString() = GetAccessibilityClassName() Then '"word7" Then
            lParam = hwndChild
            Return False
        End If
        Debug.Print(hwndChild.ToString)
        Return True
    End Function

#End Region

#Region "Office App Agnostic"
    Function GetGUID() As Guid
        Select Case Application.GetType
            Case GetType(Word.Application)
                Throw New NotImplementedException()
                Return New Guid("")
            Case GetType(Excel.Application)
                Return New Guid("00020400-0000-0000-C000-000000000046")
        End Select
        Return Nothing
    End Function

    Function GetAccessibilityClassName() As String
        Select Case Application.GetType
            Case GetType(Word.Application)
                Throw New NotImplementedException()
                'Return New Guid("")
            Case GetType(Excel.Application)
                Return "EXCEL7"
        End Select
        Return Nothing
    End Function

    Function GetOBJID_NATIVEOM() As UInteger
        Select Case Application.GetType
            Case GetType(Word.Application)
                Throw New NotImplementedException()
                'Return New Guid("")
            Case GetType(Excel.Application)
                Return &HFFFFFFF0UI
        End Select
        Return Nothing
    End Function

#End Region

#Region "Unmanaged Declarations"
    <DllImport("Oleacc.dll")> _
    Public Shared Function AccessibleObjectFromWindow(hwnd As Integer, dwObjectID As UInteger, riid As Byte(), _
                                               ByRef ptr As Microsoft.Office.Interop.Word.Window) As Integer
    End Function

    <DllImport("User32.dll")> _
    Public Shared Function EnumChildWindows(hWndParent As Integer, lpEnumFunc As EnumChildCallback, ByRef lParam As Integer) As Boolean
    End Function


    <DllImport("User32.dll")> _
    Public Shared Function GetClassName(hWnd As Integer, lpClassName As StringBuilder, nMaxCount As Integer) As Integer
    End Function

#End Region

End Class
