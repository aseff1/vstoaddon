﻿Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices
Imports System.Reflection

Module Main

    Public App As Word.Application = Globals.ThisAddIn.Application
    Public MyMacroLibrary As MacroLibrary.Main '= New MacroLibrary.Main(App)
    'Public MacroList As List(Of VBAMethod) = Nothing
    Private _MacroDictionary As Dictionary(Of String, MethodInfo) = Nothing
    Public Const MACRO_LIBARY_ASSEMBLY_NAME As String = "MacroLibrary"
    Public Const VBA_MODULES_NAMESPACE_NAME As String = "MacroLibrary.VBAModules"
    Public Const dl As String = "."

    Public Sub RefreshMacroLibraryAssembly()
        Throw New NotImplementedException
        Dim x = Assembly.LoadFile(My.Settings.MacroLibraryPath)

    End Sub

    Public Sub CallMacro(ModuleAndMacroName As String)
        'Dim ModuleName As String = Nothing
        'Dim MacroName As String = Nothing
        'Dim Halves As String() = ModuleAndMacroName.Split(dl)
        'Select Case Halves.Length
        '    Case 2
        '        ModuleName = Halves(0)
        '        MacroName = Halves(1)
        '        CallMacro(ModuleName, MacroName)
        '    Case Else
        InvokeMacro(MacroDictionary(ModuleAndMacroName))
        'End Select

    End Sub


    'Public Sub CallMacro(ModuleName As String, MacroName As String)
    '    Dim Macro As VBAMethod = MacroList.Where(Function(n) n.ModuleName = ModuleName AndAlso n.MacroName = MacroName).Single
    '    InvokeMacro(Macro.MyMethod)
    'End Sub

    Private Sub InvokeMacro(MyMethodInfo As MethodInfo)
        MyMethodInfo.Invoke(MyMacroLibrary, Nothing)
    End Sub

    '<System.Diagnostics.DebuggerStepThrough()> _

    'this has to be com visible
    'Public Sub CallMacro(MacroName As String)
    '    MyMacroManager.CallMacro(MacroName)
    'End Sub

    ''<System.Diagnostics.DebuggerStepThrough()> _
    'Public Sub CallMacro(MacroName As String)

    '    Dim x = GetType(Module1) _
    '    .GetMethods _
    '    .Where(Function(n) n.Name.Equals(MacroName, StringComparison.CurrentCultureIgnoreCase)) _
    '    .Single()

    '    x.Invoke(Me, Nothing)

    'End Sub

    Public ReadOnly Property MacroDictionary As Dictionary(Of String, MethodInfo)
        Get
            If IsNothing(_MacroDictionary) Then PopulateMacroDictionary()
            Return _MacroDictionary
        End Get
    End Property



    Private Sub PopulateMacroDictionary() 'As Dictionary(Of String, MethodInfo) 'As List(Of VBAMethod)
        Dim AllMethods As New Dictionary(Of String, MethodInfo)  ' New List(Of VBAMethod)

        Dim MyModules As IEnumerable(Of Type) = Assembly.GetAssembly(MyMacroLibrary.GetType()).GetTypes() _
                                                .Where(Function(MyModule) MyModule.Namespace = VBA_MODULES_NAMESPACE_NAME)

        'Dim MyModules = Assembly.GetAssembly(GetType(Main)).GetTypes().Select(Function(MyModule) MyModule.Namespace).Distinct.ToList

        For Each MyModule As Type In MyModules
            Dim MyMethods As List(Of MethodInfo) = MyModule.GetMethods( _
                        Reflection.BindingFlags.DeclaredOnly Or Reflection.BindingFlags.Instance _
                        Or Reflection.BindingFlags.Static Or Reflection.BindingFlags.Public).ToList
            MyMethods = MyMethods.Where(Function(MyMethod) Not MethodHasRequiredArguments(MyMethod)).ToList

            MyMethods.ForEach(Sub(MyMethod) AllMethods.Add(MyModule.Name & dl & MyMethod.Name, MyMethod))
        Next

        _MacroDictionary = AllMethods

    End Sub

    Function MethodHasRequiredArguments(MyMethodInfo As MethodInfo) As Boolean
        Return MyMethodInfo.GetParameters.Where(Function(n) n.IsOptional = False).Any
        '.GetParameters.Where(Function (MyParameter) MyParameter.IsOptional=False).any) _
        'MyMethod.GetParameters.Where(Function(n) n.IsOptional = False).Any
    End Function

    'Public Function GetAsm() As Assembly
    '    Dim a = GetType(Main)
    '    Dim b = Assembly.GetAssembly(a)
    '    Return b

    'End Function

End Module

'Public Class VBAMethod
'    Public ModuleName As String
'    Public MacroName As String
'    Public MyMethod As MethodInfo
'    Public Sub New(ModuleName As String, MacroName As String, MyMethod As MethodInfo)
'        Me.ModuleName = ModuleName
'        Me.MacroName = MacroName
'        Me.MyMethod = MyMethod
'    End Sub

'End Class
