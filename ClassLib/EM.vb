﻿Imports System.Runtime.CompilerServices
Imports System.Math
Imports System.Threading


Namespace Global.System

    Public Module General
        Public Function GetKeyValuePair(Of KType, VType)(Key As KType, Value As VType) As KeyValuePair(Of KType, VType)
            Return New KeyValuePair(Of KType, VType)(Key, Value)
        End Function
    End Module
    Public Module ExceptionExtensionMethods

        <Extension> _
        Public Function GetInnerMostException(ex As Exception) As Exception
            If IsNothing(ex.InnerException) Then Return ex
            Return ex.InnerException.GetInnerMostException
        End Function

        <Extension> _
        Public Function GetAllExceptionMessages(ex As Exception) As List(Of String)
            Dim ret As New List(Of String)
            ret.Add(ex.Message)
            If Not IsNothing(ex.InnerException) Then ret.AddRange(ex.InnerException.GetAllExceptionMessages)
            Return ret
        End Function

        <Extension> _
        Public Function GetAllExceptionMessagesNumbered(ex As Exception) As Dictionary(Of Integer, String)
            Return ex.GetAllExceptionMessages.Select(Function(x, i) GetKeyValuePair(i + 1, x)).ToDictionary

        End Function

    End Module

End Namespace



Namespace Global.System.Linq    'http://stackoverflow.com/a/17360357/1378356

    Public Module EnumerableExtensionMethods


        <Extension> _
        Public Function ToDictionary(Of TKey, TValue)(IEnumerableOfKeyValuePair As IEnumerable(Of KeyValuePair(Of TKey, TValue))) As Dictionary(Of TKey, TValue)
            Return IEnumerableOfKeyValuePair.ToDictionary(Function(n) n.Key, Function(n) n.Value)
        End Function

        <Extension> _
        Public Function ForEach(Of T)(source As IEnumerable(Of T), action As Action(Of T)) As IEnumerable(Of T)
            If action Is Nothing Then Throw New ArgumentNullException("action")

            For Each item As T In source
                action(item)
            Next

            'This allows the caller to chain additional calls to the IEnumerable:
            'http://mfelicio.wordpress.com/2010/01/13/c-tips-ienumerablet-foreach-extension-method/
            '--
            'eg: 
            'Dim persons = RetrievePersons()
            'Dim maxAge As Integer = persons
            '   .Where(Function(p) p.Country = "Portugal")
            '   .ForEach(Function(p) Console.WriteLine("Name: {0} ; Age: {1}", p.Name, p.Age))
            '   .Max(Function(p) p.Age)
            '--
            Return source
        End Function


        ''' <summary>
        ''' http://stackoverflow.com/a/521894/1378356
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="ie"></param>
        ''' <param name="action"></param>
        ''' <remarks></remarks>
        <Extension> _
        Public Sub ForEach(Of T)(ie As IEnumerable(Of T), action As Action(Of T, Integer))
            Dim i = 0
            For Each e As T In ie
                action(e, Max(Interlocked.Increment(i), i - 1))
            Next
        End Sub


    End Module
End Namespace
