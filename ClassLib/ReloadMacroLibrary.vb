﻿Imports System.Reflection

Public Class ReloadMacroLibrary
    Public Shared Function ReloadMacroLibrary(MacroLibraryPath As String)
        'Throw New NotImplementedException
        Dim MyAssembly As Assembly = Assembly.LoadFile(MacroLibraryPath)
        Dim typeMain As Type = MyAssembly.GetType("MacroLibrary.Main")
        Return Activator.CreateInstance(typeMain)
    End Function

End Class
