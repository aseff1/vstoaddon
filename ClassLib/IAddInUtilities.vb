﻿Imports System.Runtime.InteropServices

''' <summary>
''' http://msdn.microsoft.com/en-us/library/bb608614
''' </summary>
''' <remarks></remarks>
<ComVisible(True)> _
Public Interface IAddInUtilities
    Sub RefreshMacroLibraryAssembly()
    Sub CallMacro(ModuleAndMacroName As String)

End Interface


