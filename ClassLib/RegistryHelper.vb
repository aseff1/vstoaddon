﻿Imports Microsoft.Win32

Public Module RegistryHelper

    Enum FirstLevelRegistryKeyType
        HEKY_CLASSES_ROOT
        HEKY_CURRENT_USER
        HEKY_LOCAL_MACHINE
        HEKY_USERS
        HEKY_CURRENT_CONFIG
    End Enum
    Public Function GetFirstLevelRegistryKey(MyFirstLevelRegistryKeyType As FirstLevelRegistryKeyType) As RegistryKey
        Select Case MyFirstLevelRegistryKeyType
            Case FirstLevelRegistryKeyType.HEKY_CLASSES_ROOT : Return Registry.ClassesRoot
            Case FirstLevelRegistryKeyType.HEKY_CURRENT_CONFIG : Return Registry.CurrentConfig
            Case FirstLevelRegistryKeyType.HEKY_CURRENT_USER : Return Registry.CurrentUser
            Case FirstLevelRegistryKeyType.HEKY_LOCAL_MACHINE : Return Registry.LocalMachine
            Case FirstLevelRegistryKeyType.HEKY_USERS : Return Registry.Users
        End Select

    End Function
    Public Function GetRegValue(FirstLevelNode As RegistryKey, FullSubPath As String, Key As String)

        Using akey As RegistryKey = FirstLevelNode.OpenSubKey(FullSubPath) '"Software\MyProduct")
            Return akey.GetValue(Key)
        End Using

    End Function

End Module
