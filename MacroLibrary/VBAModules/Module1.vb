﻿Imports Microsoft.Office.Interop '.Word
Imports Microsoft.Office.Interop.Word
'Imports GlobalStuff '<--we might not need this since all it's members are anyway public shared...
'Imports other word stuff such as enums which are referenced implicitly in VBA
'consider creating a VS plugin which overrides Project > Add Module with adding an Office (Word) Module with all theses imports added automatically.

Namespace VBAModules
    'I DON'T want this on here:     <System.Diagnostics.DebuggerStepThrough()> _
    Module Module1


        Sub Test1() 'a As String)
            Application.Selection.MoveRight(WdUnits.wdCharacter, 2, WdMovementType.wdExtend)

        End Sub

        Sub Test2()
            System.Windows.Forms.MessageBox.Show("aa")
        End Sub

        Sub Test3()
            MsgBox("Test3")
        End Sub
    End Module
End Namespace

