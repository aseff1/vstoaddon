﻿#Region "Imports"
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices
Imports System.Reflection

#End Region


Public Class Main
    Implements ClassLib.IWordApplication

    Private Shared _Application As Word.Application

    'Public Shared Application As Word.Application implements 'iwordapplication.
    Public Property Application As Word.Application Implements ClassLib.IWordApplication.Application
        Get
            Return _Application
        End Get
        Set(value As Word.Application)
            _Application = value
        End Set
    End Property


End Class
