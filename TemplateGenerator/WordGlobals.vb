﻿Imports Microsoft.Office.Interop
Imports Microsoft.Office.Core
Imports Microsoft.Vbe.Interop
'Imports Microsoft.Office.Interop.Word

Public Module GlobalStuff
'we also need the Methods (and enum values??), not just properties...
    Private _Main As Main

    Public ReadOnly Property Application As Word.Application
        Get
            If IsNothing(_Main) Then _Main = New Main
            'Dont worry about this--it's taken care of by the MacroManager
            'If IsNothing(Main.Application) Then Dim MyMain As New Main(Main.CreateIfDoesntExist.No)
            Return _Main.Application
        End Get
    End Property

    'http://msdn.microsoft.com/en-us/library/office/ff193414(v=office.15).aspx

#Region "Global Methods"

'
    Public ReadOnly Property Creator As Int32
        Get
            Return Application.Creator
        End Get
    End Property

    Public ReadOnly Property Parent As Object
        Get
            Return Application.Parent
        End Get
    End Property

    Public ReadOnly Property Name As String
        Get
            Return Application.Name
        End Get
    End Property

    Public ReadOnly Property Documents As Word.Documents
        Get
            Return Application.Documents
        End Get
    End Property

    Public ReadOnly Property Windows As Word.Windows
        Get
            Return Application.Windows
        End Get
    End Property

    Public ReadOnly Property ActiveDocument As Word.Document
        Get
            Return Application.ActiveDocument
        End Get
    End Property

    Public ReadOnly Property ActiveWindow As Word.Window
        Get
            Return Application.ActiveWindow
        End Get
    End Property

    Public ReadOnly Property Selection As Word.Selection
        Get
            Return Application.Selection
        End Get
    End Property

    Public ReadOnly Property WordBasic As Object
        Get
            Return Application.WordBasic
        End Get
    End Property

    Public Property PrintPreview As Boolean
        Get
            Return Application.PrintPreview
        End Get
        Set(value As Boolean)
            Application.PrintPreview = value
        End Set
    End Property
    Public ReadOnly Property RecentFiles As Word.RecentFiles
        Get
            Return Application.RecentFiles
        End Get
    End Property

    Public ReadOnly Property NormalTemplate As Word.Template
        Get
            Return Application.NormalTemplate
        End Get
    End Property

    Public ReadOnly Property System As Word.System
        Get
            Return Application.System
        End Get
    End Property

    Public ReadOnly Property AutoCorrect As Word.AutoCorrect
        Get
            Return Application.AutoCorrect
        End Get
    End Property

    Public ReadOnly Property FontNames As Word.FontNames
        Get
            Return Application.FontNames
        End Get
    End Property

    Public ReadOnly Property LandscapeFontNames As Word.FontNames
        Get
            Return Application.LandscapeFontNames
        End Get
    End Property

    Public ReadOnly Property PortraitFontNames As Word.FontNames
        Get
            Return Application.PortraitFontNames
        End Get
    End Property

    Public ReadOnly Property Languages As Word.Languages
        Get
            Return Application.Languages
        End Get
    End Property

    Public ReadOnly Property Assistant As Assistant
        Get
            Return Application.Assistant
        End Get
    End Property

    Public ReadOnly Property FileConverters As Word.FileConverters
        Get
            Return Application.FileConverters
        End Get
    End Property

    Public ReadOnly Property Dialogs As Word.Dialogs
        Get
            Return Application.Dialogs
        End Get
    End Property

    Public ReadOnly Property CaptionLabels As Word.CaptionLabels
        Get
            Return Application.CaptionLabels
        End Get
    End Property

    Public ReadOnly Property AutoCaptions As Word.AutoCaptions
        Get
            Return Application.AutoCaptions
        End Get
    End Property

    Public ReadOnly Property AddIns As Word.AddIns
        Get
            Return Application.AddIns
        End Get
    End Property

    Public ReadOnly Property Tasks As Word.Tasks
        Get
            Return Application.Tasks
        End Get
    End Property

    Public ReadOnly Property MacroContainer As Object
        Get
            Return Application.MacroContainer
        End Get
    End Property

    Public ReadOnly Property CommandBars As CommandBars
        Get
            Return Application.CommandBars
        End Get
    End Property

'Word As System.String, 
'Word As System.String, LanguageID As System.Object, 
    Public ReadOnly Property SynonymInfo(Word As String, LanguageID As Object) As Word.SynonymInfo
        Get
            Return Application.SynonymInfo(Word, LanguageID)
        End Get
    End Property

    Public ReadOnly Property VBE As VBE
        Get
            Return Application.VBE
        End Get
    End Property

    Public ReadOnly Property ListGalleries As Word.ListGalleries
        Get
            Return Application.ListGalleries
        End Get
    End Property

    Public Property ActivePrinter As String
        Get
            Return Application.ActivePrinter
        End Get
        Set(value As String)
            Application.ActivePrinter = value
        End Set
    End Property
    Public ReadOnly Property Templates As Word.Templates
        Get
            Return Application.Templates
        End Get
    End Property

    Public Property CustomizationContext As Object
        Get
            Return Application.CustomizationContext
        End Get
        Set(value As Object)
            Application.CustomizationContext = value
        End Set
    End Property
    Public ReadOnly Property KeyBindings As Word.KeyBindings
        Get
            Return Application.KeyBindings
        End Get
    End Property

'KeyCategory As Microsoft.Office.Interop.Word.WdKeyCategory, 
'KeyCategory As Microsoft.Office.Interop.Word.WdKeyCategory, Command As System.String, 
'KeyCategory As Microsoft.Office.Interop.Word.WdKeyCategory, Command As System.String, CommandParameter As System.Object, 
    Public ReadOnly Property KeysBoundTo(KeyCategory As Word.WdKeyCategory, Command As String, CommandParameter As Object) As Word.KeysBoundTo
        Get
            Return Application.KeysBoundTo(KeyCategory, Command, CommandParameter)
        End Get
    End Property

'KeyCode As System.Int32, 
'KeyCode As System.Int32, KeyCode2 As System.Object, 
    Public ReadOnly Property FindKey(KeyCode As Int32, KeyCode2 As Object) As Word.KeyBinding
        Get
            Return Application.FindKey(KeyCode, KeyCode2)
        End Get
    End Property

    Public ReadOnly Property Options As Word.Options
        Get
            Return Application.Options
        End Get
    End Property

    Public ReadOnly Property CustomDictionaries As Word.Dictionaries
        Get
            Return Application.CustomDictionaries
        End Get
    End Property

	Public WriteOnly Property StatusBar As Object
        Set(value As Object)
            Application.StatusBar = value
        End Set
    End Property
    Public Property ShowVisualBasicEditor As Boolean
        Get
            Return Application.ShowVisualBasicEditor
        End Get
        Set(value As Boolean)
            Application.ShowVisualBasicEditor = value
        End Set
    End Property
'MyObject As System.Object, 
    Public ReadOnly Property IsObjectValid(MyObject As Object) As Boolean
        Get
            Return Application.IsObjectValid(MyObject)
        End Get
    End Property

    Public ReadOnly Property HangulHanjaDictionaries As Word.HangulHanjaConversionDictionaries
        Get
            Return Application.HangulHanjaDictionaries
        End Get
    End Property

    Public ReadOnly Property LanguageSettings As LanguageSettings
        Get
            Return Application.LanguageSettings
        End Get
    End Property

    Public ReadOnly Property AnswerWizard As AnswerWizard
        Get
            Return Application.AnswerWizard
        End Get
    End Property

    Public ReadOnly Property AutoCorrectEmail As Word.AutoCorrect
        Get
            Return Application.AutoCorrectEmail
        End Get
    End Property

    Public ReadOnly Property ProtectedViewWindows As Word.ProtectedViewWindows
        Get
            Return Application.ProtectedViewWindows
        End Get
    End Property

    Public ReadOnly Property ActiveProtectedViewWindow As Word.ProtectedViewWindow
        Get
            Return Application.ActiveProtectedViewWindow
        End Get
    End Property

    Public ReadOnly Property IsSandboxed As Boolean
        Get
            Return Application.IsSandboxed
        End Get
    End Property

#End Region

End Module