﻿Imports Microsoft.Office.Interop.Word
Imports Microsoft.VisualBasic.Interaction

Module modMain

    Sub Main(args() As String)
        'RefreshMacroLibraryAssembly(args(0))
        Dim WordProcesses As Process() = Process.GetProcessesByName("word")

        Select Case WordProcesses.Count
            Case 0
            Case 1
                Dim App As Application = DirectCast(GetObject(Nothing, "Word.Application"), Application)
                RefreshMacroLibraryAssemblyForSingleInstanceOfWord(App)
            Case Is > 1
                'loop through each one somehow
            Case Else
                'can't be <0 !!
        End Select

    End Sub

    'Private Sub RefreshMacroLibraryAssembly(MacroLibraryPath As String)
    '    MyMacroLibraryMain = ClassLib.ReloadMacroLibrary.ReloadMacroLibrary(MacroLibraryPath)
    'End Sub

    Private Sub RefreshMacroLibraryAssemblyForSingleInstanceOfWord(App As Application)
        Dim MyCOMAddIn As Microsoft.Office.Core.COMAddIn = App.COMAddIns("MacroManager")
        MyCOMAddIn.Object.RefreshMacroLibraryAssembly()

    End Sub

    
End Module
